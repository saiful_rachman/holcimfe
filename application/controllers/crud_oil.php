<?php

class Crud_oil extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');	
	}
	
public function index() {
		$crud = new grocery_CRUD();
		$crud->set_table('record_oil_analysis');
		$crud->set_subject('Oil Analysis');
		$crud->required_fields('record_id');
		$crud->set_field_upload('upload_file','media/images');
	 
		$crud->columns('record_id', 'upload_file');
		$crud->fields('record_id', 'upload_file');
	 
		$crud->callback_after_insert(array($this, 'oil_after_insert'));
	 
		$output = $crud->render();
	 
		$this->load->view('page_crud.php',$output); 
}
 
function oil_after_insert($post_array,$primary_key){
 
		$data_insert_record = array('inspection_type' => 'OIL','inspection_id' => $primary_key, 'datetime' => date('Y-m-d H:i:s'));

		$this->db->insert('record',$data_insert_record);

		$record_id = mysql_insert_id();

		$data_update_record_ut = array('record_id' => $record_id);
		$where = array('id' => $primary_key);

		$this->db->update('record_oil_analysis',$data_update_record_ut,$where);

	return true;

	}	
}
 