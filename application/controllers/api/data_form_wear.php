<?php

class Data_form_wear extends CI_controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index(){
	
		$this->load->view("test_form/ultrasonic");
	}
	
	function get_list_form()
	{
		$data_list = $this->main_model->get_list_where('form_measuring_copy',array('publish' => 'publish'));
		
		$data_json['status'] = 'Success';
		foreach($data_list->result() as $list_form): 
		
			$data_this_form = $this->main_model->get_detail('form_measuring_copy',array('id' => $list_form->id));
			$data_json['data_list'][] = $data_this_form;
		
		endforeach;
		
		echo json_encode($data_json);
	}
	
	function update_form()
	{
		$user_id = $this->input->post('user_id');
		$form_id = $this->input->post('form_id');
		$roller = $this->input->post('roller');
		$grinding = $this->input->post('grinding');
		$running_hour = $this->input->post('running_hour');
		$total_production = $this->input->post('total_production');
		$remarks = $this->input->post('remarks');
		$recomendation = $this->input->post('recomendation');
		$severity_level = $this->input->post('severity_level');
		
		$roller = json_decode($roller);
		$grinding = json_decode($grinding);
		
		$data_roller = $roller->data;
		$data_grinding = $grinding->data;
		
		// Roller	
		foreach($data_roller as $rollers):
		
			$val = 1;
			foreach($rollers->data as $point):
				
				
				$this_val = 'val'.$val;
				$data_val[$this_val] = $point;
				
			$val++;	
			endforeach;
			
			$data_val['date'] = date('Y-m-d');
			$where = array('form_id' => $form_id,'roller_id' => $rollers->roller); 
			$this->db->update('roller_copy',$data_val,$where);
			
		endforeach;
		
		// Grinding	
		foreach($data_grinding as $grindings):
		
			$val = 1;
			foreach($grindings->data as $point):
				
				
				$this_val = 'val'.$val;
				$data_val[$this_val] = $point;
				
			$val++;	
			endforeach;
			
			$data_val['date'] = date('Y-m-d');
			$where = array('form_id' => $form_id,'grinding_id' => $grindings->grinding); 
			$this->db->update('grinding_copy',$data_val,$where);
			
		endforeach;
		
		// Update form_copy 
		$data_update_form = array(
								'user' => $user_id,
								'running_hour' => $running_hour,
								'total_production' => $total_production,
								'remarks' => $remarks,
								'recomendation' => $recomendation,
								'severity_level' => $severity_level
								);
		
		$where_update_form = array('id' => $form_id);	
		$this->db->update('form_measuring_copy',$data_update_form,$where_update_form);
		
		$data_json = array('Status' => 'Success');
		echo json_encode($data_json);
	}
	
	function add()
	{
		/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$model = $this->input->post('model');
		$couplant = $this->input->post('couplant');
		$probe_type = $this->input->post('probe_type');
		$frequency = $this->input->post('frequency');
		$p_zero = $this->input->post('p_zero');
		$thickness = $this->input->post('thickness');
		$gain = $this->input->post('gain');
		$vel = $this->input->post('vel');
		$range = $this->input->post('range');
		$sa = $this->input->post('sa');
		$ra = $this->input->post('ra');
		$da = $this->input->post('da');
		$upload_file = $this->input->post('upload_file');
		$engineer_id = $this->input->post('engineer_id');
		
		/* PARAM POST/INSERT TO Table record_ultrasonic_test */
		$data_post = array(
						'date' => $date,
						'hac' => $hac,
						'model' => $model,
						'couplant' => $couplant,
						'probe_type' => $probe_type,
						'frequency' => $frequency,
						'p_zero' => $p_zero,
						'thickness' => $thickness,
						'gain' => $gain,
						'vel' => $vel,
						'sa' => $sa,
						'ra' => $ra,
						'da' => $da,
						'upload_file' => $upload_file,
						'inspector_id' => $user,
						'engineer_id' => $engineer_id
						);		
		/* ---- END --- */
		
		/* PROCESS INSERT TO Table record_ultrasonic_test */
		if($this->db->insert('record_ultrasonic_test',$data_post))
		{
			$data_post['status'] = 'Success';
			
			$inspection_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
			
			/* PARAM TO INSERT RECORD to table record */
			$data_post_record = array(
									'hac' => $hac,
									'inspection_type' => 'UT', // Ubah Sesuai code inspection
									'inspection_id' => $inspection_id,
									'datetime' => $datetime,
									'remarks' => $remarks,
									'recomendation' => $recomendation,
									'severity_level' => $severity_level,
									'user' => $user
									);
									
			/* PROCESS TO INSERT */						
			$this->db->insert('record',$data_post_record);
			
		}
		else
		{
			$data_post['status'] = 'Failed';
		}
		
		$data_json = array('record' => $data_post_record, 'record_detail'=> $data_post);
		
		echo json_encode($data_json);
		
			
					
	}
}