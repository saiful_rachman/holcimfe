<?php

class Data_form_penetrant extends CI_controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index(){
	
		$this->load->view("test_form/penetrant");
	}
	
	function add()
	{
		/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$hac = $this->input->post('hac');
		$test_object = $this->input->post('test_object');
		$reference_proc_spec = $this->input->post('reference_proc_spec');
		$acceptance_criteria = $this->input->post('acceptance_criteria');
		$method = $this->input->post('method');
		$penetrant_type = $this->input->post('penetrant_type');
		$penetrant_manufacture = $this->input->post('penetrant_manufacture');
		$cleaner_type = $this->input->post('cleaner_type');
		$cleaner_manufacture = $this->input->post('cleaner_manufacture');
		$developer_type = $this->input->post('developer_type');
		$developer_manufacture = $this->input->post('developer_manufacture');
		$pre_cleaning_method = $this->input->post('pre_cleaning_method');
		$penetrant_application = $this->input->post('penetrant_application');
		$developer_application = $this->input->post('developer_application');
		$dwell_time = $this->input->post('dwell_time');
		$developing_time = $this->input->post('developing_time');
		$result = $this->input->post('result');
		$upload_file = $this->input->post('upload_file');
		$inspector_id = $this->input->post('inspector_id');
		$engineer_id = $this->input->post('engineer_id');
		
		/* PARAM POST/INSERT TO Table record_vibration */
		$data_post_record = array(
									'hac' => $hac,
									'inspection_type' => 'PT', // Ubah Sesuai code inspection
									'datetime' => $datetime,
									'remarks' => $remarks,
									'recomendation' => $recomendation,
									'severity_level' => $severity_level,
									'user' => $user
									);
									
			/* PROCESS TO INSERT */						
			
		
		/* ---- end --- */
		
		/* PROCESS INSERT TO Table record_penetrant_test */
		if($this->db->insert('record',$data_post_record))
		{
			$data_post['status'] = 'Success';
			
			$record_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
			
			/* PARAM TO INSERT RECORD to table record */
			
			$data_post = array(
						'record_id' => $record_id,
						'date' => $date,
						'hac' => $hac,
						'test_object' => $test_object,
						'reference_proc_spec' => $reference_proc_spec,
						'acceptance_criteria' => $acceptance_criteria,
						'method' => $method,
						'penetrant_type' => $penetrant_type,
						'penetrant_manufacture' => $penetrant_manufacture,
						'cleaner_type' => $cleaner_type,
						'cleaner_manufacture' => $cleaner_manufacture,
						'developer_type' => $developer_type,
						'developer_manufacture' => $developer_manufacture,
						'pre_cleaning_method' => $pre_cleaning_method,
						'penetrant_application' => $penetrant_application,
						'developer_application' => $developer_application,
						'dwell_time' => $dwell_time,
						'developing_time' => $developing_time,
						'result' => $result,
						'upload_file' => $upload_file,
						'inspector_id' => $user,
						'engineer_id' => $engineer_id
						);
				$this->db->insert('record_penetrant_test',$data_post);

			$inspection_id = mysql_insert_id();
				
			$data_update_inspection = array('inspection_id' => $inspection_id);
			$where = array('id' => $record_id);
			
			$this->db->update('record', $data_update_inspection, $where);

						
		}
		else
		{
			$data_post['status'] = 'Failed';
		}
		
		$data_json = array('record' => $data_post_record, 'record_detail'=> $data_post);
		
		echo json_encode($data_json);
			
					
	}
}