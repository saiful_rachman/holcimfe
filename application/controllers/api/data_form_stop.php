<?php

class Data_form_stop extends CI_controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index(){
	
		$this->load->view("test_form/stop");
	}
	
	function add()
	{
		/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$form_id = $this->input->post('form_id'); 
		$maker_type = $this->input->post('maker_type');
		$engineer_id = $this->input->post('engineer_id');
		
		/* PARAM POST/INSERT TO Table record_stop */
		$data_post_record = array(
									'hac' => $hac,
									'inspection_type' => 'STOP', // Ubah Sesuai code inspection
									'datetime' => $datetime,
									'remarks' => $remarks,
									'recomendation' => $recomendation,
									'severity_level' => $severity_level,
									'user' => $user
									);
									
			/* PROCESS TO INSERT */						
			
		
		/* ---- end --- */
		
		/* PROCESS INSERT TO Table record_stop */
		if($this->db->insert('record',$data_post_record))
		{
			$data_post['status'] = 'Success';
			
			$record_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
			
			/* PARAM TO INSERT RECORD to table record */
			
				$data_post = array(
						'record_id' => $record_id,
						'form_id' => $form_id,
						'maker_type' => $maker_type,
						'inspector_id' => $user,
						'engineer_id' => $engineer_id
						);
				$this->db->insert('record_stop',$data_post);
				
			$inspection_id = mysql_insert_id();
		
			$data_update_inspection = array('inspection_id' => $inspection_id);
			$where = array('id' => $record_id);
			
			$this->db->update('record', $data_update_inspection, $where);
			
						
		}
		else
		{
			$data_post['status'] = 'Failed';
		}
		
		$data_json = array('record' => $data_post_record, 'record_detail'=> $data_post);
		
		echo json_encode($data_json);
			
					
	}
    
	
	function user_area(){
        
        $data['user_id'] = $this->db->query('select * from users')->result();
        
        
        $this->load->view('test_form/stop_user_area',$data);
        //echo json_encode($data);
        }
        
        
     function get_user_area(){
     
        $user_id = $this->input->post('user_id');
     
        $user_area = $this->db->query('select * from rel_users_to_area where users_id="'.$user_id.'"')->result();
        if($user_area != null){
        $form = array();
                foreach ($user_area as $row_area) {
                    $form[] = $row_area->area_id;
                    
                }
                
                $this->db->select('*');
                $this->db->from('form_stop_copy');
                $this->db->where_in('area', $form);
                $running = $this->db->get();

              $data = array();
                foreach ($running->result() as $row) {
                        $data['form_id'] = $row->id;
                        $data['frequency'] = $row->frequency;
                        $data['periode'] = $row->periode;
                        $data['mechanichal_type'] = $row->type;
                        
                        $data_json[] = array('form_id'=>$data['form_id'], 'frequency'=>$data['frequency'], 'periode'=>$data['periode'], 'mechanichal_type'=>$data['mechanichal_type']);
                }
                

            echo json_encode($data_json, JSON_NUMERIC_CHECK);
                
            }else{
                $data_message['status'] = 'error';
                 echo json_encode($data_message, JSON_NUMERIC_CHECK);
            }
        
    }
	
     function form_to_stop(){
        
        $data['form_id'] = $this->db->query('select * from form_stop_copy')->result();
        
        
        $this->load->view('test_form/stop_form_to_stop',$data);
        }
    
    function get_form_detail()
		{
			$form_id = $this->input->post('form_id');
			
			$data_form_result = $this->main_model->get_list_where('form_stop_copy',array('id' => $form_id));
			if($data_form_result->num_rows() > 0)
			{
			
				$data_form_stop = $this->main_model->get_detail('form_stop_copy',array('id' => $form_id));
				
				$data_form = array(
								'form_id' => $form_id,
								'hac_id' => $data_form_stop['hac'],
								'form_type' => $data_form_stop['form_type'],
								'area_id' => $data_form_stop['area'],
								'frequency' => $data_form_stop['frequency'],
								'mechanichal_type' => $data_form_stop['type'],
								'form_number' => $data_form_stop['form_number'],
								'periode' => $data_form_stop['periode']	
								
								);
				
				
				$i = 1;
	
				//GET HAC DATA
																
				$data_activity = $this->main_model->get_list_where('rel_component_to_form_stop_copy',array('form_id' => $form_id),null,array('by' => 'component','sorting' => 'ASC'));									
						
					foreach($data_activity->result() as $activities):
						$data_list_component_activity[] = array(
															'component_id' => $activities->component,
															'item_check' => $activities->item_check,
															'method' => $activities->method,
															'standard' => $activities->standard
															
														);
						
							
					endforeach;
					

			
			  
					$data_form['inspection_list'] = $data_list_component_activity;
					$data_form['status'] = 'Success';
					$data_json = $data_form;
				
			}
			else
			{
				$data_form = array('status' => 'Error');
				$data_json = $data_form;
			}
			
			echo json_encode($data_json);	
  
  	}
    
    
     function stop_view(){
        
         $this->load->view('test_form/stop_post');
     }
        
     function stop_view_post(){
        
          //record
          $user = $this->input->post('user_id');
          $hac_id = $this->input->post('hac_id');
          $remarks = $this->input->post('remarks');
          $recomendation = $this->input->post('recomendation');
          $severity_level = $this->input->post('severity_level');
         
                 
          //record_stop
          $form_id = $this->input->post('form_id');
          $form_type = $this->input->post('form_type');
          $maker_type = $this->input->post('maker_type');
                        
          //record_running_activity//      
          $component_id = $this->input->post('component_id');
          $item_check = $this->input->post('item_check');
          $method = $this->input->post('method');
          $standard= $this->input->post('standard');
          $value = $this->input->post('value');
                        
          date_default_timezone_set('Asia/Jakarta');  
		
		  // INSERT RECORD TABLE
		  $data_record = array(
								'hac' => $hac_id,
                                'inspection_type' => 'STOP',
                                'inspection_id' => 0,
								'datetime' => date('Y-m-d H:i:s'),
                                'remarks' => $remarks,
                                'recomendation' => $recomendation,
                                'severity_level' => $severity_level,
                                'status' => 'Unpublish',
                                'user' => $user
							);
			
          $this->db->insert('record',$data_record);
             
          $record_id =  mysql_insert_id();
          $data_record_stop = array(
    							'record_id' => $record_id,
                                'form_id' => $form_id,
                                'form_type' => $form_type,
    							'maker_type' => $maker_type,
                                'inspector_id' => $user,
                                                 
    						);
            
          $this->db->insert('record_stop',$data_record_stop);
          
          $record_stop_id = mysql_insert_id();
          
          $data_update_record = array('inspection_id' => $record_stop_id);
          $where = array('id' => $record_id);
          
          $this->db->update('record',$data_update_record,$where);
          
          // LOOP DATA FOR ITEM CHECK ACTIVITY      
          $component_id_decode = json_decode($component_id);
          $item_check_decode = json_decode($item_check);
          $method_decode = json_decode($method);
          $standard_decode= json_decode($standard);
          $value_decode = json_decode($value);
            
          date_default_timezone_set('Asia/Jakarta');  
          
          $i = 0;
          foreach($component_id_decode as $component)
          {
		  		 $data_record_stop_activity = array(
    								'record_id' => $record_id,
                                    'form_id' => $form_id,
                                    'hac_id' => $hac_id,
                                    'component_id' => $component,
    								'item_check' => $item_check_decode[$i],
                                    'method' => $method_decode[$i],
                                    'standard' => $standard_decode[$i],
                                    'value' => $value_decode[$i],
                                    'sys_create_date'=>date('Y-m-d H:i:s'),
    							);
    							
    			$this->db->insert('record_stop_activity',$data_record_stop_activity);		
    			
    			$i++;		
		  }
          
         
        $data_json = array('status' => 'Success');
		
		echo json_encode($data_json);
              
           
    
     }
     
}