<?php

class Data_form_oil_analyst_trend extends CI_controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index(){
	
		$this->load->view("test_form/oil_analyst_trend");
	}
	
	function add()
	{
		/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$record_oil_analysis_id = $this->input->post('record_oil_analysis_id');
		$trend_name = $this->input->post('trend_name');
		$trend_type = $this->input->post('trend_type');
		$value_1 = $this->input->post('value_1');
		$value_2 = $this->input->post('value_2');
		$value_3 = $this->input->post('value_3');
		$value_4 = $this->input->post('value_4');
		$value_5 = $this->input->post('value_5');
		$value_6 = $this->input->post('value_6');
		$color = $this->input->post('color');
		
		/* PARAM POST/INSERT TO Table record_oil_analysis_trend */
		$data_post = array(
						'record_oil_analysis_id' => $record_oil_analysis_id,
						'trend_name' => $trend_name,
						'trend_type' => $trend_type,
						'value_1' => $value_1,
						'value_2' => $value_2,
						'value_3' => $value_3,
						'value_4' => $value_4,
						'value_5' => $value_5,
						'value_6' => $value_6,
						'color' => $color
						
						);
		
		/* ---- end --- */
		
		/* PROCESS INSERT TO Table record_oil_analysis_trend */
		$this->db->insert('record_oil_analysis_trend',$data_post)
		
		
		echo json_encode($data_post);
			
					
	}
}