<?php

class Data_form_oil_analyst extends CI_controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index(){
	
		$this->load->view("test_form/oil_analyst");
	}
	
	function add()
	{
		/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$upload_file = $this->input->post('upload_file');
		
		/* PARAM POST/INSERT TO Table record_oil_analysis */
		$data_post = array(
						'upload_file' => $upload_file
						);
		
		/* ---- end --- */
		
		/* PROCESS INSERT TO Table record_oil_analysis */
		if($this->db->insert('record_oil_analysis',$data_post))
		{
			$data_post['status'] = 'Success';
			
			$inspection_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
			
			/* PARAM TO INSERT RECORD to table record */
			$data_post_record = array(
									'hac' => $hac,
									'inspection_type' => 'OA', // Ubah Sesuai code inspection
									'inspection_id' => $inspection_id,
									'datetime' => $datetime,
									'remarks' => $remarks,
									'recomendation' => $recomendation,
									'severity_level' => $severity_level,
									'user' => $user
									);
									
			/* PROCESS TO INSERT */						
			$this->db->insert('record',$data_post_record);
			
		}
		else
		{
			$data_post['status'] = 'Failed';
		}
		
		$data_json = array('record' => $data_post_record, 'record_detail'=> $data_post);
		
		echo json_encode($data_json);
			
					
	}
}