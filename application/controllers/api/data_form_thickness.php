<?php

class Data_form_thickness extends CI_controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index(){
	
		$this->load->view("test_form/thickness");
	}
	
	function add()
	{
		/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
         // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
        $inspection_type = $this->input->post('inspection_type');
		$form_type = $this->input->post('form_type');
		$test_object = $this->input->post('test_object');
		$model = $this->input->post('model');
		$couplant = $this->input->post('couplant');
		$probe_type = $this->input->post('probe_type');
		$frequency = $this->input->post('frequency');
		$thickness = $this->input->post('thickness');
		$upload_file = $this->input->post('upload_file');
		$point = $this->input->post('point');
		
		/* PARAM POST/INSERT TO Table record_thickness */
		$data_post_record = array(
									'hac' => $hac,
									'inspection_type' => $inspection_type, // Ubah Sesuai code inspection
									'datetime' => $datetime,
									'remarks' => $remarks,
									'recomendation' => $recomendation,
									'severity_level' => $severity_level,
									'user' => $user
									);
									
			
		
		/* ---- end --- */
		
		/* PROCESS INSERT TO Table record_thickness */
		if($this->db->insert('record',$data_post_record))
		{
			$data_post['status'] = 'Success';
			
			$record_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
			
			/* PARAM TO INSERT RECORD to table record */
			
			$data_post = array(
						'record_id' => $record_id,
						'date' => $date,
						'hac' => $hac,
						'form_type' => $form_type,
						'test_object' => $test_object,
						'model' => $model,
						'couplant' => $couplant,
						'probe_type' => $probe_type,
						'frequency' => $frequency,
						'thickness' => $thickness,
						'upload_file' => $upload_file
						);
						
			for($i=0;$i<80;$i++){
				
				$a = $i + 1;
				$point_name = 'point_'.$a;
			
				$data_post[$point_name] = $point[$i];
			}
			
			$this->db->insert('record_thickness',$data_post);
			
			
			
			$inspection_id = mysql_insert_id();
		
			$data_update_inspection = array('inspection_id' => $inspection_id);
			$where = array('id' => $record_id);
			
			$this->db->update('record', $data_update_inspection, $where);
			
			
			
		}
		else
		{
			$data_post['status'] = 'Failed';
		}
		
		$data_json = array('record' => $data_post_record, 'record_detail'=> $data_post);
		
		echo json_encode($data_json);
			
					
	}
}