<?php

class List_running extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');	
	}
	

	function index()
	{
		$status = 'publish';
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
        $crud->set_table('record');
        $crud->set_subject('Ultrasonic');
		$crud->columns('hac', 'datetime', 'status','user');
		$crud->add_action('View', '', 'report/main_report/report_vibration','ui-icon-search');
        $crud->add_action('Print', '', 'print_data_report/vibration','ui-icon-print');
		
		$crud->where('inspection_type','RUN');
		$crud->where('status',$status);
		
		$crud->set_relation('hac','hac','hac_code');
		
		$crud->unset_export();
		$crud->unset_read();
		$crud->unset_print();
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
        
		if($this->session->userdata('users_level') == 'Inspector')
		{
			$crud->where('inspector_id',$this->session->userdata('users_id'));
		}
		
		$crud->callback_column('user',array($this,'call_back_collom_user'));
		
        $output = $crud->render();
 
        $this->output($output);
		
		
		
	}
	
	function call_back_collom_user($value, $row){
	
		$data_user = $this->main_model->get_detail('users',array('id'=> $row->user));
		
		return $data_user['nip']." - ".$data_user['nama'];
	
	
	}
        
	
	
	
	function output($output = null)
    {
        $this->load->view('report/list_page.php',$output);    
    }
    
    
    function add_trend($primary_key , $row)
    {
        return site_url('record/main/record_trend').'/'.$row->inspection_id;
    }
}	

	