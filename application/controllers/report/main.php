<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->library('access');
		$this->load->library('grocery_crud');	
	}
	function index(){
		
		$this->load->view('report/main');
	}
	
	
	
	
}	