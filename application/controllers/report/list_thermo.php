<?php

class List_thermo extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');
                $this->load->model('users_model');	
	}
	

//	function index()
//	{	
//		$status = 'publish';
//		$crud = new grocery_CRUD();
//		$crud->set_theme('datatables');
//        $crud->set_table('record');
//        $crud->set_subject('Thermography');
//		$crud->columns('hac', 'datetime', 'status','user');
//        $crud->add_action('View', '', '','ui-icon-search',array($this,'view_map'));
//		
//		$crud->where('inspection_type','THERMO');
//		$crud->where('status',$status);
//		
//		$crud->set_relation('hac','hac','hac_code');
//		// $crud->set_relation('user','users','nip');
//		
//		$crud->unset_export();
//		$crud->unset_read();
//		$crud->unset_print();
//		$crud->unset_add();
//		$crud->unset_edit();
//		$crud->unset_delete();
//        
//		if($this->session->userdata('users_level') == 'Inspector')
//		{
//			$crud->where('inspector_id',$this->session->userdata('users_id'));
//		}
//		
//		$crud->callback_column('user',array($this,'call_back_collom_user'));
//		
//        $output = $crud->render();
// 
//        $this->output($output);
//		
//		
//		
//	}
//	
//	function call_back_collom_user($value, $row){
//	
//		$data_user = $this->main_model->get_detail('users',array('id'=> $row->user));
//		
//		return $data_user['nip']." - ".$data_user['nama'];
//	
//	
//	}
//        
//	
//	
//	
//	function output($output = null)
//    {
//        $this->load->view('report/list_page.php',$output);    
//    }
//    
//    
//      function view_map($primary_key , $row)
//    {
//        return site_url('report/main_report_list/report_thermo_list').'/'.$row->inspection_id;
//    }
        function index(){
        $status='publish';
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="record.id";
        }else{
            $field=$fieldx;
        }
        $listing="nama,nip";
        $listing2="hac_code";
        $config['base_url'] = base_url().'report/list_thermo/index/'.$status;
        $config['total_rows'] = $this->users_model->count_page('record','THERMO','inspection_type',$status,'status','users','user','id',$listing,'hac','hac','id',$listing2,$field,$val)->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 5;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['list']=$this->users_model->select_all_where2_join_2('record','THERMO','inspection_type',$status,'status','users','user','id',$listing,'hac','hac','id',$listing2,$field,$val,$config['per_page'],$pg)->result();
        $data['to']="list_vibration";
        $this->load->view('report/list_thermo',$data);
      }
}	

	