<?php

class Crud_frequency extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('master_model');
        $this->load->model('form_manager_model');	
    }
    
    public function insert_log_activity($type,$primarykey,$description){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
    
    function index(){
        $data['list']=$this->master_model->select_all('master_frequency')->result();
        $this->load->view('master_view/v_master_frequency',$data);
    }
    function edit($id){
        $data['list']=$this->master_model->select_where('master_frequency',$id)->row();
        $this->load->view('master_view/v_master_frequency_edit',$data);
    }
    
    function edit_proses(){
        $id=$this->input->post('id');
        $frequency=$this->input->post('frequency');
        $data=array('frequency'=>$frequency);
        $this->master_model->edit_proses('master_frequency',$id,$data);
        $this->insert_log_activity("Frequency Management", $id,"Update Frequency '$frequency'");
        redirect('engine/crud_frequency');
    }
    
    function delete($id){
        $freq=$this->master_model->select_where('master_frequency',$id)->row('frequency');
        $this->insert_log_activity("Frequency Management", $id,"Delete Frequency '$freq'");
        $this->master_model->delete('master_frequency',$id);
        redirect('engine/crud_frequency');
    }
    
    function add(){
        $this->load->view('master_view/v_master_frequency_add');
    }
    
    function add_proses(){
        $frequency=$this->input->post('frequency');
        $data=array('frequency'=>$frequency);
        $this->master_model->add_proses('master_frequency',$data);
        $this->insert_log_activity("Frequency Management", "","Add Frequency '$frequency'");
        redirect('engine/crud_frequency');
    }
}
