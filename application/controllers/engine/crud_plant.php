<?php

class Crud_plant extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('users_model');
                $this->load->model('form_manager_model');	
	}
	
        public function insert_log_activity($type,$primarykey,$description){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
        function index(){
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="id";
                }else{
                    $field=$fieldx;
                }
		$config['base_url'] = base_url().'engine/crud_plant/index/';
                $config['total_rows'] = $this->db->query("select * from master_plant where $field LIKE '%$val%'")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select * from master_plant where $field LIKE '%$val%' limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('plant/plant_main', $data); 
        }
        
        function add(){
            //$data['list_area']=$this->users_model->select_all('main_area')->result();
            $this->load->view('plant/plant_add');
        }
        
        function add_proses(){
            $plant_name=$this->input->post('plant_name');
             $data=array(
                 'plant_name'=>$plant_name
             );
             $this->users_model->insert('master_plant',$data);
             $this->insert_log_activity("Plant Management", "","Add Plant '$plant_name'");
             redirect('engine/crud_plant');
        }
        
        function edit($id){
            $data['list']=$this->users_model->select_all_where('master_plant',$id,'id')->row();
            //$data['list_area']=$this->users_model->select_all('main_area')->result();
            $this->load->view('plant/plant_edit',$data);
            
        }
        
        function edit_proses(){
            $id=$this->input->post('id');
            $plant_name=$this->input->post('plant_name');
            
             $data=array(
                 'plant_name'=>$plant_name
             );
             $this->users_model->update('master_plant',$id,'id',$data);
             $this->insert_log_activity("Plant Management", $id,"Edit Plant '$plant_name'");
             redirect('engine/crud_plant');
        }
        
        function delete($id){
            $plant=$this->users_model->select_all_where('master_plant',$id,'id')->row('plant_name');
            $this->insert_log_activity("Plant Management", $id,"Delete Plant '$plant'");
            $this->users_model->delete('master_plant',$id,'id');
            redirect('engine/crud_plant');
        }
}	