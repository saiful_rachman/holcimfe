<?php

class Crud_periode extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('master_model');
        $this->load->model('form_manager_model');
    }
    
    public function insert_log_activity($type,$primarykey,$description){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
    
    function index(){
        $data['list']=$this->master_model->select_all('master_periode')->result();
        $this->load->view('master_view/v_master_periode',$data);
    }
    function edit($id){
        $data['list']=$this->master_model->select_where('master_periode',$id)->row();
        $this->load->view('master_view/v_master_periode_edit',$data);
    }
    
    function edit_proses(){
        $id=$this->input->post('id');
        $periode=$this->input->post('periode');
        $data=array('periode'=>$periode);
        $this->master_model->edit_proses('master_periode',$id,$data);
        $this->insert_log_activity("Periode Management", $id,"Update Periode '$periode'");
        redirect('engine/crud_periode');
    }
    
    function delete($id){
        $periode=$this->master_model->select_where('master_periode',$id)->row('periode');
        $this->insert_log_activity("Periode Management", $id,"Delete Periode '$periode'");
        $this->master_model->delete('master_periode',$id);
        redirect('engine/crud_periode');
    }
    
    function add(){
        $this->load->view('master_view/v_master_periode_add');
    }
    
    function add_proses(){
        $periode=$this->input->post('periode');
        $data=array('periode'=>$periode);
        $this->master_model->add_proses('master_periode',$data);
        $this->insert_log_activity("Periode Management", "","Add Periode '$periode'");
        redirect('engine/crud_periode');
    }
}
