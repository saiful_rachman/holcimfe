<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_assembly extends CI_Controller {
   public function __construct(){
        parent::__construct();
            $this->load->model('users_model');	
                $this->load->model('form_manager_model');
   }
   
   public function insert_log_activity($type,$primarykey,$description){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
   
   function get_data(){
       $id=$this->input->post('id');
       $sub=$this->input->post('sub');
       $sqlx = mysql_query("select a.main_area_name,
                            b.plant_name,
                            SUBSTR(b.plant_name,-1,1) as coba,
                            c.area_code
                            from master_mainarea a
                            inner join master_plant b on a.id_plant=b.id 
                            inner join area c on a.id=c.area
                            where a.id='$id' and c.id='$sub'");
            $datay=mysql_fetch_array($sqlx);
            foreach($datay as $dt){
                echo $dt.'|';
            }
   }
   
   function get_area(){
        $data="";
        $id=$this->input->post('id');
        $val=$this->users_model->select_all_where("master_mainarea",$id,"id_plant")->result();
        $data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
            $data .="<option value='$value->id'>$value->description</option>\n";
        }
        echo $data;
    }
    
    function get_area_edit(){
        $data="";
        $plant_id=$this->input->post('plant_id');
        $main_area_id=$this->input->post('main_area_id');
        $val=$this->users_model->select_all_where("master_mainarea",$plant_id,"id_plant")->result();
        $data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
            if($value->id==$main_area_id){
                $cek="selected";
            }else{
                $cek="";
            }
            $data .="<option value='$value->id' $cek >$value->description</option>\n";
        }
        echo $data;
    }
    
    function get_subarea(){
        $data="";
        $id=$this->input->post('id');
        $val=$this->users_model->select_all_where("area",$id,"area")->result();
        $data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
            $data .="<option value='$value->id'>$value->area_name</option>\n";
        }
        echo $data;
    }
    
    function get_subarea_edit(){
        $data="";
        $main_area_id=$this->input->post('main_area_id');
        $sub_area_id=$this->input->post('sub_area_id');
        $val=$this->users_model->select_all_where("area",$main_area_id,"area")->result();
        $data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
            if($value->id==$sub_area_id){
                $cek="selected";
            }else{
                $cek="";
            }
            $data .="<option value='$value->id' $cek >$value->area_name</option>\n";
        }
        echo $data;
    }
   
   function index($id){
       $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="id";
                }else{
                    $field=$fieldx;
                }
		$config['base_url'] = base_url().'engine/crud_assembly/index/'.$id.'/';
                $config['total_rows'] = $this->db->query("select * from hac_assembly where $field LIKE '%$val%' and assembly_hac_id='$id'")->num_rows();
                $config['per_page'] = 20;
                $config['num_links'] = 2;
                $config['uri_segment'] = 5;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
                $data['id_hac']=$id;
		$data['data'] = $this->db->query("select * from hac_assembly where $field LIKE '%$val%' and assembly_hac_id='$id' order by id desc limit ".$pg.",".$config['per_page']."")->result();
                $data['hac']=$this->db->query("select * from hac where id='$id'")->row();
                $this->load->view('assembly/list_assembly', $data); 
   }
   
   function add($id){
       $data['id_hac']=$id;
       //$data['list_plant']=$this->users_model->select_all('master_plant')->result();
       //$data['list_area']=$this->users_model->select_all('master_mainarea')->result();
       $this->load->view('assembly/add_assembly', $data); 
   }
   
   function add_proses(){
       $hac_id=$this->input->post('hac_id');
       $assembly_name=$this->input->post('assembly_name');
       $assembly_code=$this->input->post('assembly_code');
       
        $config['upload_path']	= "./media/images/";
        $config['upload_url']	= base_url().'media/images/';
        $config['allowed_types']= '*';
        $config['max_size']     = '2000';
        $config['max_width']  	= '2000';
        $config['max_height']  	= '2000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        if($this->upload->do_upload('image'))
         {
        $image_data1 = $this->upload->data();    
         }
         
         $data=array(
             'assembly_hac_id'=>$hac_id,
             'assembly_code'=>$assembly_code,
             'assembly_name'=>$assembly_name,
             'image'=>$image_data1['file_name']
         );
         $this->users_model->insert('hac_assembly',$data);
         $this->insert_log_activity("Assembly Management", "","Add New Assembly '$assembly_code'");
         redirect('engine/crud_assembly/index/'.$hac_id);
   }
   
   function edit($id_hac,$id_assembly){
       //$data['list_plant']=$this->users_model->select_all('master_plant')->result();
       //$data['list_area']=$this->users_model->select_all('master_mainarea')->result();
       $data['id_hac']=$id_hac;
       $data['list']=$this->users_model->select_all_where('hac_assembly',$id_assembly,'id')->row();
       $this->load->view('assembly/edit_assembly',$data);
   }
   
   function edit_proses(){
       $id=$this->input->post('id');
       $hac_id=$this->input->post('hac_id');
       $assembly_name=$this->input->post('assembly_name');
       $assembly_code=$this->input->post('assembly_code');
       $image_hidden=$this->input->post('image_hidden');
       
        $config['upload_path']	= "./media/images/";
        $config['upload_url']	= base_url().'media/images/';
        $config['allowed_types']= '*';
        $config['max_size']     = '2000';
        $config['max_width']  	= '2000';
        $config['max_height']  	= '2000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        if($this->upload->do_upload('image')){
            $image_data1 = $this->upload->data();    
            $img=$image_data1['file_name'];
        }else{
             $img=$image_hidden;
        }
         
         $data=array(
             'assembly_hac_id'=>$hac_id,
             'assembly_code'=>$assembly_code,
             'assembly_name'=>$assembly_name,
             'image'=>$img
         );
         
         $this->users_model->update("hac_assembly",$id,"id",$data);
         $this->insert_log_activity("Assembly Management", "","Update Assembly '$assembly_code'");
         redirect('engine/crud_assembly/index/'.$hac_id);
   }
   
   function delete($hac_id,$id){
        //$hac=$this->users_model->select_all_where('hac_assembly',$id,'id')->row('assembly_code');
        $this->insert_log_activity("Assembly Management", "","Delete Assembly '$id'");
        $this->users_model->delete('hac_assembly',$id,'id');
        $this->users_model->delete('hac_component',$id,'assembly_id');
        redirect('engine/crud_assembly/index/'.$hac_id);
   }
}