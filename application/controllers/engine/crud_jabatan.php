<?php

class Crud_jabatan extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('master_model');
                $this->load->model('form_manager_model');	
	}
        
        public function insert_log_activity($type,$primarykey,$description){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
        function index()
	{
//                $val=$this->input->post('val');
//                $fieldx = $this->input->post('field');
//                if($fieldx==""){
//                    $field="re.id";
//                }else{
//                    $field=$fieldx;
//                }
		$config['base_url'] = base_url().'record/add_inspection/index/';
                $config['total_rows'] = $this->db->query("select * from master_jabatan order by id asc")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select * from master_jabatan order by id asc limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('master_view/v_master_jabatan', $data); 
	}
        
        function add(){
            $this->load->view('master_view/v_master_jabatan_add');
        }
        
        function add_proses(){
            $jabatan=$this->input->post('jabatan');
            $data=array('jabatan'=>$jabatan);
            $this->master_model->add_proses('master_jabatan',$data);
            $this->insert_log_activity("Jabatan Management", "","Add Jabatan '$jabatan'");
            redirect('engine/crud_jabatan');
        }
        
        function edit($id){
            $data['list']=$this->master_model->select_where('master_jabatan',$id)->row();
            $this->load->view('master_view/v_master_jabatan_edit',$data);
        }
        
        function edit_proses(){
        $id=$this->input->post('id');
        $frequency=$this->input->post('jabatan');
        $data=array('jabatan'=>$frequency);
        $this->master_model->edit_proses('master_jabatan',$id,$data);
        $this->insert_log_activity("Jabatan Management", $id,"Update Jabatan '$frequency'");
        redirect('engine/crud_jabatan');
        }
        
        function delete($id){
        $jabatan = $this->master_model->select_where('master_jabatan',$id)->row('jabatan'); 
        $this->insert_log_activity("Jabatan Management", $id,"Delete Jabatan '$jabatan'");
        $this->master_model->delete('master_jabatan',$id);
        redirect('engine/crud_jabatan');
    }
}

