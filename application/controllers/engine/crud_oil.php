<?php

class Crud_oil extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');	
	}
	

	 
public function index() {
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('record_oil_analysis');
		$crud->set_subject('Oil');
		$crud->required_fields('record_id');
		$crud->set_field_upload('upload_file','media/pdf');
	 
		$crud->columns('record_id', 'upload_file');
		$crud->fields('record_id', 'upload_file');
	 
		$crud->callback_after_insert(array($this, 'oil_after_insert'));
        
        $crud->callback_after_update(array($this, 'log_user_after_update'));
        
        $crud->callback_after_delete(array($this, 'log_user_after_delete'));
        
        
		$crud->unset_export();
		$output = $crud->render();
	 
		$this->load->view('page_crud.php',$output); 
}
 
function oil_after_insert($post_array,$primary_key){
 
		$data_insert_record = array('inspection_type' => 'OIL','inspection_id' => $primary_key, 'datetime' => date('Y-m-d H:i:s'));

		$this->db->insert('record',$data_insert_record);

		$record_id = mysql_insert_id();

		$data_update_record_oil = array('record_id' => $record_id);
		$where = array('id' => $primary_key);

		$this->db->update('record_oil_analysis',$data_update_record_oil,$where);

	return true;

	}
    
     function log_user_after_update($post_array,$primary_key)
    {
        $id = $this->session->userdata('users_id');
        $user_logs_insert = array(
            "user_id" => $id,
            "date_activity" => date('Y-m-d H:i:s'),
            "description" => 'UPDATE',
            "type" => 'OA MASTER',
            "record_id" => $primary_key
        );
     
        $this->db->insert('users_activity',$user_logs_insert);
     
        return true;
    }
    
     function log_user_after_delete($post_array,$primary_key)
    {
        $id = $this->session->userdata('users_id');
        $user_logs_delete = array(
            "user_id" => $id,
            "date_activity" => date('Y-m-d H:i:s'),
            "description" => 'DELETE',
            "type" => 'OA MASTER',
            "record_id" => $primary_key
        );
     
        $this->db->insert('users_activity',$user_logs_delete);
     
        return true;
    }	
}
 