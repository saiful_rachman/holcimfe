<?php

class Form_manager extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');
                $this->load->model('form_manager_model');	
                $this->load->model('users_model');
	}
	
	public function index() {
                $this->load->model('form_manager_model');
                $data['list_plant']=$this->users_model->select_all("master_plant")->result();
                $data['component']= $this->form_manager_model->get_hac_assembly();
                $data['area']= $this->form_manager_model->get_area();
                $data['hac']= $this->form_manager_model->get_hac();
                $data['frequency']= $this->form_manager_model->select_all('master_frequency');
                $data['periode']= $this->form_manager_model->select_all('master_periode');
		$this->load->view('form_manager/running_form_step1new',$data); 
	}
        
        public function insert_log_activity($type,$primarykey,$description){
            if($primarykey==""){
                $pk=1;
            }else{
                $pk=$primarykey;
            }
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$pk
            );
            $this->form_manager_model->log_activity($data);
        }
 
	function vibra_after_insert($post_array,$primary_key){
 
		$data_insert_record = array('inspection_type' => 'VIB','inspection_id' => $primary_key, 'datetime' => date('Y-m-d H:i:s'));

		$this->db->insert('record',$data_insert_record);

		$record_id = mysql_insert_id();

		$data_update_record_vib = array('record_id' => $record_id);
		$where = array('id' => $primary_key);

		$this->db->update('record_vibration',$data_update_record_vib,$where);

	return true;

	}	
        
        function simpan_step1(){
            $this->load->model('form_manager_model');
            $idmx = $this->form_manager_model->get_max_id("form_running");
            $area = $this->input->post('area');
            $frequency = $this->input->post('frequency');
            $mechanical_type = $this->input->post('mechanical_type');
            //$form_no = $this->input->post('form_no');
            $publish = $this->input->post('publish');
            $periode = $this->input->post('periode');
            $form_name=$this->input->post('form_name');
            $dt=array(
                'plant' =>$area,
                'form_name'=>$form_name,
                'area'=>$area,
                'frequency'=> $frequency,
                'mechanichal_type' => $mechanical_type,
                'periode'=>$periode,
                'sys_create_user'=>$this->session->userdata('users_id'),
                'sys_create_date'=>date("Y-m-d H:i:s")
            );
            $this->form_manager_model->formtop($dt);
            $max = $this->form_manager_model->get_max_id('form_running');
            $areax = $this->input->post('areax');
            $frequencyxy = $this->input->post('frequencyx');
            $hx=$this->input->post('hx');
            $equipment_name = $this->input->post('en');
            //echo count($equipment_name);
            $data = array();
            $count = count($frequencyxy);
            echo $countc = count($areax);
			
// debug missing hac
// $counter = array(
//					hac" => $hx,
//					"area" => $areax
//			  );

            for($x=0; $x<$countc; $x++) {
            $data = array(
                    "form_id" => $max,
                    "hac" => $hx[$x],
                    "equipment_name" => $equipment_name[$x],
                    "hac_to_form_running_id"=>"",
                    "component" => $areax[$x]
                );
                $this->form_manager_model->form_managerstep1('rel_component_to_form_running',$data);
             
            }
			
// output debug missing hac
// $output = array("test" => $counter);
// $this->load->view("test",$output);

                $this->insert_log_activity("Form Running", $idmx,"Add Form Running Report '$mechanical_type'");
                $this->form_step2($max);
        }
   
       public function getFunction($id)
         {
        $this->load->model('form_manager_model');
       if ( !isset($_GET['term']) )
           exit;
           $term = $_REQUEST['term'];
               $data = array();
               $rows = $this->form_manager_model->getData($id,$term);
                   foreach( $rows as $row )
                   {
                       $data[] = array(
                           'label' => $row->hac_code,
                           'value' => $row->hac_code);   // here i am taking name as value so it will display name in text field, you can change it as per your choice.
                   }
               echo json_encode($data);
               flush();

       }
       
       public function getFunctionx($id,$plant)
         {
        $this->load->model('form_manager_model');
        if($plant=="3"){
            $px="";
        }else{
            $px=$plant;
        }
       if ( !isset($_GET['term']) )
           exit;
           $term = $_REQUEST['term'];
               $data = array();
               $rows = $this->db->query('select * from hac where hac_code like "'. mysql_real_escape_string($term) .'%" and hac_code like "TQ.%%'.$px.'-%%" order by hac_code asc limit 0,10')->result();
                   foreach( $rows as $row )
                   {
                       $data[] = array(
                           'label' => $row->hac_code,
                           'value' => $row->hac_code);   // here i am taking name as value so it will display name in text field, you can change it as per your choice.
                   }
               echo json_encode($data);
               flush();

       }
       
       function form_step2($id){
        $this->load->model('form_manager_model');
        $data['data_1stform'] = $this->form_manager_model->get_topform1xx($id,"form_running")->row();
        $data['data_2ndform'] = $this->form_manager_model->get_topformrun($id,"rel_component_to_form_running")->result();
        $this->load->view('form_manager/running_form_step2',$data);
       }
       
       function simpan_step2(){
           $this->load->model('form_manager_model');
           $status= $this->input->post('status');
           $id_form1=$this->input->post('form_id1');
           $value = $this->input->post('value');
           $ins = $this->input->post('inspection');
           $id = $this->input->post('idr');
           $hc = $this->input->post('hc');
           $en = $this->input->post('en');
           $com = $this->input->post('com');
           $vibra = $this->input->post('vibra');
           //$data = array();
           $countjum = count($ins);
            for($i=0; $i<$countjum; $i++) {
                $data = array(
                    "form_id" => $id_form1,
                    "inspection_activity" => $ins[$i],
                    "target_value"=>$value[$i],
                    "rel_component_id" => $id[$i],
                    "form_status"=>$status,
                    "hac"=>$hc[$i],
                    "equipment_name"=>$en[$i],
                    "component"=>$com[$i],
                    "vibration_check"=>$vibra[$i]
                );
                $this->db->insert("rel_activity_inspection", $data);
            }
            if($status == "R"){
            redirect('engine/form_manager/form_detailrunning1');
            }else{
            redirect('engine/form_manager/form_detailstop1');
            }
       } 
       
       public function stop_inspection() {
            $this->load->model('form_manager_model');
            $data['list_plant']=$this->users_model->select_all("master_plant")->result();
            $data['component']= $this->form_manager_model->get_component();
            $data['area']= $this->form_manager_model->get_area();
            $data['hac']= $this->form_manager_model->get_hac();
            $data['frequency']= $this->form_manager_model->select_all('master_frequency');
            $this->load->view('form_manager/stop_form_step1new',$data); 
	}
       
      public function simpan_stop_step1(){
            $this->load->model('form_manager_model');
            $idmx = $this->form_manager_model->get_max_id("form_stop");
            $form_type = $this->input->post('form_type');
            $image1x = $this->input->post('image1');
            $image2x = $this->input->post('image2');
            $image3x = $this->input->post('image3');
            $image4x = $this->input->post('image4');
            $area = $this->input->post('subarea');
            $frequency = $this->input->post('frequency');
            $mechanical_type = $this->input->post('mechanical_type');
            $periode = $this->input->post('periode');
            //$publish = $this->input->post('publish');
            $hac = $this->input->post('hac');
            $get_idhac = $this->form_manager_model->get_idhac($hac);
            
            $config['upload_path']	= "./media/images/";
	    $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000';
            $config['max_width']  	= '2000';
            $config['max_height']  	= '2000';
            $this->load->library('upload');
	    $this->upload->initialize($config);
            
            if($this->upload->do_upload('image1'))
             {
            $image_data1 = $this->upload->data();    
             }
             if($this->upload->do_upload('image2'))
             {
            $image_data2= $this->upload->data();    
             }
             if($this->upload->do_upload('image3'))
             {
            $image_data3 = $this->upload->data();    
             }
             if($this->upload->do_upload('image4'))
             {
            $image_data4 = $this->upload->data();    
             }
           $dt=array(
                'area' =>$area,
                'frequency'=> $frequency,
                'type' => $mechanical_type,
                'form_type'=>$form_type,
                'image1'=>$image_data1['file_name'],
                'image2'=>$image_data2['file_name'],
                'image3'=>$image_data3['file_name'],
                'image4'=>$image_data4['file_name'],
                'hac'=>$get_idhac,
                'periode'=>$periode,
                'publish'=>"unpublish",
                'datetime'=>date("Y-m-d H:i:s")
            );
            $this->form_manager_model->formtopstop($dt);
            
            $max = $this->form_manager_model->get_max_id('form_stop');
            $component = $this->input->post('component');
            $item_check =$this->input->post('item_check');
            $method = $this->input->post('method');
            $standard = $this->input->post('standard');
            for($i=0;$i<count($component);$i++){
            $dt2=array(
                        "form_id"=>$max,
                        "hac"=>$get_idhac,
                        "component"=>$component[$i],
                        "item_check"=>$item_check[$i],
                        "method"=>$method[$i],
                        "standard"=>$standard[$i]
                       );
            $this->form_manager_model->formstop_managerstep1($dt2);
            }
            $this->insert_log_activity("Form Stop", $idmx,"Add Form Stop Report '$mechanical_type'");
            redirect('engine/form_manager/form_detailstop1');
      }
      function form_stopstep2($id){
        $this->load->model('form_manager_model');
        $data['data_1stform'] = $this->form_manager_model->get_topform2($id,"form_stop")->row();
        $data['data_2ndform'] = $this->form_manager_model->get_topformx($id,"rel_component_to_form_stop")->result();
        $this->load->view('form_manager/stop_form_step2',$data);
       }
     function edit_detailrunning1($id){
        $this->load->model('form_manager_model');
        $data['area']= $this->form_manager_model->get_area();
        $data['area_detail']= $this->form_manager_model->get_area_detail('form_running',$id);
        $data['form_link'] = base_url()."engine/form_manager/simpan_detail_running1";
        $this->load->view('form_manager/edit_detail_running1',$data); 
     }
     function edit_detailstop1($id){
        $this->load->model('form_manager_model');
        $data['area']= $this->form_manager_model->get_area();
        $data['form_type']= $this->form_manager_model->get_formtype()->result();
        $data['area_detail']= $this->form_manager_model->get_area_detail('form_stop',$id);
        $data['form_link'] = base_url()."engine/form_manager/simpan_detail_stop1";
        $this->load->view('form_manager/edit_detailstop1',$data); 
     }
     function edit_detailrunning2($id){
        $this->load->model('form_manager_model');
        $data['data_1stform'] = $this->form_manager_model->get_topform1x($id,"form_running")->row();
        $data['data_2ndform'] = $this->form_manager_model->get_topform($id,"rel_component_to_form_running")->result();
        $data['component']= $this->form_manager_model->get_assembly();
        $data['area']= $this->form_manager_model->get_area();
        $data['hac']= $this->form_manager_model->get_hac();
        $this->load->view('form_manager/edit_detailrunning2',$data);
        //$this->load->view('form_manager/view_coba',$data);
     } 
     function edit_detailstop2($id){
        $this->load->model('form_manager_model');
        $data['data_1stform'] = $this->form_manager_model->get_topform1($id,"form_stop")->row();
        $data['data_2ndform'] = $this->form_manager_model->get_topformstop($id,"rel_component_to_form_stop")->result();
        $data['component']= $this->form_manager_model->get_assembly();
        $data['area']= $this->form_manager_model->get_area();
        $data['hac']= $this->form_manager_model->get_hac();
        $data['id']= $id;
        $this->load->view('form_manager/edit_detailstop2',$data);
        //$this->load->view('form_manager/view_coba',$data);
     } 
     function edit_detailrunning3($id){
        $this->load->model('form_manager_model');
        $id_form = $this->form_manager_model->get_idform($id);
        $data['data_1stform'] = $this->form_manager_model->get_topform1x($id_form,"form_running")->row();
        $data['data_2ndform'] = $this->form_manager_model->get_topformedit($id,"rel_component_to_form_running")->row();
        $data['value'] = $this->form_manager_model->get_data_value($id);
        
        $this->load->view('form_manager/edit_detailrunning3',$data);
     } 
     
     function edit_detailstop3($id){
        $this->load->model('form_manager_model');
        $id_form = $this->form_manager_model->get_idform($id);
        $data['data_1stform'] = $this->form_manager_model->get_topform1($id,"form_stop")->row();
        $data['data_2ndform'] = $this->form_manager_model->get_topformedit($id,"rel_component_to_form_stop")->row();
        $data['value'] = $this->form_manager_model->get_data_value($id);
        
        $this->load->view('form_manager/edit_detailrunning3',$data);
        //$this->insert_log_activity("Form Running", $id,"Update Form Running Report");
     } 
     
     function simpan_detail_running1(){
         $this->load->model('form_manager_model');
         $id = $this->input->post('id');
         //$area = $this->input->post('area');
         $form_name=$this->input->post('form_name');
         $frequency = $this->input->post('frequency');
         $type = $this->input->post('mechanical_type');
         $form = $this->input->post('form_no');
         $periode = $this->input->post('periode');
         $publish = $this->input->post('publish');
         $data=array(
             'form_name'=>$form_name,
             'frequency'=>$frequency,
             'mechanichal_type'=>$type,
             'periode'=>$periode,
         );
         $this->form_manager_model->simpan_detail_running1('form_running',$id,$data);
         $this->insert_log_activity("Form Running", $id,"Update Form Running Report '$type' ");
         redirect('engine/form_manager/form_detailrunning1');
     }
     
     function simpan_detail_stop1(){
         $this->load->model('form_manager_model');
         $id = $this->input->post('id');
         $area = $this->input->post('area');
         $frequency = $this->input->post('frequency');
         $type = $this->input->post('mechanical_type');
         $form_type = $this->input->post('form_type');
         $hidden_photo1 = $this->input->post('hidden_photo1');
         $hidden_photo2 = $this->input->post('hidden_photo2');
         $hidden_photo3 = $this->input->post('hidden_photo3');
         $hidden_photo4 = $this->input->post('hidden_photo4');
         $periode = $this->input->post('periode');
         $publish = $this->input->post('publish');
         
            $config['upload_path']	= "./media/images/";
	    $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000';
            $config['max_width']  	= '2000';
            $config['max_height']  	= '2000';
            $this->load->library('upload');
	    $this->upload->initialize($config);
             if($this->upload->do_upload('image1'))
             {
                 $image_data1 = $this->upload->data();  
                 $img1 = $image_data1['file_name'];
             }else{
                 $img1 = $hidden_photo1;   
             }
             
             if($this->upload->do_upload('image2'))
             {
                $image_data2= $this->upload->data();   
                $img2 = $image_data2['file_name'];
             }else{
                $img2=$hidden_photo2;
             }
             
             if($this->upload->do_upload('image3'))
             {
                $image_data3 = $this->upload->data(); 
                $img3 = $image_data3['file_name'];
             }else{
                $img3 = $hidden_photo3;
             }
             
             if($this->upload->do_upload('image4'))
             {
                $image_data4 = $this->upload->data();    
                $img4 = $image_data4['file_name'];
             }else{
                $img4 = $hidden_photo4;
             }
        
              $data=array(
             'area'=>$area,
             'frequency'=>$frequency,
             'type'=>$type,
             'form_type'=>$form_type,
             'image1'=>$img1,
             'image2'=>$img2,
             'image3'=>$img3,
             'image4'=>$img4,
             'periode'=>$periode,
             'publish'=>$publish
         );
         $this->form_manager_model->simpan_detail_running1('form_stop',$id,$data);
         $this->insert_log_activity("Form Stop", "","Update Form Stop Report '$type'");
         redirect('engine/form_manager/form_detailstop1');
     }
     
     function simpan_detail_running2(){
        $this->load->model('form_manager_model');
        //$frequencyx = $this->input->post('frequencyx');
            $old_frequency = $this->input->post('old_frequency');
            $old_area = $this->input->post('old_area');
            $get_idfom = $this->input->post('get_idform');
            $areax = $this->input->post('areax');
            $data = array();
            $count = count($get_idfom);
            if($count > 0){
            for($i=0; $i<$count; $i++) {
            $frequencyx = $this->input->post('frequencyx');
            $get_idhac = $this->form_manager_model->get_idhac($frequencyx[$i]);
            $dt_form = $get_idfom[$i];
            $data = array(
                    "hac" => $get_idhac,
                    "hac_to_form_running_id"=>"",
                    "component" => $areax[$i]
                );
            $data2=array(
                        "hac" => $get_idhac,
                        "component" => $areax[$i]
                        );
            $this->form_manager_model->update_detail_running2($data,$dt_form);
            $this->form_manager_model->update_inspection($data2,$dt_form,$old_area[$i],$old_frequency[$i]);
                //$this->form_manager_model->form_managerstep1('rel_component_to_form_running_temp',$data);//proses input ke table temporary
                //$this->form_manager_model->delete_rel('rel_component_to_form_running',$id);//proses delete data di table rel_component_to_form_running
                //$this->form_manager_model->insert_temp($id);//memindahkan data dari table temp ke table rel_component_to_form_running berdasarkan id
                //$this->form_manager_model->delete_rel('rel_component_to_form_running_temp',$id);//menghapus semua data yang ada di table temp sesuai dengan id
             }
            }
            //$frequency = $this->input->post('frequency');
            $id = $this->input->post('form_id1');
            $area = $this->input->post('area');
            $jum = $this->input->post('jum');
            $countx = count($area);
            if($jum > 0){
            for($i=0; $i<$countx; $i++) {
            $frequency = $this->input->post('frequency');
            $get_idhacx = $this->form_manager_model->get_idhac($frequency[$i]);
            $data = array(
                    "form_id" => $id,
                    "hac" => $get_idhacx,
                    "hac_to_form_running_id"=>"",
                    "component" => $area[$i]
                );
             $this->form_manager_model->form_managerstep1('rel_component_to_form_running',$data);//proses input ke table 
            }
            }
            $this->form_manager_model->delete_0();//menghapus data yang ada pada table rel_component_to_form_running yang mempunyai nilai hac dan component 0
            $this->insert_log_activity("Form Running", "","Update Form Running Report");
            redirect('engine/form_manager/form_detailrunning2/'.$id);
     }
     function simpan_detail_stop2(){
         $this->load->model('form_manager_model');
         $idform = $this->input->post('idform');
         $hacx = $this->input->post('hacy');
         $component= $this->input->post('component');
         $item_check = $this->input->post('item_check');
         $method = $this->input->post('method');
         $standard = $this->input->post('standard');
         
         $this->form_manager_model->delete('rel_component_to_form_stop','form_id',$idform);
         for($i=0;$i<count($method);$i++){
         $data = array(
             "form_id"=>$idform,
             "hac"=>$hacx,
             "component"=>$component[$i],
             "item_check"=>$item_check[$i],
             "method"=>$method[$i],
             "standard"=>$standard[$i]
         );
         $this->form_manager_model->insert('rel_component_to_form_stop',$data);
         }
         $this->insert_log_activity("Form Stop", $idform,"Update Form Stop Report");
         redirect('engine/form_manager/form_detailstop2/'.$idform);
     }
      function simpan_detail_running3(){
           $this->load->model('form_manager_model');
           $idx = $this->input->post('idx');//rel_component_to_form_running id
           $status= $this->input->post('status');
           $id_form1=$this->input->post('form_id1');//form_running id
           $value = $this->input->post('value');
           $ins = $this->input->post('inspection');
           $id = $this->input->post('idr');
           $hc = $this->input->post('hc');
           $com = $this->input->post('com');
           $vibra = $this->input->post('vibra');
           $data = array();
           $countjum = count($ins);
            for($i=0; $i<$countjum; $i++) {
                $data = array(
                    "form_id" => $id_form1,
                    "inspection_activity" => $ins[$i],
                    "target_value"=>$value[$i],
                    "rel_component_id" => $idx,
                    "form_status"=>$status,
                    "hac"=>$hc,
                    "component"=>$com,
                    "vibration_check"=>$vibra[$i]
                );
                $this->form_manager_model->form_managerstep1('rel_activity_inspection_temp',$data);//proses input ke table temporary
                $this->form_manager_model->delete_rel_inspection('rel_activity_inspection',$idx);//proses delete data di table rel_component_to_form_running
                $this->form_manager_model->insert_temp_inspection($idx);//memindahkan data dari table temp ke table rel_component_to_form_running berdasarkan id

                //$this->form_manager_model->form_managerstep2($data);
            }
            if($status == "R"){
            //$this->update_log_activity("form runningstep3", $idx);
            redirect('engine/form_manager/form_detailrunning3/'.$idx);
            }else{
            $this->insert_log_activity("Form Running", $idx,"Update Form Running Report");
            redirect('engine/form_manager/form_detailstop3/'.$idx.'/'.$id_form1);
            }
       } 
       
       function form_detailrunning1(){
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="a.id";
                }else{
                    $field=$fieldx;
                }
		$config['base_url'] = base_url().'engine/form_manager/form_detailrunning1/';
                $config['total_rows'] = $this->db->query("select a.*,b.area_name,c.frequency,d.periode from form_running a
                                                        left join area b on a.area=b.id
                                                        left join master_frequency c on a.frequency=c.id
                                                        left join master_periode d on a.periode=d.id
                                                        where
                                                        $field LIKE '%$val%'
                                                        order by a.id desc")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select a.*,b.area_name,c.frequency,d.periode from form_running a
                                                left join area b on a.area=b.id
                                                left join master_frequency c on a.frequency=c.id
                                                left join master_periode d on a.periode=d.id
                                                where $field LIKE '%$val%' 
                                                order by id desc limit ".$pg.",".$config['per_page']."")->result();
		$this->load->view('form_manager/form_detail1',$data); 
       }
//     function form_detailrunning1(){
//        $this->load->model('form_manager_model');
//        $data['list']=$this->form_manager_model->get_list_running1()->result();
//        $this->load->view('form_manager/form_detail1',$data);
//     }
    function user_after_delete_runningstep1($primary_key){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>"DELETE",
                "type"=>"running step1 form",
                "record_id"=>$primary_key,
            );
            $this->form_manager_model->log_activity($data);
        }
        
    function form_detailstop1(){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="a.id";
        }else{
            $field=$fieldx;
        }
        $config['base_url'] = base_url().'engine/form_manager/form_detailstop1/';
        $config['total_rows'] = $this->db->query("select a.*,b.hac_code,c.frequency,d.area_name from form_stop a
                                               left join hac b on a.hac=b.id
                                               left join master_frequency c on a.frequency=c.id
                                               left join area d on a.area=d.id
                                               where
                                               $field LIKE '%$val%'
                                               order by a.id desc")->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 4;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['data'] = $this->db->query("select a.*,b.hac_code,c.frequency,d.area_name from form_stop a
                                        left join hac b on a.hac=b.id
                                        left join master_frequency c on a.frequency=c.id
                                        left join area d on a.area=d.id
                                        where
                                        $field LIKE '%$val%'
                                        order by a.id desc limit ".$pg.",".$config['per_page']."")->result();
        //$this->load->view('form_manager/form_detail1',$data); 
//        $sql = $this->db->query("select a.*,b.hac_code,c.frequency,d.area_name from form_stop a
//                                 left join hac b on a.hac=b.id
//                                 left join master_frequency c on a.frequency=c.id
//                                 left join area d on a.area=d.id order by a.id desc
//                                ");
//        $data['list'] = $sql->result();
        $this->load->view('form_manager/form_detailstop1',$data);    
    }
    function user_after_delete_stopstep1($primary_key){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>"DELETE",
                "type"=>"stop step1 form",
                "record_id"=>$primary_key,
            );
            $this->form_manager_model->log_activity($data);
        }
        
        
    
    function form_detailrunning2($id){
        $this->load->model('form_manager_model');
        $data['list']=$this->form_manager_model->get_list_running2($id)->result();
        $this->load->view('form_manager/form_detail2',$data);    
    }
    function user_after_delete_runningstep2($primary_key){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>"DELETE",
                "type"=>"running step2 form",
                "record_id"=>$primary_key,
            );
            $this->form_manager_model->log_activity($data);
        }
        
        
    
    function form_detailstop2($id){
        $this->load->model('form_manager_model');
        $idx=$this->form_manager_model->select_all_where('form_stop','id',$id)->row('hac');
        $this->form_manager_model->delete_rel('rel_component_to_form_running_temp',$id);//menghapus semua data yang ada di table temp sesuai dengan id
        $sql = $this->db->query("select a.*,b.hac_code,c.assembly_name from rel_component_to_form_stop a
                                 left join hac b on a.hac=b.id
                                 left join hac_assembly c on a.component=c.id
                                 where a.form_id='$id' and a.hac='$idx'
                                ");
        $data['list'] = $sql->result();
        $this->load->view('form_manager/form_detailstop2',$data);    
    }
    function user_after_delete_stopstep2($primary_key){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>"DELETE",
                "type"=>"stop step2 form",
                "record_id"=>$primary_key,
            );
            $this->form_manager_model->log_activity($data);
        }
        
        
    
    function form_detailrunning3($id){
        $this->load->model('form_manager_model');
        $this->db->query('truncate table rel_activity_inspection_temp');
        $data['list']=$this->form_manager_model->get_detailrunning3($id)->result();
        $this->load->view('form_manager/form_detail3',$data);    
    }
    function user_after_delete_runningstep3($primary_key){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>"DELETE",
                "type"=>"runnig step3 form",
                "record_id"=>$primary_key,
            );
            $this->form_manager_model->log_activity($data);
        }
        
        
    
    function form_detailstop3($id){
        $this->load->model('form_manager_model');
        $id_form = $this->uri->segment(5);
        $this->form_manager_model->delete_rel_inspection2('rel_activity_inspection_temp',$id_form);//menghapus semua data yang ada di table temp sesuai dengan id
        $crud = new grocery_CRUD();
	$crud->set_theme('datatables');
        $crud->set_table('rel_activity_inspection');
        $crud->set_subject('Running Inspection');
        $crud->where('rel_component_id',$id);
        $crud->where('form_status','S');
        $crud->order_by('id','desc');
        //$crud->add_action('Update', 'http://png-4.findicons.com/files/icons/990/vistaico_toolbar/16/edit.png', 'engine/form_manager/edit_detailrunning1');
        //$crud->add_action('Sub', 'http://png-1.findicons.com/files/icons/577/refresh_cl/16/windows_view_detail.png', 'demo/action_smiley');
        //$crud->callback_before_insert(array($this,'encrypt_password_callback'));
        //$crud->set_relation('hac','hac','hac_code');
        //$crud->set_relation('component','hac_component','component_code');
        $crud->columns('inspection_activity', 'target_value');
        $crud->callback_after_delete(array($this,'user_after_delete_stopstep3'));
        $crud->unset_export();
        $crud->unset_read();
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();
        $crud->unset_print();
        $output = $crud->render();
 
        $this->outputstop3($output);
     }
       function outputstop3($output = null)
    {
        $this->load->view('form_manager/form_detailstop3.php',$output);    
    }
    function user_after_delete_stopstep3($primary_key){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>"DELETE",
                "type"=>"stop step3 form",
                "record_id"=>$primary_key,
            );
            $this->form_manager_model->log_activity($data);
        }
        
        
    
    function delete_back_form_run(){ //fungsi untuk menghapus data terakhir ketika tombol cancel pada form pertama di klick
        $this->load->model('form_manager_model');
        $max = $this->form_manager_model->get_max_id('form_running');
        $this->form_manager_model->delete_back_form_run('form_running','id',$max);
        $this->form_manager_model->delete_back_form_run('rel_component_to_form_running','form_id',$max);
        echo"Success";
    }
    function delete_back_form_stop(){ //fungsi untuk menghapus data terakhir ketika tombol cancel pada form pertama di klick
        $this->load->model('form_manager_model');
        $max = $this->form_manager_model->get_max_id('form_stop');
        $this->form_manager_model->delete_back_form_run('form_stop','id',$max);
        $this->form_manager_model->delete_back_form_run('rel_component_to_form_stop','form_id',$max);
        echo"Success";
    }
    function delete_back_form_wear(){ //fungsi untuk menghapus data terakhir ketika tombol cancel pada form pertama di klick
        $this->load->model('form_manager_model');
        $max = $this->form_manager_model->get_max_id('form_measuring');
        $this->form_manager_model->delete_back_form_run('form_measuring','id',$max);
        $this->form_manager_model->delete_back_form_run('roller','form_id',$max);
        $this->form_manager_model->delete_back_form_run('grinding','form_id',$max);
        echo"Success";
    }
    
    function delete_edit_detail_running2(){
        $id = $this->input->post('id');
        $this->load->model('form_manager_model');
        $this->form_manager_model->delete_edit_detail_running2($id);
        $this->form_manager_model->delete_rel_inspection('rel_activity_inspection',$id);
        return true;
    }
    function delete_edit_detail_stop2(){
        $id = $this->input->post('id');
        $this->load->model('form_manager_model');
        $this->form_manager_model->delete_rel_inspection_stop('rel_component_to_form_stop',$id);
        return true;
    }
    function get_chain(){
        $id = $this->input->post('id');
        $this->load->model('form_manager_model');
        $get_idhac = $this->form_manager_model->get_idhac($id);
        $get_idassembly = $this->form_manager_model->get_idassembly2($get_idhac);
        $data="";
        foreach ($get_idassembly as $mp){
			$data .= "<option value='$mp->id'>$mp->assembly_name</option>\n";	
		}
		echo $data;
    }
    
    function wearing_listtable(){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="a.id";
        }else{
            $field=$fieldx;
        }
        $config['base_url'] = base_url().'engine/form_manager/wearing_listtable/';
        $config['total_rows'] = $this->db->query("select a.*,b.hac_code,c.area_name from form_measuring a
                                                 left join hac b on a.hac=b.id
                                                 left join area c on a.area=c.id
                                                 where
                                                 $field LIKE '%$val%'
                                                 order by a.id desc")->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 4;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['data'] = $this->db->query("select a.*,b.hac_code,c.area_name from form_measuring a
                                        left join hac b on a.hac=b.id
                                        left join area c on a.area=c.id
                                        where
                                        $field LIKE '%$val%'
                                        order by a.id desc limit ".$pg.",".$config['per_page']."")->result();
//        $sql=$this->db->query("select a.*,b.hac_code,c.area_name from form_measuring a
//                               left join hac b on a.hac=b.id
//                               left join area c on a.area=c.id
//                               order by a.id DESC
//                              ");
        $this->load->view('form_manager/listtable_wearing',$data);    
    }
    function user_after_delete_measurement($primary_key){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>"DELETE",
                "type"=>"measurement form",
                "record_id"=>$primary_key,
            );
            $this->form_manager_model->log_activity($data);
        }
        
    
    function wearing_form_step1(){
        $this->load->model('form_manager_model');
                $data['list_plant']=$this->users_model->select_all("master_plant")->result();
        $data['area']= $this->form_manager_model->get_area();
        $data['hac']= $this->form_manager_model->get_hac();
        $this->load->view('form_manager/wearing_form_step1',$data);
    }
    function wearing_form_step2($id){
        $this->load->model('form_manager_model');
        $data['list'] = $this->form_manager_model->select_wearing($id)->row();
        $this->load->view('form_manager/wearing_form_stepx',$data);
    }
    
    function show_edit_wearingstep1($id){
        $this->load->model('form_manager_model');
        $data['detail'] = $this->form_manager_model->get_detail_relation1('form_measuring',$id)->row();
        $data['area']= $this->form_manager_model->get_area();
        $data['hac']= $this->form_manager_model->get_hac();
        $this->load->view('form_manager/show_edit_wearingstep1',$data);
    }
    
    function simpan_wearingstep1(){
        $this->load->model('form_manager_model');
        $hac = $this->input->post('hac');
        $get_idhacx = $this->form_manager_model->get_idhac($hac);
        $area = $this->input->post('subarea');
        $roller = $this->input->post('roller');
        $grinding = $this->input->post('grinding');
        $meas_no = $this->input->post('meas_no');
        $material = $this->input->post('material');
		$running_hour = $this->input->post('running_hour');
		$total_production = $this->input->post('total_production');
        $date = $this->input->post('date');
        $instalation_date = $this->input->post('instalation_date');
        $value = $this->input->post('value');
        
            $config['upload_path']	= "./media/images/";
			$config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000';
            $config['max_width']  	= '2000';
            $config['max_height']  	= '2000';
            $this->load->library('upload');
			$this->upload->initialize($config);
             if($this->upload->do_upload('image'))
             {
                 $image_data = $this->upload->data();
                 $data = array(
                                'hac'=> $get_idhacx,
                                'area'=>$area,
                                'meas_no'=>$meas_no,
                                'material'=>$material,
								'running_hour' => $running_hour,
								'total_production' => $total_production,
                                'instalation_date'=>$instalation_date,
                                'create_date'=>$date,
                                'point'=>$value,
                                'gambar'=>$image_data['file_name'],
                                'grinding'=>$grinding,
                                'roller'=>$roller
                               );
                 $this->form_manager_model->insert('form_measuring',$data);                 
             }else{
                 $data = array(
                                'hac'=> $get_idhacx,
                                'area'=>$area,
                                'meas_no'=>$meas_no,
                                'material'=>$material,
								'running_hour' => $running_hour,
								'total_production' => $total_production,
                                'instalation_date'=>$instalation_date,
                                'create_date'=>$date,
                                'point'=>$value,
                                'grinding'=>$grinding,
                                'roller'=>$roller
                               );
                 $this->form_manager_model->insert('form_measuring',$data);                 
             }
              $id = $this->form_manager_model->get_max_id("form_measuring");
              $this->insert_log_activity("Measurement Form",$id,"Add Form Measurement Report area '$area'");
              redirect('engine/form_manager/wearing_form_step2/'.$id);
    }
    
    function simpan_wearingstep2(){
        $this->load->model('form_manager_model');
        $form=$this->input->post('form_id');
        $roller = $this->input->post('roller');
        $rol1=$this->input->post('rol1');
        $rol2=$this->input->post('rol2');
        $rol3=$this->input->post('rol3');
        $rol4=$this->input->post('rol4');
        $rol5=$this->input->post('rol5');
        $rol6=$this->input->post('rol6');
        $rol7=$this->input->post('rol7');
        $rol8=$this->input->post('rol8');
        $rol9=$this->input->post('rol9');
        $rol10=$this->input->post('rol10');
        $rol11=$this->input->post('rol11');
        $rol12=$this->input->post('rol12');
        $rol13=$this->input->post('rol13');
        $rol14=$this->input->post('rol14');
        $rol15=$this->input->post('rol15');
        $rol16=$this->input->post('rol16');
        $rol17=$this->input->post('rol17');
        $rol18=$this->input->post('rol18');
        $rol19=$this->input->post('rol19');
        $rol20=$this->input->post('rol20');
        for($i=0;$i<count($rol1);$i++){
                    $data = array("form_id"=>$form,
                    "date"=>date("Y-m-d"),
                    "no_roller"=>$roller[$i],
                    "meas_no"=>0,
                    "val1"=>$rol1[$i],"val2"=>$rol2[$i],"val3"=>$rol3[$i],"val4"=>$rol4[$i],"val5"=>$rol5[$i],
                    "val6"=>$rol6[$i],"val7"=>$rol7[$i],"val8"=>$rol8[$i],"val9"=>$rol9[$i],"val10"=>$rol10[$i],
                    "val11"=>$rol11[$i],"val12"=>$rol12[$i],"val13"=>$rol13[$i],"val14"=>$rol14[$i],"val15"=>$rol15[$i],
                    "val16"=>$rol16[$i],"val17"=>$rol17[$i],"val18"=>$rol18[$i],"val19"=>$rol19[$i],"val20"=>$rol20[$i]);
                    $this->form_manager_model->insert("roller",$data);
        }
        $grinding1=$this->input->post('grinding1');
        $grinding2=$this->input->post('grinding2');
        $grinding3=$this->input->post('grinding3');
        $no_urut = $this->input->post('no_urut');
        
        $a1=$this->input->post('1a');
        $a2=$this->input->post('2a');
        $a3=$this->input->post('3a');
        $a4=$this->input->post('4a');
        $a5=$this->input->post('5a');
        $a6=$this->input->post('6a');
        $a7=$this->input->post('7a');
        $a8=$this->input->post('8a');
        $a9=$this->input->post('9a');
        $a10=$this->input->post('10a');
        $a11=$this->input->post('11a');
        $a12=$this->input->post('12a');
        $a13=$this->input->post('13a');
        $a14=$this->input->post('14a');
        $a15=$this->input->post('15a');
        $a16=$this->input->post('16a');
        $a17=$this->input->post('17a');
        $a18=$this->input->post('18a');
        $a19=$this->input->post('19a');
        $a20=$this->input->post('20a');
        
        $b1=$this->input->post('1b');
        $b2=$this->input->post('2b');
        $b3=$this->input->post('3b');
        $b4=$this->input->post('4b');
        $b5=$this->input->post('5b');
        $b6=$this->input->post('6b');
        $b7=$this->input->post('7b');
        $b8=$this->input->post('8b');
        $b9=$this->input->post('9b');
        $b10=$this->input->post('10b');
        $b11=$this->input->post('11b');
        $b12=$this->input->post('12b');
        $b13=$this->input->post('13b');
        $b14=$this->input->post('14b');
        $b15=$this->input->post('15b');
        $b16=$this->input->post('16b');
        $b17=$this->input->post('17b');
        $b18=$this->input->post('18b');
        $b19=$this->input->post('19b');
        $b20=$this->input->post('20b');
        
        $c1=$this->input->post('1c');
        $c2=$this->input->post('2c');
        $c3=$this->input->post('3c');
        $c4=$this->input->post('4c');
        $c5=$this->input->post('5c');
        $c6=$this->input->post('6c');
        $c7=$this->input->post('7c');
        $c8=$this->input->post('8c');
        $c9=$this->input->post('9c');
        $c10=$this->input->post('10c');
        $c11=$this->input->post('11c');
        $c12=$this->input->post('12c');
        $c13=$this->input->post('13c');
        $c14=$this->input->post('14c');
        $c15=$this->input->post('15c');
        $c16=$this->input->post('16c');
        $c17=$this->input->post('17c');
        $c18=$this->input->post('18c');
        $c19=$this->input->post('19c');
        $c20=$this->input->post('20c');
        
        for($i=0;$i<count($c1);$i++){
            $data = array("form_id"=>$form,
                    "date"=>date("Y-m-d"),
                    "no_grinding"=>$grinding1,
                    "no_urut"=>$no_urut[$i],
                    "meas_no"=>0,
                    "val1"=>$a1[$i],"val2"=>$a2[$i],"val3"=>$a3[$i],"val4"=>$a4[$i],"val5"=>$a5[$i],
                    "val6"=>$a6[$i],"val7"=>$a7[$i],"val8"=>$a8[$i],"val9"=>$a9[$i],"val10"=>$a10[$i],
                    "val11"=>$a11[$i],"val12"=>$a12[$i],"val13"=>$a13[$i],"val14"=>$a14[$i],"val15"=>$a15[$i],
                    "val16"=>$a16[$i],"val17"=>$a17[$i],"val18"=>$a18[$i],"val19"=>$a19[$i],"val20"=>$a20[$i]);
                    $this->form_manager_model->insert("grinding",$data);
           $data2 = array("form_id"=>$form,
                    "date"=>date("Y-m-d"),
                    "no_urut"=>$no_urut[$i],
                    "no_grinding"=>$grinding2,
                    "meas_no"=>0,
                    "val1"=>$b1[$i],"val2"=>$b2[$i],"val3"=>$b3[$i],"val4"=>$b4[$i],"val5"=>$b5[$i],
                    "val6"=>$b6[$i],"val7"=>$b7[$i],"val8"=>$b8[$i],"val9"=>$b9[$i],"val10"=>$b10[$i],
                    "val11"=>$b11[$i],"val12"=>$b12[$i],"val13"=>$b13[$i],"val14"=>$b14[$i],"val15"=>$b15[$i],
                    "val16"=>$b16[$i],"val17"=>$b17[$i],"val18"=>$b18[$i],"val19"=>$b19[$i],"val20"=>$b20[$i]);
                    $this->form_manager_model->insert("grinding",$data2);
            $data3 = array("form_id"=>$form,
                    "date"=>date("Y-m-d"),
                    "no_grinding"=>$grinding3,
                    "no_urut"=>$no_urut[$i],
                    "meas_no"=>0,
                    "val1"=>$c1[$i],"val2"=>$c2[$i],"val3"=>$c3[$i],"val4"=>$c4[$i],"val5"=>$c5[$i],
                    "val6"=>$c6[$i],"val7"=>$c7[$i],"val8"=>$c8[$i],"val9"=>$c9[$i],"val10"=>$c10[$i],
                    "val11"=>$c11[$i],"val12"=>$c12[$i],"val13"=>$c13[$i],"val14"=>$c14[$i],"val15"=>$c15[$i],
                    "val16"=>$c16[$i],"val17"=>$c17[$i],"val18"=>$c18[$i],"val19"=>$c19[$i],"val20"=>$c20[$i]);
                    $this->form_manager_model->insert("grinding",$data3);
        }
                 redirect('engine/form_manager/wearing_listtable');
    }
    
    function simpan_edit_wearingstop1(){
        $this->load->model('form_manager_model');
        $id = $this->input->post('id');
        $hac = $this->form_manager_model->get_idhac($this->input->post('hac'));
        $area = $this->input->post('area');
        $meas_no = $this->input->post('meas_no');
        $material = $this->input->post('material');
		$running_hour = $this->input->post('running_hour');
		$total_production = $this->input->post('total_production');
        $date = $this->input->post('date');
        $instalation_date = $this->input->post('instalation_date');
        $value = $this->input->post('value');
        
            $config['upload_path']	= "./media/images/";
	    $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000';
            $config['max_width']  	= '2000';
            $config['max_height']  	= '2000';
            $this->load->library('upload');
	    $this->upload->initialize($config);
             if($this->upload->do_upload('image'))
             {
                 $image_data = $this->upload->data();
                  $data = array(
                                'hac'=> $hac,
                                'area'=>$area,
                                'meas_no'=>$meas_no,
                                'material'=>$material,
								'running_hour' => $running_hour,
								'total_production' => $total_production,
                                'instalation_date'=>$instalation_date,
                                'create_date'=>$date,
                                'point'=>$value,
                                'gambar'=>$image_data['file_name'],
                               );
             }else{
                 $data = array(
                                'hac'=> $hac,
                                'area'=>$area,
                                'meas_no'=>$meas_no,
                                'material'=>$material,
								'running_hour' => $running_hour,
								'total_production' => $total_production,
                                'instalation_date'=>$instalation_date,
                                'create_date'=>$date,
                                'point'=>$value,
                               );
             }
             $this->form_manager_model->update('form_measuring',$data,$id);
             
            $form=$this->input->post('form_id');
            $this->form_manager_model->delete('roller','form_id',$form);
            $this->form_manager_model->delete('grinding','form_id',$form);
            $roller = $this->input->post('roller');
            $rol1=$this->input->post('rol1');
            $rol2=$this->input->post('rol2');
            $rol3=$this->input->post('rol3');
            $rol4=$this->input->post('rol4');
            $rol5=$this->input->post('rol5');
            $rol6=$this->input->post('rol6');
            $rol7=$this->input->post('rol7');
            $rol8=$this->input->post('rol8');
            $rol9=$this->input->post('rol9');
            $rol10=$this->input->post('rol10');
            $rol11=$this->input->post('rol11');
            $rol12=$this->input->post('rol12');
            $rol13=$this->input->post('rol13');
            $rol14=$this->input->post('rol14');
            $rol15=$this->input->post('rol15');
            $rol16=$this->input->post('rol16');
            $rol17=$this->input->post('rol17');
            $rol18=$this->input->post('rol18');
            $rol19=$this->input->post('rol19');
            $rol20=$this->input->post('rol20');
            for($i=0;$i<count($rol1);$i++){
                        $data = array("form_id"=>$form,
                        "date"=>date("Y-m-d"),
                        "no_roller"=>$roller[$i],
                        "meas_no"=>0,
                        "val1"=>$rol1[$i],"val2"=>$rol2[$i],"val3"=>$rol3[$i],"val4"=>$rol4[$i],"val5"=>$rol5[$i],
                        "val6"=>$rol6[$i],"val7"=>$rol7[$i],"val8"=>$rol8[$i],"val9"=>$rol9[$i],"val10"=>$rol10[$i],
                        "val11"=>$rol11[$i],"val12"=>$rol12[$i],"val13"=>$rol13[$i],"val14"=>$rol14[$i],"val15"=>$rol15[$i],
                        "val16"=>$rol16[$i],"val17"=>$rol17[$i],"val18"=>$rol18[$i],"val19"=>$rol19[$i],"val20"=>$rol20[$i]);
                        $this->form_manager_model->insert("roller",$data);
            }
            $grinding1=$this->input->post('grinding1');
            $grinding2=$this->input->post('grinding2');
            $grinding3=$this->input->post('grinding3');
            $no_urut = $this->input->post('no_urut');

            $a1=$this->input->post('1a');
            $a2=$this->input->post('2a');
            $a3=$this->input->post('3a');
            $a4=$this->input->post('4a');
            $a5=$this->input->post('5a');
            $a6=$this->input->post('6a');
            $a7=$this->input->post('7a');
            $a8=$this->input->post('8a');
            $a9=$this->input->post('9a');
            $a10=$this->input->post('10a');
            $a11=$this->input->post('11a');
            $a12=$this->input->post('12a');
            $a13=$this->input->post('13a');
            $a14=$this->input->post('14a');
            $a15=$this->input->post('15a');
            $a16=$this->input->post('16a');
            $a17=$this->input->post('17a');
            $a18=$this->input->post('18a');
            $a19=$this->input->post('19a');
            $a20=$this->input->post('20a');

            $b1=$this->input->post('1b');
            $b2=$this->input->post('2b');
            $b3=$this->input->post('3b');
            $b4=$this->input->post('4b');
            $b5=$this->input->post('5b');
            $b6=$this->input->post('6b');
            $b7=$this->input->post('7b');
            $b8=$this->input->post('8b');
            $b9=$this->input->post('9b');
            $b10=$this->input->post('10b');
            $b11=$this->input->post('11b');
            $b12=$this->input->post('12b');
            $b13=$this->input->post('13b');
            $b14=$this->input->post('14b');
            $b15=$this->input->post('15b');
            $b16=$this->input->post('16b');
            $b17=$this->input->post('17b');
            $b18=$this->input->post('18b');
            $b19=$this->input->post('19b');
            $b20=$this->input->post('20b');

            $c1=$this->input->post('1c');
            $c2=$this->input->post('2c');
            $c3=$this->input->post('3c');
            $c4=$this->input->post('4c');
            $c5=$this->input->post('5c');
            $c6=$this->input->post('6c');
            $c7=$this->input->post('7c');
            $c8=$this->input->post('8c');
            $c9=$this->input->post('9c');
            $c10=$this->input->post('10c');
            $c11=$this->input->post('11c');
            $c12=$this->input->post('12c');
            $c13=$this->input->post('13c');
            $c14=$this->input->post('14c');
            $c15=$this->input->post('15c');
            $c16=$this->input->post('16c');
            $c17=$this->input->post('17c');
            $c18=$this->input->post('18c');
            $c19=$this->input->post('19c');
            $c20=$this->input->post('20c');

            for($i=0;$i<count($c20);$i++){
                $data = array("form_id"=>$form,
                        "date"=>date("Y-m-d"),
                        "no_grinding"=>$grinding1,
                        "no_urut"=>$no_urut[$i],
                        "meas_no"=>0,
                        "val1"=>$a1[$i],"val2"=>$a2[$i],"val3"=>$a3[$i],"val4"=>$a4[$i],"val5"=>$a5[$i],
                        "val6"=>$a6[$i],"val7"=>$a7[$i],"val8"=>$a8[$i],"val9"=>$a9[$i],"val10"=>$a10[$i],
                        "val11"=>$a11[$i],"val12"=>$a12[$i],"val13"=>$a13[$i],"val14"=>$a14[$i],"val15"=>$a15[$i],
                        "val16"=>$a16[$i],"val17"=>$a17[$i],"val18"=>$a18[$i],"val19"=>$a19[$i],"val20"=>$a20[$i]);
                        $this->form_manager_model->insert("grinding",$data);
               $data2 = array("form_id"=>$form,
                        "date"=>date("Y-m-d"),
                        "no_urut"=>$no_urut[$i],
                        "no_grinding"=>$grinding2,
                        "meas_no"=>0,
                        "val1"=>$b1[$i],"val2"=>$b2[$i],"val3"=>$b3[$i],"val4"=>$b4[$i],"val5"=>$b5[$i],
                        "val6"=>$b6[$i],"val7"=>$b7[$i],"val8"=>$b8[$i],"val9"=>$b9[$i],"val10"=>$b10[$i],
                        "val11"=>$b11[$i],"val12"=>$b12[$i],"val13"=>$b13[$i],"val14"=>$b14[$i],"val15"=>$b15[$i],
                        "val16"=>$b16[$i],"val17"=>$b17[$i],"val18"=>$b18[$i],"val19"=>$b19[$i],"val20"=>$b20[$i]);
                        $this->form_manager_model->insert("grinding",$data2);
                $data3 = array("form_id"=>$form,
                        "date"=>date("Y-m-d"),
                        "no_grinding"=>$grinding3,
                        "no_urut"=>$no_urut[$i],
                        "meas_no"=>0,
                        "val1"=>$c1[$i],"val2"=>$c2[$i],"val3"=>$c3[$i],"val4"=>$c4[$i],"val5"=>$c5[$i],
                        "val6"=>$c6[$i],"val7"=>$c7[$i],"val8"=>$c8[$i],"val9"=>$c9[$i],"val10"=>$c10[$i],
                        "val11"=>$c11[$i],"val12"=>$c12[$i],"val13"=>$c13[$i],"val14"=>$c14[$i],"val15"=>$c15[$i],
                        "val16"=>$c16[$i],"val17"=>$c17[$i],"val18"=>$c18[$i],"val19"=>$c19[$i],"val20"=>$c20[$i]);
                        $this->form_manager_model->insert("grinding",$data3);
            }
                 $this->insert_log_activity("Measurement Form",$id,"Update Form Measurement Report Area '$area'");
                 redirect('engine/form_manager/wearing_listtable');
    }
        function print_form_running(){
            $this->load->model('form_manager_model');
            $id =  $this->uri->segment(4);
            $data['id'] = $id;
            $data['form'] = $this->form_manager_model->get_topform1xx($id,'form_running')->row();
            $type=$this->form_manager_model->get_topform1xx($id,'form_running')->row('mechanichal_type');
            $data['data_2ndform'] = $this->form_manager_model->get_topformrun_print($id,"rel_component_to_form_running")->result();
           // $data['data_3rdform'] = $this->form_manager_model->get_inspection($id,"rel_activity_inspection")->result();
            $this->insert_log_activity("Running Form",$id,"Print Form Running Report '$type'");
            $this->load->view('form_manager/running_print',$data);
        }
        
        function print_form_stop(){
            $this->load->model('form_manager_model');
            $id =  $this->uri->segment(4);
            $data['form'] = $this->form_manager_model->get_topform1($id,'form_stop')->row();
            $type= $this->form_manager_model->get_topform1($id,'form_stop')->row('type');
            //$data['data_2ndform'] = $this->form_manager_model->get_topform($id,"rel_component_to_form_running")->result();
           // $data['data_3rdform'] = $this->form_manager_model->get_inspection($id,"rel_activity_inspection")->result();
            $this->insert_log_activity("Stop Form",$id,"Print Form Stop Report '$type'");
            $this->load->view('form_manager/stop_print',$data);
        }
        function print_form_wearing(){
            $this->load->model('form_manager_model');
            $id =  $this->uri->segment(4);
            $data['form'] = $this->form_manager_model->get_topform1($id,'form_measuring')->row();
            $material = $this->form_manager_model->get_topform1($id,'form_measuring')->row('material');
            //$data['data_2ndform'] = $this->form_manager_model->get_topform($id,"rel_component_to_form_running")->result();
           // $data['data_3rdform'] = $this->form_manager_model->get_inspection($id,"rel_activity_inspection")->result();
            $this->insert_log_activity("Measurement Form",$id,"Print Form Measurement Report '$material'");
            $this->load->view('form_manager/wearing_print',$data);
        }
        function get_haxx(){
            $id = $this->input->post('id');
            $get_idhac = $this->form_manager_model->get_idhac($id);
            echo $get_idhac;
        }
        
        //////////////////////////////////////////////////////////////publish function////////////////////////////////////////////////////////
        
        function publish_form_running($id){
            $this->load->model('form_manager_model');
            
            $order=$this->db->query("select * from form_running where id = '$id'")->row();
            $jum=$order->publish_order + 1;
            $this->db->query("update form_running set publish_order='$jum' where id='$id'");
            
            $max_id_copy = $this->form_manager_model->get_max_id_copy("form_running_copy","form_running_id",$id);
            if($max_id_copy == "" || $max_id_copy == 0){
                $maxid = "1";
            }else{
                $maxid = $max_id_copy + 1;
            }
            $form_number =$this->form_manager_model->form_number('F/RUN-');
            $no_form=$form_number->form_no + 1;
            $form_code=$form_number->form_code.$no_form;
            $this->form_manager_model->insert_copy_table_form_running($id,$maxid,$form_code,$jum);
            $this->form_manager_model->insert_copy_table_rel_component_to_form_running($maxid,$id);
            $this->form_manager_model->insert_copy_table_rel_inspection_copy($maxid,$id);
            //redirect('form_manager/form_detailrunning1');
            $data=array('form_no'=>$no_form);
            $this->form_manager_model->update('auto_form_number',$data,"1");
            $this->insert_log_activity("Running Form",$id,"Publish Form Running Report '$form_code'");
            redirect("engine/form_manager/form_detailrunning1");
        }
        
        function publish_form_stop($id){
            $this->load->model('form_manager_model');
            
            $order=$this->db->query("select * from form_stop where id = '$id'")->row();
            $jum=$order->publish_order + 1;
            $this->db->query("update form_stop set publish_order='$jum' where id='$id'");
            
            $max_id_copy = $this->form_manager_model->get_max_id_copy("form_stop_copy","form_stop_id",$id);
            if($max_id_copy == "" || $max_id_copy == 0){
                $maxid = "1";
            }else{
                $maxid = $max_id_copy + 1;
            }
            $form_number =$this->form_manager_model->form_number('F/STOP-');
            $no_form=$form_number->form_no + 1;
            $form_code=$form_number->form_code.$no_form;
            $this->form_manager_model->insert_copy_table_form_stop($id,$maxid,$form_code,$jum);
            $this->form_manager_model->insert_copy_table_rel_component_to_form_stop($maxid,$id);
            $data=array('form_no'=>$no_form);
            $this->form_manager_model->update('auto_form_number',$data,"2");
            $this->insert_log_activity("Stop Form",$id,"Publish Form Stop Report '$form_code'");
            redirect("engine/form_manager/form_detailstop1");
            //$this->form_manager_model->insert_copy_table_rel_inspection_copy($maxid,$id);
        }
        
        function publish_form_wear($id){
            $this->load->model('form_manager_model');
            
            //update publish order
            $order=$this->db->query("select * from form_measuring where id = '$id'")->row();
            $jum=$order->publish_order + 1;
            $this->db->query("update form_measuring set publish_order='$jum' where id='$id'");
            
            $max_id_copy = $this->form_manager_model->get_max_id_copy("form_measuring_copy","form_measuring_id",$id);
            $max_meas_no = $this->form_manager_model->get_max_id_copy("form_measuring_copy","form_measuring_id",$id)+1;
            if($max_id_copy == "" || $max_id_copy == 0){
                $maxid = "1";
            }else{
                $maxid = $max_id_copy + 1;
            }
            $form_number =$this->form_manager_model->form_number('F/WEAR-');
            $no_form=$form_number->form_no + 1;
            $form_code=$form_number->form_code.$no_form;
            $this->form_manager_model->insert_copy_table_form_wear($id,$maxid,$form_code,$max_meas_no,$jum);
            //$this->form_manager_model->insert_copy_roller($maxid,$id,$max_meas_no);
            //$this->form_manager_model->insert_copy_grinding($maxid,$id,$max_meas_no);
            $data=array('form_no'=>$no_form);
            $this->form_manager_model->update('auto_form_number',$data,"3");
            
            $this->insert_log_activity("Wearing Form",$id,"Publish Form Wearing Report '$form_code'");
            redirect("engine/form_manager/wearing_listtable");
        }
        
        function delete_running($id){
            $this->db->query("delete from form_running where id='$id'");
            $this->db->query("delete from rel_component_to_form_running where form_id='$id'");
            $this->db->query("delete from rel_activity_inspection where form_id='$id'");
            $this->insert_log_activity("Running Form",$id,"Delete Form Running Report");
            redirect('engine/form_manager/form_detailrunning1');
        }
        function delete_stop($id){
            $this->db->query("delete from form_stop where id='$id'");
            $this->db->query("delete from rel_component_to_form_stop where form_id='$id'");
            $this->insert_log_activity("Stop Form",$id,"Delete Form Stop Report");
            $this->form_detailstop1();
        }
        function delete_wear($id){
            $this->db->query("delete from form_measuring where id='$id'");
            $this->db->query("delete from roller where form_id='$id'");
            $this->db->query("delete from grinding where form_id='$id'");
            $this->insert_log_activity("Wearing Form",$id,"Delete Form Wearing Report");
            $this->wearing_listtable();
        }
        
        
}