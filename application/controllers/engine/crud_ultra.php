<?php

class Crud_ultra extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');	
	}
	

	 
public function index() {
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
		$crud->set_table('record_ultrasonic_test');
		$crud->set_subject('Ultrasonic');
		$crud->required_fields('record_id');
	 
		$crud->columns('hac', 'model', 'couplant', 'probe_type', 'frequency');
		$crud->fields('hac', 'model', 'couplant', 'probe_type', 'frequency', 'p_zero', 'thickness', 'gain', 'vel', 'range', 'sa', 'ra', 'da', 'upload_file', 'inspector_id', 'engineer_id', 'record_id', 'date' );
		$crud->callback_after_insert(array($this, 'ultra_after_insert'));
        
         $crud->callback_after_update(array($this, 'log_user_after_update'));
        
        $crud->callback_after_delete(array($this, 'log_user_after_delete'));
        
        
		$crud->unset_export();
		$output = $crud->render();
	 
		$this->load->view('page_crud.php',$output); 
}
 
function ultra_after_insert($post_array,$primary_key){
 
		$data_insert_record = array('inspection_type' => 'UT','inspection_id' => $primary_key, 'datetime' => date('Y-m-d H:i:s'));

		$this->db->insert('record',$data_insert_record);

		$record_id = mysql_insert_id();

		$data_update_record_ut = array('record_id' => $record_id);
		$where = array('id' => $primary_key);

		$this->db->update('record_ultrasonic_test',$data_update_record_ut,$where);

	return true;

	}
    
    function log_user_after_update($post_array,$primary_key)
    {
        $id = $this->session->userdata('users_id');
        $user_logs_insert = array(
            "user_id" => $id,
            "date_activity" => date('Y-m-d H:i:s'),
            "description" => 'UPDATE',
            "type" => 'UT',
            "record_id" => $primary_key
        );
     
        $this->db->insert('users_activity',$user_logs_insert);
     
        return true;
    }
    
     function log_user_after_delete($post_array,$primary_key)
    {
        $id = $this->session->userdata('users_id');
        $user_logs_delete = array(
            "user_id" => $id,
            "date_activity" => date('Y-m-d H:i:s'),
            "description" => 'DELETE',
            "type" => 'UT',
            "record_id" => $primary_key
        );
     
        $this->db->insert('users_activity',$user_logs_delete);
     
        return true;
    }	
}
 