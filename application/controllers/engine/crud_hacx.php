<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_hacx extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	    $this->load->helper('url');
        
		$this->load->library('access');

		$this->load->library('grocery_crud');
        
        $this->load->library("phpexcel/PHPExcel");
		$this->load->library("phpexcel/PHPExcel/IOFactory");
	}

	public function _example_output($output = null)
	{
		$this->load->view('crud/page_crud',$output);
	}

	function index()
	{
	$this->access->check_access();
		$this->hac();
	}

	function view_list()
	{
		
//		$perpage = 20;	
//		
//		$data['total_data_hac'] = $this->main_model->get_list('hac');
//		
//		$config = array(
//					'base_url' => base_url().'engine/crud_hac/view_list',
//					'per_page' => $perpage,
//					'total_rows' => $data['total_data_hac']->num_rows,
//					'uri_segment' => 4,
//					'next_tag_open' => '<span class="btn">',
//					'next_tag_close' => '</span>',
//					'prev_tag_open' => '<span class="btn">',
//					'prev_tag_close' => '</span>',
//					'last_tag_open' => '<span class="btn">',
//					'last_tag_close' => '</span>',
//					'first_tag_open' => '<span class="btn">',
//					'first_tag_close' => '</span>',
//					'cur_tag_open' => '<span class="btn btn-inverse">',
//					'cur_tag_close' => '</span>',
//					'num_tag_open' => '<span class="btn">',
//					'num_tag_close' => '</span>'
//					);
//		
//		$this->load->library('pagination',$config);
//		// $this->pagination->initialize($config);	
//		
//		$data['data_hac'] = $this->main_model->get_list('hac',array('perpage' => $perpage, 'offset' => $offset),array('by' => 'id' , 'sorting' => $offset));
//		
//		$data['q'] = null;
                $search = $this->input->post('q');
		$config['base_url'] = base_url().'engine/crud_hac/view_list/';
                $config['total_rows'] = $this->db->query("select * from hac where hac_code like '%$search%'")->num_rows();
                $config['per_page'] = 20;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
                $data['q'] = $search;
		$data['data_hac'] = $this->db->query("select * from hac where hac_code like '%$search%' order by id asc limit ".$pg.",".$config['per_page']."")->result();
		$this->load->view('crud/page_crud_hac',$data);
	}
	
	function search_hac()
	{
		$data_search = $this->input->post('q');
		
		redirect('engine/crud_hac/search_hac_result/'.$data_search);
	}
	
	function search_hac_result($query,$offset = 0)
	{
		$perpage = 20;	
		
		// Total hac by search
		$this->db->like('hac_code',$query);
		$this->db->or_like('description',$query);
		$data['total_data_hac'] = $this->db->get('hac');
		
		
		$config = array(
					'base_url' => base_url().'engine/crud_hac/search_hac_result/'.$query,
					'per_page' => $perpage,
					'total_rows' => $data['total_data_hac']->num_rows,
					'uri_segment' => 5,
					'next_tag_open' => '<span class="btn">',
					'next_tag_close' => '</span>',
					'prev_tag_open' => '<span class="btn">',
					'prev_tag_close' => '</span>',
					'last_tag_open' => '<span class="btn">',
					'last_tag_close' => '</span>',
					'first_tag_open' => '<span class="btn">',
					'first_tag_close' => '</span>',
					'cur_tag_open' => '<span class="btn btn-inverse">',
					'cur_tag_close' => '</span>',
					'num_tag_open' => '<span class="btn">',
					'num_tag_close' => '</span>'
					);
		
		$this->load->library('pagination',$config);	
		
		$this->db->like('hac_code',$query);
		$this->db->or_like('description',$query);
		$this->db->limit($perpage ,$offset);
		$this->db->order_by('id','ASC');
		$data['data_hac'] =  $this->db->get('hac');
		
		$data['q'] = $query;
		
		$this->load->view('crud/page_crud_hac',$data);
	}

	function get_assembly($hac_id)
	{
		$data_assembly = $this->main_model->get_list_where('hac_assembly',array('assembly_hac_id' => $hac_id));
		 
		echo "<table width='100%' cellpadding='3' border='1' cellspacing='0'><tbody>";
		foreach($data_assembly->result() as $assemblies):
			
			echo "
			<tr bgcolor='#ffb385' class='clickable'>
					<td width='20'><a href='#' class='expand_assembly' rel='".$assemblies->id."'><i class='icon-chevron-right'></i></a></td>
					<td width='200'>".$assemblies->assembly_code."</td>
					<td>".$assemblies->assembly_name."</td>
			</tr>
			<tr class='component_area clickable' id='component_area_".$assemblies->id."'>
				<td></td>
						<td colspan='2' id='content_component_area_".$assemblies->id."'>
							Loading...
						</td>
			</tr>
			
			";

		endforeach; 
		
		echo "</tbody></table>
		<script type='text/javascript' src='".base_url()."application/views/assets/js/jquery-1.9.0.min.js'></script>
			<script>
				$('.expand_assembly').click(function(){
			
					var assembly_id = $(this).attr('rel');
					
					$('#component_area_'+assembly_id).show(); 
					
				
					$.ajax({
						type:'GET',
						url:'".base_url()."engine/crud_hac/get_component/'+assembly_id,
						}).done(function(data){
							$('#content_component_area_'+assembly_id).html(data);
						});
					
				});
                
                $('.clickable').click(function() {

                        $(this).next().toggle();
                        
                    });
				</script>
		";
	}
	
	function get_component($assembly_id)
	{
		$data_component = $this->main_model->get_list_where('hac_component',array('assembly_id' => $assembly_id));
		 
		echo "<table width='100%' cellpadding='3' border='1' cellspacing='0'>";
		foreach($data_component->result() as $components):
			
			echo "
			<tr bgcolor='#85adff'>
					<td width='20'><i class='icon-chevron-right'></i></td>
					<td width='200'>".$components->component_code."</td>
					<td>".strip_tags($components->component_name)."</td>
			</tr>
			
			
			
			";

		endforeach; 
		
		echo "</table>
		<script type='text/javascript' src='".base_url()."application/views/assets/js/jquery-1.9.0.min.js'></script>
			<script>
			     $('.clickable').click(function() {

                        $(this).next().toggle();
                        
                    });
				</script>
		";
	}

	public function hac()
	{
		$crud = new grocery_crud();
		$crud->set_theme('datatables');
		$crud->set_table('hac');
		$crud->set_subject('Hac');
		$crud->columns('hac_code', 'description');
		$crud->set_field_upload('image','media/images');
		$crud->unset_export();
		$crud->unset_print();
		$crud->add_action('Assembly', '', 'engine/crud_hac/assembly','ui-icon-search');

		$output = $crud->render();

		$this->load->view('crud/page_crud2',$output);
	}

	public function assembly($hac_id = null)
	{
		$crud = new grocery_crud();

		$crud->set_theme('datatables');
		$crud->set_table('hac_assembly');

		$crud->set_subject('Assembly');
		$crud->unset_export();
		$crud->unset_print();

		if($hac_id != null)
		{
			$crud->where('assembly_hac_id',$hac_id);
			
			$data['hac'] = $this->main_model->get_detail('hac',array('id' => $hac_id));
		}	

		$crud->columns('assembly_code','assembly_name','image');
		$crud->fields('assembly_code','assembly_hac_id','assembly_name','image');
		
		$crud->add_action('Components', '', 'engine/crud_hac/component','ui-icon-search');
		
		$crud->change_field_type('assembly_hac_id','hidden',$hac_id);
		
		$crud->set_field_upload('image','media/images');
		
		$data['send_output'] = $crud->render();
		
		$this->load->view('crud/page_crud_assembly',$data);	

	}

	public function component($assembly_id = 1)
	{
		$crud = new grocery_crud();
		$crud->set_theme('datatables');
		$crud->set_table('hac_component');
		$crud->set_subject('Component');
		$crud->unset_export();
		$crud->unset_print();
		
		if($assembly_id != null)
		{
			$crud->where('assembly_id',$assembly_id);
			
			$data['assembly'] = $this->main_model->get_detail('hac_assembly',array('id' => $assembly_id));
			$data['hac'] = $this->main_model->get_detail('hac',array('id' => $data['assembly']['assembly_hac_id']));
		}	
		
		$crud->columns('component_code','component_name','stock','unit','image');
		$crud->fields('assembly_id','component_code','component_name','stock','unit','image');
		
		$crud->change_field_type('assembly_id','hidden',$assembly_id);
		
		$crud->set_field_upload('image','media/images');

		$data['send_output'] = $crud->render();
		
		$this->load->view('crud/page_crud_component',$data);	
	}
    
    
    
    function export_xl_hac(){
		$this->load->library("phpexcel/PHPExcel");
		$this->load->library("phpexcel/PHPExcel/IOFactory");
                
        $data['default'] = $this->db->query('select * from hac')->result_array();
		$excel = new PHPExcel();
		
		$excel->setActiveSheetIndex(0);
		$page = $excel->getActiveSheet();
		$page->setTitle("Test");
		
		$header_style = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			"font" => array(
				"bold" => true
			)
		);
		
		$body_style_huruf = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$italic_center = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			"font" => array(
				"italic" => true,
				"bold" => false
			)
		);
		
		$center = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$bordered = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		
		$page->getColumnDimension("A")->setWidth(15);
		$page->getColumnDimension("B")->setWidth(15);
		$page->getColumnDimension("C")->setWidth(15);
		$page->getColumnDimension("D")->setWidth(22);
		$page->getColumnDimension("E")->setWidth(22);
		$page->getColumnDimension("F")->setWidth(22);
		$page->getColumnDimension("G")->setWidth(22);
		$page->getColumnDimension("H")->setWidth(22);
		
		//$page->setCellValue("A1", "PT. Holcim Indonesia");
		//$page->mergeCells("A1:H1");
		
		$page->setCellValue("A1","ID");
		$page->mergeCells("A1:A1");
		
		$page->setCellValue("B1","HAC ID");
		$page->mergeCells("B1:B1");
		
		$page->setCellValue("C1","HAC CODE");
		$page->mergeCells("C1:C1");
		
		$page->setCellValue("D1","AREA ID");
		$page->mergeCells("D1:D1");
		
		$page->setCellValue("E1","EQUIPMENT");
		$page->mergeCells("E1:E1");
		
		$page->setCellValue("F1","DESCRIPTION");
		$page->mergeCells("F1:F1");
		
		$page->setCellValue("G1","FUNC LOC");
		$page->mergeCells("G1:G1");
		
		$page->setCellValue("H1","DESCRIPTION LOC");
		$page->mergeCells("H1:H1");
		
		$page->setCellValue("I1","INDICATOR");
		$page->mergeCells("I1:I1");
		
		$page->setCellValue("J1","OBJECT TYPE");
		$page->mergeCells("J1:J1");
        
        
		$page->setCellValue("K1","MAKER TYPE");
		$page->mergeCells("K1:K1");
        
        
		$page->setCellValue("L1","PLANNING PLANT");
		$page->mergeCells("L1:L1");
        
        
		$page->setCellValue("M1","IMAGE");
		$page->mergeCells("M1:M1");
		
		//$page->getStyle("A1:M1")->applyFromArray($header_style);
		
		
		$pos = 2;
		for($i=0;$i<count($data['default']);$i++){
			$page->setCellValue("A".($i+2), $data['default'][$i]['id']);
			$page->setCellValue("B".($i+2), $data['default'][$i]['hac_id']);
			$page->setCellValue("C".($i+2), $data['default'][$i]['hac_code']);
			$page->setCellValue("D".($i+2), $data['default'][$i]['area_id']);
			$page->setCellValue("E".($i+2), $data['default'][$i]['equipment']);
			$page->setCellValue("F".($i+2), $data['default'][$i]['description']);
			$page->setCellValue("G".($i+2), $data['default'][$i]['func_loc']);
			$page->setCellValue("H".($i+2), $data['default'][$i]['description_loc']);
			$page->setCellValue("I".($i+2), $data['default'][$i]['indicator']);
			$page->setCellValue("J".($i+2), $data['default'][$i]['object_type']);
            $page->setCellValue("K".($i+2), $data['default'][$i]['maker_type']);
			$page->setCellValue("L".($i+2), $data['default'][$i]['planning_plant']);
            $page->setCellValue("M".($i+2), $data['default'][$i]['image']);
			$pos++;
		}
		
		
		
		$page->getStyle("A1:M".($pos-1))->applyFromArray($bordered);
		
        $date_export = date('Y-m-d H:i:s');
        
		$objWriter = IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save("data_export_hac.xls");
        
        redirect ("./data_export_hac.xls");
	}
    
    
    
    
     function export_xl_assembly(){
        $this->load->library("phpexcel/PHPExcel");
		$this->load->library("phpexcel/PHPExcel/IOFactory");
               
        $data['default'] = $this->db->query('select * from hac_assembly')->result_array();
		$excel = new PHPExcel();
		
		$excel->setActiveSheetIndex(0);
		$page = $excel->getActiveSheet();
		$page->setTitle("Test");
		
		$header_style = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			"font" => array(
				"bold" => true
			)
		);
		
		$body_style_huruf = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$italic_center = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			"font" => array(
				"italic" => true,
				"bold" => false
			)
		);
		
		$center = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$bordered = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		
		$page->getColumnDimension("A")->setWidth(15);
		$page->getColumnDimension("B")->setWidth(15);
		$page->getColumnDimension("C")->setWidth(15);
		$page->getColumnDimension("D")->setWidth(22);
		$page->getColumnDimension("E")->setWidth(22);
		$page->getColumnDimension("F")->setWidth(22);
		
		//$page->setCellValue("A1", "PT. Holcim Indonesia");
		//$page->mergeCells("A1:H1");
		
		$page->setCellValue("A1","ID");
		$page->mergeCells("A1:A1");
		
		$page->setCellValue("B1","ASSEMBLY ID");
		$page->mergeCells("B1:B1");
		
		$page->setCellValue("C1","ASSEMBLY CODE");
		$page->mergeCells("C1:C1");
		
		$page->setCellValue("D1","ASSEMBLY NAME");
		$page->mergeCells("D1:D1");
		
		$page->setCellValue("E1","ASSEMBLY HAC ID");
		$page->mergeCells("E1:E1");
		
		$page->setCellValue("F1","IMAGE");
		$page->mergeCells("F1:F1");
		
		
		//$page->getStyle("A1:M1")->applyFromArray($header_style);
		
		
		$pos = 2;
		for($i=0;$i<count($data['default']);$i++){
			$page->setCellValue("A".($i+2), $data['default'][$i]['id']);
			$page->setCellValue("B".($i+2), $data['default'][$i]['assembly_id']);
			$page->setCellValue("C".($i+2), $data['default'][$i]['assembly_code']);
			$page->setCellValue("D".($i+2), $data['default'][$i]['assembly_name']);
			$page->setCellValue("E".($i+2), $data['default'][$i]['assembly_hac_id']);
            $page->setCellValue("F".($i+2), $data['default'][$i]['image']);
			$pos++;
		}
		
		$page->getStyle("A1:F".($pos-1))->applyFromArray($bordered);
		
        $date_export = date('Y-m-d H:i:s');
        
		$objWriter = IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save("data_export_assembly.xls");
        
        
        redirect ("./data_export_assembly.xls");
	}
    
    
    
    function export_xl_material(){
        $this->load->library("phpexcel/PHPExcel");
		$this->load->library("phpexcel/PHPExcel/IOFactory");
                
        $data['default'] = $this->db->query('select * from hac_component')->result_array();
		$excel = new PHPExcel();
		
		$excel->setActiveSheetIndex(0);
		$page = $excel->getActiveSheet();
		$page->setTitle("Test");
		
		$header_style = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			"font" => array(
				"bold" => true
			)
		);
		
		$body_style_huruf = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$italic_center = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			),
			"font" => array(
				"italic" => true,
				"bold" => false
			)
		);
		
		$center = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			),
			"alignment" => array(
				"horizontal" => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				"vertical" => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		);
		
		$bordered = array(
			"borders" => array(
				"allborders" => array(
					"style" => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		
		$page->getColumnDimension("A")->setWidth(15);
		$page->getColumnDimension("B")->setWidth(15);
		$page->getColumnDimension("C")->setWidth(15);
		$page->getColumnDimension("D")->setWidth(22);
		$page->getColumnDimension("E")->setWidth(22);
		$page->getColumnDimension("F")->setWidth(22);
        $page->getColumnDimension("G")->setWidth(22);
        $page->getColumnDimension("H")->setWidth(22);
		
		//$page->setCellValue("A1", "PT. Holcim Indonesia");
		//$page->mergeCells("A1:H1");
		
		$page->setCellValue("A1","ID");
		$page->mergeCells("A1:A1");
		
		$page->setCellValue("B1","MATERIAL ID");
		$page->mergeCells("B1:B1");
		
		$page->setCellValue("C1","MATERIAL CODE");
		$page->mergeCells("C1:C1");
		
		$page->setCellValue("D1","MATERIAL NAME");
		$page->mergeCells("D1:D1");
		
		$page->setCellValue("E1","ASSEMBLY ID");
		$page->mergeCells("E1:E1");
        
        $page->setCellValue("F1","STOCK");
		$page->mergeCells("F1:F1");
        
         $page->setCellValue("G1","UNIT");
		$page->mergeCells("G1:G1");
		
		$page->setCellValue("H1","IMAGE");
		$page->mergeCells("H1:H1");
		
		
		//$page->getStyle("A1:M1")->applyFromArray($header_style);
		
		
		$pos = 2;
		for($i=0;$i<count($data['default']);$i++){
			$page->setCellValue("A".($i+2), $data['default'][$i]['id']);
			$page->setCellValue("B".($i+2), $data['default'][$i]['component_id']);
			$page->setCellValue("C".($i+2), $data['default'][$i]['component_code']);
			$page->setCellValue("D".($i+2), $data['default'][$i]['component_name']);
			$page->setCellValue("E".($i+2), $data['default'][$i]['assembly_id']);
            $page->setCellValue("F".($i+2), $data['default'][$i]['stock']);
            $page->setCellValue("G".($i+2), $data['default'][$i]['unit']);
            $page->setCellValue("H".($i+2), $data['default'][$i]['image']);
			$pos++;
		}
		
		$page->getStyle("A1:H".($pos-1))->applyFromArray($bordered);
		
        $date_export = date('Y-m-d H:i:s');
        
		$objWriter = IOFactory::createWriter($excel, 'Excel5');
		$objWriter->save("data_export_material.xls");
        
        
        redirect ("./data_export_material.xls");
	}
    
    
    function view_import_hac(){
       
        $this->db->empty_table('hac');
     
     $this->load->view("crud/import_hac");
       
	}
    
     function import_hac(){
       
	   
       if ($this->input->post('save')) {
            
			$fileName = $_FILES['import']['name'];
 
            $config['upload_path'] = './media/file/';
            $config['file_name'] = $fileName;
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']        = 10000;
 
            $this->load->library('upload');
            $this->upload->initialize($config);
 
            if(! $this->upload->do_upload('import') )
                $this->upload->display_errors();
 
            $media = $this->upload->data('import');
            $inputFileName = './media/file/'.$media['file_name'];
 
            //  Read your Excel workbook
            try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
 
            //  Loop through each row of the worksheet in turn
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                //  Insert row data array into your database of choice here
                $data = array(
                            "id"=> $rowData[0][0],
                            "hac_id"=> $rowData[0][1],
                            "hac_code"=> $rowData[0][2],
                            "area_id"=> $rowData[0][3],
                            "equipment"=> $rowData[0][4],
                            "description"=> $rowData[0][5],
                            "func_loc"=> $rowData[0][6],
                            "description_loc"=> $rowData[0][7],
                            "indicator"=> $rowData[0][8],
                            "object_type"=> $rowData[0][9],
                            "maker_type"=> $rowData[0][10],
                            "planning_plant"=> $rowData[0][11],
                            "image"=> $rowData[0][12]
                            
                        );
 
                $this->db->insert("hac",$data);
            } 
         }
       
       
        redirect("engine/crud_hac/view_list");
	}
    
    
    function view_import_assembly(){
	    
        $this->db->empty_table('hac_assembly');
        
     $this->load->view("crud/import_assembly");
       
	}
    
    
    function import_assembly(){
       
	   
       if ($this->input->post('save')) {
            
			$fileName = $_FILES['import']['name'];
 
            $config['upload_path'] = './media/file/';
            $config['file_name'] = $fileName;
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']        = 10000;
 
            $this->load->library('upload');
            $this->upload->initialize($config);
 
            if(! $this->upload->do_upload('import') )
                $this->upload->display_errors();
 
            $media = $this->upload->data('import');
            $inputFileName = './media/file/'.$media['file_name'];
 
            //  Read your Excel workbook
            try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
 
            //  Loop through each row of the worksheet in turn
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                //  Insert row data array into your database of choice here
                $data = array(
                            "id"=> $rowData[0][0],
                            "assembly_id"=> $rowData[0][1],
                            "assembly_code"=> $rowData[0][2],
                            "assembly_name"=> $rowData[0][3],
                            "assembly_hac_id"=> $rowData[0][4],
                            "image"=> $rowData[0][5]
                            
                        );
 
                $this->db->insert("hac_assembly",$data);
            } 
         }
       
       
        redirect("engine/crud_hac/view_list");
	}
    
    
    function view_import_material(){
	   
       $this->db->empty_table('hac_component');
       
     $this->load->view("crud/import_material");
       
	}
    
    
    function import_material(){
       
	   
       if ($this->input->post('save')) {
            
			$fileName = $_FILES['import']['name'];
 
            $config['upload_path'] = './media/file/';
            $config['file_name'] = $fileName;
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']        = 10000;
 
            $this->load->library('upload');
            $this->upload->initialize($config);
 
            if(! $this->upload->do_upload('import') )
                $this->upload->display_errors();
 
            $media = $this->upload->data('import');
            $inputFileName = './media/file/'.$media['file_name'];
 
            //  Read your Excel workbook
            try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
 
            //  Loop through each row of the worksheet in turn
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                //  Insert row data array into your database of choice here
                $data = array(
                            "id"=> $rowData[0][0],
                            "component_id"=> $rowData[0][1],
                            "component_code"=> $rowData[0][2],
                            "component_name"=> $rowData[0][3],
                            "assembly_id"=> $rowData[0][4],
                            "stock"=> $rowData[0][5],
                            "unit"=> $rowData[0][6],
                            "image"=> $rowData[0][7]
                            
                        );
 
                $this->db->insert("hac_component",$data);
            } 
         }
       
       
        redirect("engine/crud_hac/view_list");
	}
    
    
    
	

}