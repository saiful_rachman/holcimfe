<?php

class Inspection_manager extends CI_Controller{
    function __construct(){
        parent ::__construct();
            $this->load->library('grocery_crud');
            //$this->load->model('inspection_model');
            $this->load->model('inspection_model');
            $this->load->model('form_manager_model');
            $this->load->model('main_model');
    }
    
    function severity_level_running($id,$val){
        $axx=$this->db->query("select * from hac where id='$id'")->row();
            //print_r($axx);
            $ax=  explode(",",$axx->severity_level);
            $running=$val;
            $stop=$ax[1];
            $vibra=$ax[2];
            $lubricant=$ax[3];
            $oil=$ax[4];
            $ultrasonic=$ax[5];
            $penetrant=$ax[6];
            $thick_gen=$ax[7];
            $thick_stack=$ax[8];
            $thick_kiln=$ax[9];
            $thermo=$ax[10];
            $mca=$ax[11];
            $mcsa=$ax[12];
            $inspection=$ax[13];
            $wear=$ax[14];
            $other=$ax[15];
            $jadi=$running.",".$stop.",".$vibra.",".$lubricant.",".$oil.",".$ultrasonic.",".$penetrant.",".$thick_gen.",".$thick_stack.",".$thick_kiln.",".$thermo.",".$mca.",".$mcsa.",".$inspection.",".$wear.",".$other;
            $this->db->query("update hac set severity_level='$jadi' where id='$id'");
    }
    
    function severity_level_stop($id,$val){
        $axx=$this->db->query("select * from hac where id='$id'")->row();
            //print_r($axx);
            $ax=  explode(",",$axx->severity_level);
            $running=$ax[0];
            $stop=$val;
            $vibra=$ax[2];
            $lubricant=$ax[3];
            $oil=$ax[4];
            $ultrasonic=$ax[5];
            $penetrant=$ax[6];
            $thick_gen=$ax[7];
            $thick_stack=$ax[8];
            $thick_kiln=$ax[9];
            $thermo=$ax[10];
            $mca=$ax[11];
            $mcsa=$ax[12];
            $inspection=$ax[13];
            $wear=$ax[14];
            $other=$ax[15];
            $jadi=$running.",".$stop.",".$vibra.",".$lubricant.",".$oil.",".$ultrasonic.",".$penetrant.",".$thick_gen.",".$thick_stack.",".$thick_kiln.",".$thermo.",".$mca.",".$mcsa.",".$inspection.",".$wear.",".$other;
            $this->db->query("update hac set severity_level='$jadi' where id='$id'");
    }
    
    function severity_level_wearing($id,$val){
        $axx=$this->db->query("select * from hac where id='$id'")->row();
            //print_r($axx);
            $ax=  explode(",",$axx->severity_level);
            $running=$ax[0];
            $stop=$ax[1];
            $vibra=$ax[2];
            $lubricant=$ax[3];
            $oil=$ax[4];
            $ultrasonic=$ax[5];
            $penetrant=$ax[6];
            $thick_gen=$ax[7];
            $thick_stack=$ax[8];
            $thick_kiln=$ax[9];
            $thermo=$ax[10];
            $mca=$ax[11];
            $mcsa=$ax[12];
            $inspection=$ax[13];
            $wear=$val;
            $other=$ax[15];
            $jadi=$running.",".$stop.",".$vibra.",".$lubricant.",".$oil.",".$ultrasonic.",".$penetrant.",".$thick_gen.",".$thick_stack.",".$thick_kiln.",".$thermo.",".$mca.",".$mcsa.",".$inspection.",".$wear.",".$other;
            $this->db->query("update hac set severity_level='$jadi' where id='$id'");
    }
    
    function insert_log_activity($type,$primarykey,$description){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
    function index(){
        
    }
    function list_running_inspection(){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="a.id";
        }else{
            $field=$fieldx;
        }
        $config['base_url'] = base_url().'engine/inspection_manager/list_running_inspection/';
        $config['total_rows'] = $this->db->query("select a.*,b.area_name,c.frequency,d.periode from form_running_copy a
                                                left join area b on a.area=b.id
                                                left join master_frequency c on a.frequency=c.id
                                                left join master_periode d on a.periode=d.id
                                                where $field LIKE '%$val%' and a.status_proses='0' and a.publish LIKE '%unpublish%' OR a.publish LIKE '%reject%'
                                                order by a.id desc")->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 4;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['data'] = $this->db->query("select a.*,b.area_name,c.frequency,d.periode from form_running_copy a
                                         left join area b on a.area=b.id
                                         left join master_frequency c on a.frequency=c.id
                                         left join master_periode d on a.periode=d.id
                                         where $field LIKE '%$val%' and a.status_proses='0' and a.publish LIKE '%unpublish%' OR a.publish LIKE '%reject%'
                                         order by a.id desc limit ".$pg.",".$config['per_page']."")->result();
//        $sql=$this->db->query("select a.*,b.area_name,c.frequency,d.periode from form_running_copy a
//                               left join area b on a.area=b.id
//                               left join master_frequency c on a.frequency=c.id
//                               left join master_periode d on a.periode=d.id
//                               where a.status_proses='0' and a.publish LIKE '%unpublish%' OR a.publish LIKE '%reject%' order by id desc
//                              ");
//        $data['list']=$sql->result();
        $this->load->view('inspection_form/list_running',$data);    
    }
    
    function list_stop_inspection(){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="a.id";
        }else{
            $field=$fieldx;
        }
        $config['base_url'] = base_url().'engine/inspection_manager/list_stop_inspection/';
        $config['total_rows'] = $this->db->query("select a.*,b.area_name,c.frequency from form_stop_copy a
                                 left join area b on a.area=b.id
                                 left join master_frequency c on a.frequency=c.id
                                 where $field LIKE '%$val%' and a.status_proses='0' and a.publish LIKE '%unpublish%' OR a.publish LIKE '%reject%'
                                 order by a.id desc")->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 4;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['data'] = $this->db->query("select a.*,b.area_name,c.frequency from form_stop_copy a
                                 left join area b on a.area=b.id
                                 left join master_frequency c on a.frequency=c.id
                                 where $field LIKE '%$val%' and a.status_proses='0' and a.publish LIKE '%unpublish%' OR a.publish LIKE '%reject%'
                                 order by a.id desc limit ".$pg.",".$config['per_page']."")->result();
//        $sql=$this->db->query("select a.*,b.area_name,c.frequency from form_stop_copy a
//                               left join area b on a.area=b.id
//                               left join master_frequency c on a.frequency=c.id
//                               where a.status_proses='0' and a.publish LIKE '%unpublish%' OR a.publish LIKE '%reject%' order by a.id desc
//                              ");
//        $data['list']=$sql->result();
        $this->load->view('inspection_form/list_stop',$data);    
    }
    
    function list_wearing_inspection(){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="a.id";
        }else{
            $field=$fieldx;
        }
        $config['base_url'] = base_url().'engine/inspection_manager/list_wearing_inspection/';
        $config['total_rows'] = $this->db->query("select a.*,b.area_name,c.hac_code from form_measuring_copy a
                                                left join area b on a.area=b.id
                                                left join hac c on a.hac=c.id
                                                where $field LIKE '%$val%' and a.status_proses='0' and a.publish LIKE '%unpublish%' OR a.publish LIKE '%reject%'
                                                order by a.id desc")->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 4;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['data'] = $this->db->query("select a.*,b.area_name,c.hac_code from form_measuring_copy a
                                        left join area b on a.area=b.id
                                        left join hac c on a.hac=c.id
                                        where $field LIKE '%$val%' and a.status_proses='0' and a.publish LIKE '%unpublish%' OR a.publish LIKE '%reject%'
                                        order by a.id desc limit ".$pg.",".$config['per_page']."")->result();
//         $sql=$this->db->query("select a.*,b.area_name,c.hac_code from form_measuring_copy a
//                               left join area b on a.area=b.id
//                               left join hac c on a.hac=c.id
//                               where a.status_proses='0' and a.publish LIKE '%unpublish%' OR a.publish LIKE '%reject%'
//                              ");
//        $data['list']=$sql->result();
        $this->load->view('inspection_form/list_wear',$data);    
    }
    
    function update_running_activity($id){
     $data['form1']= $this->form_manager_model->get_topform1x($id,'form_running_copy')->row();
     $data['form2']= $this->form_manager_model->get_topformrun($id,'rel_component_to_form_running_copy')->result();
     $this->load->view('inspection_form/running_inspection',$data);
    }
    function save_running_activity(){
        $form_number=$this->input->post('form_number');
        $form_id=$this->input->post('form_id');
        $publish_status = $this->input->post('status_publish');
        $hacxx=$this->input->post('hacxx');
        $form_id1=$this->input->post('form_id1');
        $rel_component=$this->input->post('rel_component');
        $hac=$this->input->post('hac');
        $component=$this->input->post('component');
        $vib=$this->input->post('vib');
        
        $ddate = date('Y-m-d', strtotime($this->input->post('ddate')) + 7 * 60 * 60);
        
        //$de = $this->input->post('de');
        //$nde=$this->input->post('nde');
        $remarks = $this->input->post('remarks');
        $recomendation=$this->input->post('recomendation');
        $actual_value=$this->input->post('actual_value');
        $status=$this->input->post('status');
        $comment=$this->input->post('comment');
        $inspection_activity=$this->input->post('inspection_activity');
        $target_value=$this->input->post('target_value');
        $severity = $this->input->post('severity');
        $dx=array(
            'status_proses'=>'1',
            'inspection_type'=>"RUN",
            'publish'=>"unpublish",
            'datetime'=>$ddate,
            'inspection_date'=>$ddate,
            'remarks'=>$remarks,
            'recomendation'=>$recomendation,
            'user'=>$this->session->userdata('users_id'),
            'publish_by'=>"-"
        );
        $this->inspection_model->update('form_running_copy',$form_id1,'id',$dx);
        if($publish_status == "reject"){
            $idt=$this->input->post('idt');
            $this->db->query("delete from record where inspection_id='$idt' and inspection_type='RUN'");
            for($i=0;$i<count($severity);$i++){
            $data_to_record=array(
                'hac'=>$hac[$i],
                'inspection_id'=>$form_id[$i],
                'inspection_type'=>'RUN',
                'datetime'=>$ddate,
                'remarks'=>$remarks,
                'recomendation'=>$recomendation,
                'severity_level'=>$severity[$i],
                'status'=>'unpublish',
                'user'=>$this->session->userdata('users_id'),
                'publish_by'=>"-"
            );
        $this->inspection_model->insert('record',$data_to_record);
        }
        $acc=$this->db->query("select hac,max(severity_level) as severity from record where inspection_id='$form_id1' and inspection_type='RUN' GROUP BY hac")->result();
        foreach($acc as $acckey){
            $this->severity_level_running($acckey->hac, $acckey->severity);
        }
        }elseif($publish_status == "unpublish"){
         for($i=0;$i<count($severity);$i++){
            $max_id=$this->form_manager_model->get_max_id("form_running_copy");
            $data_to_record=array(
                'hac'=>$hac[$i],
                'inspection_id'=>$max_id,
                'inspection_type'=>'RUN',
                'datetime'=>$ddate,
                'remarks'=>$remarks,
                'recomendation'=>$recomendation,
                'severity_level'=>$severity[$i],
                'status'=>'unpublish',
                'user'=>$this->session->userdata('users_id'),
                'publish_by'=>"-"
            );
        $this->inspection_model->insert('record',$data_to_record);
        $acc=$this->db->query("select hac,max(severity_level) as severity from record where inspection_id='$max_id'  and inspection_type='RUN' GROUP BY hac")->result();
        foreach($acc as $acckey){
            $this->severity_level_running($acckey->hac, $acckey->severity);
        }
        }
        }
        $this->inspection_model->delete('record_running_activity',$form_id1,"form_id");
        for($i=0;$i<count($severity);$i++){
            $data=array(
                'form_id'=>$form_id[$i],
                'record_id'=>$rel_component[$i],
                'hac_id'=>$hac[$i],
                'component_id'=>$component[$i],
                'inspection_activity'=>$inspection_activity[$i],
                'target_value'=>$target_value[$i],
                'actual_value'=>$actual_value[$i],
                'severity_level'=>$severity[$i],
                'status'=>$status[$i],
                'comment'=>$comment[$i],
                'vibration_check'=>$vib[$i],
                'sys_create_date'=>$ddate
            );
            //$hac_code=$this->db->query("select * from hac where id='$hacxx'")->row('hac_code');
            $this->inspection_model->insert('record_running_activity',$data);
        }
        $this->insert_log_activity("Running Report","","Update Running Report '$form_number'");
        redirect('engine/inspection_manager/list_running_inspection');
    }
    function update_stop_activity($id){
     $data['form1']= $this->form_manager_model->get_topform1($id,'form_stop_copy')->row();
     $data['form2']= $this->form_manager_model->get_topform($id,'rel_component_to_form_stop_copy')->result();
     $this->load->view('inspection_form/stop_inspection',$data);
    }
    
    function save_stop_activity(){
        $status=$this->input->post('status');
        $form_number=$this->input->post('form_number');
        $id=$this->input->post('id');
        $form_id1=$this->input->post('form_id1');
        $hac = $this->input->post('hac');
        $hack = $this->input->post('hack');
        $component = $this->input->post('component');
        $item_check = $this->input->post('item_check');
        $method = $this->input->post('method');
        $standard = $this->input->post('standard');
        $value = $this->input->post('value');
        $remarks = $this->input->post('remarks');
        $recomendation = $this->input->post('recomendation');
        $duration = $this->input->post('duration');
        $dtx = $this->input->post('dtx');
        $dx=array(
            'status_proses'=>'1',
            'inspection_type'=>"STOP",
            'publish'=>"unpublish",
            'datetime'=>date("Y-m-d H:i:s"),
            'remarks'=>$remarks,
            'duration'=>$duration,
            'recomendation'=>$recomendation,
            'user'=>$this->session->userdata('users_id'),
            'publish_by'=>"-"
        );
        $this->inspection_model->update('form_stop_copy',$form_id1,'id',$dx);
        
        //for($i=0;$i<count($hac);$i++){
            if($status=="unpublish"){
            $max_id=$this->form_manager_model->get_max_id("form_stop_copy");
            $data_to_record=array(
                'hac'=>$hack,
                'inspection_id'=>$max_id,
                'inspection_type'=>'STOP',
                'datetime'=>date("Y-m-d H:i:s"),
                'remarks'=>$remarks,
                'recomendation'=>$recomendation,
                'severity_level'=>max($value),
                'status'=>'unpublish',
                'user'=>$this->session->userdata('users_id'),
                'publish_by'=>"-"
            );
          $this->inspection_model->insert('record',$data_to_record);
                $acc=$this->db->query("select hac,severity_level as severity from record where inspection_id='$max_id' and inspection_type='STOP'")->row();
                    $this->severity_level_stop($acc->hac, $acc->severity);
            }else{
                $this->db->query("delete from record where inspection_id='$form_id1' and inspection_type='STOP'");
                $data_to_record=array(
                    'hac'=>$hack,
                    'inspection_id'=>$form_id1,
                    'inspection_type'=>'STOP',
                    'datetime'=>date("Y-m-d H:i:s"),
                    'remarks'=>$remarks,
                    'recomendation'=>$recomendation,
                    'severity_level'=>max($value),
                    'status'=>'unpublish',
                    'user'=>$this->session->userdata('users_id'),
                    'publish_by'=>"-"
                );
            $this->inspection_model->insert('record',$data_to_record);
                $acc=$this->db->query("select hac,severity_level as severity from record where inspection_id='$form_id1' and inspection_type='STOP'")->result();
                foreach($acc as $acckey){
                $this->severity_level_stop($acckey->hac, $acckey->severity);
                }
            }
        //}
//        
//        $data_to_record = array(
//            'hac'=>"-",
//            'inspection_type'=>"STOP",
//            'inspection_id'=>$form_id1,
//            'datetime'=>date("Y-m-d H:i:s"),
//            'remarks'=>"-",
//            'recomendation'=>"-",
//            'status'=>"unpublish",
//            'user'=>$this->session->userdata('users_id'),
//            'publish_by'=>"-"
//        );
//        $this->inspection_model->insert('record',$data_to_record);
        $this->inspection_model->delete('record_stop_activity',$form_id1,"form_id");
        for($i=0;$i<count($value);$i++){
            $data=array(
                'record_id'=>$id[$i],
                'hac_id'=>$hac[$i],
                'component_id'=>$component[$i],
                'item_check'=>$item_check[$i],
                'method'=>$method[$i],
                'form_id'=>$form_id1,
                'standard'=>$standard[$i],
                'value'=>$value[$i],
                'sys_create_date'=>date("Y-m-d H:i:s") 
           );
        $this->inspection_model->insert('record_stop_activity',$data);
        }
        //$hac_code=$this->db->query("select * from hac where id='$hac'")->row('hac_code');
        $this->insert_log_activity("Stop Report","","Update Stop Report '$form_number'");
        redirect('engine/inspection_manager/list_stop_inspection');
    }
    function update_wearing_activity($id){
        $this->load->model('form_manager_model');
        $data['detail'] = $this->inspection_model->get_detail_relation1('form_measuring_copy',$id)->row();
        $data['area']= $this->form_manager_model->get_area();
        $data['hac']= $this->form_manager_model->get_hac();
        $data['max'] = $this->inspection_model->get_max($id,'meas_no','update_form_measuring','form_id');
        $this->load->view('inspection_form/wearing_inspection',$data);
    }
    
    function save_wearing_activity(){
        $form_number = $this->input->post('form_number');
        $status = $this->input->post('status');
        $meas_no_x = $this->input->post('meas_no_x');
        $id_form = $this->input->post('id_form');
        $form_id=$this->input->post('form_id');
        $no_roller = $this->input->post('no_roller');
        $meas_no = $this->input->post('meas_no');
        $date = $this->input->post('date');
        $roller_id=$this->input->post('roller_id');
        $remarks=$this->input->post('remarks');
        $recomendation=$this->input->post('recomendation');
        $severity_level=$this->input->post('severity_level');
        $hac=$this->input->post('hac');
        if($status=="reject"){
            $ms_rol=$meas_no_x;
        }else{
            $ms_rol=$meas_no;
        }
        $rol1=$this->input->post('val1');
        $rol2=$this->input->post('val2');
        $rol3=$this->input->post('val3');
        $rol4=$this->input->post('val4');
        $rol5=$this->input->post('val5');
        $rol6=$this->input->post('val6');
        $rol7=$this->input->post('val7');
        $rol8=$this->input->post('val8');
        $rol9=$this->input->post('val9');
        $rol10=$this->input->post('val10');
        $rol11=$this->input->post('val11');
        $rol12=$this->input->post('val12');
        $rol13=$this->input->post('val13');
        $rol14=$this->input->post('val14');
        $rol15=$this->input->post('val15');
        $rol16=$this->input->post('val16');
        $rol17=$this->input->post('val17');
        $rol18=$this->input->post('val18');
        $rol19=$this->input->post('val19');
        $rol20=$this->input->post('val20');
        if($status == "reject"){
                $this->form_manager_model->delete_roller("roller_copy",$id_form,$meas_no_x);
        }
        for($i=0;$i<count($no_roller);$i++){
                    $data = array(
                    "form_id"=>$form_id,
                    "date"=>date("Y-m-d "),
                    "no_roller"=>$no_roller[$i],
                    "roller_id"=>$roller_id[$i],
                    "meas_no"=>$ms_rol,
                    "form_number"=>$form_number,
                    "val1"=>$rol1[$i],"val2"=>$rol2[$i],"val3"=>$rol3[$i],"val4"=>$rol4[$i],"val5"=>$rol5[$i],
                    "val6"=>$rol6[$i],"val7"=>$rol7[$i],"val8"=>$rol8[$i],"val9"=>$rol9[$i],"val10"=>$rol10[$i],
                    "val11"=>$rol11[$i],"val12"=>$rol12[$i],"val13"=>$rol13[$i],"val14"=>$rol14[$i],"val15"=>$rol15[$i],
                    "val16"=>$rol16[$i],"val17"=>$rol17[$i],"val18"=>$rol18[$i],"val19"=>$rol19[$i],"val20"=>$rol20[$i]);
                    $this->form_manager_model->insert("roller_copy",$data);
        }
        
        $no_grinding = $this->input->post('no_grinding');
        $grinding_id = $this->input->post('grinding_id');
        $roller_id=$this->input->post('roller_id');
        $meas_no_grinding = $this->input->post('meas_no_grinding');
        $no_urut = $this->input->post('no_urut');
        if($status=="reject"){
            $ms_gri=$meas_no_x;
        }else{
            $ms_gri=$meas_no_grinding;
        }
        $gri1=$this->input->post('gri1');
        $gri2=$this->input->post('gri2');
        $gri3=$this->input->post('gri3');
        $gri4=$this->input->post('gri4');
        $gri5=$this->input->post('gri5');
        $gri6=$this->input->post('gri6');
        $gri7=$this->input->post('gri7');
        $gri8=$this->input->post('gri8');
        $gri9=$this->input->post('gri9');
        $gri10=$this->input->post('gri10');
        $gri11=$this->input->post('gri11');
        $gri12=$this->input->post('gri12');
        $gri13=$this->input->post('gri13');
        $gri14=$this->input->post('gri14');
        $gri15=$this->input->post('gri15');
        $gri16=$this->input->post('gri16');
        $gri17=$this->input->post('gri17');
        $gri18=$this->input->post('gri18');
        $gri19=$this->input->post('gri19');
        $gri20=$this->input->post('gri20');
        if($status == "reject"){
                $this->form_manager_model->delete_roller("grinding_copy",$id_form,$meas_no_x);
        }
        for($i=0;$i<count($no_grinding);$i++){
                    $data = array(
                    "form_id"=>$form_id,
                    "date"=>date("Y-m-d"),
                    "no_grinding"=>$no_grinding[$i],
                    "grinding_id"=>$grinding_id[$i],
                    "no_urut"=>$no_urut[$i],
                    "meas_no"=>$ms_gri,
                    "form_number"=>$form_number,
                    "val1"=>$gri1[$i],"val2"=>$gri2[$i],"val3"=>$gri3[$i],"val4"=>$gri4[$i],"val5"=>$gri5[$i],
                    "val6"=>$gri6[$i],"val7"=>$gri7[$i],"val8"=>$gri8[$i],"val9"=>$gri9[$i],"val10"=>$gri10[$i],
                    "val11"=>$gri11[$i],"val12"=>$gri12[$i],"val13"=>$gri13[$i],"val14"=>$gri14[$i],"val15"=>$gri15[$i],
                    "val16"=>$gri16[$i],"val17"=>$gri17[$i],"val18"=>$gri18[$i],"val19"=>$gri19[$i],"val20"=>$gri20[$i]);
                    $this->form_manager_model->insert("grinding_copy",$data);
        }
        $update_wear=array(
            "remarks"=>$remarks,
            "recomendation"=>$recomendation,
            "publish"=>"unpublish",
            "publish_date"=>date("Y-m-d H:i:s"),
            "user"=>$this->session->userdata('users_id'),
            "publish_by"=>"-",
            "status_proses"=>"1",
            'sys_create_date'=>date("Y-m-d H:i:s"),
            "severity_level"=>$severity_level
        );
        $this->inspection_model->update("form_measuring_copy",$form_id,"id",$update_wear);
        if($status == "reject"){
            $this->db->query("delete from record where inspection_type='WEAR' and inspection_id='$form_id'");
        }
        $data_to_record = array(
            'hac'=>$hac,
            'inspection_type'=>"WEAR",
            'inspection_id'=>$form_id,
            'datetime'=>date("Y-m-d H:i:s"),
            'remarks'=>$remarks,
            'recomendation'=>$recomendation,
            'status'=>"publish",
            'user'=>$this->session->userdata('users_id'),
            'severity_level'=>$severity_level,
            'publish_by'=>"-"
        );
        $this->inspection_model->insert('record',$data_to_record);
        
        $acc=$this->db->query("select hac,severity_level as severity from record where inspection_id='$form_id' and inspection_type='WEAR'")->row();
        $this->severity_level_wearing($acc->hac, $acc->severity);
        /*if($ms_rol == "2"){
            for($i=0;$i<count($roller_id);$i++){
            $ex_dt=array("expired"=>1);
            $this->inspection_model->update("roller_copy",$roller_id[$i],"roller_id",$ex_dt);
            $this->inspection_model->update("grinding_copy",$grinding_id[$i],"grinding_id",$ex_dt);
            }
        }**/
        
        //$hac_code=$this->db->query("select * from hac where id='$hac'")->row('hac_code');
        $this->insert_log_activity("Measuring Report","","Update Measuring Report '$form_number'");
        redirect('engine/inspection_manager/list_wearing_inspection');
    }
    
 ///////////////////////////////////////////////////////view record running////////////////////////////////////////////////   
    function list_vrunning_inspection(){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="a.id";
        }else{
            $field=$fieldx;
        }
        $config['base_url'] = base_url().'engine/inspection_manager/list_vrunning_inspection/';
        $config['total_rows'] = $this->db->query("select a.*,b.area_name,c.frequency,d.periode,e.nama from form_running_copy a
                                                left join area b on a.area=b.id
                                                left join master_frequency c on a.frequency=c.id
                                                left join master_periode d on a.periode=d.id
                                                left join users e on a.user=e.id
                                                where $field LIKE '%$val%' and a.status_proses='1' and a.publish='unpublish'
                                                order by a.id desc")->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 4;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['data'] = $this->db->query("select a.*,b.area_name,c.frequency,d.periode,e.nama from form_running_copy a
                                                left join area b on a.area=b.id
                                                left join master_frequency c on a.frequency=c.id
                                                left join master_periode d on a.periode=d.id
                                                left join users e on a.user=e.id
                                                where $field LIKE '%$val%' and a.status_proses='1' and a.publish='unpublish'
                                                order by a.id desc limit ".$pg.",".$config['per_page']."")->result();
//        $sql=$this->db->query("select a.*,b.area_name,c.frequency,d.periode,e.nama from form_running_copy a
//                               left join area b on a.area=b.id
//                               left join master_frequency c on a.frequency=c.id
//                               left join master_periode d on a.periode=d.id
//                               left join users e on a.user=e.id
//                               where a.status_proses='1' and a.publish='unpublish' order by id desc
//                              ");
//        $data['list']=$sql->result();
        $this->load->view('inspection_form/listvrunning',$data);    
    }
    function view_running_activity($id){
     $data['form1']= $this->form_manager_model->get_topform1x($id,'form_running_copy')->row();
     $data['form2']= $this->form_manager_model->get_topformrun($id,'rel_component_to_form_running_copy')->result();
     $this->load->view('inspection_form/view_running',$data);
    }
    function update_view_running(){
        $id_form = $this->input->post('id');
        $form_no =$this->input->post('form_no');
		$remarks = $this->input->post('remarks');
		$recomendation = $this->input->post('recomendation');
        //update 5 maret
        $date=substr($this->input->post('datetime'),0,10);
        $form_running_id=$this->input->post('form_running_id');
        $get_data=$this->db->query("select * from form_running_copy where form_running_id='$form_running_id' and date(datetime)='$date' and id !='$id_form'")->result();
        foreach($get_data as $get_datax){
        $this->db->query("delete from record where inspection_id='$get_datax->id'");
        $this->db->query("delete from record_running_activity where form_id='$get_datax->id'");
        $this->db->query("delete from rel_activity_inspection_copy where form_id='$get_datax->id'");
        $this->db->query("delete from rel_component_to_form_running_copy where form_id='$get_datax->id'");
        $this->db->query("delete from form_running_copy where id='$get_datax->id'");
        }
        ////
        $data=array(
			"remarks" => $remarks,
			"recomendation" => $recomendation,
            "publish"=>"publish",
            "publish_by"=>$this->session->userdata('users_id')
        );
        $this->inspection_model->update("form_running_copy",$id_form,"id",$data);
        $this->db->query("update record set status='publish' where inspection_type='RUN' and inspection_id='$id_form'");
        $this->insert_log_activity("Running Report","","Publish Running Report With Form No. '$form_no'");
        return true;
    }
    function update_view_running2(){
        $id_form = $this->input->post('id');
        $form_no =$this->input->post('form_no');
		$remarks = $this->input->post('remarks');
		$recomendation = $this->input->post('recomendation');
        $data=array(
			"remarks" => $remarks,
			"recomendation" => $recomendation,
            "publish"=>"reject",
            "status_proses"=>"0"
        );
		//$output = array("test"=>$data);
		//$this->load->view("test",$output);
        $this->inspection_model->update("form_running_copy",$id_form,"id",$data);
	 	$this->db->query("delete from record where inspection_type='RUN' and inspection_id='$id_form'");
        $this->insert_log_activity("Running Report","","Reject Running Report With Form No. '$form_no'");
        return true;
    }
    
//////////////////////////////////////////////////report record running ///////////////////////////////////////////
    function list_report_running_inspection(){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="a.id";
        }else{
            $field=$fieldx;
        }
        $config['base_url'] = base_url().'engine/inspection_manager/list_report_running_inspection/';
        $config['total_rows'] = $this->db->query("select a.*,b.area_name,c.frequency,d.periode,e.nama from form_running_copy a
                                                left join area b on a.area=b.id
                                                left join master_frequency c on a.frequency=c.id
                                                left join master_periode d on a.periode=d.id
                                                left join users e on a.user=e.id
                                                where $field LIKE '%$val%' and a.status_proses='1' and a.publish='publish'
                                                order by a.id desc")->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 4;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['data'] = $this->db->query("select a.*,b.area_name,c.frequency,d.periode,e.nama from form_running_copy a
                                        left join area b on a.area=b.id
                                        left join master_frequency c on a.frequency=c.id
                                        left join master_periode d on a.periode=d.id
                                        left join users e on a.user=e.id
                                        where $field LIKE '%$val%' and a.status_proses='1' and a.publish='publish'
                                        order by a.id desc limit ".$pg.",".$config['per_page']."")->result();
//        $sql=$this->db->query("select a.*,b.area_name,c.frequency,d.periode,e.nama from form_running_copy a
//                               left join area b on a.area=b.id
//                               left join master_frequency c on a.frequency=c.id
//                               left join master_periode d on a.periode=d.id
//                               left join users e on a.user=e.id
//                               where a.status_proses='1' and a.publish='publish' order by id desc
//                              ");
//        $data['list']=$sql->result();
        $this->load->view('inspection_form/list_report_running',$data);    
    }
    function print_form_running(){
            $id =  $this->uri->segment(4);
            $data['form'] = $this->form_manager_model->get_topform1x($id,'form_running_copy')->row();
            $data['data_2ndform'] = $this->db->query("select a.*,b.hac_code,equipment,description,c.assembly_name
                                                    from rel_component_to_form_running_copy a
                                                    left join hac b on a.hac=b.id
                                                    left join hac_assembly c on a.component=c.id
                                                    where form_id='$id'
                                                    group by a.hac ORDER BY a.id ASC")->result();
           // $data['data_3rdform'] = $this->form_manager_model->get_inspection($id,"rel_activity_inspection")->result();
            $type=$this->form_manager_model->get_topform1x($id,'form_running_copy')->row('form_number');
            $this->insert_log_activity("Running Report","","Print Running Report '$type'");
            $this->load->view('inspection_form/running_print',$data);
        }
    function report_running_activity($id){
     $data['form1']= $this->form_manager_model->get_topform1x($id,'form_running_copy')->row();
     $data['form2']= $this->form_manager_model->get_topformrun($id,'rel_component_to_form_running_copy')->result();
     $this->load->view('inspection_form/report_running',$data);
    }

//////////////////////////////////////////view record stop //////////////////////////////////////////////
function list_vstop_inspection(){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="a.id";
        }else{
            $field=$fieldx;
        }
        $config['base_url'] = base_url().'engine/inspection_manager/list_vstop_inspection/';
        $config['total_rows'] = $this->db->query("select a.*,b.area_name,c.frequency,d.nama,e.hac_code from form_stop_copy a
                                                left join area b on a.area=b.id
                                                left join master_frequency c on a.frequency=c.id
                                                left join users d on a.user=d.id
                                                left join hac e on a.hac=e.id
                                                where $field LIKE '%$val%' and a.status_proses='1' and a.publish='unpublish'
                                                order by a.id desc")->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 4;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['data'] = $this->db->query("select a.*,b.area_name,c.frequency,d.nama,e.hac_code from form_stop_copy a
                                        left join area b on a.area=b.id
                                        left join master_frequency c on a.frequency=c.id
                                        left join users d on a.user=d.id
                                        left join hac e on a.hac=e.id
                                        where $field LIKE '%$val%' and a.status_proses='1' and a.publish='unpublish'
                                        order by a.id desc limit ".$pg.",".$config['per_page']."")->result();
//        $sql=$this->db->query("select a.*,b.area_name,c.frequency,d.nama from form_stop_copy a
//                               left join area b on a.area=b.id
//                               left join master_frequency c on a.frequency=c.id
//                               left join users d on a.user=d.id
//                               where a.status_proses='1' and a.publish='unpublish' order by a.id desc
//                              ");
//        $data['list']=$sql->result();
        $this->load->view('inspection_form/listvstop2',$data);    
    }
    
    function view_stop_activity($id){
        $this->load->model('form_manager_model');
        $data['data_1stform'] = $this->form_manager_model->get_topform1($id,"form_stop_copy")->row();
        $data['data_2ndform'] = $this->inspection_model->get_topformst2($id,"rel_component_to_form_stop_copy")->result();
        $data['component']= $this->form_manager_model->get_component();
        $data['area']= $this->form_manager_model->get_area();
        $data['hac']= $this->form_manager_model->get_hac();
        $data['id']= $id;
        $this->load->view('inspection_form/view_stop',$data);
    }
    function update_view_stop(){
        $form_no= $this->input->post('form_no');
        $id_form = $this->input->post('id');
        $form_stop_id = $this->input->post('form_stop_id');
        $tanggal=substr($this->input->post('tanggal'),0,10);
        $cek=$this->db->query("select * from form_stop_copy where date(datetime)='$tanggal' and form_stop_id='$form_stop_id' and id !='$id_form'")->result();
        foreach ($cek as $cekx){
            $this->db->query("delete from rel_component_to_form_stop_copy where form_id='$cekx->id'");
            $this->db->query("delete from record_stop_activity where form_id='$cekx->id'");
            $this->db->query("delete from record where inspection_id='$cekx->id' and inspection_type='STOP'");
            $this->db->query("delete from form_stop_copy where id='$cekx->id'");
        }
        $data=array(
            "publish"=>"publish",
            "publish_by"=>$this->session->userdata('users_id')
        );
        $this->inspection_model->update("form_stop_copy",$id_form,"id",$data);
        $this->insert_log_activity("Stop Report","","Publish Stop Report With Form No. '$form_no'");
        return true;
    }
    function update_view_stop2(){
        $form_no= $this->input->post('form_no');
        $remarks =$this->input->post('remarks');
        $id_form = $this->input->post('id');
        $data=array(
            "publish"=>"reject",
            "status_proses"=>"0"
        );
        $this->inspection_model->update("form_stop_copy",$id_form,"id",$data);
        $this->insert_log_activity("Stop Report","","Publish Stop Report With Form No. '$form_no'");
        return true;
    }
    
    //////////////////////////////////////////////////report record stop ///////////////////////////////////////////
    function list_report_stop_inspection(){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="a.id";
        }else{
            $field=$fieldx;
        }
        $config['base_url'] = base_url().'engine/inspection_manager/list_report_stop_inspection/';
        $config['total_rows'] = $this->db->query("select a.*,b.area_name,c.frequency,d.nama,e.hac_code from form_stop_copy a
                                                left join area b on a.area=b.id
                                                left join master_frequency c on a.frequency=c.id
                                                left join users d on a.user=d.id
                                                left join hac e on a.hac=e.id
                                                where $field LIKE '%$val%' and a.status_proses='1' and a.publish='publish'
                                                order by a.id desc")->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 4;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['data'] = $this->db->query("select a.*,b.area_name,c.frequency,d.nama,e.hac_code from form_stop_copy a
                                        left join area b on a.area=b.id
                                        left join master_frequency c on a.frequency=c.id
                                        left join users d on a.user=d.id
                                        left join hac e on a.hac=e.id
                                        where $field LIKE '%$val%' and a.status_proses='1' and a.publish='publish'
                                        order by a.id desc limit ".$pg.",".$config['per_page']."")->result();
//        $sql=$this->db->query("select a.*,b.area_name,c.frequency,d.nama from form_stop_copy a
//                               left join area b on a.area=b.id
//                               left join master_frequency c on a.frequency=c.id
//                               left join users d on a.user=d.id
//                               where a.status_proses='1' and a.publish='publish' order by a.id desc
//                              ");
//        $data['list']=$sql->result();
        $this->load->view('inspection_form/list_report_stop',$data);    
    }
    
    function report_stop_activity($id){
        $this->load->model('form_manager_model');
        $data['data_1stform'] = $this->form_manager_model->get_topform1($id,"form_stop_copy")->row();
        $data['data_2ndform'] = $this->inspection_model->get_topformst2($id,"rel_component_to_form_stop_copy")->result();
        $data['component']= $this->form_manager_model->get_component();
        $data['area']= $this->form_manager_model->get_area();
        $data['hac']= $this->form_manager_model->get_hac();
        $data['id']= $id;
        $this->load->view('inspection_form/report_stop',$data);
    }
    
     function print_form_stop(){
            $this->load->model('form_manager_model');
            $id =  $this->uri->segment(4);
            $data['form'] = $this->form_manager_model->get_topform1($id,'form_stop_copy')->row();
            //$data['data_2ndform'] = $this->form_manager_model->get_topform($id,"rel_component_to_form_running")->result();
           // $data['data_3rdform'] = $this->form_manager_model->get_inspection($id,"rel_activity_inspection")->result();
            $type=$this->form_manager_model->get_topform1($id,'form_stop_copy')->row('form_number');
            $this->insert_log_activity("Stop Report","","Print Stop Report '$type'");
            $this->load->view('inspection_form/stop_print',$data);
        }
        
        ///////////////////////////////////////////////////////view record meassuring////////////////////////////////////////////////   
    function list_vmeasuring_inspection(){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="a.id";
        }else{
            $field=$fieldx;
        }
        $config['base_url'] = base_url().'engine/inspection_manager/list_vmeasuring_inspection/';
        $config['total_rows'] = $this->db->query("select a.*,b.area_name,c.hac_code,d.nama from form_measuring_copy a
                                                left join area b on a.area=b.id
                                                left join hac c on a.hac=c.id
                                                left join users d on a.user=d.id
                                                where $field LIKE '%$val%' and  a.status_proses='1' and a.publish='unpublish'
                                                order by a.id desc")->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 4;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['data'] = $this->db->query("select a.*,b.area_name,c.hac_code,d.nama from form_measuring_copy a
                                        left join area b on a.area=b.id
                                        left join hac c on a.hac=c.id
                                        left join users d on a.user=d.id
                                        where $field LIKE '%$val%' and  a.status_proses='1' and a.publish='unpublish'
                                        order by a.id desc limit ".$pg.",".$config['per_page']."")->result();
//        $sql=$this->db->query("select a.*,b.area_name,c.hac_code from form_measuring_copy a
//                               left join area b on a.area=b.id
//                               left join hac c on a.hac=c.id
//                               where a.status_proses='1' and a.publish='unpublish'
//                              ");
//        $data['list']=$sql->result();
        $this->load->view('inspection_form/listvmeasuring',$data);    
    }
    
    function view_wearing_activity($id){
        $this->load->model('form_manager_model');
        $data['data_1stform'] = $this->form_manager_model->get_topform1($id,"form_measuring_copy")->row();
        $data['data_2ndform'] = $this->inspection_model->get_topformst2($id,"rel_component_to_form_stop_copy")->result();
        $data['component']= $this->form_manager_model->get_component();
        $data['area']= $this->form_manager_model->get_area();
        $data['hac']= $this->form_manager_model->get_hac();
        $data['id']= $id;
        $this->load->view('inspection_form/view_wear',$data);
    }
    
    function update_view_wear(){
        $form_no=$this->input->post('form_no');
        $id_form = $this->input->post('id');
        $data=array(
            "publish"=>"reject",
            "status_proses"=>"0"
        );
        $this->inspection_model->update("form_measuring_copy",$id_form,"id",$data);
        $this->insert_log_activity("Measuring Report","","Reject Measuring Report With Form No. '$form_no'");
        return true;
    }
    
     function update_view_wear2(){
         $id_form = $this->input->post('id');
         $form_no=$this->input->post('form_no');
         $data=array(
            "publish"=>"publish",
            "publish_by"=>$this->session->userdata('users_id')
        );
        $this->inspection_model->update("form_measuring_copy",$id_form,"id",$data);
        
        $ab = $this->inspection_model->select_all_where_group("roller_copy",$id_form,"form_id","meas_no")->row('meas_no');
        $ac = $this->inspection_model->select_all_where_roller("roller_copy",$id_form,"form_id")->result();
        $ad = $this->inspection_model->select_all_where_grinding("grinding_copy",$id_form,"form_id")->result();
        if($ab == "2"){
            foreach($ac as $key){
            $ex_dt=array("expired"=>1);
            $this->inspection_model->update("roller_copy",$key->roller_id,"roller_id",$ex_dt);
            }
            foreach($ad as $key){
            $ex_dt=array("expired"=>1);
            $this->inspection_model->update("grinding_copy",$key->grinding_id,"grinding_id",$ex_dt);
            }
            $form_number =$this->form_manager_model->form_number('F/WEAR-');
            $no_form=$form_number->form_no+1;
            $data=array('form_no'=>$no_form);
            $this->form_manager_model->update('auto_form_number',$data,"3");
        }
        $this->insert_log_activity("Measuring Report","","Publish Measuring Report With Form No. '$form_no'");
        return true;
    }
        ////////////////////////////////report record measuring//////////
    
    function list_report_wear_inspection(){
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="a.id";
        }else{
            $field=$fieldx;
        }
        $config['base_url'] = base_url().'engine/inspection_manager/list_report_wear_inspection/';
        $config['total_rows'] = $this->db->query("select a.*,b.area_name,c.hac_code,d.nama from form_measuring_copy a
                                                left join area b on a.area=b.id
                                                left join hac c on a.hac=c.id
                                                left join users d on a.user=d.id
                                                where $field LIKE '%$val%' and a.status_proses='1' and a.publish='publish' 
                                                group by a.form_number
                                                order by a.id desc")->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 2;
        $config['uri_segment'] = 4;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['data'] = $this->db->query("select a.*,b.area_name,c.hac_code,d.nama from form_measuring_copy a
                                                left join area b on a.area=b.id
                                                left join hac c on a.hac=c.id
                                                left join users d on a.user=d.id
                                                where $field LIKE '%$val%' and a.status_proses='1' and a.publish='publish' 
                                                group by a.form_number
                                                order by a.id desc limit ".$pg.",".$config['per_page']."")->result();
//        $sql=$this->db->query("select a.*,b.area_name,c.hac_code from form_measuring_copy a
//                               left join area b on a.area=b.id
//                               left join hac c on a.hac=c.id
//                               where a.status_proses='1' and a.publish='publish' group by a.form_number order by a.id desc
//                              ");
//        $data['list']=$sql->result();
        $this->load->view('inspection_form/list_report_measuring',$data);    
    }
    
    function report_wearing_activity($id){
        $this->load->model('form_manager_model');
        $data['data_1stform'] = $this->form_manager_model->get_topform1($id,"form_measuring_copy")->row();
        $data['data_2ndform'] = $this->inspection_model->get_topformst2($id,"rel_component_to_form_stop_copy")->result();
        $data['component']= $this->form_manager_model->get_component();
        $data['area']= $this->form_manager_model->get_area();
        $data['hac']= $this->form_manager_model->get_hac();
        $data['id']= $id;
        $this->load->view('inspection_form/report_wear',$data);
    }
    
    function print_form_wear($id){
        $this->load->model('form_manager_model');
        $data['form'] = $this->form_manager_model->get_topform1($id,"form_measuring_copy")->row();
        $type = $this->form_manager_model->get_topform1($id,"form_measuring_copy")->row('form_number');
        $this->insert_log_activity("Measuring Report","","Print Measuring Report '$type'");
        $this->load->view('inspection_form/wearing_print',$data);
    }
    
    function delete_running_activity($id){
            $detail=$this->db->query("select * from form_running_copy where id='$id'");
            $this->insert_log_activity("Record Running Inspection","","Delete Running Inspection Record With Form No. '$detail->form_number'");
            $this->db->query("delete from form_running_copy where id='$id'");
            $this->db->query("delete from rel_component_to_form_running_copy where form_id='$id'");
            $this->db->query("delete from rel_activity_inspection_copy where form_id='$id'");
            $this->db->query("delete from record_running_activity where form_id='$id'");
            $this->db->query("delete from record where inspection_id='$id' and inspection_type='RUN'");  
            redirect("engine/inspection_manager/list_running_inspection");
        }
    
    function delete_stop_activity($id){
            $detail=$this->db->query("select * from form_stop_copy where id='$id'");
            $this->insert_log_activity("Record Stop Inspection","","Delete Stop Inspection Record With Form No. '$detail->form_number'");
            $this->db->query("delete from form_stop_copy where id='$id'");
            $this->db->query("delete from rel_component_to_form_stop_copy where form_id='$id'");
            $this->db->query("delete from record_stop_activity where form_id='$id'");
            $this->db->query("delete from record where inspection_id='$id' and inspection_type='STOP'");  
            redirect("engine/inspection_manager/list_stop_inspection");
    }
    
    function delete_wear_activity($id){
            $detail=$this->db->query("select * from form_measuring_copy where id='$id'");
            $this->insert_log_activity("Record Wearing Inspection","","Delete Wearinng Inspection Record With Form No. '$detail->form_number'");
            $this->db->query("delete from form_measuring_copy where id='$id'");
            $this->db->query("delete from roller_copy where form_id='$id'");
            $this->db->query("delete from grinding_copy where form_id='$id'");
            $this->db->query("delete from record where inspection_id='$id' and inspection_type='WEAR'");  
            redirect("engine/inspection_manager/list_wearing_inspection");
    }
    }