<?php

class Crud_area extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('users_model');	
                $this->load->model('form_manager_model');
	}
	
        
        public function insert_log_activity($type,$primarykey,$description){
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
        function get_value(){
        $data="";
        $id=$this->input->post('id');
        $val=$this->users_model->select_all_where("master_mainarea",$id,"id_plant")->result();
        //$data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
            $data .="<option value='$value->id'>$value->description</option>\n";
        }
        echo $data;
    }
    
    function get_value2(){
        $data="";
        $id=$this->input->post('id');
        $area=$this->input->post('area');
        $val=$this->users_model->select_all_where("master_mainarea",$id,"id_plant")->result();
        //$data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
                if($value->id==$area){
                    $cek="selected";
                }else{
                    $cek="";
                }
            $data .="<option value='$value->id' $cek>$value->description</option>\n";
        }
        echo $data;
    }
        
//	function index()
//	{
//		$crud = new grocery_CRUD();
//		$crud->set_theme('datatables');
//        $crud->set_table('area');
//        $crud->set_subject('Area');
//        $crud->columns('area_name','area','sub_area','image');
//		$crud->set_field_upload('image','media/images');
//		if( $crud->getState() == 'insert_validation' ) {
//		$crud->set_rules('area_name', 'Area Name', 'trim|required');
//		$crud->set_rules('area', 'Area', 'trim|required');
//		$crud->set_rules('sub_area', 'Sub Area', 'trim|required');	
//		$crud->set_rules('line_prod', 'Line Production', 'trim|required');
//		$crud->set_rules('image', 'image', 'trim|required');
//		
//		}
//        
//        $crud->callback_after_update(array($this, 'log_user_after_update'));
//        
//        $crud->callback_after_delete(array($this, 'log_user_after_delete'));
//        
//         //$crud->unset_back_to_list();
//		$crud->unset_print();
//        $crud->unset_export();
//        $output = $crud->render();
// 
//        $this->output($output);
//	
//	}
//    
//    
//    function log_user_after_update($post_array,$primary_key)
//    {
//        $id = $this->session->userdata('users_id');
//        $user_logs_insert = array(
//            "user_id" => $id,
//            "date_activity" => date('Y-m-d H:i:s'),
//            "description" => 'UPDATE',
//            "type" => 'AREA',
//            "record_id" => $primary_key
//        );
//     
//        $this->db->insert('users_activity',$user_logs_insert);
//     
//        return true;
//    }
//    
//     function log_user_after_delete($post_array,$primary_key)
//    {
//        $id = $this->session->userdata('users_id');
//        $user_logs_delete = array(
//            "user_id" => $id,
//            "date_activity" => date('Y-m-d H:i:s'),
//            "description" => 'DELETE',
//            "type" => 'AREA',
//            "record_id" => $primary_key
//        );
//     
//        $this->db->insert('users_activity',$user_logs_delete);
//     
//        return true;
//    }
//	
//	function output($output = null)
//    {
//        $this->load->view('crud/page_crud.php',$output);    
//    }
        function index(){
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="a.id";
                }else{
                    $field=$fieldx;
                }
		$config['base_url'] = base_url().'engine/crud_area/index/';
                $config['total_rows'] = $this->db->query("select a.*,b.description as areax,c.plant_name from area a left join master_mainarea b on a.area=b.id left join master_plant c on b.id_plant=c.id where $field LIKE '%$val%'")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select a.*,b.description as areax,c.plant_name from area a left join master_mainarea b on a.area=b.id left join master_plant c on b.id_plant=c.id where $field LIKE '%$val%' limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('area/area_main', $data); 
        }
        
        function add(){
            $data['list_plant']=$this->users_model->select_all('master_plant')->result();
            $data['list_area']=$this->users_model->select_all('master_mainarea')->result();
            $this->load->view('area/area_add',$data);
        }
        
        function add_proses(){
            $area_name=$this->input->post('area_name');
            $area_code=$this->input->post('area_code');
            $main_area=$this->input->post('main_area');
            $description=$this->input->post('description');
            $image=$this->input->post('image');
            
            $config['upload_path']	= "./media/images/";
	    $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000';
            $config['max_width']  	= '2000';
            $config['max_height']  	= '2000';
            $this->load->library('upload');
	    $this->upload->initialize($config);
            
            if($this->upload->do_upload('image'))
             {
            $image_data1 = $this->upload->data();    
             }
             
             $data=array(
                 'area_name'=>$area_name,
                 'area_code'=>$area_code,
                 'description'=>$description,
                 'area'=>$main_area,
                 'image'=>$image_data1['file_name']
             );
             $this->users_model->insert('area',$data);
             $this->insert_log_activity("Sub Area Management", "","Add Sub Area '$area_name'");
             redirect('engine/crud_area');
        }
        
        function edit($id){
            $data['list_plant']=$this->users_model->select_all('master_plant')->result();
            $data['list']=$this->users_model->select_all_where('area',$id,'id')->row();
            $data['list_area']=$this->users_model->select_all('master_mainarea')->result();
            $this->load->view('area/area_edit',$data);
            
        }
        
        function edit_proses(){
            $id=$this->input->post('id');
            $area_code=$this->input->post('area_code');
            $area_name=$this->input->post('area_name');
            $main_area=$this->input->post('main_area');
            $description=$this->input->post('description');
            $image=$this->input->post('image');
            $image_hidden=$this->input->post('image_hidden');
            
            $config['upload_path']	= "./media/images/";
	    $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000';
            $config['max_width']  	= '2000';
            $config['max_height']  	= '2000';
            $this->load->library('upload');
	    $this->upload->initialize($config);
            
            if($this->upload->do_upload('image'))
             {
                 $image_data1 = $this->upload->data();  
                 $img1 = $image_data1['file_name'];
             }else{
                 $img1 = $image_hidden;   
             }
             
             $data=array(
                 'area_name'=>$area_name,
                 'description'=>$description,
                 'area_code'=>$area_code,
                 'area'=>$main_area,
                 'image'=>$img1
             );
             $this->users_model->update('area',$id,'id',$data);
             $this->insert_log_activity("Sub Area Management", "","Update Sub Area '$area_name'");
             redirect('engine/crud_area');
        }
        
        function delete($id){
            $area=$data['list']=$this->users_model->select_all_where('area',$id,'id')->row('area_name');
            $this->insert_log_activity("Sub Area Management", "","Delete Sub Area '$area'");
            $this->users_model->delete('area',$id,'id');
            redirect('engine/crud_area');
        }
}	