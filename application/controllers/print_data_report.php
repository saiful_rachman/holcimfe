<?php

class Print_data_report extends CI_controller {

function __construct(){
		parent::__construct();
		$this->load->library('access');
		$this->load->helper(array('form', 'url'));
		
}

     //INSPECTION REPORT//
    function inspection_report($id){
       // $this->access->check_access(); 
        $data['list_image']=$this->db->query("select * from record_inspection_report_image where record_id='$id'")->result();
        $data['list']=$this->db->query("select a.*,b.*,c.hac_code,d.nama,a.id as idx from record a inner join record_inspection_report b on b.record_id=a.id inner join hac c on a.hac=c.id inner join users d on a.user=d.id where a.id='$id'")->row();	
        $data['display']="none";
        $this->load->view('print/inspection_report', $data);
}
    
    
   
    //------------------// 
	
	
	//MCA//
    function mca($id){
			//$this->access->check_access(); 
			$this->load->model('main_model');
            
            $row = $this->main_model->get_record($id)->row();
			$data['record_main']['id'] = $row->id;
			$data['record_main']['status'] = $row->status;
			$data['record_main']['datetime'] = $row->datetime;
			$data['record_main']['remarks'] = $row->remarks;
			$data['record_main']['user'] = $row->user;
			
			$result = $this->main_model->get_mca($id)->row();
			$data['record_detail']['id'] = $result->id;
			$data['record_detail']['record_id'] = $result->record_id;
			
			$user = $row->user;
			
			$result_user = $this->main_model->get_user($user)->row();
			$data['record_user']['id'] = $result_user->id;
			$data['record_user']['nama'] = $result_user->nama;
		$this->load->view('print_report/mca', $data);
}
    
    
   
    //------------------// 
	
	
	
	//MCA//
    function mcsa($id){
			//$this->access->check_access(); 
			$this->load->model('main_model');
            
            $row = $this->main_model->get_record($id)->row();
			$data['record_main']['id'] = $row->id;
			$data['record_main']['status'] = $row->status;
			$data['record_main']['datetime'] = $row->datetime;
			$data['record_main']['remarks'] = $row->remarks;
			$data['record_main']['user'] = $row->user;
			
			$result = $this->main_model->get_mcsa($id)->row();
			$data['record_detail']['id'] = $result->id;
			$data['record_detail']['record_id'] = $result->record_id;
			
			$user = $row->user;
			
			$result_user = $this->main_model->get_user($user)->row();
			$data['record_user']['id'] = $result_user->id;
			$data['record_user']['nama'] = $result_user->nama;
		$this->load->view('print_report/mcsa', $data);
}
    
    
   
    //------------------// 
    
    
	
	 //RUNNING//
	function running_inspection($id){
		//$this->access->check_access(); 
			$this->load->model('main_model');
            
            $row = $this->main_model->get_record($id)->row();
			$data['record_main']['id'] = $row->id;
			$data['record_main']['status'] = $row->status;
			$data['record_main']['datetime'] = $row->datetime;
			$data['record_main']['remarks'] = $row->remarks;
			$data['record_main']['user'] = $row->user;
			
			$result = $this->main_model->get_running($id)->row();
			$data['record_detail']['id'] = $result->id;
			$data['record_detail']['record_id'] = $result->record_id;
			
			$user = $row->user;
			
			$result_user = $this->main_model->get_user($user)->row();
			$data['record_user']['id'] = $result_user->id;
			$data['record_user']['nama'] = $result_user->nama;
		$this->load->view('print_report/running-inspection', $data);
	}
    
    
 //----------------------//

    
  
     //STOP//
    function stop_inspection($id){
		//$this->access->check_access(); 
        $this->load->model('main_model');
        
		$row = $this->main_model->get_record($id)->row();
		$data['record_main']['id'] = $row->id;
		$data['record_main']['inspection_type'] = $row->inspection_type;
		$data['record_main']['status'] = $row->status;
		$data['record_main']['datetime'] = $row->datetime;
		$data['record_main']['remarks'] = $row->remarks;
		$data['record_main']['user'] = $row->user;
		
		$type_form = $row->inspection_type;
			
		if($type_form == "STOP_1M"){
		
			$this->load->view('print_report/stop_inspection_1m');
		
		}elseif ($type_form == "STOP_3M"){
		
			$this->load->view('print_report/stop_inspection_3m');
		
		}elseif ($type_form == "STOP_6M"){
		
			$this->load->view('print_report/stop_inspection_6m');
		
		}elseif ($type_form == "STOP_1Y"){
		
			$this->load->view('print_report/stop_inspection_1y');
		
		}
	
	}
    
    
     //---------------------//
     
     
    
	 //VIBRATION//
	function vibration($id){
           // $this->access->check_access(); 
            $data['list']=$this->db->query("select a.*,b.*,c.hac_code,d.nama,e.area_name from record a inner join record_vibration b on b.record_id=a.id inner join hac c on a.hac=c.id left join users d on a.user=d.id left join area e on b.area=e.id where a.id='$id'")->row();
	    $get_data=$this->db->query("select a.*,b.*,c.hac_code,d.nama,e.area_name from record a inner join record_vibration b on b.record_id=a.id inner join hac c on a.hac=c.id left join users d on a.user=d.id left join area e on b.area=e.id where a.id='$id'")->row();
            $data['meas']=$this->db->query("select a.* from record_vibration a left join record b on a.record_id=b.id where b.hac='$get_data->hac' and a.sys_create_date<='$get_data->sys_create_date' order by a.id asc")->result();
            $data['display']="none";
            $this->load->view('print/vibration', $data);
	}
    
     
	//------//



    
    //ULTRASONIC//
	function ultrasonic($id){
		
            $//this->access->check_access(); 
            $data['list']=$this->db->query("select a.*,b.*,c.hac_code,d.nama as nama_ins,d.signature as signature_ins,a.id as idx,e.nama as nama_pub,e.signature as signature_pub from record a inner join record_ultrasonic_test b on b.record_id=a.id inner join hac c on a.hac=c.id inner join users d on a.user=d.id inner join users e on a.publish_by=e.id where a.id='$id'")->row();	
            $data['display']='none';
            $this->load->view('print/ultrasonic', $data);
	}
    
    
    //----------------//
    
    
    
    //LUBRICANT//
    function lubricant(){
            
            //$this->access->check_access(); 
			$this->load->model('main_model');
            
            $row = $this->main_model->get_record($id)->row();
			$data['record_main']['id'] = $row->id;
			$data['record_main']['status'] = $row->status;
			$data['record_main']['datetime'] = $row->datetime;
			$data['record_main']['remarks'] = $row->remarks;
			$data['record_main']['user'] = $row->user;
			
			$result = $this->main_model->get_lubricant($id)->row();
			$data['record_detail']['id'] = $result->id;
			$data['record_detail']['date'] = $result->date;
			$data['record_detail']['record_id'] = $result->record_id;
			$data['record_detail']['test_object'] = $result->test_object;
			
			$user = $row->user;
			
			$result_user = $this->main_model->get_user($user)->row();
			$data['record_user']['id'] = $result_user->id;
			$data['record_user']['nama'] = $result_user->nama;
			
			
			$hac = $result->hac;
			$result_hac = $this->main_model->get_hac_code($hac)->row();
			$data['record_hac']['id'] = $result_hac->id;
			$data['record_hac']['hac_code'] = $result_hac->hac_code;
		$this->load->view('print_report/lubricant', $data);
	}
	//----------------//
    
    
	
    //THERMO//
    function thermo($id){
		//$this->access->check_access(); 
			$this->load->model('main_model');
            
            $row = $this->main_model->get_record($id)->row();
			$data['record_main']['id'] = $row->id;
			$data['record_main']['status'] = $row->status;
			$data['record_main']['datetime'] = $row->datetime;
			$data['record_main']['remarks'] = $row->remarks;
			$data['record_main']['user'] = $row->user;
			
			$result = $this->main_model->get_thermo($id)->row();
			$data['record_detail']['id'] = $result->id;
			$data['record_detail']['record_id'] = $result->record_id;
			
			$user = $row->user;
			
			$result_user = $this->main_model->get_user($user)->row();
			$data['record_user']['id'] = $result_user->id;
			$data['record_user']['nama'] = $result_user->nama;
		$this->load->view('print_report/thermo', $data);
	}
    
  

//-------------------//


	//THICKNESS//
	function thickness($id){
	//	$this->access->check_access(); 
       //$this->access->check_access(); 
                $data['list']=$this->db->query("select a.*,b.*,c.hac_code,d.nama as nama_ins,d.signature as signature_ins,a.id as idx,e.nama as nama_pub,e.signature as signature_pub from record a inner join record_thickness b on b.record_id=a.id inner join hac c on a.hac=c.id inner join users d on a.user=d.id left join users e on a.publish_by=e.id where a.id='$id'")->row();	
                $sx=$this->db->query("select a.*,b.*,c.hac_code,a.id as idx from record a inner join record_thickness b on b.record_id=a.id inner join hac c on a.hac=c.id inner join users d on a.user=d.id where a.id='$id'")->row();	
		$data['display']='none';
		$type_form = $sx->inspection_type;
			
		if($type_form == "THICK_GENERAL"){
		
			$this->load->view('print/thickness', $data);
		
		}elseif ($type_form == "THICK_KILN"){
		
			$this->load->view('print/thickness_kiln', $data);
		
		}elseif ($type_form == "THICK_STACK"){
		
			$this->load->view('print/thickness_stack', $data);
		
		}
	}
    
    
    
  //-------------------------------//
  
    
    //PENETRANT//	
	function penetrant($id){
            
            //$this->access->check_access(); 
            $data['list_image']=$this->db->query("select * from record_penetrant_image where record_id='$id'")->result();
            $data['list']=$this->db->query("select a.*,b.*,c.hac_code,d.nama as nama_ins,d.signature as signature_ins,a.id as idx,e.nama as nama_pub,e.signature as signature_pub from record a inner join record_penetrant_test b on b.record_id=a.id inner join hac c on a.hac=c.id inner join users d on a.user=d.id inner join users e on a.publish_by=e.id where a.id='$id'")->row();	
	    $data['display']='none';	
            $this->load->view('print/penetrant', $data);
	}
    
    

//----------------------------------//


    //OIL ANALYSIS//
    function oil_analysis($id){
		
            //$this->access->check_access(); 
            $data['list']=$this->db->query("select a.*,b.nama from record_oil_analysis a inner join users b on a.user=b.id where a.id='$id'")->row();
            $data['display']='none';
            $this->load->view('print/oil_analysis', $data);
	}
    
      
    //-------------------------------------//
    
	

	
	
}	