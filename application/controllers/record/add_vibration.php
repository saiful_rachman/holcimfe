<?php

class Add_vibration extends CI_controller {

	function __construct()
	{
		parent::__construct();	
                $this->load->model('users_model');
                $this->load->model('form_manager_model');
		$this->load->library('grocery_crud');	
	}
        
        
	
        function insert_log_activity($type,$primarykey,$description){
            $this->load->model('form_manager_model');
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
        function remark_engineer($record_id,$status,$remarks,$type){
            $data=array(
                'record_id'=>$record_id,
                'remarks'=>$remarks,
                'inspector_id'=>$this->session->userdata('users_id'),
                'date_remarks'=>date("Y-m-d H:i:s"),
                'publish'=>'1',
                'type_report'=>$type,
                'status'=>$status
            );
            $this->db->insert('engineer_remark',$data);
        }
        
	function index()
	{
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="re.id";
                }else{
                    $field=$fieldx;
                }
                $config['base_url'] = base_url().'record/add_vibration/index/';
                $config['total_rows'] = $this->db->query("select *,re.id as idx from record re, record_vibration revib, hac where re.inspection_type='VIB' and re.hac=hac.id and $field LIKE '%$val%' group by re.id")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select *,re.id as idx,re.severity_level from record re, record_vibration revib, hac where re.inspection_type='VIB' and re.hac=hac.id and $field LIKE '%$val%' group by re.id order by re.id desc limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('record/add_vibration', $data); 
	}
	
	function add()
	{
                $data['list_plant']=$this->users_model->select_all("master_plant")->result();
		$this->load->view('record/form_add_vibration',$data); 
	}
	
//		function add_post()
//	{
//		/* -- DO NOT CHANGE -- */
//		$user = $this->input->post('user'); // REQUIRE
//		$hac = $this->input->post('hac'); // REQUIRE
//		$remarks = $this->input->post('remarks'); // REQUIRE
//		$recomendation = $this->input->post('recomendation'); // REQUIRE
//		$severity_level = $this->input->post('severity_level'); // REQUIRE
//		
//		$datetime = date('Y-m-d H:i:s'); // REQUIRE
//		$date= date('Y-m-d'); // REQUIRE
//		/* -- END -- */
//		
//		
//		/* INSPECTION TYPE RECORD */
//		/* ubah parameter sesuai dengan record table di Inspection masing2 */
//		$description = $this->input->post('description'); 
//		$engineer_id = $this->input->post('engineer_id');
//		$image_guide = $this->input->post('image_guide');
//		$upload_image_1 = $this->input->post('upload_image_1');
//		$upload_image_2 = $this->input->post('upload_image_2');
//		
//		/* PARAM POST/INSERT TO Table record_vibration */
//		/* PARAM TO INSERT RECORD to table record */
//			$data_post_record = array(
//									'hac' => $hac,
//									'inspection_type' => 'VIB', // Ubah Sesuai code inspection
//									'datetime' => $datetime,
//									'remarks' => $remarks,
//									'recomendation' => $recomendation,
//									'severity_level' => $severity_level,
//									'user' => $user
//									);
//									
//			/* PROCESS TO INSERT */						
//			
//		
//		/* ---- end --- */
//		
//		/* PROCESS INSERT TO Table record_vibration */
//		if($this->db->insert('record',$data_post_record))
//		{
//			$data_post['status'] = 'Success';
//			
//			$record_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
//			
//			
//			$data_post = array(
//						'record_id' => $record_id,
//						'date_vibration' => $date,
//						'description' => $description,
//						'inspector_id' => $user,
//						'engineer_id' => $engineer_id,
//						'image_guide' => $image_guide,
//						'upload_image_1' => $upload_image_1,
//						'upload_image_2' => $upload_image_2
//						);
//			
//			$this->db->insert('record_vibration',$data_post);
//			
//			$inspection_id = mysql_insert_id();
//		
//			$data_update_inspection = array('inspection_id' => $inspection_id);
//			$where = array('id' => $record_id);
//			
//			$this->db->update('record', $data_update_inspection, $where);
//			
//		}
//		else
//		{
//			$data_post['status'] = 'Failed';
//		}
//		
//		$data_json = array('record' => $data_post_record, 'record_detail'=> $data_post);
//		
//		redirect("record/add_vibration/"); 
//			
//					
//	}
        
    function add_post(){
                /* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
                $get_idhac = $this->form_manager_model->get_idhac($hac);
		/* -- END -- */
                
                /* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$description = $this->input->post('description'); 
		//$engineer_id = $this->input->post('engineer_id');
		$image_guide = $this->input->post('image_guide');
		$upload_image_1 = $this->input->post('upload_image_1');
		$upload_image_2 = $this->input->post('upload_image_2');
                $subarea=$this->input->post('subarea');
                //measurement point
                $h1=$this->input->post('1h');
                $h2=$this->input->post('2h');
                $v2=$this->input->post('2v');
                $a2=$this->input->post('2a');
                $h3=$this->input->post('3h');
                $v3=$this->input->post('3v');
                $a3=$this->input->post('3a');
                $h4=$this->input->post('4h');
                $h5=$this->input->post('5h');
                $h6=$this->input->post('6h');
                $a6=$this->input->post('6a');
                $h7=$this->input->post('7h');
                $a7=$this->input->post('7a');
                $h8=$this->input->post('8h');
                $h9=$this->input->post('9h');
		
                
                //insert into table record
                $data_record=array(
                                                'hac'=>$get_idhac,
                                                'inspection_type'=>'VIB',
                                                'inspection_id'=>'',
                                                'datetime'=>$datetime,
                                                'remarks'=>$remarks,
                                                'recomendation'=>$recomendation,
                                                'severity_level'=>$severity_level,
                                                'status'=>'unpublish',
                                                'user'=>$user,
                                                'publish_by'=>''
                                            );
                $this->users_model->insert("record",$data_record);
                                
                //upload image
                $config['upload_path']	= "./media/images/";
                $config['upload_url']	= base_url().'media/images/';
                $config['allowed_types']= '*';
                $config['max_size']     = '2000000';
                $config['max_width']  	= '2000000';
                $config['max_height']  	= '2000000';
                $this->load->library('upload');
                $this->upload->initialize($config);

                if($this->upload->do_upload('image_guide'))
                 {
                $image_data1 = $this->upload->data();    
                 }
                 if($this->upload->do_upload('upload_image_1'))
                 {
                $image_data2= $this->upload->data();    
                 }
                 if($this->upload->do_upload('upload_image_2'))
                 {
                $image_data3 = $this->upload->data();    
                 }
                
                //select max id record
                $idmax=$this->users_model->get_max_table("id","record");
                
                //insert into table recod_vibration
                $data_record_vibration=array(
                                                'record_id'=>$idmax,
                                                'date_vibration'=>$date,
                                                'description'=>$description,
                                                'inspector_id'=>$user,
                                                'area'=>$subarea,
                                                'image_guide'=>$image_data1['file_name'],
                                                'upload_image_1'=>$image_data2['file_name'],
                                                'upload_image_2'=>$image_data3['file_name'],
                                                'x1h'=>$h1,
                                                'x2h'=>$h2,
                                                'x2v'=>$v2,
                                                'x2a'=>$a2,
                                                'x3h'=>$h3,
                                                'x3v'=>$v3,
                                                'x3a'=>$a3,
                                                'x4h'=>$h4,
                                                'x5h'=>$h5,
                                                'x6h'=>$h6,
                                                'x6a'=>$a6,
                                                'x7h'=>$h7,
                                                'x7a'=>$a7,
                                                'x8h'=>$h8,
                                                'x9h'=>$h9,
                                                'sys_create_date'=>date("Y-m-d H:i:s")
                                            );
                $this->users_model->insert("record_vibration",$data_record_vibration);
                
                //Insert into engineer Remak
                $this->remark_engineer($idmax,'NEW','Add New Record Vibration Analysis','Vibration Analysis');
                //insert into activity log
                $this->insert_log_activity("Record Vibration",$idmax,"Create New Record Vibration");
                
                redirect("record/add_vibration/"); 
    }
	
    function edit($id){
	
//        $data['id'] = $id;
//	
//	$row = $this->db->query('select * from record re, record_vibration revib  where re.Inspection_id="'.$id.'" and re.Inspection_type="VIB"')->row();
//
//	$data['default']['hac'] = $row->hac; 
//	$data['default']['status'] = $row->status; 
//	$data['default']['severity_level'] = $row->severity_level;
//	$data['default']['description'] = $row->description; 
//	$data['default']['remarks'] = $row->remarks; 
//	$data['default']['recomendation'] = $row->recomendation; 
//        $data['default']['image_guide'] = $row->image_guide; 
//        $data['default']['upload_image_1'] = $row->upload_image_1; 
//        $data['default']['upload_image_2'] = $row->upload_image_2; 
          $data['list_plant']=$this->users_model->select_all("master_plant")->result();
	  $data['list']=$this->db->query("select a.*,b.*,c.hac_code from record a inner join record_vibration b on b.record_id=a.id inner join hac c on a.hac=c.id where a.id='$id'")->row();
	  $this->load->view('record/form_edit_vibration', $data);
	
	}
	
	
	function autocomplete_hac(){
		$keyword = $this->input->post("term");
		$result = $this->db->query('select * from hac where hac_code like "'.$keyword.'%" LIMIT 10')->result_array();
		foreach($result as $row){
			$data[] = array('label'=>$row['hac_code'], 'value'=>$row['hac_code']);
		}
		echo json_encode($data);
	}
	
	
	function autocomplete_hac_detail(){
		$hac = $this->input->post("term");
		
		$main = $this->db->query('select * from hac where hac_code="'.$hac.'"')->row_array();
		$data = array(
			"main" => $main
		);
		
		echo json_encode($data);
	}
	
	function edit_post(){
	
            $id = $this->input->post("id");
            $user = $this->input->post('user'); // REQUIRE
            $hac = $this->input->post('hac'); // REQUIRE
            $remarks = $this->input->post('remarks'); // REQUIRE
            $recomendation = $this->input->post('recomendation'); // REQUIRE
            $severity_level = $this->input->post('severity_level'); // REQUIRE

            $datetime = date('Y-m-d H:i:s'); // REQUIRE
            $date= date('Y-m-d'); // REQUIRE
            $get_idhac = $this->form_manager_model->get_idhac($hac);
            /* -- END -- */

            /* INSPECTION TYPE RECORD */
            /* ubah parameter sesuai dengan record table di Inspection masing2 */
            $description = $this->input->post('description'); 
            //$engineer_id = $this->input->post('engineer_id');
            $image_guide_hidden = $this->input->post('image_guide_hidden');
            $upload_image_1_hidden = $this->input->post('upload_image_1_hidden');
            $upload_image_2_hidden = $this->input->post('upload_image_2_hidden');
            $subarea=$this->input->post('subarea');
            
            //measurement point
                $h1=$this->input->post('1h');
                $h2=$this->input->post('2h');
                $v2=$this->input->post('2v');
                $a2=$this->input->post('2a');
                $h3=$this->input->post('3h');
                $v3=$this->input->post('3v');
                $a3=$this->input->post('3a');
                $h4=$this->input->post('4h');
                $h5=$this->input->post('5h');
                $h6=$this->input->post('6h');
                $a6=$this->input->post('6a');
                $h7=$this->input->post('7h');
                $a7=$this->input->post('7a');
                $h8=$this->input->post('8h');
                $h9=$this->input->post('9h');


            //insert into table record
            $data_record=array(
                'hac'=>$get_idhac,
                'inspection_type'=>'VIB',
                'inspection_id'=>'',
                'datetime'=>$datetime,
                'remarks'=>$remarks,
                'recomendation'=>$recomendation,
                'severity_level'=>$severity_level,
                'status'=>'unpublish',
                'user'=>$user,
                'publish_by'=>''
            );
            $this->users_model->update("record",$id,"id",$data_record);

            //upload image
            $config['upload_path']	= "./media/images/";
            $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';
            $config['max_size']     = '2000000';
            $config['max_width']  	= '2000000';
            $config['max_height']  	= '2000000';
            $this->load->library('upload');
            $this->upload->initialize($config);

             if($this->upload->do_upload('image_guide'))
             {
                $image_data1 = $this->upload->data();    
                $img1 = $image_data1['file_name'];
             }else{
                $img1=$image_guide_hidden;
             }
             
             if($this->upload->do_upload('upload_image_1'))
             {
                $image_data2= $this->upload->data();    
                $img2 = $image_data2['file_name'];
             }else{
                $img2 = $upload_image_1_hidden;
             }
             if($this->upload->do_upload('upload_image_2'))
             {
                $image_data3 = $this->upload->data();    
                $img3 = $image_data3['file_name'];
             }else{
                $img3 = $upload_image_2_hidden;
             }

            //select max id record
            //$idmax=$this->users_model->get_max_table("id","record");

            //insert into table recod_vibration
            $data_record_vibration=array(
                                            'record_id'=>$id,
                                            'date_vibration'=>$date,
                                            'description'=>$description,
                                            'inspector_id'=>$user,
                                            'area'=>$subarea,
                                            'image_guide'=>$img1,
                                            'upload_image_1'=>$img2,
                                            'upload_image_2'=>$img3,
                                            'x1h'=>$h1,
                                            'x2h'=>$h2,
                                            'x2v'=>$v2,
                                            'x2a'=>$a2,
                                            'x3h'=>$h3,
                                            'x3v'=>$v3,
                                            'x3a'=>$a3,
                                            'x4h'=>$h4,
                                            'x5h'=>$h5,
                                            'x6h'=>$h6,
                                            'x6a'=>$a6,
                                            'x7h'=>$h7,
                                            'x7a'=>$a7,
                                            'x8h'=>$h8,
                                            'x9h'=>$h9
                                        );
            $this->users_model->update("record_vibration",$id,"record_id",$data_record_vibration);
            
            //Insert into engineer Remak
            $this->remark_engineer($id,'UPDATE','Update Record Vibration Analysis','Vibration Analysis');
            
            //insert into activity log
            $this->insert_log_activity("Record Vibration",$id,"Update Record Vibration");
                
            redirect("record/add_vibration/"); 
	
	}
	
        function delete($id){
            $this->users_model->delete("record",$id,"id");
            $this->users_model->delete("record_vibration",$id,"record_id");
            
            //insert into activity log
            $this->insert_log_activity("Record Vibration",$id,"Delete Record Vibration");
            redirect("record/add_vibration/"); 
        }
	
	
	
	
}	

	