<?php

class Add_thickness extends CI_controller {

	function __construct()
	{
		parent::__construct();
                $this->load->model('users_model');
                $this->load->model('form_manager_model');
		
		$this->load->library('grocery_crud');	
	}
	
        function insert_log_activity($type,$primarykey,$description){
            $this->load->model('form_manager_model');
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
        function remark_engineer($record_id,$status,$remarks,$type){
            $data=array(
                'record_id'=>$record_id,
                'remarks'=>$remarks,
                'inspector_id'=>$this->session->userdata('users_id'),
                'date_remarks'=>date("Y-m-d H:i:s"),
                'publish'=>'1',
                'type_report'=>$type,
                'status'=>$status
            );
            $this->db->insert('engineer_remark',$data);
        }

	function index()
	{
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="re.id";
                }else{
                    $field=$fieldx;
                }
		$config['base_url'] = base_url().'record/add_thickness/index/';
                $config['total_rows'] = $this->db->query("select * from record re, record_thickness rethick, hac where re.inspection_type LIKE 'THICK%' and re.hac=hac.id and $field LIKE '%$val%' group by re.id")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select *,re.id as idx,re.severity_level from record re, record_thickness rethick, hac where re.inspection_type LIKE 'THICK%' and re.hac=hac.id and $field LIKE '%$val%' group by re.id order by re.id desc limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('record/add_thickness', $data); 
	}
	
	function add()
	{
                $data['list_plant']=$this->users_model->select_all("master_plant")->result();
		$this->load->view('record/form_add_thickness',$data); 
	}
	
		function add_post()
	{
                $user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
                $datetime = date('Y-m-d H:i:s'); // REQUIRE
        	$date= date('Y-m-d'); // REQUIRE
                $subarea=$this->input->post('subarea');
                $get_idhac = $this->form_manager_model->get_idhac($hac);
                $type=$this->input->post('thickness_type');
                $description=$this->input->post('description');
                $test_object=$this->input->post('test_object');
                $model=$this->input->post('model');
                $couplant=$this->input->post('couplant');
                $probe_type=$this->input->post('probe_type');
                $frequency=$this->input->post('frequency');
                $thickness=$this->input->post('thickness');
                $equipment=$this->input->post('equipment');
                
                if($type=='general'){
                    $typex='THICK_GENERAL';
                }
                else if($type=='stack'){
                    $typex='THICK_STACK';
                }else{
                    $typex='THICK_KILN';
                }
                //masukkan data ke table record
                $data_record=array(
                    'hac'=>$get_idhac,
                    'inspection_type'=>$typex,
                    'datetime'=>$datetime,
                    'remarks'=>$remarks,
                    'recomendation'=>$recomendation,
                    'severity_level'=>$severity_level,
                    'status'=>'unpublish',
                    'user'=>$user
                );
                $this->users_model->insert("record",$data_record);
                
                //upload file
                $config['upload_path']	= "./media/pdf/";
                $config['upload_url']	= base_url().'media/pdf/';
                $config['allowed_types']= '*';
                $config['max_size']     = '2000000';
                $config['max_width']  	= '2000000';
                $config['max_height']  	= '2000000';
                $this->load->library('upload');
                $this->upload->initialize($config);

                if($this->upload->do_upload('upload_file'))
                 {
                $image_data1 = $this->upload->data();    
                 }
                 
                //get max id form record table
                $idmax=$this->users_model->get_max_table("id","record");
                
                    $dtp1="";$dtp2="";$dtp3="";$dtp4="";$dtp5="";$dtp6="";$dtp7="";$dtp8="";$dtp9="";$dtp10="";
                    $dtp11="";$dtp12="";$dtp13="";$dtp14="";$dtp15="";$dtp16="";$dtp17="";$dtp18="";$dtp19="";$dtp20="";
                    $dtp21="";$dtp22="";$dtp23="";$dtp24="";$dtp25="";$dtp26="";$dtp27="";$dtp28="";$dtp29="";$dtp30="";
                    $dtp31="";$dtp32="";$dtp33="";$dtp34="";$dtp35="";$dtp36="";$dtp37="";$dtp38="";$dtp39="";$dtp40="";
                    $dtp41="";$dtp42="";$dtp43="";$dtp44="";$dtp45="";$dtp46="";$dtp47="";$dtp48="";$dtp49="";$dtp50="";
                    $dtp51="";$dtp52="";$dtp53="";$dtp54="";$dtp55="";$dtp56="";$dtp57="";$dtp58="";$dtp59="";$dtp60="";
                    $dtp61="";$dtp62="";$dtp63="";$dtp64="";$dtp65="";$dtp66="";$dtp67="";$dtp68="";$dtp69="";$dtp70="";
                    $dtp71="";$dtp72="";$dtp73="";$dtp74="";$dtp75="";$dtp76="";$dtp77="";$dtp78="";$dtp79="";$dtp80="";
                if($type=='general' || $type=='stack'){
                    $point1=$this->input->post('point1');
                        $dtp1="";
                        foreach($point1 as $pt1){
                            $dtp1 .=$pt1.",";
                        }
                    $point2=$this->input->post('point2');
                        $dtp2="";
                        foreach($point2 as $pt2){
                            $dtp2 .=$pt2.",";
                        }
                    $point3=$this->input->post('point3');
                        $dtp3="";
                        foreach($point3 as $pt3){
                            $dtp3 .=$pt3.",";
                        }
                    $point4=$this->input->post('point4');
                        $dtp4="";
                        foreach($point4 as $pt4){
                            $dtp4 .=$pt4.",";
                        } 
                    $point5=$this->input->post('point5');
                        $dtp5="";
                        foreach($point5 as $pt5){
                            $dtp5 .=$pt5.",";
                        }
                    $point6=$this->input->post('point6');
                        $dtp6="";
                        foreach($point6 as $pt6){
                            $dtp6 .=$pt6.",";
                        }
                    $point7=$this->input->post('point7');
                        $dtp7="";
                        foreach($point7 as $pt7){
                            $dtp7 .=$pt7.",";
                        }
                    $point8=$this->input->post('point8');
                        $dtp8="";
                        foreach($point8 as $pt8){
                            $dtp8 .=$pt8.",";
                        }
                    $point9=$this->input->post('point9');
                        $dtp9="";
                        foreach($point9 as $pt9){
                            $dtp9 .=$pt9.",";
                        }   
                    $point10=$this->input->post('point10');
                        $dtp10="";
                        foreach($point10 as $pt10){
                            $dtp10 .=$pt10.",";
                        }
                    $point11=$this->input->post('point11');
                        $dtp11="";
                        foreach($point11 as $pt11){
                            $dtp11 .=$pt11.",";
                        }
                    $point12=$this->input->post('point12');
                        $dtp12="";
                        foreach($point12 as $pt12){
                            $dtp12 .=$pt12.",";
                        }
                    $point13=$this->input->post('point13');
                        $dtp13="";
                        foreach($point13 as $pt13){
                            $dtp13 .=$pt13.",";
                        }
                    $point14=$this->input->post('point14');
                        $dtp14="";
                        foreach($point14 as $pt14){
                            $dtp14 .=$pt14.",";
                        }
                    $point15=$this->input->post('point15');
                        $dtp15="";
                        foreach($point15 as $pt15){
                            $dtp15 .=$pt15.",";
                        }
                    $point16=$this->input->post('point16');
                        $dtp16="";
                        foreach($point16 as $pt16){
                            $dtp16 .=$pt16.",";
                        }
                        
                    $data_rec_thick=array(
                        'record_id'=>$idmax,
                        'date'=>$date,
                        'thick_type'=>$type,
                        'form_type'=>'16',
                        'test_object'=>$test_object,
                        'model'=>$model,
                        'couplant'=>$couplant,
                        'probe_type'=>$probe_type,
                        'frequency'=>$frequency,
                        'thickness'=>$thickness,
                        'area'=>$subarea,
                        'equipment'=>$equipment,
                        'upload_file'=>$image_data1['file_name'],
                        'point_1'=>chop($dtp1,','),
                        'point_2'=>chop($dtp2,','),
                        'point_3'=>chop($dtp3,','),
                        'point_4'=>chop($dtp4,','),
                        'point_5'=>chop($dtp5,','),
                        'point_6'=>chop($dtp6,','),
                        'point_7'=>chop($dtp7,','),
                        'point_8'=>chop($dtp8,','),
                        'point_9'=>chop($dtp9,','),
                        'point_10'=>chop($dtp10,','),
                        'point_11'=>chop($dtp11,','),
                        'point_12'=>chop($dtp12,','),
                        'point_13'=>chop($dtp13,','),
                        'point_14'=>chop($dtp14,','),
                        'point_15'=>chop($dtp15,','),
                        'point_16'=>chop($dtp16,',')
                        );
                    $this->users_model->insert("record_thickness",$data_rec_thick);
                    
                }else if($type=='kiln'){
                    $point1=$this->input->post('point1');
                        $dtp1="";
                        foreach($point1 as $pt1){
                            $dtp1 .=$pt1.",";
                        }
                    $point2=$this->input->post('point2');
                        $dtp2="";
                        foreach($point2 as $pt2){
                            $dtp2 .=$pt2.",";
                        }
                    $point3=$this->input->post('point3');
                        $dtp3="";
                        foreach($point3 as $pt3){
                            $dtp3 .=$pt3.",";
                        }
                    $point4=$this->input->post('point4');
                        $dtp4="";
                        foreach($point4 as $pt4){
                            $dtp4 .=$pt4.",";
                        } 
                    $point5=$this->input->post('point5');
                        $dtp5="";
                        foreach($point5 as $pt5){
                            $dtp5 .=$pt5.",";
                        }
                    $point6=$this->input->post('point6');
                        $dtp6="";
                        foreach($point6 as $pt6){
                            $dtp6 .=$pt6.",";
                        }
                    $point7=$this->input->post('point7');
                        $dtp7="";
                        foreach($point7 as $pt7){
                            $dtp7 .=$pt7.",";
                        }
                    $point8=$this->input->post('point8');
                        $dtp8="";
                        foreach($point8 as $pt8){
                            $dtp8 .=$pt8.",";
                        }
                    $point9=$this->input->post('point9');
                        $dtp9="";
                        foreach($point9 as $pt9){
                            $dtp9 .=$pt9.",";
                        }   
                    $point10=$this->input->post('point10');
                        $dtp10="";
                        foreach($point10 as $pt10){
                            $dtp10 .=$pt10.",";
                        }
                        
                    $point11=$this->input->post('point11');
                        $dtp11="";
                        foreach($point11 as $pt11){
                            $dtp11 .=$pt11.",";
                        }
                    $point12=$this->input->post('point12');
                        $dtp12="";
                        foreach($point12 as $pt12){
                            $dtp12 .=$pt12.",";
                        }
                    $point13=$this->input->post('point13');
                        $dtp13="";
                        foreach($point13 as $pt13){
                            $dtp13 .=$pt13.",";
                        }
                    $point14=$this->input->post('point14');
                        $dtp14="";
                        foreach($point14 as $pt14){
                            $dtp14 .=$pt14.",";
                        }
                    $point15=$this->input->post('point15');
                        $dtp15="";
                        foreach($point15 as $pt15){
                            $dtp15 .=$pt15.",";
                        }
                    $point16=$this->input->post('point16');
                        $dtp16="";
                        foreach($point16 as $pt16){
                            $dtp16 .=$pt16.",";
                        }
                    $point17=$this->input->post('point17');
                        $dtp17="";
                        foreach($point17 as $pt17){
                            $dtp17 .=$pt17.",";
                        }
                    $point18=$this->input->post('point18');
                        $dtp18="";
                        foreach($point18 as $pt18){
                            $dtp18 .=$pt18.",";
                        }
                    $point19=$this->input->post('point19');
                        $dtp19="";
                        foreach($point19 as $pt19){
                            $dtp19 .=$pt19.",";
                        }
                    $point20=$this->input->post('point20');
                        $dtp20="";
                        foreach($point20 as $pt20){
                            $dtp20 .=$pt20.",";
                        }
                        
                    $point21=$this->input->post('point21');
                        $dtp21="";
                        foreach($point21 as $pt21){
                            $dtp21 .=$pt21.",";
                        }
                    $point22=$this->input->post('point22');
                        $dtp22="";
                        foreach($point22 as $pt22){
                            $dtp22 .=$pt22.",";
                        }
                    $point23=$this->input->post('point23');
                        $dtp23="";
                        foreach($point23 as $pt23){
                            $dtp23 .=$pt23.",";
                        }
                    $point24=$this->input->post('point24');
                        $dtp24="";
                        foreach($point24 as $pt24){
                            $dtp24 .=$pt24.",";
                        }
                    $point25=$this->input->post('point25');
                        $dtp25="";
                        foreach($point25 as $pt25){
                            $dtp25 .=$pt25.",";
                        }
                    $point26=$this->input->post('point26');
                        $dtp26="";
                        foreach($point26 as $pt26){
                            $dtp26 .=$pt26.",";
                        }
                    $point27=$this->input->post('point27');
                        $dtp27="";
                        foreach($point27 as $pt27){
                            $dtp27 .=$pt27.",";
                        }
                    $point28=$this->input->post('point28');
                        $dtp28="";
                        foreach($point28 as $pt28){
                            $dtp28 .=$pt28.",";
                        }
                    $point29=$this->input->post('point29');
                        $dtp29="";
                        foreach($point29 as $pt29){
                            $dtp29 .=$pt29.",";
                        }
                    $point30=$this->input->post('point30');
                        $dtp30="";
                        foreach($point30 as $pt30){
                            $dtp30 .=$pt30.",";
                        }
                        
                    $point31=$this->input->post('point31');
                        $dtp31="";
                        foreach($point31 as $pt31){
                            $dtp31 .=$pt31.",";
                        }
                    $point32=$this->input->post('point32');
                        $dtp32="";
                        foreach($point32 as $pt32){
                            $dtp32 .=$pt32.",";
                        }
                    $point33=$this->input->post('point33');
                        $dtp33="";
                        foreach($point33 as $pt33){
                            $dtp33 .=$pt33.",";
                        }
                    $point34=$this->input->post('point34');
                        $dtp34="";
                        foreach($point34 as $pt34){
                            $dtp34 .=$pt34.",";
                        }
                    $point35=$this->input->post('point35');
                        $dtp35="";
                        foreach($point35 as $pt35){
                            $dtp35 .=$pt35.",";
                        }
                    $point36=$this->input->post('point36');
                        $dtp36="";
                        foreach($point36 as $pt36){
                            $dtp36 .=$pt36.",";
                        }
                    $point37=$this->input->post('point37');
                        $dtp37="";
                        foreach($point37 as $pt37){
                            $dtp37 .=$pt37.",";
                        }
                    $point38=$this->input->post('point38');
                        $dtp38="";
                        foreach($point38 as $pt38){
                            $dtp38 .=$pt38.",";
                        }
                    $point39=$this->input->post('point39');
                        $dtp39="";
                        foreach($point39 as $pt39){
                            $dtp39 .=$pt39.",";
                        }
                    $point40=$this->input->post('point40');
                        $dtp40="";
                        foreach($point40 as $pt40){
                            $dtp40 .=$pt40.",";
                        }
                        
                    $point41=$this->input->post('point41');
                        $dtp41="";
                        foreach($point41 as $pt41){
                            $dtp41 .=$pt41.",";
                        }
                    $point42=$this->input->post('point42');
                        $dtp42="";
                        foreach($point42 as $pt42){
                            $dtp42 .=$pt42.",";
                        }
                    $point43=$this->input->post('point43');
                        $dtp43="";
                        foreach($point43 as $pt43){
                            $dtp43 .=$pt43.",";
                        }
                    $point44=$this->input->post('point44');
                        $dtp44="";
                        foreach($point44 as $pt44){
                            $dtp44 .=$pt44.",";
                        }
                    $point45=$this->input->post('point45');
                        $dtp45="";
                        foreach($point45 as $pt45){
                            $dtp45 .=$pt45.",";
                        }
                    $point46=$this->input->post('point46');
                        $dtp46="";
                        foreach($point46 as $pt46){
                            $dtp46 .=$pt46.",";
                        }
                    $point47=$this->input->post('point47');
                        $dtp47="";
                        foreach($point47 as $pt47){
                            $dtp47 .=$pt47.",";
                        }
                    $point48=$this->input->post('point48');
                        $dtp48="";
                        foreach($point48 as $pt48){
                            $dtp48 .=$pt48.",";
                        }
                    $point49=$this->input->post('point49');
                        $dtp49="";
                        foreach($point49 as $pt49){
                            $dtp49 .=$pt49.",";
                        }
                    $point50=$this->input->post('point50');
                        $dtp50="";
                        foreach($point50 as $pt50){
                            $dtp50 .=$pt50.",";
                        }
                        
                    $point51=$this->input->post('point51');
                        $dtp51="";
                        foreach($point51 as $pt51){
                            $dtp51 .=$pt51.",";
                        }
                    $point52=$this->input->post('point52');
                        $dtp52="";
                        foreach($point52 as $pt52){
                            $dtp52 .=$pt52.",";
                        }
                    $point53=$this->input->post('point53');
                        $dtp53="";
                        foreach($point53 as $pt53){
                            $dtp53 .=$pt53.",";
                        }
                    $point54=$this->input->post('point54');
                        $dtp54="";
                        foreach($point54 as $pt54){
                            $dtp54 .=$pt54.",";
                        }
                    $point55=$this->input->post('point55');
                        $dtp55="";
                        foreach($point55 as $pt55){
                            $dtp55 .=$pt55.",";
                        }
                    $point56=$this->input->post('point56');
                        $dtp56="";
                        foreach($point56 as $pt56){
                            $dtp56 .=$pt56.",";
                        }
                    $point57=$this->input->post('point57');
                        $dtp57="";
                        foreach($point57 as $pt57){
                            $dtp57 .=$pt57.",";
                        }
                    $point58=$this->input->post('point58');
                        $dtp58="";
                        foreach($point58 as $pt58){
                            $dtp58 .=$pt58.",";
                        }
                    $point59=$this->input->post('point59');
                        $dtp59="";
                        foreach($point59 as $pt59){
                            $dtp59 .=$pt59.",";
                        }
                    $point60=$this->input->post('point60');
                        $dtp60="";
                        foreach($point60 as $pt60){
                            $dtp60 .=$pt60.",";
                        }
                        
                    $point61=$this->input->post('point61');
                        $dtp61="";
                        foreach($point61 as $pt61){
                            $dtp61 .=$pt61.",";
                        }
                    $point62=$this->input->post('point62');
                        $dtp62="";
                        foreach($point62 as $pt62){
                            $dtp62 .=$pt62.",";
                        }
                    $point63=$this->input->post('point63');
                        $dtp63="";
                        foreach($point63 as $pt63){
                            $dtp63 .=$pt13.",";
                        }
                    $point64=$this->input->post('point64');
                        $dtp64="";
                        foreach($point64 as $pt64){
                            $dtp64 .=$pt64.",";
                        }
                    $point65=$this->input->post('point65');
                        $dtp65="";
                        foreach($point65 as $pt65){
                            $dtp65 .=$pt65.",";
                        }
                    $point66=$this->input->post('point66');
                        $dtp66="";
                        foreach($point66 as $pt66){
                            $dtp66 .=$pt66.",";
                        }
                    $point67=$this->input->post('point67');
                        $dtp67="";
                        foreach($point67 as $pt67){
                            $dtp67 .=$pt67.",";
                        }
                    $point68=$this->input->post('point68');
                        $dtp68="";
                        foreach($point68 as $pt68){
                            $dtp68 .=$pt68.",";
                        }
                    $point69=$this->input->post('point69');
                        $dtp69="";
                        foreach($point69 as $pt69){
                            $dtp69 .=$pt69.",";
                        }
                    $point70=$this->input->post('point70');
                        $dtp70="";
                        foreach($point70 as $pt70){
                            $dtp70 .=$pt70.",";
                        }
                        
                    $point71=$this->input->post('point71');
                        $dtp71="";
                        foreach($point71 as $pt71){
                            $dtp71 .=$pt71.",";
                        }
                    $point72=$this->input->post('point72');
                        $dtp72="";
                        foreach($point72 as $pt72){
                            $dtp72 .=$pt72.",";
                        }
                    $point73=$this->input->post('point73');
                        $dtp73="";
                        foreach($point73 as $pt73){
                            $dtp73 .=$pt73.",";
                        }
                    $point74=$this->input->post('point74');
                        $dtp74="";
                        foreach($point74 as $pt74){
                            $dtp74 .=$pt74.",";
                        }
                    $point75=$this->input->post('point75');
                        $dtp75="";
                        foreach($point75 as $pt75){
                            $dtp75 .=$pt75.",";
                        }
                    $point76=$this->input->post('point76');
                        $dtp76="";
                        foreach($point76 as $pt76){
                            $dtp76 .=$pt76.",";
                        }
                    $point77=$this->input->post('point77');
                        $dtp77="";
                        foreach($point77 as $pt77){
                            $dtp77 .=$pt77.",";
                        }
                    $point78=$this->input->post('point78');
                        $dtp78="";
                        foreach($point78 as $pt78){
                            $dtp78 .=$pt78.",";
                        }
                    $point79=$this->input->post('point79');
                        $dtp79="";
                        foreach($point79 as $pt79){
                            $dtp79 .=$pt79.",";
                        }
                    $point80=$this->input->post('point80');
                        $dtp80="";
                        foreach($point80 as $pt80){
                            $dtp80 .=$pt80.",";
                        }
                $data_rec_thick=array(
                        'record_id'=>$idmax,
                        'date'=>$date,
                        'thick_type'=>$type,
                        'form_type'=>'16',
                        'test_object'=>$test_object,
                        'model'=>$model,
                        'couplant'=>$couplant,
                        'probe_type'=>$probe_type,
                        'frequency'=>$frequency,
                        'thickness'=>$thickness,
                        'area'=>$subarea,
                        'equipment'=>$equipment,
                        'upload_file'=>$image_data1['file_name'],
                        'point_1'=>chop($dtp1,','),
                        'point_2'=>chop($dtp2,','),
                        'point_3'=>chop($dtp3,','),
                        'point_4'=>chop($dtp4,','),
                        'point_5'=>chop($dtp5,','),
                        'point_6'=>chop($dtp6,','),
                        'point_7'=>chop($dtp7,','),
                        'point_8'=>chop($dtp8,','),
                        'point_9'=>chop($dtp9,','),
                        'point_10'=>chop($dtp10,','),
                    
                        'point_11'=>chop($dtp11,','),
                        'point_12'=>chop($dtp12,','),
                        'point_13'=>chop($dtp13,','),
                        'point_14'=>chop($dtp14,','),
                        'point_15'=>chop($dtp15,','),
                        'point_16'=>chop($dtp16,','),
                        'point_17'=>chop($dtp17,','),
                        'point_18'=>chop($dtp18,','),
                        'point_19'=>chop($dtp19,','),
                        'point_20'=>chop($dtp20,','),
                    
                        'point_21'=>chop($dtp21,','),
                        'point_22'=>chop($dtp22,','),
                        'point_23'=>chop($dtp23,','),
                        'point_24'=>chop($dtp24,','),
                        'point_25'=>chop($dtp25,','),
                        'point_26'=>chop($dtp26,','),
                        'point_27'=>chop($dtp27,','),
                        'point_28'=>chop($dtp28,','),
                        'point_29'=>chop($dtp29,','),
                        'point_30'=>chop($dtp30,','),
                    
                        'point_31'=>chop($dtp31,','),
                        'point_32'=>chop($dtp32,','),
                        'point_33'=>chop($dtp33,','),
                        'point_34'=>chop($dtp34,','),
                        'point_35'=>chop($dtp35,','),
                        'point_36'=>chop($dtp36,','),
                        'point_37'=>chop($dtp37,','),
                        'point_38'=>chop($dtp38,','),
                        'point_39'=>chop($dtp39,','),
                        'point_40'=>chop($dtp40,','),
                    
                        'point_41'=>chop($dtp41,','),
                        'point_42'=>chop($dtp42,','),
                        'point_43'=>chop($dtp43,','),
                        'point_44'=>chop($dtp44,','),
                        'point_45'=>chop($dtp45,','),
                        'point_46'=>chop($dtp46,','),
                        'point_47'=>chop($dtp47,','),
                        'point_48'=>chop($dtp48,','),
                        'point_49'=>chop($dtp49,','),
                        'point_50'=>chop($dtp50,','),
                    
                        'point_51'=>chop($dtp51,','),
                        'point_52'=>chop($dtp52,','),
                        'point_53'=>chop($dtp53,','),
                        'point_54'=>chop($dtp54,','),
                        'point_55'=>chop($dtp55,','),
                        'point_56'=>chop($dtp56,','),
                        'point_57'=>chop($dtp57,','),
                        'point_58'=>chop($dtp58,','),
                        'point_59'=>chop($dtp59,','),
                        'point_60'=>chop($dtp60,','),
                    
                        'point_51'=>chop($dtp51,','),
                        'point_52'=>chop($dtp52,','),
                        'point_53'=>chop($dtp53,','),
                        'point_54'=>chop($dtp54,','),
                        'point_55'=>chop($dtp55,','),
                        'point_56'=>chop($dtp56,','),
                        'point_57'=>chop($dtp57,','),
                        'point_58'=>chop($dtp58,','),
                        'point_59'=>chop($dtp59,','),
                        'point_60'=>chop($dtp60,','),
                    
                        'point_61'=>chop($dtp61,','),
                        'point_62'=>chop($dtp62,','),
                        'point_63'=>chop($dtp63,','),
                        'point_64'=>chop($dtp64,','),
                        'point_65'=>chop($dtp65,','),
                        'point_66'=>chop($dtp66,','),
                        'point_67'=>chop($dtp67,','),
                        'point_68'=>chop($dtp68,','),
                        'point_69'=>chop($dtp69,','),
                        'point_70'=>chop($dtp70,','),
                    
                        'point_71'=>chop($dtp71,','),
                        'point_72'=>chop($dtp72,','),
                        'point_73'=>chop($dtp63,','),
                        'point_74'=>chop($dtp74,','),
                        'point_75'=>chop($dtp75,','),
                        'point_76'=>chop($dtp76,','),
                        'point_77'=>chop($dtp77,','),
                        'point_78'=>chop($dtp78,','),
                        'point_79'=>chop($dtp79,','),
                        'point_80'=>chop($dtp80,',')
                    
                        );
                    $this->users_model->insert("record_thickness",$data_rec_thick);
              
                }
                    //Insert into engineer Remak
                    $this->remark_engineer($idmax,'NEW','Add New Record Thickness Measurement','Thickness Measurement');
                    //insert into activity log
                    $this->insert_log_activity("Record Thickness Measurement",$idmax,"Create New Record Thickness Measurement");
                    redirect("record/add_thickness/"); 
            }
	
	
	function edit($id){
	
          $data['list_plant']=$this->users_model->select_all("master_plant")->result();
	  $data['list']=$this->db->query("select a.*,b.*,c.hac_code from record a inner join record_thickness b on b.record_id=a.id inner join hac c on a.hac=c.id where a.id='$id'")->row();
          $point=$this->db->query("select a.*,b.*,c.hac_code from record a inner join record_thickness b on b.record_id=a.id inner join hac c on a.hac=c.id where a.id='$id'")->row();
	  $data['point1']=$point->point_1;
          $data['point2']=$point->point_2;
          $data['point3']=$point->point_3;
          $data['point4']=$point->point_4;
          $data['point5']=$point->point_5;
          $data['point6']=$point->point_6;
          $data['point7']=$point->point_7;
          $data['point8']=$point->point_8;
          $data['point9']=$point->point_9;
          $data['point10']=$point->point_10;
          
          $data['point11']=$point->point_11;
          $data['point12']=$point->point_12;
          $data['point13']=$point->point_13;
          $data['point14']=$point->point_14;
          $data['point15']=$point->point_15;
          $data['point16']=$point->point_16;
          $data['point17']=$point->point_17;
          $data['point18']=$point->point_18;
          $data['point19']=$point->point_19;
          $data['point20']=$point->point_20;
          
          $data['point21']=$point->point_21;
          $data['point22']=$point->point_22;
          $data['point23']=$point->point_23;
          $data['point24']=$point->point_24;
          $data['point25']=$point->point_25;
          $data['point26']=$point->point_26;
          $data['point27']=$point->point_27;
          $data['point28']=$point->point_28;
          $data['point29']=$point->point_29;
          $data['point30']=$point->point_30;
          
          $data['point31']=$point->point_31;
          $data['point32']=$point->point_32;
          $data['point33']=$point->point_33;
          $data['point34']=$point->point_34;
          $data['point35']=$point->point_35;
          $data['point36']=$point->point_36;
          $data['point37']=$point->point_37;
          $data['point38']=$point->point_38;
          $data['point39']=$point->point_39;
          $data['point40']=$point->point_40;
          
          $data['point41']=$point->point_41;
          $data['point42']=$point->point_42;
          $data['point43']=$point->point_43;
          $data['point44']=$point->point_44;
          $data['point45']=$point->point_45;
          $data['point46']=$point->point_46;
          $data['point47']=$point->point_47;
          $data['point48']=$point->point_48;
          $data['point49']=$point->point_49;
          $data['point50']=$point->point_50;
          
          $data['point51']=$point->point_51;
          $data['point52']=$point->point_52;
          $data['point53']=$point->point_53;
          $data['point54']=$point->point_54;
          $data['point55']=$point->point_55;
          $data['point56']=$point->point_56;
          $data['point57']=$point->point_57;
          $data['point58']=$point->point_58;
          $data['point59']=$point->point_59;
          $data['point60']=$point->point_60;
          
          $data['point61']=$point->point_61;
          $data['point62']=$point->point_62;
          $data['point63']=$point->point_63;
          $data['point64']=$point->point_64;
          $data['point65']=$point->point_65;
          $data['point66']=$point->point_66;
          $data['point67']=$point->point_67;
          $data['point68']=$point->point_68;
          $data['point69']=$point->point_69;
          $data['point70']=$point->point_60;
         
          $data['point71']=$point->point_71;
          $data['point72']=$point->point_72;
          $data['point73']=$point->point_73;
          $data['point74']=$point->point_74;
          $data['point75']=$point->point_75;
          $data['point76']=$point->point_76;
          $data['point77']=$point->point_77;
          $data['point78']=$point->point_78;
          $data['point79']=$point->point_79;
          $data['point80']=$point->point_80;
          
          $this->load->view('record/form_edit_thickness', $data);
	
	}
	
	
	function autocomplete_hac(){
		$keyword = $this->input->post("term");
		$result = $this->db->query('select * from hac where hac_code like "'.$keyword.'%" LIMIT 10')->result_array();
		foreach($result as $row){
			$data[] = array('label'=>$row['hac_code'], 'value'=>$row['hac_code']);
		}
		echo json_encode($data);
	}
	
	
	function autocomplete_hac_detail(){
		$hac = $this->input->post("term");
		
		$main = $this->db->query('select * from hac where hac_code="'.$hac.'"')->row_array();
		$data = array(
			"main" => $main
		);
		
		echo json_encode($data);
	}
	
	function edit_post(){
	
                $id = $this->input->post("id");
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
                $datetime = date('Y-m-d H:i:s'); // REQUIRE
        	$date= date('Y-m-d'); // REQUIRE
                $subarea=$this->input->post('subarea');
                $get_idhac = $this->form_manager_model->get_idhac($hac);
                $type=$this->input->post('thickness_type');
                $equipment=$this->input->post('equipment');
                $test_object=$this->input->post('test_object');
                $model=$this->input->post('model');
                $couplant=$this->input->post('couplant');
                $probe_type=$this->input->post('probe_type');
                $frequency=$this->input->post('frequency');
                $thickness=$this->input->post('thickness');
                $upload_file_hidden=$this->input->post('upload_file_hidden');
                
                if($type=='general'){
                    $typex='THICK_GENERAL';
                }
                else if($type=='stack'){
                    $typex='THICK_STACK';
                }else{
                    $typex='THICK_KILN';
                }
                //masukkan data ke table record
                $data_record=array(
                    'hac'=>$get_idhac,
                    'inspection_type'=>$typex,
                    'datetime'=>$datetime,
                    'remarks'=>$remarks,
                    'recomendation'=>$recomendation,
                    'severity_level'=>$severity_level,
                    'status'=>'unpublish',
                    'user'=>$user
                );
                $this->users_model->update('record',$id,'id',$data_record);
                //upload file
                $config['upload_path']	= "./media/pdf/";
                $config['upload_url']	= base_url().'media/pdf/';
                $config['allowed_types']= '*';
                $config['max_size']     = '2000000';
                $config['max_width']  	= '2000000';
                $config['max_height']  	= '2000000';
                $this->load->library('upload');
                $this->upload->initialize($config);

                if($this->upload->do_upload('upload_file'))
                 {
                    $image_data1 = $this->upload->data();   
                    $img1=$image_data1['file_name'];
                 }else{
                     $img1=$upload_file_hidden;
                 }
                 
                //get max id form record table
                //$idmax=$this->users_model->get_max_table("id","record");
                $dtp1="";$dtp2="";$dtp3="";$dtp4="";$dtp5="";$dtp6="";$dtp7="";$dtp8="";$dtp9="";$dtp10="";
                $dtp11="";$dtp12="";$dtp13="";$dtp14="";$dtp15="";$dtp16="";$dtp17="";$dtp18="";$dtp19="";$dtp20="";
                $dtp21="";$dtp22="";$dtp23="";$dtp24="";$dtp25="";$dtp26="";$dtp27="";$dtp28="";$dtp29="";$dtp30="";
                $dtp31="";$dtp32="";$dtp33="";$dtp34="";$dtp35="";$dtp36="";$dtp37="";$dtp38="";$dtp39="";$dtp40="";
                $dtp41="";$dtp42="";$dtp43="";$dtp44="";$dtp45="";$dtp46="";$dtp47="";$dtp48="";$dtp49="";$dtp50="";
                $dtp51="";$dtp52="";$dtp53="";$dtp54="";$dtp55="";$dtp56="";$dtp57="";$dtp58="";$dtp59="";$dtp60="";
                $dtp61="";$dtp62="";$dtp63="";$dtp64="";$dtp65="";$dtp66="";$dtp67="";$dtp68="";$dtp69="";$dtp70="";
                $dtp71="";$dtp72="";$dtp73="";$dtp74="";$dtp75="";$dtp76="";$dtp77="";$dtp78="";$dtp79="";$dtp80="";
                if($type=='general' || $type=='stack'){
                    $point1=$this->input->post('point1');
                        $dtp1="";
                        foreach($point1 as $pt1){
                            $dtp1 .=$pt1.",";
                        }
                    $point2=$this->input->post('point2');
                        $dtp2="";
                        foreach($point2 as $pt2){
                            $dtp2 .=$pt2.",";
                        }
                    $point3=$this->input->post('point3');
                        $dtp3="";
                        foreach($point3 as $pt3){
                            $dtp3 .=$pt3.",";
                        }
                    $point4=$this->input->post('point4');
                        $dtp4="";
                        foreach($point4 as $pt4){
                            $dtp4 .=$pt4.",";
                        } 
                    $point5=$this->input->post('point5');
                        $dtp5="";
                        foreach($point5 as $pt5){
                            $dtp5 .=$pt5.",";
                        }
                    $point6=$this->input->post('point6');
                        $dtp6="";
                        foreach($point6 as $pt6){
                            $dtp6 .=$pt6.",";
                        }
                    $point7=$this->input->post('point7');
                        $dtp7="";
                        foreach($point7 as $pt7){
                            $dtp7 .=$pt7.",";
                        }
                    $point8=$this->input->post('point8');
                        $dtp8="";
                        foreach($point8 as $pt8){
                            $dtp8 .=$pt8.",";
                        }
                    $point9=$this->input->post('point9');
                        $dtp9="";
                        foreach($point9 as $pt9){
                            $dtp9 .=$pt9.",";
                        }   
                    $point10=$this->input->post('point10');
                        $dtp10="";
                        foreach($point10 as $pt10){
                            $dtp10 .=$pt10.",";
                        }
                    $point11=$this->input->post('point11');
                        $dtp11="";
                        foreach($point11 as $pt11){
                            $dtp11 .=$pt11.",";
                        }
                    $point12=$this->input->post('point12');
                        $dtp12="";
                        foreach($point12 as $pt12){
                            $dtp12 .=$pt12.",";
                        }
                    $point13=$this->input->post('point13');
                        $dtp13="";
                        foreach($point13 as $pt13){
                            $dtp13 .=$pt13.",";
                        }
                    $point14=$this->input->post('point14');
                        $dtp14="";
                        foreach($point14 as $pt14){
                            $dtp14 .=$pt14.",";
                        }
                    $point15=$this->input->post('point15');
                        $dtp15="";
                        foreach($point15 as $pt15){
                            $dtp15 .=$pt15.",";
                        }
                    $point16=$this->input->post('point16');
                        $dtp16="";
                        foreach($point16 as $pt16){
                            $dtp16 .=$pt16.",";
                        }
                        
                    $data_rec_thick=array(
                        'record_id'=>$id,
                        'date'=>$date,
                        'thick_type'=>$type,
                        'form_type'=>'16',
                        'test_object'=>$test_object,
                        'model'=>$model,
                        'couplant'=>$couplant,
                        'probe_type'=>$probe_type,
                        'frequency'=>$frequency,
                        'thickness'=>$thickness,
                        'area'=>$subarea,
                        'equipment'=>$equipment,
                        'upload_file'=>$img1,
                        'point_1'=>chop($dtp1,','),
                        'point_2'=>chop($dtp2,','),
                        'point_3'=>chop($dtp3,','),
                        'point_4'=>chop($dtp4,','),
                        'point_5'=>chop($dtp5,','),
                        'point_6'=>chop($dtp6,','),
                        'point_7'=>chop($dtp7,','),
                        'point_8'=>chop($dtp8,','),
                        'point_9'=>chop($dtp9,','),
                        'point_10'=>chop($dtp10,','),
                        'point_11'=>chop($dtp11,','),
                        'point_12'=>chop($dtp12,','),
                        'point_13'=>chop($dtp13,','),
                        'point_14'=>chop($dtp14,','),
                        'point_15'=>chop($dtp15,','),
                        'point_16'=>chop($dtp16,',')
                        );
                    $this->users_model->update('record_thickness',$id,'record_id',$data_rec_thick);
                    
                }else if($type=='kiln'){
                    $point1=$this->input->post('point1');
                        $dtp1="";
                        foreach($point1 as $pt1){
                            $dtp1 .=$pt1.",";
                        }
                    $point2=$this->input->post('point2');
                        $dtp2="";
                        foreach($point2 as $pt2){
                            $dtp2 .=$pt2.",";
                        }
                    $point3=$this->input->post('point3');
                        $dtp3="";
                        foreach($point3 as $pt3){
                            $dtp3 .=$pt3.",";
                        }
                    $point4=$this->input->post('point4');
                        $dtp4="";
                        foreach($point4 as $pt4){
                            $dtp4 .=$pt4.",";
                        } 
                    $point5=$this->input->post('point5');
                        $dtp5="";
                        foreach($point5 as $pt5){
                            $dtp5 .=$pt5.",";
                        }
                    $point6=$this->input->post('point6');
                        $dtp6="";
                        foreach($point6 as $pt6){
                            $dtp6 .=$pt6.",";
                        }
                    $point7=$this->input->post('point7');
                        $dtp7="";
                        foreach($point7 as $pt7){
                            $dtp7 .=$pt7.",";
                        }
                    $point8=$this->input->post('point8');
                        $dtp8="";
                        foreach($point8 as $pt8){
                            $dtp8 .=$pt8.",";
                        }
                    $point9=$this->input->post('point9');
                        $dtp9="";
                        foreach($point9 as $pt9){
                            $dtp9 .=$pt9.",";
                        }   
                    $point10=$this->input->post('point10');
                        $dtp10="";
                        foreach($point10 as $pt10){
                            $dtp10 .=$pt10.",";
                        }
                        
                    $point11=$this->input->post('point11');
                        $dtp11="";
                        foreach($point11 as $pt11){
                            $dtp11 .=$pt11.",";
                        }
                    $point12=$this->input->post('point12');
                        $dtp12="";
                        foreach($point12 as $pt12){
                            $dtp12 .=$pt12.",";
                        }
                    $point13=$this->input->post('point13');
                        $dtp13="";
                        foreach($point13 as $pt13){
                            $dtp13 .=$pt13.",";
                        }
                    $point14=$this->input->post('point14');
                        $dtp14="";
                        foreach($point14 as $pt14){
                            $dtp14 .=$pt14.",";
                        }
                    $point15=$this->input->post('point15');
                        $dtp15="";
                        foreach($point15 as $pt15){
                            $dtp15 .=$pt15.",";
                        }
                    $point16=$this->input->post('point16');
                        $dtp16="";
                        foreach($point16 as $pt16){
                            $dtp16 .=$pt16.",";
                        }
                    $point17=$this->input->post('point17');
                        $dtp17="";
                        foreach($point17 as $pt17){
                            $dtp17 .=$pt17.",";
                        }
                    $point18=$this->input->post('point18');
                        $dtp18="";
                        foreach($point18 as $pt18){
                            $dtp18 .=$pt18.",";
                        }
                    $point19=$this->input->post('point19');
                        $dtp19="";
                        foreach($point19 as $pt19){
                            $dtp19 .=$pt19.",";
                        }
                    $point20=$this->input->post('point20');
                        $dtp20="";
                        foreach($point20 as $pt20){
                            $dtp20 .=$pt20.",";
                        }
                        
                    $point21=$this->input->post('point21');
                        $dtp21="";
                        foreach($point21 as $pt21){
                            $dtp21 .=$pt21.",";
                        }
                    $point22=$this->input->post('point22');
                        $dtp22="";
                        foreach($point22 as $pt22){
                            $dtp22 .=$pt22.",";
                        }
                    $point23=$this->input->post('point23');
                        $dtp23="";
                        foreach($point23 as $pt23){
                            $dtp23 .=$pt23.",";
                        }
                    $point24=$this->input->post('point24');
                        $dtp24="";
                        foreach($point24 as $pt24){
                            $dtp24 .=$pt24.",";
                        }
                    $point25=$this->input->post('point25');
                        $dtp25="";
                        foreach($point25 as $pt25){
                            $dtp25 .=$pt25.",";
                        }
                    $point26=$this->input->post('point26');
                        $dtp26="";
                        foreach($point26 as $pt26){
                            $dtp26 .=$pt26.",";
                        }
                    $point27=$this->input->post('point27');
                        $dtp27="";
                        foreach($point27 as $pt27){
                            $dtp27 .=$pt27.",";
                        }
                    $point28=$this->input->post('point28');
                        $dtp28="";
                        foreach($point28 as $pt28){
                            $dtp28 .=$pt28.",";
                        }
                    $point29=$this->input->post('point29');
                        $dtp29="";
                        foreach($point29 as $pt29){
                            $dtp29 .=$pt29.",";
                        }
                    $point30=$this->input->post('point30');
                        $dtp30="";
                        foreach($point30 as $pt30){
                            $dtp30 .=$pt30.",";
                        }
                        
                    $point31=$this->input->post('point31');
                        $dtp31="";
                        foreach($point31 as $pt31){
                            $dtp31 .=$pt31.",";
                        }
                    $point32=$this->input->post('point32');
                        $dtp32="";
                        foreach($point32 as $pt32){
                            $dtp32 .=$pt32.",";
                        }
                    $point33=$this->input->post('point33');
                        $dtp33="";
                        foreach($point33 as $pt33){
                            $dtp33 .=$pt33.",";
                        }
                    $point34=$this->input->post('point34');
                        $dtp34="";
                        foreach($point34 as $pt34){
                            $dtp34 .=$pt34.",";
                        }
                    $point35=$this->input->post('point35');
                        $dtp35="";
                        foreach($point35 as $pt35){
                            $dtp35 .=$pt35.",";
                        }
                    $point36=$this->input->post('point36');
                        $dtp36="";
                        foreach($point36 as $pt36){
                            $dtp36 .=$pt36.",";
                        }
                    $point37=$this->input->post('point37');
                        $dtp37="";
                        foreach($point37 as $pt37){
                            $dtp37 .=$pt37.",";
                        }
                    $point38=$this->input->post('point38');
                        $dtp38="";
                        foreach($point38 as $pt38){
                            $dtp38 .=$pt38.",";
                        }
                    $point39=$this->input->post('point39');
                        $dtp39="";
                        foreach($point39 as $pt39){
                            $dtp39 .=$pt39.",";
                        }
                    $point40=$this->input->post('point40');
                        $dtp40="";
                        foreach($point40 as $pt40){
                            $dtp40 .=$pt40.",";
                        }
                        
                    $point41=$this->input->post('point41');
                        $dtp41="";
                        foreach($point41 as $pt41){
                            $dtp41 .=$pt41.",";
                        }
                    $point42=$this->input->post('point42');
                        $dtp42="";
                        foreach($point42 as $pt42){
                            $dtp42 .=$pt42.",";
                        }
                    $point43=$this->input->post('point43');
                        $dtp43="";
                        foreach($point43 as $pt43){
                            $dtp43 .=$pt43.",";
                        }
                    $point44=$this->input->post('point44');
                        $dtp44="";
                        foreach($point44 as $pt44){
                            $dtp44 .=$pt44.",";
                        }
                    $point45=$this->input->post('point45');
                        $dtp45="";
                        foreach($point45 as $pt45){
                            $dtp45 .=$pt45.",";
                        }
                    $point46=$this->input->post('point46');
                        $dtp46="";
                        foreach($point46 as $pt46){
                            $dtp46 .=$pt46.",";
                        }
                    $point47=$this->input->post('point47');
                        $dtp47="";
                        foreach($point47 as $pt47){
                            $dtp47 .=$pt47.",";
                        }
                    $point48=$this->input->post('point48');
                        $dtp48="";
                        foreach($point48 as $pt48){
                            $dtp48 .=$pt48.",";
                        }
                    $point49=$this->input->post('point49');
                        $dtp49="";
                        foreach($point49 as $pt49){
                            $dtp49 .=$pt49.",";
                        }
                    $point50=$this->input->post('point50');
                        $dtp50="";
                        foreach($point50 as $pt50){
                            $dtp50 .=$pt50.",";
                        }
                        
                    $point51=$this->input->post('point51');
                        $dtp51="";
                        foreach($point51 as $pt51){
                            $dtp51 .=$pt51.",";
                        }
                    $point52=$this->input->post('point52');
                        $dtp52="";
                        foreach($point52 as $pt52){
                            $dtp52 .=$pt52.",";
                        }
                    $point53=$this->input->post('point53');
                        $dtp53="";
                        foreach($point53 as $pt53){
                            $dtp53 .=$pt53.",";
                        }
                    $point54=$this->input->post('point54');
                        $dtp54="";
                        foreach($point54 as $pt54){
                            $dtp54 .=$pt54.",";
                        }
                    $point55=$this->input->post('point55');
                        $dtp55="";
                        foreach($point55 as $pt55){
                            $dtp55 .=$pt55.",";
                        }
                    $point56=$this->input->post('point56');
                        $dtp56="";
                        foreach($point56 as $pt56){
                            $dtp56 .=$pt56.",";
                        }
                    $point57=$this->input->post('point57');
                        $dtp57="";
                        foreach($point57 as $pt57){
                            $dtp57 .=$pt57.",";
                        }
                    $point58=$this->input->post('point58');
                        $dtp58="";
                        foreach($point58 as $pt58){
                            $dtp58 .=$pt58.",";
                        }
                    $point59=$this->input->post('point59');
                        $dtp59="";
                        foreach($point59 as $pt59){
                            $dtp59 .=$pt59.",";
                        }
                    $point60=$this->input->post('point60');
                        $dtp60="";
                        foreach($point60 as $pt60){
                            $dtp60 .=$pt60.",";
                        }
                        
                    $point61=$this->input->post('point61');
                        $dtp61="";
                        foreach($point61 as $pt61){
                            $dtp61 .=$pt61.",";
                        }
                    $point62=$this->input->post('point62');
                        $dtp62="";
                        foreach($point62 as $pt62){
                            $dtp62 .=$pt62.",";
                        }
                    $point63=$this->input->post('point63');
                        $dtp63="";
                        foreach($point63 as $pt63){
                            $dtp63 .=$pt13.",";
                        }
                    $point64=$this->input->post('point64');
                        $dtp64="";
                        foreach($point64 as $pt64){
                            $dtp64 .=$pt64.",";
                        }
                    $point65=$this->input->post('point65');
                        $dtp65="";
                        foreach($point65 as $pt65){
                            $dtp65 .=$pt65.",";
                        }
                    $point66=$this->input->post('point66');
                        $dtp66="";
                        foreach($point66 as $pt66){
                            $dtp66 .=$pt66.",";
                        }
                    $point67=$this->input->post('point67');
                        $dtp67="";
                        foreach($point67 as $pt67){
                            $dtp67 .=$pt67.",";
                        }
                    $point68=$this->input->post('point68');
                        $dtp68="";
                        foreach($point68 as $pt68){
                            $dtp68 .=$pt68.",";
                        }
                    $point69=$this->input->post('point69');
                        $dtp69="";
                        foreach($point69 as $pt69){
                            $dtp69 .=$pt69.",";
                        }
                    $point70=$this->input->post('point70');
                        $dtp70="";
                        foreach($point70 as $pt70){
                            $dtp70 .=$pt70.",";
                        }
                        
                    $point71=$this->input->post('point71');
                        $dtp71="";
                        foreach($point71 as $pt71){
                            $dtp71 .=$pt71.",";
                        }
                    $point72=$this->input->post('point72');
                        $dtp72="";
                        foreach($point72 as $pt72){
                            $dtp72 .=$pt72.",";
                        }
                    $point73=$this->input->post('point73');
                        $dtp73="";
                        foreach($point73 as $pt73){
                            $dtp73 .=$pt73.",";
                        }
                    $point74=$this->input->post('point74');
                        $dtp74="";
                        foreach($point74 as $pt74){
                            $dtp74 .=$pt74.",";
                        }
                    $point75=$this->input->post('point75');
                        $dtp75="";
                        foreach($point75 as $pt75){
                            $dtp75 .=$pt75.",";
                        }
                    $point76=$this->input->post('point76');
                        $dtp76="";
                        foreach($point76 as $pt76){
                            $dtp76 .=$pt76.",";
                        }
                    $point77=$this->input->post('point77');
                        $dtp77="";
                        foreach($point77 as $pt77){
                            $dtp77 .=$pt77.",";
                        }
                    $point78=$this->input->post('point78');
                        $dtp78="";
                        foreach($point78 as $pt78){
                            $dtp78 .=$pt78.",";
                        }
                    $point79=$this->input->post('point79');
                        $dtp79="";
                        foreach($point79 as $pt79){
                            $dtp79 .=$pt79.",";
                        }
                    $point80=$this->input->post('point80');
                        $dtp80="";
                        foreach($point80 as $pt80){
                            $dtp80 .=$pt80.",";
                        }
                $data_rec_thick=array(
                        'record_id'=>$id,
                        'date'=>$date,
                        'thick_type'=>$type,
                        'form_type'=>'16',
                        'test_object'=>$test_object,
                        'model'=>$model,
                        'couplant'=>$couplant,
                        'probe_type'=>$probe_type,
                        'frequency'=>$frequency,
                        'thickness'=>$thickness,
                        'area'=>$subarea,
                        'equipment'=>$equipment,
                        'upload_file'=>$img1,
                        'point_1'=>chop($dtp1,','),
                        'point_2'=>chop($dtp2,','),
                        'point_3'=>chop($dtp3,','),
                        'point_4'=>chop($dtp4,','),
                        'point_5'=>chop($dtp5,','),
                        'point_6'=>chop($dtp6,','),
                        'point_7'=>chop($dtp7,','),
                        'point_8'=>chop($dtp8,','),
                        'point_9'=>chop($dtp9,','),
                        'point_10'=>chop($dtp10,','),
                    
                        'point_11'=>chop($dtp11,','),
                        'point_12'=>chop($dtp12,','),
                        'point_13'=>chop($dtp13,','),
                        'point_14'=>chop($dtp14,','),
                        'point_15'=>chop($dtp15,','),
                        'point_16'=>chop($dtp16,','),
                        'point_17'=>chop($dtp17,','),
                        'point_18'=>chop($dtp18,','),
                        'point_19'=>chop($dtp19,','),
                        'point_20'=>chop($dtp20,','),
                    
                        'point_21'=>chop($dtp21,','),
                        'point_22'=>chop($dtp22,','),
                        'point_23'=>chop($dtp23,','),
                        'point_24'=>chop($dtp24,','),
                        'point_25'=>chop($dtp25,','),
                        'point_26'=>chop($dtp26,','),
                        'point_27'=>chop($dtp27,','),
                        'point_28'=>chop($dtp28,','),
                        'point_29'=>chop($dtp29,','),
                        'point_30'=>chop($dtp30,','),
                    
                        'point_31'=>chop($dtp31,','),
                        'point_32'=>chop($dtp32,','),
                        'point_33'=>chop($dtp33,','),
                        'point_34'=>chop($dtp34,','),
                        'point_35'=>chop($dtp35,','),
                        'point_36'=>chop($dtp36,','),
                        'point_37'=>chop($dtp37,','),
                        'point_38'=>chop($dtp38,','),
                        'point_39'=>chop($dtp39,','),
                        'point_40'=>chop($dtp40,','),
                    
                        'point_41'=>chop($dtp41,','),
                        'point_42'=>chop($dtp42,','),
                        'point_43'=>chop($dtp43,','),
                        'point_44'=>chop($dtp44,','),
                        'point_45'=>chop($dtp45,','),
                        'point_46'=>chop($dtp46,','),
                        'point_47'=>chop($dtp47,','),
                        'point_48'=>chop($dtp48,','),
                        'point_49'=>chop($dtp49,','),
                        'point_50'=>chop($dtp50,','),
                    
                        'point_51'=>chop($dtp51,','),
                        'point_52'=>chop($dtp52,','),
                        'point_53'=>chop($dtp53,','),
                        'point_54'=>chop($dtp54,','),
                        'point_55'=>chop($dtp55,','),
                        'point_56'=>chop($dtp56,','),
                        'point_57'=>chop($dtp57,','),
                        'point_58'=>chop($dtp58,','),
                        'point_59'=>chop($dtp59,','),
                        'point_60'=>chop($dtp60,','),
                    
                        'point_51'=>chop($dtp51,','),
                        'point_52'=>chop($dtp52,','),
                        'point_53'=>chop($dtp53,','),
                        'point_54'=>chop($dtp54,','),
                        'point_55'=>chop($dtp55,','),
                        'point_56'=>chop($dtp56,','),
                        'point_57'=>chop($dtp57,','),
                        'point_58'=>chop($dtp58,','),
                        'point_59'=>chop($dtp59,','),
                        'point_60'=>chop($dtp60,','),
                    
                        'point_61'=>chop($dtp61,','),
                        'point_62'=>chop($dtp62,','),
                        'point_63'=>chop($dtp63,','),
                        'point_64'=>chop($dtp64,','),
                        'point_65'=>chop($dtp65,','),
                        'point_66'=>chop($dtp66,','),
                        'point_67'=>chop($dtp67,','),
                        'point_68'=>chop($dtp68,','),
                        'point_69'=>chop($dtp69,','),
                        'point_70'=>chop($dtp70,','),
                    
                        'point_71'=>chop($dtp71,','),
                        'point_72'=>chop($dtp72,','),
                        'point_73'=>chop($dtp63,','),
                        'point_74'=>chop($dtp74,','),
                        'point_75'=>chop($dtp75,','),
                        'point_76'=>chop($dtp76,','),
                        'point_77'=>chop($dtp77,','),
                        'point_78'=>chop($dtp78,','),
                        'point_79'=>chop($dtp79,','),
                        'point_80'=>chop($dtp80,',')
                    
                        );
                    $this->users_model->update('record_thickness',$id,'record_id',$data_rec_thick);
              
                }      
                    //Insert into engineer Remak
                    $this->remark_engineer($id,'UPDATE','Update Record Thickness Measurement','Thickness Measurement');
                    //insert into activity log
                    $this->insert_log_activity("Record Thickness Measurement",$id,"Update Record Thickness Measurement");
                    redirect("record/add_thickness/"); 
	
	}
	
	function delete($id){
            $this->users_model->delete("record",$id,"id");
            $this->users_model->delete("record_thickness",$id,"record_id");
            //insert into activity log
            $this->insert_log_activity("Record Thickness Measurement",$id,"Delete Record Thickness Measurement");
            redirect("record/add_thickness/"); 
        }
	
        
        function get_point(){
            $id=$this->input->post('id');
            $point=$this->input->post('point');
            $sql=mysql_query("select point_$point from record_thickness where record_id='$id'");
            $data=  mysql_fetch_assoc($sql);
           
               echo $data['point_'.$point];
            
        }
	
	
}	

	