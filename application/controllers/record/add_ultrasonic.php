<?php

class Add_ultrasonic extends CI_controller {

	function __construct()
	{
		parent::__construct();
                $this->load->model('users_model');
                $this->load->model('form_manager_model');
		$this->load->library('grocery_crud');	
	}
	
        
        
        function insert_log_activity($type,$primarykey,$description){
            $this->load->model('form_manager_model');
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
        function remark_engineer($record_id,$status,$remarks,$type){
            $data=array(
                'record_id'=>$record_id,
                'remarks'=>$remarks,
                'inspector_id'=>$this->session->userdata('users_id'),
                'date_remarks'=>date("Y-m-d H:i:s"),
                'publish'=>'1',
                'type_report'=>$type,
                'status'=>$status
            );
            $this->db->insert('engineer_remark',$data);
        }
        
	function index()
	{
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="re.id";
                }else{
                    $field=$fieldx;
                }
                $config['base_url'] = base_url().'record/add_ultrasonic/index/';
                $config['total_rows'] = $this->db->query("select * from record re, record_ultrasonic_test reultra, hac where re.inspection_type='UT' and re.hac=hac.id and $field LIKE '%$val%' group by re.id")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select *,re.id as idx,re.severity_level from record re, record_ultrasonic_test reultra, hac where re.inspection_type='UT' and re.hac=hac.id and $field LIKE '%$val%' group by re.id order by re.datetime DESC limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('record/add_ultrasonic', $data); 
	}
	
	function add()
	{
                $data['list_plant']=$this->users_model->select_all("master_plant")->result();
                $data['list_frequency']=$this->users_model->select_all("master_frequency")->result();
		$this->load->view('record/form_add_ultrasonic',$data); 
	}
	
		function add_post()
	{
			/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
                $test_object=$this->input->post('test_object');
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
                $get_idhac = $this->form_manager_model->get_idhac($hac);
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$upload_file_simpan = $this->input->post('upload_file');
		$model = $this->input->post('model');
		$couplant = $this->input->post('couplant');
		$probe_type = $this->input->post('probe_type');
		$frequency = $this->input->post('frequency');
		$p_zero = $this->input->post('p_zero');
		$thickness = $this->input->post('thickness');
		$gain = $this->input->post('gain');
		$vel = $this->input->post('vel');
		$range = $this->input->post('range');
		$sa = $this->input->post('sa');
		$ra = $this->input->post('ra');
		$da = $this->input->post('da');
                $area = $this->input->post('subarea');
        
		/* PARAM POST/INSERT TO Table record */
		$data_post_record = array(
                                        'hac' => $get_idhac,
                                        'inspection_type' => 'UT', // Ubah Sesuai code inspection
                                        'datetime' => $datetime,
                                        'remarks' => $remarks,
                                        'recomendation' => $recomendation,
                                        'severity_level' => $severity_level,
                                        'user' => $user
                                        );
                $this->users_model->insert("record",$data_post_record);
			/* PROCESS TO INSERT */						
		//upload image
                $config['upload_path']	= "./media/images/";
                $config['upload_url']	= base_url().'media/images/';
                $config['allowed_types']= '*';
                $config['max_size']     = '20000000';
                $config['max_width']  	= '20000000';
                $config['max_height']  	= '20000000';
                $this->load->library('upload');
                $this->upload->initialize($config);

                if($this->upload->do_upload('upload_file'))
                 {
                $image_data1 = $this->upload->data();    
                 }
		/* ---- end --- */
		
		///select max id record
                $idmax=$this->users_model->get_max_table("id","record");
                $data_post = array(
                                        'record_id' => $idmax,
                                        'area'=>$area,
                                        'model' => $model,
                                        'couplant' => $couplant,
                                        'probe_type' => $probe_type,
                                        'frequency' => $frequency,
                                        'p_zero' => $p_zero,
                                        'thickness' => $thickness,
                                        'gain' => $gain,
                                        'vel' => $vel,
                                        'range' => $range,
                                        'sa' => $sa,
                                        'date'=>date("Y-m-d H:i:s"),
                                        'hac' => $get_idhac,
                                        'ra' => $ra,
                                        'da' => $da,
                                        'test_object'=>$test_object,
                                        'upload_file' => $image_data1['file_name'],
                                        'inspector_id'=>$user
                                        );
						
		$this->users_model->insert("record_ultrasonic_test",$data_post);
		
                //Insert into engineer Remak
                $this->remark_engineer($idmax,'NEW','Add New Record Ultrasonic Test ','Ultrasonic Test');
                //insert into activity log
                $this->insert_log_activity("Record Ultrasonic Test",$idmax,"Create New Record Ultrasonic Test");
		redirect("record/add_ultrasonic/"); 
			
					
	}
	
	
	function edit($id){
	
//       $data['id'] = $id;
//	
//	$row = $this->db->query('SELECT * FROM `record` as re, record_ultrasonic_test as reultra WHERE `inspection_type`="UT" and re.inspection_id="'.$id.'" and re.`inspection_id`=reultra.id')->row();
//
//	$data['default']['hac'] = $row->hac; 
//	$data['default']['status'] = $row->status;
//	$data['default']['severity_level'] = $row->severity_level;
//	$data['default']['remarks'] = $row->remarks; 
//	$data['default']['recomendation'] = $row->recomendation; 
//	$data['default']['model'] = $row->model; 
//	$data['default']['couplant'] = $row->couplant; 
//	$data['default']['probe_type'] = $row->probe_type; 
//	$data['default']['frequency'] = $row->frequency; 
//	$data['default']['p_zero'] = $row->p_zero;
//	$data['default']['thickness'] = $row->thickness;
//	$data['default']['gain'] = $row->gain;
//	$data['default']['vel'] = $row->vel;
//	$data['default']['range'] = $row->range;
//	$data['default']['sa'] = $row->sa;
//	$data['default']['ra'] = $row->ra;
//	$data['default']['da'] = $row->da;
//						
//    $data['default']['upload_file'] = $row->upload_file; 
        $data['list_plant']=$this->users_model->select_all("master_plant")->result();
        $data['list_frequency']=$this->users_model->select_all("master_frequency")->result();
        $data['list']=$this->db->query("select a.*,b.*,c.hac_code from record a inner join record_ultrasonic_test b on b.record_id=a.id inner join hac c on a.hac=c.id where a.id='$id'")->row();
	$this->load->view('record/form_edit_ultrasonic', $data);
	
	}
	
	
	function autocomplete_hac(){
		$keyword = $this->input->post("term");
		$result = $this->db->query('select * from hac where hac_code like "'.$keyword.'%" LIMIT 10')->result_array();
		foreach($result as $row){
			$data[] = array('label'=>$row['hac_code'], 'value'=>$row['hac_code']);
		}
		echo json_encode($data);
	}
	
	
	function autocomplete_hac_detail(){
		$hac = $this->input->post("term");
		
		$main = $this->db->query('select * from hac where hac_code="'.$hac.'"')->row_array();
		$data = array(
			"main" => $main
		);
		
		echo json_encode($data);
	}
	
	function edit_post(){
	
        $id = $this->input->post("id");
        $user = $this->input->post('user'); // REQUIRE
        $hac = $this->input->post('hac'); // REQUIRE
        $remarks = $this->input->post('remarks'); // REQUIRE
        $recomendation = $this->input->post('recomendation'); // REQUIRE
        $severity_level = $this->input->post('severity_level'); // REQUIRE
        $test_object=$this->input->post('test_object');

        $datetime = date('Y-m-d H:i:s'); // REQUIRE
        $date= date('Y-m-d'); // REQUIRE
        $get_idhac = $this->form_manager_model->get_idhac($hac);
        /* -- END -- */


        /* INSPECTION TYPE RECORD */
        /* ubah parameter sesuai dengan record table di Inspection masing2 */
        $upload_file_simpan = $this->input->post('upload_file');
        $model = $this->input->post('model');
        $couplant = $this->input->post('couplant');
        $probe_type = $this->input->post('probe_type');
        $frequency = $this->input->post('frequency');
        $p_zero = $this->input->post('p_zero');
        $thickness = $this->input->post('thickness');
        $gain = $this->input->post('gain');
        $vel = $this->input->post('vel');
        $range = $this->input->post('range');
        $sa = $this->input->post('sa');
        $ra = $this->input->post('ra');
        $da = $this->input->post('da');
        $area = $this->input->post('subarea');
        $upload_file_hidden=$this->input->post('upload_image_hidden');

        /* PARAM POST/INSERT TO Table record */
        $data_post_record = array(
                                'hac' => $get_idhac,
                                'inspection_type' => 'UT', // Ubah Sesuai code inspection
                                'datetime' => $datetime,
                                'remarks' => $remarks,
                                'recomendation' => $recomendation,
                                'severity_level' => $severity_level,
                                'status'=>'unpublish',
                                'user' => $user
                                );
        $this->users_model->update('record',$id,'id',$data_post_record);
                /* PROCESS TO INSERT */						
        //upload image
        $config['upload_path']	= "./media/images/";
        $config['upload_url']	= base_url().'media/images/';
        $config['allowed_types']= '*';
        $config['max_size']     = '2000000';
        $config['max_width']  	= '2000000';
        $config['max_height']  	= '2000000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        if($this->upload->do_upload('upload_file'))
         {
            $image_data1 = $this->upload->data();  
            $img=$image_data1['file_name'];
         }else{
             $img=$upload_file_hidden;
         }
        /* ---- end --- */

        ///select max id record
        $idmax=$this->users_model->get_max_table("id","record");
        $data_post = array(
                                'record_id' => $id,
                                'area'=>$area,
                                'model' => $model,
                                'couplant' => $couplant,
                                'probe_type' => $probe_type,
                                'frequency' => $frequency,
                                'p_zero' => $p_zero,
                                'thickness' => $thickness,
                                'gain' => $gain,
                                'vel' => $vel,
                                'range' => $range,
                                'date'=>$datetime,
                                'hac' => $get_idhac,
                                'sa' => $sa,
                                'date'=>date("Y-m-d H:i:s"),
                                'ra' => $ra,
                                'da' => $da,
                                'test_object'=>$test_object,
                                'upload_file' => $img,
                                'inspector_id'=>$user
                                );

        $this->users_model->update('record_ultrasonic_test',$id,'record_id',$data_post);
        
         //Insert into engineer Remak
        $this->remark_engineer($id,'UPDATE','Update Record Ultrasonic Test','Ultrasonic Test');
            
        //insert into activity log
        $this->insert_log_activity("Record Ultrasonic Test",$id,"Update Record Ultrasonic Test");
        redirect("record/add_ultrasonic/"); 
	
	}
        function delete($id){
            $this->users_model->delete("record",$id,"id");
            $this->users_model->delete("record_ultrasonic_test",$id,"record_id");
            redirect("record/add_vibration/"); 
        }
	
	
	
	
	
}	

	