<?php

class Add_lubricant extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');	
	}
	

	function index()
	{
	
		$data['data'] = $this->db->query('select * from record re, record_mca remca, hac where re.inspection_type="MCA" and re.hac=hac.id group by re.id')->result();
   	    
		$this->load->view('record/add_lubricant', $data); 
	}
	
	function add()
	{
		$this->load->view('record/form_add_mca'); 
	}
	
		function add_post()
	{
			/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$upload_file_simpan = $this->input->post('upload_file_simpan');
        
		$config['upload_path']	= "./media/pdf/";
		$config['upload_url']	= base_url().'media/pdf/';
        $config['allowed_types']= '*';
        $config['max_size']     = '2000000';
        $config['max_width']  	= '2000000';
        $config['max_height']  	= '2000000';
 
        $this->load->library('upload');
		$this->upload->initialize($config);
		$field_name= 'upload_file';
		
		if ( ! $this->upload->do_upload($field_name))
		{
			
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$upload_file = $data['file_name'];
		}
		
		/* PARAM POST/INSERT TO Table record_oil_analysis */
		$data_post_record = array(
									'hac' => $hac,
									'inspection_type' => 'MCA', // Ubah Sesuai code inspection
									'datetime' => $datetime,
									'remarks' => $remarks,
									'recomendation' => $recomendation,
									'severity_level' => $severity_level,
									'user' => $user
									);
									
			/* PROCESS TO INSERT */						
			
		
		/* ---- end --- */
		
		/* PROCESS INSERT TO Table record_oil_analysis */
		if($this->db->insert('record',$data_post_record))
		{
			$data_post['status'] = 'Success';
			
			$record_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
			
			/* PARAM TO INSERT RECORD to table record */
			
			$data_post = array(
						'record_id' => $record_id,
						'upload_file' => $upload_file_simpan
						);
						
				$this->db->insert('record_mca',$data_post);
				
			$inspection_id = mysql_insert_id();
				
			$data_update_inspection = array('inspection_id' => $inspection_id);
			$where = array('id' => $record_id);
			
			$this->db->update('record', $data_update_inspection, $where);
		}
		else
		{
			$data_post['status'] = 'Failed';
		}
		
		$data_json = array('record' => $data_post_record, 'record_detail'=> $data_post);
		
		redirect("record/add_mca/"); 
			
					
	}
	
	
	function edit($id){
	
    $data['id'] = $id;
	
	$row = $this->db->query('SELECT * FROM `record` as re, record_mca as remca WHERE `inspection_type`="MCA" and re.inspection_id="'.$id.'" and re.`inspection_id`=remca.id')->row();

	$data['default']['hac'] = $row->hac; 
	$data['default']['status'] = $row->status;
	$data['default']['severity_level'] = $row->severity_level;
	$data['default']['remarks'] = $row->remarks; 
	$data['default']['recomendation'] = $row->recomendation; 
    $data['default']['upload_file'] = $row->upload_file; 
	  
	$this->load->view('record/form_edit_mca', $data);
	
	}
	
	
	function autocomplete_hac(){
		$keyword = $this->input->post("term");
		$result = $this->db->query('select * from hac where hac_code like "'.$keyword.'%" LIMIT 10')->result_array();
		foreach($result as $row){
			$data[] = array('label'=>$row['hac_code'], 'value'=>$row['hac_code']);
		}
		echo json_encode($data);
	}
	
	
	function autocomplete_hac_detail(){
		$hac = $this->input->post("term");
		
		$main = $this->db->query('select * from hac where hac_code="'.$hac.'"')->row_array();
		$data = array(
			"main" => $main
		);
		
		echo json_encode($data);
	}
	
	function edit_post(){
	
        $id = $this->input->post("id");
		$hac = $this->input->post('hac');
		$severity_level = $this->input->post('severity_level');
		$remarks = $this->input->post('remarks');
		$recomendation = $this->input->post('recomendation');
		$upload_file_simpan = $this->input->post('upload_file_simpan');
        
		$config['upload_path']	= "./media/pdf/";
		$config['upload_url']	= base_url().'media/pdf/';
        $config['allowed_types']= '*';
        $config['max_size']     = '2000000';
        $config['max_width']  	= '2000000';
        $config['max_height']  	= '2000000';
 
        $this->load->library('upload');
		$this->upload->initialize($config);
		$field_name= 'upload_file';
		
		if ( ! $this->upload->do_upload($field_name))
		{
			
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$upload_file = $data['file_name'];
		}
		
		
		    
            
			$this->db->set('re.hac', $hac);
			$this->db->set('re.severity_level', $severity_level);
			$this->db->set('re.remarks', $remarks);
			$this->db->set('re.recomendation',$recomendation);
			$this->db->set('remca.upload_file',$upload_file_simpan);
			$this->db->where("re.inspection_type", "MCA");
			$this->db->where("re.inspection_id", $id);
			$this->db->where("remca.id = re.inspection_id");
			$this->db->update('record as re, record_mca as remca');
			
			redirect("record/add_mca/edit/$id"); 
	
	}
	
	
	
	
	
}	

	