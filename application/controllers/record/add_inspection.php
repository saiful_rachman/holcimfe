<?php

class Add_inspection extends CI_controller {

	function __construct()
	{
		parent::__construct();
                $this->load->model('users_model');
                $this->load->model('form_manager_model');
		
		$this->load->library('grocery_crud');	
	}
	
        function insert_log_activity($type,$primarykey,$description){
            $this->load->model('form_manager_model');
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
        function remark_engineer($record_id,$status,$remarks,$type){
            $data=array(
                'record_id'=>$record_id,
                'remarks'=>$remarks,
                'inspector_id'=>$this->session->userdata('users_id'),
                'date_remarks'=>date("Y-m-d H:i:s"),
                'publish'=>'1',
                'type_report'=>$type,
                'status'=>$status
            );
            $this->db->insert('engineer_remark',$data);
        }
        
	function index()
	{
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="re.id";
                }else{
                    $field=$fieldx;
                }
		$config['base_url'] = base_url().'record/add_inspection/index/';
                $config['total_rows'] = $this->db->query("select * from record re, record_inspection_report reins, hac where re.inspection_type='IR' and re.hac=hac.id and $field LIKE '%$val%' group by re.id")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select *,re.id as idx,re.severity_level from record re, record_inspection_report reins, hac where re.inspection_type='IR' and re.hac=hac.id and $field LIKE '%$val%' group by re.id limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('record/add_inspection', $data); 
	}
	
	function add()
	{
                $data['list_plant']=$this->users_model->select_all("master_plant")->result();
		$this->load->view('record/form_add_inspection',$data); 
	}
	
		function add_post()
	{
			/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
                $description = $this->input->post('description');
                $area = $this->input->post('subarea');
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
                $manufacture= $this->input->post('manufacture');
                $type = $this->input->post('type');
                $year_build = $this->input->post('year_build');
                $last_check = $this->input->post('last_check');
                $last_repaired = $this->input->post('last_repaired');
                $power = $this->input->post('power');
                $speed_input = $this->input->post('speed_input');
                $speed_output = $this->input->post('speed_output');
                $information1 = $this->input->post('title1')."|".$this->input->post('value1');
                $information2 = $this->input->post('title2')."|".$this->input->post('value2');
                $titlefie=$this->input->post('titlefie');
                
		//get hac id
                $get_idhac = $this->form_manager_model->get_idhac($hac);
                
                //insert into table record
                $data_record=array(
                                'hac'=>$get_idhac,
                                'inspection_type'=>'IR',
                                'inspection_id'=>'',
                                'datetime'=>$datetime,
                                'remarks'=>$remarks,
                                'recomendation'=>$recomendation,
                                'severity_level'=>$severity_level,
                                'status'=>'unpublish',
                                'user'=>$user,
                                'publish_by'=>''
                            );
                $this->users_model->insert("record",$data_record);
                
                //upload File
                $config['upload_path']	= "./media/pdf/";
                $config['upload_url']	= base_url().'media/pdf/';
                $config['allowed_types']= '*';
                $config['max_size']     = '2000000';
                $config['max_width']  	= '2000000';
                $config['max_height']  	= '2000000';
                $this->load->library('upload');
                $this->upload->initialize($config);

                if($this->upload->do_upload('upload_file'))
                 {
                $image_data1 = $this->upload->data();    
                 }
                 
                 //select max id record
                $idmax=$this->users_model->get_max_table("id","record");
                
                 //insert into table recod_vibration
                $data_record_vibration=array(
                                                'record_id'=>$idmax,
                                                'date'=>$date,
                                                'hac'=>$get_idhac,
                                                'area'=>$area,
                                                'upload_file'=>$image_data1['file_name'],
                                                'description'=>$description,
                                                'manufacture'=>$manufacture,
                                                'type'=>$type,
                                                'year_build'=>$year_build,
                                                'last_check'=>$last_check,
                                                'last_repaired'=>$last_repaired,
                                                'power'=>$power,
                                                'speed_input'=>$speed_input,
                                                'speed_output'=>$speed_output,
                                                'information1'=>$information1,
                                                'information2'=>$information2
                                            );
                $this->users_model->insert("record_inspection_report",$data_record_vibration);
                
                $this->load->library('upload');

                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                for($i=0; $i<$cpt; $i++)
                {

                    $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                    $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                    $_FILES['userfile']['size']= $files['userfile']['size'][$i];    



                $this->upload->initialize($this->set_upload_options());
                $this->upload->do_upload();
                $data_image=array('record_id'=>$idmax,'image'=>$_FILES['userfile']['name'],'title'=>$titlefie[$i]);
		$this->db->insert('record_inspection_report_image',$data_image);
                }
                
                //Insert into engineer Remak
                $this->remark_engineer($idmax,'NEW','Add New Record Inspection Report','Inspection Report');
                //insert into activity log
                $this->insert_log_activity("Record Inspection Record",$idmax,"Create New Record Inspection Report");
                
		redirect("record/add_inspection/"); 
			
					
	}
	
	
	function edit($id){
	
//    $data['id'] = $id;
//	
//	$row = $this->db->query('SELECT record.*,record.hac as hax,record_inspection_report.* from record left join record_inspection_report on record.inspection_id=record_inspection_report.id WHERE record.inspection_type="IR" and record.inspection_id="'.$id.'"')->row();
//
//	$data['default']['hac'] = $row->hax; 
//	$data['default']['status'] = $row->status;
//	$data['default']['severity_level'] = $row->severity_level;
//	$data['default']['remarks'] = $row->remarks; 
//	$data['default']['recomendation'] = $row->recomendation; 
//    $data['default']['report_content'] = $row->report_content; 
//	$data['default']['upload_file'] = $row->upload_file; 
//	  
        $data['list_image']=$this->users_model->select_all_where("record_inspection_report_image",$id,"record_id")->result();
        $data['list_plant']=$this->users_model->select_all("master_plant")->result();
        $data['list']=$this->db->query("select a.*,b.*,c.hac_code from record a inner join record_inspection_report b on b.record_id=a.id inner join hac c on a.hac=c.id where a.id='$id'")->row();
        $this->load->view('record/form_edit_inspection',$data);
	
	}
	
	
	function autocomplete_hac(){
		$keyword = $this->input->post("term");
		$result = $this->db->query('select * from hac where hac_code like "'.$keyword.'%" LIMIT 10')->result_array();
		foreach($result as $row){
			$data[] = array('label'=>$row['hac_code'], 'value'=>$row['hac_code']);
		}
		echo json_encode($data);
	}
	
	
	function autocomplete_hac_detail(){
		$hac = $this->input->post("term");
		
		$main = $this->db->query('select * from hac where hac_code="'.$hac.'"')->row_array();
		$data = array(
			"main" => $main
		);
		
		echo json_encode($data);
	}
	
	function edit_post(){
	
        $id = $this->input->post("id");
        $user = $this->input->post('user'); // REQUIRE
        $hac = $this->input->post('hac'); // REQUIRE
        $remarks = $this->input->post('remarks'); // REQUIRE
        $recomendation = $this->input->post('recomendation'); // REQUIRE
        $severity_level = $this->input->post('severity_level'); // REQUIRE
        $description = $this->input->post('description');
        $area = $this->input->post('subarea');
        $upload_file_hidden=$this->input->post('upload_file_hidden');
        $datetime = date('Y-m-d H:i:s'); // REQUIRE
        $date= date('Y-m-d'); // REQUIRE
        $userfile_hidden=$this->input->post('userfile_hidden');
        $manufacture= $this->input->post('manufacture');
        $type = $this->input->post('type');
        $year_build = $this->input->post('year_build');
        $last_check = $this->input->post('last_check');
        $last_repaired = $this->input->post('last_repaired');
        $power = $this->input->post('power');
        $speed_input = $this->input->post('speed_input');
        $speed_output = $this->input->post('speed_output');
        $information1 = $this->input->post('title1')."|".$this->input->post('value1');
        $information2 = $this->input->post('title2')."|".$this->input->post('value2');
        $titlefie=$this->input->post('titlefie');

        //get hac id
        $get_idhac = $this->form_manager_model->get_idhac($hac);

        //insert into table record
        $data_record=array(
                        'hac'=>$get_idhac,
                        'inspection_type'=>'IR',
                        'inspection_id'=>'',
                        'datetime'=>$datetime,
                        'remarks'=>$remarks,
                        'recomendation'=>$recomendation,
                        'severity_level'=>$severity_level,
                        'status'=>'unpublish',
                        'user'=>$user,
                        'publish_by'=>''
                    );
        $this->users_model->update("record",$id,"id",$data_record);

        //upload File
        $config['upload_path']	= "./media/pdf/";
        $config['upload_url']	= base_url().'media/pdf/';
        $config['allowed_types']= '*';
        $config['max_size']     = '2000000';
        $config['max_width']  	= '2000000';
        $config['max_height']  	= '2000000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        if($this->upload->do_upload('upload_file'))
         {
            $image_data1 = $this->upload->data();    
            $img1 = $image_data1['file_name'];
         }else{
            $img1=$upload_file_hidden;
         }

         //select max id record
        $idmax=$this->users_model->get_max_table("id","record");

         //insert into table recod_vibration
        $data_record_vibration=array(
                                        'record_id'=>$id,
                                        'date'=>$date,
                                        'hac'=>$get_idhac,
                                        'description'=>$description,
                                        'area'=>$area,
                                        'upload_file'=>$img1,
                                        'manufacture'=>$manufacture,
                                        'type'=>$type,
                                        'year_build'=>$year_build,
                                        'last_check'=>$last_check,
                                        'last_repaired'=>$last_repaired,
                                        'power'=>$power,
                                        'speed_input'=>$speed_input,
                                        'speed_output'=>$speed_output,
                                        'information1'=>$information1,
                                        'information2'=>$information2
                                    );
        $this->users_model->update("record_inspection_report",$id,"record_id",$data_record_vibration);
        
        $this->users_model->delete('record_inspection_report_image',$id,'record_id');
        if(!$userfile_hidden){
                    
        }else{
            for($i=0;$i<count($userfile_hidden);$i++){
                $data_image=array('record_id'=>$id,'image'=>$userfile_hidden[$i],'title'=>$titlefie[$i]);
                $this->db->insert('record_inspection_report_image',$data_image);
            }
        }
        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        for($i=0; $i<$cpt; $i++)
        {

            $_FILES['userfile']['name']= $files['userfile']['name'][$i];
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i];    



        $this->upload->initialize($this->set_upload_options());
        if($this->upload->do_upload()){
        $data_image=array('record_id'=>$id,'image'=>$_FILES['userfile']['name'],'title'=>$titlefie[$i]);
        $this->db->insert('record_inspection_report_image',$data_image);
        }
        }
        
        //Insert into engineer Remak
        $this->remark_engineer($id,'UPDATE','Update Record Inspection Report','Inspection Report');

        //insert into activity log
        $this->insert_log_activity("Record Inspection Record",$id,"Update Record Inspection Record");

        redirect("record/add_inspection/"); 
	
	}
        
        function set_upload_options()
        {   
        //  upload an image options
            $config = array();
            $config['upload_path']	= "./media/images/";
            $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';  
            $config['max_size']	= '100';
            $config['max_width'] = '1024';
            $config['max_height'] = '768';
            $config['overwrite']     = FALSE;
            return $config;
        }
	
        function delete_image(){
            $id=$this->input->post('id');
            $this->users_model->delete('record_inspection_report_image',$id,'id');
            return true;
        }
        
        function delete($id){
            $this->users_model->delete("record",$id,"id");
            $this->users_model->delete("record_inspection_report",$id,"record_id");
            $this->users_model->delete("record_inspection_report_image",$id,"record_id");
            
            $this->insert_log_activity("Record Inspection Report",$id,"Delete Record Inspection Report");
            redirect("record/add_inspection/"); 
        }
	
	
	
	
}	

	