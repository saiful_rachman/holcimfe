<?php

class Add_penetrant extends CI_controller {

	function __construct()
	{
		parent::__construct();
                $this->load->model('users_model');
                $this->load->model('form_manager_model');
		
		$this->load->library('grocery_crud');	
	}
        
        function insert_log_activity($type,$primarykey,$description){
            $this->load->model('form_manager_model');
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
        function remark_engineer($record_id,$status,$remarks,$type){
            $data=array(
                'record_id'=>$record_id,
                'remarks'=>$remarks,
                'inspector_id'=>$this->session->userdata('users_id'),
                'date_remarks'=>date("Y-m-d H:i:s"),
                'publish'=>'1',
                'type_report'=>$type,
                'status'=>$status
            );
            $this->db->insert('engineer_remark',$data);
        }

	function index()
	{
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="re.id";
                }else{
                    $field=$fieldx;
                }
		$config['base_url'] = base_url().'record/add_penetrant/index/';
                $config['total_rows'] = $this->db->query("select * from record re, record_penetrant_test repenet, hac where re.inspection_type='PT' and re.hac=hac.id and $field LIKE '%$val%' group by re.id")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select *,re.id as idx,re.severity_level from record re, record_penetrant_test repenet, hac where re.inspection_type='PT' and re.hac=hac.id and $field LIKE '%$val%' group by re.id limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('record/add_penetrant', $data); 
	}
	
	function add()
	{       $data['list_plant']=$this->users_model->select_all("master_plant")->result();
		$this->load->view('record/form_add_penetrant',$data); 
	}
	
		function add_post()
	{
			/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$upload_file_simpan = $this->input->post('upload_file_simpan');
		
		$test_object = $this->input->post('test_object');
		$reference_proc_spec = $this->input->post('reference_proc_spec');
		$acceptance_criteria = $this->input->post('acceptance_criteria');
		$method = $this->input->post('method');
		$penetrant_type = $this->input->post('penetrant_type');
		$penetrant_manufacture = $this->input->post('penetrant_manufacture');
		$cleaner_type = $this->input->post('cleaner_type');
		$cleaner_manufacture = $this->input->post('cleaner_manufacture');
		$developer_type = $this->input->post('developer_type');
		$developer_manufacture = $this->input->post('developer_manufacture');
		$pre_cleaning_method = $this->input->post('pre_cleaning_method');
		$penetrant_application = $this->input->post('penetrant_application');
		$developer_application = $this->input->post('developer_application');
		$dwell_time = $this->input->post('dwell_time');
		$developing_time = $this->input->post('developing_time');
		$result = $this->input->post('result');
                $area=$this->input->post('subarea');
                $get_idhac = $this->form_manager_model->get_idhac($hac);
		 $config['upload_path']	= "./media/pdf/";
                $config['upload_url']	= base_url().'media/pdf/';
                $config['allowed_types']= '*';
                $config['max_size']     = '2000000';
                $config['max_width']  	= '2000000';
                $config['max_height']  	= '2000000';
                $this->load->library('upload');
                $this->upload->initialize($config);

                if($this->upload->do_upload('upload_file'))
                 {
                $image_data1 = $this->upload->data();    
                 }
		
		/* PARAM POST/INSERT TO Table record_oil_analysis */
		$data_post_record = array(
                                            'hac' => $get_idhac,
                                            'inspection_type' => 'PT', // Ubah Sesuai code inspection
                                            'datetime' => $datetime,
                                            'remarks' => $remarks,
                                            'recomendation' => $recomendation,
                                            'severity_level' => $severity_level,
                                            'user' => $user
                                            );
									
			/* PROCESS TO INSERT */						
			
		
		/* ---- end --- */
		
		/* PROCESS INSERT TO Table record_oil_analysis */
		if($this->db->insert('record',$data_post_record))
		{
			$data_post['status'] = 'Success';
			
			$record_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
			
			/* PARAM TO INSERT RECORD to table record */
			
			$data_post = array(
						'record_id' => $record_id,
						'test_object' => $test_object,
						'reference_proc_spec' => $reference_proc_spec,
						'acceptance_criteria' => $acceptance_criteria,
						'method' => $method,
						'penetrant_type' => $penetrant_type,
						'penetrant_manufacture' => $penetrant_manufacture,
						'cleaner_type' => $cleaner_type,
						'cleaner_manufacture' => $cleaner_manufacture,
						'developer_type' => $developer_type,
						'developer_manufacture' => $developer_manufacture,
						'pre_cleaning_method' => $pre_cleaning_method,
						'penetrant_application' => $penetrant_application,
						'developer_application' => $developer_application,
						'dwell_time' => $dwell_time,
						'developing_time' => $developing_time,
						'result' => $result,
						'upload_file' => $image_data1['file_name'],
                                                'area'=>$area,
                                                'hac'=>$get_idhac,
                                                'date'=>$date
						);
						
				$this->db->insert('record_penetrant_test',$data_post);
				
			$inspection_id = mysql_insert_id();
				
			$data_update_inspection = array('inspection_id' => $inspection_id);
			$where = array('id' => $record_id);
			
			$this->db->update('record', $data_update_inspection, $where);
		}
		else
		{
			$data_post['status'] = 'Failed';
		}
        
        
        	$this->load->library('upload');

                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                for($i=0; $i<$cpt; $i++)
                {

                    $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                    $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                    $_FILES['userfile']['size']= $files['userfile']['size'][$i];    



                $this->upload->initialize($this->set_upload_options());
                $this->upload->do_upload();
                $data_image=array('record_id'=>$record_id,'image'=>$_FILES['userfile']['name']);
		$this->db->insert('record_penetrant_image',$data_image);
                }
                
                //Insert into engineer Remak
                $this->remark_engineer($record_id,'NEW','Add New Record Penetrant Test','Penetrant Test');
                //insert into activity log
                $this->insert_log_activity("Record Penetrant Test",$record_id,"Create New Record Penetrant Test");
                
		redirect("record/add_penetrant/"); 
			
					
	}
	
	
	function edit($id){
	
//    $data['id'] = $id;
//	
//	$row = $this->db->query('SELECT * FROM `record` as re, record_penetrant_test as repenet WHERE `inspection_type`="PT" and re.inspection_id="'.$id.'" and re.`inspection_id`=repenet.id')->row();
//
//	$data['default']['hac'] = $row->hac; 
//	$data['default']['status'] = $row->status;
//	$data['default']['severity_level'] = $row->severity_level;
//	$data['default']['remarks'] = $row->remarks; 
//	$data['default']['recomendation'] = $row->recomendation; 
//    $data['default']['upload_file'] = $row->upload_file; 
//	$data['default']['test_object'] = $row->test_object; 
//	$data['default']['reference_proc_spec'] = $row->reference_proc_spec; 
//	$data['default']['acceptance_criteria'] = $row->acceptance_criteria; 
//	$data['default']['method'] = $row->method;
//	$data['default']['penetrant_type'] = $row->penetrant_type; 
//	$data['default']['penetrant_manufacture'] = $row->penetrant_manufacture;
//	$data['default']['cleaner_type'] = $row->cleaner_type;
//	$data['default']['cleaner_manufacture'] = $row->cleaner_manufacture;
//	$data['default']['developer_type'] = $row->developer_type;
//	$data['default']['developer_manufacture'] = $row->developer_manufacture;
//	$data['default']['pre_cleaning_method'] = $row->pre_cleaning_method;
//	$data['default']['penetrant_application'] = $row->penetrant_application;
//	$data['default']['developer_application'] = $row->developer_application;
//	$data['default']['dwell_time'] = $row->dwell_time;
//	$data['default']['developing_time'] = $row->developing_time;
//	$data['default']['result'] = $row->result;
        $data['list_image']=$this->users_model->select_all_where("record_penetrant_image",$id,"record_id")->result();
        $data['list_plant']=$this->users_model->select_all("master_plant")->result();
        $data['list']=$this->db->query("select a.*,b.*,c.hac_code,e.image from record a inner join record_penetrant_test b on b.record_id=a.id inner join hac c on a.hac=c.id left join record_penetrant_image e on a.id=e.record_id where a.id='$id'")->row();
	  
	$this->load->view('record/form_edit_penetrant', $data);
	
	}
	
	
	function autocomplete_hac(){
		$keyword = $this->input->post("term");
		$result = $this->db->query('select * from hac where hac_code like "'.$keyword.'%" LIMIT 10')->result_array();
		foreach($result as $row){
			$data[] = array('label'=>$row['hac_code'], 'value'=>$row['hac_code']);
		}
		echo json_encode($data);
	}
	
	
	function autocomplete_hac_detail(){
		$hac = $this->input->post("term");
		
		$main = $this->db->query('select * from hac where hac_code="'.$hac.'"')->row_array();
		$data = array(
			"main" => $main
		);
		
		echo json_encode($data);
	}
	
	function edit_post(){
	
                $id = $this->input->post("id");
		/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$upload_file_simpan = $this->input->post('upload_file_simpan');
		
		$test_object = $this->input->post('test_object');
		$reference_proc_spec = $this->input->post('reference_proc_spec');
		$acceptance_criteria = $this->input->post('acceptance_criteria');
		$method = $this->input->post('method');
		$penetrant_type = $this->input->post('penetrant_type');
		$penetrant_manufacture = $this->input->post('penetrant_manufacture');
		$cleaner_type = $this->input->post('cleaner_type');
		$cleaner_manufacture = $this->input->post('cleaner_manufacture');
		$developer_type = $this->input->post('developer_type');
		$developer_manufacture = $this->input->post('developer_manufacture');
		$pre_cleaning_method = $this->input->post('pre_cleaning_method');
		$penetrant_application = $this->input->post('penetrant_application');
		$developer_application = $this->input->post('developer_application');
		$dwell_time = $this->input->post('dwell_time');
		$developing_time = $this->input->post('developing_time');
		$result = $this->input->post('result');
                $area=$this->input->post('subarea');
                $upload_file_hidden=$this->input->post('upload_file_hidden');
                $get_idhac = $this->form_manager_model->get_idhac($hac);
                $userfile_hidden=$this->input->post('userfile_hidden');
                
		$config['upload_path']	= "./media/pdf/";
                $config['upload_url']	= base_url().'media/pdf/';
                $config['allowed_types']= '*';
                $config['max_size']     = '2000000';
                $config['max_width']  	= '2000000';
                $config['max_height']  	= '2000000';
                $this->load->library('upload');
                $this->upload->initialize($config);

                if($this->upload->do_upload('upload_file'))
                 {
                    $image_data1 = $this->upload->data();  
                    $img1=$image_data1['file_name'];
                 }else{
                     $img1=$upload_file_hidden;
                 }
		
		/* PARAM POST/INSERT TO Table record_oil_analysis */
		$data_post_record = array(
                                        'hac' => $get_idhac,
                                        'inspection_type' => 'PT', // Ubah Sesuai code inspection
                                        'datetime' => $datetime,
                                        'remarks' => $remarks,
                                        'recomendation' => $recomendation,
                                        'severity_level' => $severity_level,
                                        'user' => $user,
                                        'status'=>'unpublish'
                                        );
									
			/* PROCESS TO INSERT */						
			
		
		/* ---- end --- */
		
		/* PROCESS INSERT TO Table record_oil_analysis */
		if($this->users_model->update("record",$id,"id",$data_post_record)){
			$data_post['status'] = 'Success';
			
			//$record_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
			
			/* PARAM TO INSERT RECORD to table record */
			
			$data_post = array(
                                        'test_object' => $test_object,
                                        'reference_proc_spec' => $reference_proc_spec,
                                        'acceptance_criteria' => $acceptance_criteria,
                                        'method' => $method,
                                        'penetrant_type' => $penetrant_type,
                                        'penetrant_manufacture' => $penetrant_manufacture,
                                        'cleaner_type' => $cleaner_type,
                                        'cleaner_manufacture' => $cleaner_manufacture,
                                        'developer_type' => $developer_type,
                                        'developer_manufacture' => $developer_manufacture,
                                        'pre_cleaning_method' => $pre_cleaning_method,
                                        'penetrant_application' => $penetrant_application,
                                        'developer_application' => $developer_application,
                                        'dwell_time' => $dwell_time,
                                        'developing_time' => $developing_time,
                                        'result' => $result,
                                        'upload_file' => $img1,
                                        'area'=>$area,
                                        'hac'=>$get_idhac,
                                        'date'=>$date
                                        );
						
                        $this->users_model->update("record_penetrant_test",$id,"record_id",$data_post);
				
			//$inspection_id = mysql_insert_id();
				
			//$data_update_inspection = array('inspection_id' => $inspection_id);
			//$where = array('id' => $record_id);
			
			//$this->db->update('record', $data_update_inspection, $where);
		}
		else
		{
			$data_post['status'] = 'Failed';
		}
                
                $this->users_model->delete('record_penetrant_image',$id,'record_id');
                if(!$userfile_hidden){
                    
                }else{
                    for($i=0;$i<count($userfile_hidden);$i++){
                        $data_image=array('record_id'=>$id,'image'=>$userfile_hidden[$i]);
                        $this->db->insert('record_penetrant_image',$data_image);
                    }
                }
        	$this->load->library('upload');

                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                for($i=0; $i<$cpt; $i++)
                {

                    $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                    $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                    $_FILES['userfile']['size']= $files['userfile']['size'][$i];    



                $this->upload->initialize($this->set_upload_options());
                if($this->upload->do_upload()){
                $data_image=array('record_id'=>$id,'image'=>$_FILES['userfile']['name']);
		$this->db->insert('record_penetrant_image',$data_image);
                }
                }
                
                //Insert into engineer Remak
                $this->remark_engineer($id,'UPDATE','Update Record Penetrant Test','Penetrant Test');
            
                //insert into activity log
                $this->insert_log_activity("Record Penetrant Test",$id,"Update Record Penetrant Test");
                
		redirect("record/add_penetrant/"); 
	
	}
        
        function set_upload_options()
        {   
        //  upload an image options
            $config = array();
            $config['upload_path']	= "./media/images/";
            $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';  
            $config['max_size']	= '1000000';
            $config['max_width'] = '10240000';
            $config['max_height'] = '7680000';
            $config['overwrite']     = FALSE;
            return $config;
        }
	
	
	function delete_image(){
            $id=$this->input->post('id');
            $this->users_model->delete('record_penetrant_image',$id,'id');
            return true;
        }
        
        function delete($id){
            $this->users_model->delete("record",$id,"id");
            $this->users_model->delete("record_penetrant_test",$id,"record_id");
            $this->users_model->delete("record_penetrant_image",$id,"record_id");
            
            $this->insert_log_activity("Report Penetrant",$id,"Delete Report Penetrant");
            redirect("record/add_penetrant/"); 
        }
	
	
}	

	
