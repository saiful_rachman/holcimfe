<?php $this->load->view('includes/header.php') ?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner">
            <div style="background-color: #5bb75b;"><b><h4>REPORT THICKNESS MEASUREMENT</b></h4></div>
			<div class="btn-group">
                             <?php       
                                if($type=="report_thickness_stack_list"){
                                    $st="warning";
                                }else {
                                    $st="default";
                                }
                                if($type=="report_thickness_kiln_list"){
                                    $kl="warning";
                                }else {
                                    $kl="default";
                                }
                                if($type=="report_thickness_general_list"){
                                    $gn="warning";
                                }else {
                                    $gn="default";
                                }
                              ?>
                            <a href="<?=base_url()?>report/list_thickness/general/" class="btn btn-<?=$gn;?>">General</a>
                            <a href="<?=base_url()?>report/list_thickness/kiln" class="btn btn-<?=$kl;?>">Thickness Kiln</a>
                            <a href="<?=base_url()?>report/list_thickness/stack" class="btn btn-<?=$st;?>">Thickness Stack</a>
<!--                        <a href="<?=base_url()?>report/list_thickness/index" class="btn btn-default">All</a>-->
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
                                                <th>HAC</th>
						<th>Datetime</th>
						<th>Status</th>
                                                <th>User</th>
                                                <th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
                                <?php
                                $offset = $this->uri->segment(5);
                                $jum=count($list);
                                if($jum == "0"){
                                    echo "<tr><td colspan='6' style='text-align: center;'>Not Data Found</td></tr>";
                                }else{
                                $id=1; foreach($list as $row) :?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->hac_code; ?></td>
						<td><?php echo $row->datetime; ?></td>
						<td><?php echo $row->status; ?></td>
                                                <td><?php echo $row->nip." - ".$row->nama; ?></td>
						<td style="width: 120px;text-align: center;">
                                                    <a href="<?=base_url();?>report/main_report_list/<?=$type;?>/<?php echo $row->id; ?>" class="btn btn-warning">View</a> 
                                                    <a href="<?=base_url();?>print_data_report/thickness/<?php echo $row->id; ?>" target="_blank" class="btn btn-warning">Print</a>
                                                </td>
						
					</tr>
					<?php endforeach; 
                                        if($row->inspection_type=="THICK_STACK"){
                                            $to="stack";
                                        }else if($row->inspection_type=="THICK_KILN"){
                                            $to="kiln";
                                        }else if($row->inspection_type=="THICK_GENERAL"){
                                            $to="general";
                                        }else{
                                            $to="index";
                                        }
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/thickness/<?=$to;?>" id="fhac_code"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('hac_code');}else{return false;};" name="val" /><input type="hidden" name="field" value="hac_code"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/thickness/<?=$to;?>" id="fdatetime"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('datetime');}else{return false;};" name="val" /><input type="hidden" name="field" value="datetime"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/thickness/<?=$to;?>" id="fstatus"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('status');}else{return false;};" name="val" /><input type="hidden" name="field" value="status"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/thickness/<?=$to;?>" id="fuser"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('nama');}else{return false;};" name="val" /><input type="hidden" name="field" value="nama"></form></td>
                                            <td></td>
                                        </tr>
                                <?php } ?>
				</tbody>
			</table>
                            <div class="pagination"><?=$halaman;?></div>
			</div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript">
  function deletex(id){
   var tanya = confirm("Are you sure?");
        if(tanya){
            var url = "<?php echo base_url(); ?>engine/crud_users/"+id;
            window.location.replace(url);
        }else{
        }
}
</script>
<script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>