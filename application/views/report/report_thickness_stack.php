<?php
	$this->load->view('includes/header.php');
?>

<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
						<?php  if ($severity_level == '0'){
                            echo "<div class='green'></div>";
                            }elseif ($severity_level == '1'){
                            echo "<div class='yellow'></div>";
                            }elseif ($severity_level == '2'){
                            echo "<div class='red'></div>";
                            }
                        ?>
					</div>
					<div class="span2 status-haccode">
						<h4><?php echo $hac_code;?></h4>
					</div>
					<div class="span4">
						<div id="pic-container">
							<center>
						<li class="dropdown" style="list-style: none;">
                        <a href="#" class="btn dropdown-toggle" data-toggle="dropdown">View User <b class="caret"></b></a>                      
                        <ul class="dropdown-menu mega-menu">
                            
                            <?php foreach($top->result() as $top_row) :?>
						    <li class="mega-menu-column">
    						    <ul>
    						        <li class="nav-header"><div class="first">
    								<img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?> " class=""/>
    								<div class="name"><?php echo $top_row->nama;?></div>
    								<div class="nip">ID: <?php echo $top_row->nip;?></div>
    							</div></li>
    						    </ul>
                            </li> 
                            <?php endforeach;?>
						
                        </ul><!-- dropdown-menu -->
                    </li>
                    </center>
						</div>
					</div>
					<div class="span2 inspection">
						<span class="icon-stack icon-inspection-top" style="float:left;"></span><p>&nbsp;Stack Thickness</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thermo">Thermography</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_stop">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_inspection">Inspection Report</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_vibration">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_general">General Thickness</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_kiln">Kiln Thickness</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_stack">Stack Thickness</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-right back-btn">
							<button onclick="history.go(-1);" class="btn"><i class="icon-chevron-left"></i>Back</button>
						</div>
					</div>
				</div>
			</div>
				
				
			<div class="row-fluid">
				<div class="spacer2"></div>
			</div>
				
			<div class="row-fluid">
				<div class="span4 stack">
					<img src="<?=base_url()?>application/views/assets/report/img/stack.png" width="320" height="725">
				</div>
				<div class="span8">
					<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Data Table</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
                                                                            <th>ANGLE</th>
                                                                            <th>1</th>
                                                                            <th>2</th>
                                                                            <th>3</th>
                                                                            <th>4</th>
                                                                            <th>5</th>
                                                                            <th>6</th>
                                                                            <th>7</th>
                                                                            <th>8</th>
                                                                            <th>9</th>
                                                                            <th>10</th>
                                                                            <th>11</th>
                                                                            <th>12</th>
                                                                            <th>13</th>
                                                                            <th>14</th>
                                                                            <th>15</th>
                                                                            <th>16</th>
									</tr>
								</thead>
								<tbody>
                                                                        <tr>
                                                                                <?php
                                                                                $pt1=explode(",", $stack_main['point_1']);
                                                                                $pt2=explode(",", $stack_main['point_2']);
                                                                                $pt3=explode(",", $stack_main['point_3']);
                                                                                $pt4=explode(",", $stack_main['point_4']);
                                                                                $pt5=explode(",", $stack_main['point_5']);
                                                                                $pt6=explode(",", $stack_main['point_6']);
                                                                                $pt7=explode(",", $stack_main['point_7']);
                                                                                $pt8=explode(",", $stack_main['point_8']);
                                                                                $pt9=explode(",", $stack_main['point_9']);
                                                                                $pt10=explode(",", $stack_main['point_10']);
                                                                                $pt11=explode(",", $stack_main['point_11']);
                                                                                $pt12=explode(",", $stack_main['point_12']);
                                                                                $pt13=explode(",", $stack_main['point_13']);
                                                                                $pt14=explode(",", $stack_main['point_14']);
                                                                                $pt15=explode(",", $stack_main['point_15']);
                                                                                $pt16=explode(",", $stack_main['point_16']);
                                                                                ?>
                                                                            <td>0<sup>o</sup></td>
                                                                            <td><?php echo $stack_main['point_1'];?></td>
                                                                            <td><?php echo $stack_main['point_2'];?></td>
                                                                            <td><?php echo $stack_main['point_3'];?></td>
                                                                            <td><?php echo $stack_main['point_4'];?></td>
                                                                            <td><?php echo $stack_main['point_5'];?></td>
                                                                            <td><?php echo $stack_main['point_6'];?></td>
                                                                            <td><?php echo $stack_main['point_7'];?></td>
                                                                            <td><?php echo $stack_main['point_8'];?></td>
                                                                            <td><?php echo $stack_main['point_9'];?></td>
                                                                            <td><?php echo $stack_main['point_10'];?></td>
                                                                            <td><?php echo $stack_main['point_11'];?></td>
                                                                            <td><?php echo $stack_main['point_12'];?></td>
                                                                            <td><?php echo $stack_main['point_13'];?></td>
                                                                            <td><?php echo $stack_main['point_14'];?></td>
                                                                            <td><?php echo $stack_main['point_15'];?></td>
                                                                            <td><?php echo $stack_main['point_16'];?></td>
									</tr>
									<tr>
                                                                            <td>90<sup>o</sup></td>
                                                                            <td><?php echo $stack_main['point_17'];?></td>
                                                                            <td><?php echo $stack_main['point_18'];?></td>
                                                                            <td><?php echo $stack_main['point_19'];?></td>
                                                                            <td><?php echo $stack_main['point_20'];?></td>
                                                                            <td><?php echo $stack_main['point_21'];?></td>
                                                                            <td><?php echo $stack_main['point_22'];?></td>
                                                                            <td><?php echo $stack_main['point_23'];?></td>
                                                                            <td><?php echo $stack_main['point_24'];?></td>
                                                                            <td><?php echo $stack_main['point_25'];?></td>
                                                                            <td><?php echo $stack_main['point_26'];?></td>
                                                                            <td><?php echo $stack_main['point_27'];?></td>
                                                                            <td><?php echo $stack_main['point_28'];?></td>
                                                                            <td><?php echo $stack_main['point_29'];?></td>
                                                                            <td><?php echo $stack_main['point_30'];?></td>
                                                                            <td><?php echo $stack_main['point_31'];?></td>
                                                                            <td><?php echo $stack_main['point_32'];?></td>
									</tr>
									<tr>
                                                                            <td>180<sup>o</sup></td>
                                                                            <td><?php echo $stack_main['point_33'];?></td>
                                                                            <td><?php echo $stack_main['point_34'];?></td>
                                                                            <td><?php echo $stack_main['point_35'];?></td>
                                                                            <td><?php echo $stack_main['point_36'];?></td>
                                                                            <td><?php echo $stack_main['point_37'];?></td>
                                                                            <td><?php echo $stack_main['point_38'];?></td>
                                                                            <td><?php echo $stack_main['point_39'];?></td>
                                                                            <td><?php echo $stack_main['point_40'];?></td>
                                                                            <td><?php echo $stack_main['point_41'];?></td>
                                                                            <td><?php echo $stack_main['point_42'];?></td>
                                                                            <td><?php echo $stack_main['point_43'];?></td>
                                                                            <td><?php echo $stack_main['point_44'];?></td>
                                                                            <td><?php echo $stack_main['point_45'];?></td>
                                                                            <td><?php echo $stack_main['point_46'];?></td>
                                                                            <td><?php echo $stack_main['point_47'];?></td>
                                                                            <td><?php echo $stack_main['point_48'];?></td>
									</tr>
									<tr>
                                                                            <td>270<sup>o</sup></td>
                                                                            <td><?php echo $stack_main['point_49'];?></td>
                                                                            <td><?php echo $stack_main['point_50'];?></td>
                                                                            <td><?php echo $stack_main['point_51'];?></td>
                                                                            <td><?php echo $stack_main['point_52'];?></td>
                                                                            <td><?php echo $stack_main['point_53'];?></td>
                                                                            <td><?php echo $stack_main['point_54'];?></td>
                                                                            <td><?php echo $stack_main['point_55'];?></td>
                                                                            <td><?php echo $stack_main['point_56'];?></td>
                                                                            <td><?php echo $stack_main['point_57'];?></td>
                                                                            <td><?php echo $stack_main['point_58'];?></td>
                                                                            <td><?php echo $stack_main['point_59'];?></td>
                                                                            <td><?php echo $stack_main['point_60'];?></td>
                                                                            <td><?php echo $stack_main['point_61'];?></td>
                                                                            <td><?php echo $stack_main['point_62'];?></td>
                                                                            <td><?php echo $stack_main['point_63'];?></td>
                                                                            <td><?php echo $stack_main['point_64'];?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Remarks</h3>
						<div><?php echo $stack_main['remarks'];?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div><?php echo $stack_main['recomendation'];?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>DATE</th>
										<th>ACTION</th>
										<th>NOTES</th>
										<th>OTHER</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($stack_detail->result() as $stack_detail_row) :?>
									<tr>
										<td><?php echo $stack_detail_row->datetime; ?></td>
										<td><span><?php echo $stack_detail_row->upload_file; ?></span></td>
										<td><?php echo $stack_detail_row->test_object; ?></td>
										<td><?php echo $stack_detail_row->nama; ?></td>
									</tr>
                                    <?php endforeach;?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
$this->load->view('includes/footer.php');
?>		

<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>	
<script type="text/javascript">
$(function () {
	$('#container').highcharts({
		title: {
			text: 'Severity Level Trend',
			x: -20 //center
		},
		subtitle: {
			text: '',
			x: -20
		},
		xAxis: {
			categories: [<?php foreach($severity_chart as $severity_chart_row) :?>'<?php echo date("d-m-Y", strtotime($severity_chart_row->datetime)) ?>',<?php endforeach;?>]
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		}
        ,
		tooltip: {
			valueSuffix: ' '
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		series: [{
			name: 'Hac',
                        
			data: [<?php foreach($severity_chart as $severity_chart_row) :?>{y:<?php echo $severity_chart_row->severity_level;?>,color:'<?php $rian = $severity_chart_row->severity_level;
                                                        
                                                        if($rian =='0'){
                                                            
                                                            echo "#2e9612";
                                                            
                                                        }elseif($rian =='1'){
                                                            
                                                            echo "#f8fb00";
                                                        }
                                                        elseif($rian =='2'){
                                                            
                                                            echo "#e40000";
                                                        }
                                                        
                                                        
                                                        ?>'},<?php endforeach;?>],
		},
        {
            name: 'Normal',
            data: [],
            marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/green.png)'
            }
        },
        {
            name: 'Warning',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/yellow.png)'
            }
        },
        {
            name: 'Danger',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/red.png)'
            }
        }]
	});
});


</script>