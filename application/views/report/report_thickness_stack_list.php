<?php
	$this->load->view('includes/header.php');
?>
<style>
    a.tt{
        position: relative;
        z-index: 24;
        text-decoration: none;
    }
    a.tt span{
        display: none;
    }
    a.tt:hover{
        z-index: 25;
    }
    a.tt:hover span.tooltipx{
        display: block;
        position: absolute;
        //top: 10px; left:0;
        //padding: 0 20px 50px 0;
        //width: 200px;
        margin-left: 54px;
        margin-top: -52px;
        color: #993300;
        text-align: center;
    }
</style>
<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
						<?php  if ($severity_level == '0'){
                            echo "<div class='green'></div>";
                            }elseif ($severity_level == '1'){
                            echo "<div class='yellow'></div>";
                            }elseif ($severity_level == '2'){
                            echo "<div class='red'></div>";
                            }
                        ?>
					</div>
					<div class="span2 status-haccode">
						<h4 style="text-align:center;"><?php echo $hac_code;?></h4>
					</div>
					<div class="span4">
						<div id="pic-container">
                       
                            <?php foreach($top as $top_row) :?>
                                <div style="float: left;padding-left: 3px;margin-top: -10px;">	    
                                    <a href="#" class="tt"><img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="50px" height="50px" style="border: 1px solid;"/>
                                    <span class="topx"></span>
                                    <span class="tooltipx">
                                        <img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="150px" height="150px" style="border: 1px solid;"/>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;NIP : <?php echo $top_row->nip;?></div>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;Nama : <?php echo $top_row->nama;?></div>
                                    </span>
                                    <span class="bottomx"></span></a>
                                </div>	
                            <?php endforeach;?>
                    
						</div>
					</div>
					<div class="span2 inspection">
						<span class="icon-stack icon-inspection-top" style="float:left;"></span><p>&nbsp;Stack Thickness</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_running_inspection">Running Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_stop_inspection">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_vibration/index/publish">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_lubricant/index/publish">Lubricant Logbook</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_oil/index/publish">Oil Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_ultrasonic/index/publish">Ultrasonic Test</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_penetrant/index/publish">Penetrant Test</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_thickness/general/publish">Thicknes Measurement</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_thermo/index/publish">Thermography</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_mca/index/publish">MCA</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_mcsa/index/publish">MCSA</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_inspection/index/publish">Inspection Report</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_wear_inspection">Wear Measurement</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_others/index/publish">Other Report</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-right back-btn">
                                                    <a href="<?=base_url();?>report/list_thickness/stack/"><button class="btn"><i class="icon-chevron-left"></i>Back</button></a>
						</div>
					</div>
				</div>
			</div>
				
				
			<div class="row-fluid">
                            <div class="span12" style="padding-top: 10px;">
					<div class="pull-left">
					<?php if($prev_last->id == $id ) {?>
							
					<?php }else{?>
							<a href="<?php echo base_url()?>report/main_report_list/report_thickness_stack_list/<?php echo $prev->id; ?>" class="btn"><i class="icon-chevron-left"></i>Prev</a>
						<?php }?>
					</div>
				<div class="pull-right">
				<?php if($next_last->id == $id ) {?>
					
					<?php }else{?>
						<a href="<?php echo base_url()?>report/main_report_list/report_thickness_stack_list/<?php echo $next->id; ?>" class="btn">Next<i class="icon-chevron-right"></i></a>
					<?php }?>
				</div>
				</div>
                            <div class="span12"></div>
			</div>
				
			<div class="row-fluid">
				<div class="span4 stack">
					<img src="<?=base_url()?>application/views/assets/report/img/stack.png" width="320" height="725">
				</div>
				<div class="span8">
					<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Data Table</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>ANGLE</th>
                                                                                <th style="text-align:center;">1</th>
										<th style="text-align:center;">2</th>
										<th style="text-align:center;">3</th>
										<th style="text-align:center;">4</th>
										<th style="text-align:center;">5</th>
										<th style="text-align:center;">6</th>
										<th style="text-align:center;">7</th>
										<th style="text-align:center;">8</th>
										<th style="text-align:center;">9</th>
										<th style="text-align:center;">10</th>
										<th style="text-align:center;">11</th>
										<th style="text-align:center;">12</th>
										<th style="text-align:center;">13</th>
										<th style="text-align:center;">14</th>
										<th style="text-align:center;">15</th>
										<th style="text-align:center;">16</th>
									</tr>
								</thead>
								<tbody>
                                                                    <?php
                                                                                $pt1=explode(",", $stack_main['point_1']);
                                                                                $pt2=explode(",", $stack_main['point_2']);
                                                                                $pt3=explode(",", $stack_main['point_3']);
                                                                                $pt4=explode(",", $stack_main['point_4']);
                                                                                $pt5=explode(",", $stack_main['point_5']);
                                                                                $pt6=explode(",", $stack_main['point_6']);
                                                                                $pt7=explode(",", $stack_main['point_7']);
                                                                                $pt8=explode(",", $stack_main['point_8']);
                                                                                $pt9=explode(",", $stack_main['point_9']);
                                                                                $pt10=explode(",", $stack_main['point_10']);
                                                                                $pt11=explode(",", $stack_main['point_11']);
                                                                                $pt12=explode(",", $stack_main['point_12']);
                                                                                $pt13=explode(",", $stack_main['point_13']);
                                                                                $pt14=explode(",", $stack_main['point_14']);
                                                                                $pt15=explode(",", $stack_main['point_15']);
                                                                                $pt16=explode(",", $stack_main['point_16']);
                                                                                ?>
                                                                        <tr>
                                                                            <td>0<sup>o</sup></td>
                                                                            <td style="text-align:center;"><?php echo $pt1[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt2[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt3[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt4[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt7[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt8[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt9[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt10[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt11[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt12[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt13[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt14[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt15[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt16[0];?></td>
									</tr>
									<tr>
                                                                            <td>90<sup>o</sup></td>
                                                                            <td style="text-align:center;"><?php echo $pt1[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt2[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt3[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt4[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt7[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt8[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt9[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt10[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt11[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt12[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt13[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt14[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt15[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt16[1];?></td>
									</tr>
									<tr>
                                                                            <td>180<sup>o</sup></td>
                                                                            <td style="text-align:center;"><?php echo $pt1[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt2[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt3[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt4[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt7[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt8[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt9[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt10[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt11[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt12[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt13[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt14[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt15[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt16[2];?></td>
									</tr>
									<tr>
                                                                            <td>270<sup>o</sup></td>
                                                                            <td style="text-align:center;"><?php echo $pt1[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt2[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt3[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt4[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt7[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt8[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt9[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt10[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt11[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt12[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt13[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt14[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt15[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt16[3];?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Remarks</h3>
						<div><?php echo $stack_main['remarks'];?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div><?php echo $stack_main['recomendation'];?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th style="text-align:center;">DATE</th>
										<th style="text-align:center;">ACTION</th>
										<th style="text-align:center;">NOTES</th>
										<th style="text-align:center;">OTHER</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($stack_detail->result() as $stack_detail_row) :?>
									<tr>
										<td style="text-align:center;"><?php echo $stack_detail_row->datetime; ?></td>
										<td><span style="text-align:center;"><?php echo $stack_detail_row->upload_file; ?></span></td>
										<td style="text-align:center;"><?php echo $stack_detail_row->test_object; ?></td>
										<td><a href="<?=base_url();?>report/main_report_list/report_thickness_stack_list/<?=$kiln_detail_row->id;?>">View</a></td>
									</tr>
                                    <?php endforeach;?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
$this->load->view('includes/footer.php');
?>		

<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>	
<script type="text/javascript">
$(function () {
	$('#container').highcharts({
		title: {
			text: 'Severity Level Trend',
			x: -20 //center
		},
		subtitle: {
			text: '',
			x: -20
		},
		xAxis: {
			categories: [<?php foreach($severity_chart as $severity_chart_row) :?>'<?php echo date("d-m-Y", strtotime($severity_chart_row->datetime)) ?>',<?php endforeach;?>]
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		}
        ,
		tooltip: {
			valueSuffix: ' '
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		series: [{
			name: 'Hac',
                        
			data: [<?php foreach($severity_chart as $severity_chart_row) :?>{y:<?php echo $severity_chart_row->severity_level;?>,color:'<?php $rian = $severity_chart_row->severity_level;
                                                        
                                                        if($rian =='0'){
                                                            
                                                            echo "#2e9612";
                                                            
                                                        }elseif($rian =='1'){
                                                            
                                                            echo "#f8fb00";
                                                        }
                                                        elseif($rian =='2'){
                                                            
                                                            echo "#e40000";
                                                        }
                                                        
                                                        
                                                        ?>'},<?php endforeach;?>],
		},
        {
            name: 'Normal',
            data: [],
            marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/green.png)'
            }
        },
        {
            name: 'Warning',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/yellow.png)'
            }
        },
        {
            name: 'Danger',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/red.png)'
            }
        }]
	});
});


</script>