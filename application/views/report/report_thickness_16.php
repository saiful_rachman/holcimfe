<?php
	$this->load->view('includes/header.php');
?>

<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
						<div class="yellow"></div>
					</div>
					<div class="span2 status-haccode">
						<h2>491-AD4</h2>
					</div>
					<div class="span4">
						<div id="pic-container">
							<div class="first">
								<img src="https://s3.amazonaws.com/uifaces/faces/twitter/jennyshen/128.jpg" class="thumb">
								<div class="name">Lili Liliput</div>
								<div class="nip">ID: 2948573587</div>
							</div>
							<div class="second">
								<img src="https://s3.amazonaws.com/uifaces/faces/twitter/diesellaws/128.jpg" class="thumb">
								<div class="name">Jackie Permana</div>
								<div class="nip">ID: 2948573988</div>
							</div>
						</div>
					</div>
					<div class="span2 inspection">
						<span class="icon-stack icon-inspection-top" style="float:left;"></span><p>&nbsp;Stack Thickness</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thermo">Thermography</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_stop">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_inspection">Inspection Report</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_vibration">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_80">Kiln Thickness</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_16">Stack Thickness</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-right back-btn">
							<a href="<?=base_url()?>report/main_report/hac" class="btn"><i class="icon-chevron-left"></i>Back</a>
						</div>
					</div>
				</div>
			</div>
				
				
			<div class="row-fluid">
				<div class="spacer2"></div>
			</div>
				
			<div class="row-fluid">
				<div class="span4 stack">
					<img src="<?=base_url()?>application/views/assets/report/img/stack.png" width="320" height="725">
				</div>
				<div class="span8">
					<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Data Table</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>ANGLE</th>
										<th>1</th>
										<th>2</th>
										<th>3</th>
										<th>4</th>
										<th>5</th>
										<th>6</th>
										<th>7</th>
										<th>8</th>
										<th>9</th>
										<th>10</th>
										<th>11</th>
										<th>12</th>
										<th>13</th>
										<th>14</th>
										<th>15</th>
										<th>16</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>0<sup>o</sup></td>
										<td>5</td>
										<td>10</td>
										<td>8</td>
										<td>3</td>
										<td>56</td>
										<td>23</td>
										<td>7</td>
										<td>9</td>
										<td>1</td>
										<td>23</td>
										<td>65</td>
										<td>78</td>
										<td>90</td>
										<td>132</td>
										<td>55</td>
										<td>89</td>
									</tr>
									<tr>
										<td>90<sup>o</sup></td>
										<td>3</td>
										<td>6</td>
										<td>9</td>
										<td>30</td>
										<td>12</td>
										<td>54</td>
										<td>8</td>
										<td>9</td>
										<td>34</td>
										<td>98</td>
										<td>21</td>
										<td>60</td>
										<td>25</td>
										<td>109</td>
										<td>22</td>
										<td>44</td>
									</tr>
									<tr>
										<td>180<sup>o</sup></td>
										<td>8</td>
										<td>9</td>
										<td>34</td>
										<td>67</td>
										<td>95</td>
										<td>44</td>
										<td>10</td>
										<td>1</td>
										<td>1</td>
										<td>2</td>
										<td>4</td>
										<td>8</td>
										<td>78</td>
										<td>24</td>
										<td>45</td>
										<td>67</td>
									</tr>
									<tr>
										<td>270<sup>o</sup></td>
										<td>5</td>
										<td>10</td>
										<td>8</td>
										<td>3</td>
										<td>56</td>
										<td>23</td>
										<td>7</td>
										<td>9</td>
										<td>1</td>
										<td>23</td>
										<td>65</td>
										<td>78</td>
										<td>90</td>
										<td>132</td>
										<td>55</td>
										<td>89</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Remarks</h3>
						<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus rutrum adipiscing. Donec sollicitudin ultricies dignissim. Aenean blandit, lacus vel aliquet bibendum, ante libero lacinia purus, quis dictum metus purus id ante. Nam rhoncus neque ut magna gravida, non faucibus tortor porttitor. Mauris ultrices ligula sit amet libero porttitor auctor. Donec gravida elementum posuere. Aliquam scelerisque quam sit amet lorem hendrerit porta. Duis bibendum dapibus orci. Vivamus in libero hendrerit, auctor velit quis, imperdiet odio. Duis hendrerit velit eget pretium feugiat. Quisque metus eros, dictum eget faucibus vel, tincidunt ac est. Sed dapibus pretium sollicitudin.</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus rutrum adipiscing. Donec sollicitudin ultricies dignissim. Aenean blandit, lacus vel aliquet bibendum, ante libero lacinia purus, quis dictum metus purus id ante. Nam rhoncus neque ut magna gravida, non faucibus tortor porttitor. Mauris ultrices ligula sit amet libero porttitor auctor. Donec gravida elementum posuere. Aliquam scelerisque quam sit amet lorem hendrerit porta. Duis bibendum dapibus orci. Vivamus in libero hendrerit, auctor velit quis, imperdiet odio. Duis hendrerit velit eget pretium feugiat. Quisque metus eros, dictum eget faucibus vel, tincidunt ac est. Sed dapibus pretium sollicitudin.</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>DATE</th>
										<th>ACTION</th>
										<th>NOTES</th>
										<th>OTHER</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>14/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>13/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>11/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>10/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>05/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
$this->load->view('includes/footer.php');
?>		

<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>	
<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Thickness Level'
            },
            subtitle: {
                text: 'Source: Holcim CBM Tuban'
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Thickness'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Thickness',
                data: [4, 8, 15, 2, 8, 6, 10, 9, 8, 11, 12, 1]
    
            }]
        });
    });
    

</script>	