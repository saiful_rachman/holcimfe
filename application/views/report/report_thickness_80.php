<?php
	$this->load->view('includes/header.php');
?>

<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
						<div class="red"></div>
					</div>
					<div class="span2 status-haccode">
						<h2>491-AB1</h2>
					</div>
					<div class="span4">
						<div id="pic-container">
							<div class="first">
								<img src="https://s3.amazonaws.com/uifaces/faces/twitter/teeragit/128.jpg" class="thumb">
								<div class="name">Yoseph Otto</div>
								<div class="nip">ID: 2948573564</div>
							</div>
							<div class="second">
								<img src="https://s3.amazonaws.com/uifaces/faces/twitter/ilya_pestov/128.jpg" class="thumb">
								<div class="name">Johan Hendra</div>
								<div class="nip">ID: 2948573991</div>
							</div>
						</div>
					</div>
					<div class="span2 inspection">
						<span class="icon-kiln icon-inspection-top"></span><p>&nbsp;Kiln Thickness</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thermo">Thermography</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_stop">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_inspection">Inspection Report</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_vibration">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_80">Kiln Thickness</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_16">Stack Thickness</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-right back-btn">
							<a href="<?=base_url()?>report/main_report/hac" class="btn"><i class="icon-chevron-left"></i>Back</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer2"></div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="sparepart kiln">
						<img src="<?=base_url()?>application/views/assets/report/img/kiln.png" width="900">
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="span6">
						<div id="container11"></div>
					</div>
					<div class="span6">
						<div id="container12"></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer2"></div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div id="container2"></div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Data Table</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>ANGLE</th>
										<th>1</th>
										<th>2</th>
										<th>3</th>
										<th>4</th>
										<th>5</th>
										<th>6</th>
										<th>7</th>
										<th>8</th>
										<th>9</th>
										<th>10</th>
										<th>11</th>
										<th>12</th>
										<th>13</th>
										<th>14</th>
										<th>15</th>
										<th>16</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>0<sup>o</sup></td>
										<td>5</td>
										<td>10</td>
										<td>8</td>
										<td>3</td>
										<td>56</td>
										<td>23</td>
										<td>7</td>
										<td>9</td>
										<td>1</td>
										<td>23</td>
										<td>65</td>
										<td>78</td>
										<td>90</td>
										<td>132</td>
										<td>55</td>
										<td>89</td>
									</tr>
									<tr>
										<td>90<sup>o</sup></td>
										<td>3</td>
										<td>6</td>
										<td>9</td>
										<td>30</td>
										<td>12</td>
										<td>54</td>
										<td>8</td>
										<td>9</td>
										<td>34</td>
										<td>98</td>
										<td>21</td>
										<td>60</td>
										<td>25</td>
										<td>109</td>
										<td>22</td>
										<td>44</td>
									</tr>
									<tr>
										<td>180<sup>o</sup></td>
										<td>8</td>
										<td>9</td>
										<td>34</td>
										<td>67</td>
										<td>95</td>
										<td>44</td>
										<td>10</td>
										<td>1</td>
										<td>1</td>
										<td>2</td>
										<td>4</td>
										<td>8</td>
										<td>78</td>
										<td>24</td>
										<td>45</td>
										<td>67</td>
									</tr>
									<tr>
										<td>270<sup>o</sup></td>
										<td>5</td>
										<td>10</td>
										<td>8</td>
										<td>3</td>
										<td>56</td>
										<td>23</td>
										<td>7</td>
										<td>9</td>
										<td>1</td>
										<td>23</td>
										<td>65</td>
										<td>78</td>
										<td>90</td>
										<td>132</td>
										<td>55</td>
										<td>89</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Remarks</h3>
						<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus rutrum adipiscing. Donec sollicitudin ultricies dignissim. Aenean blandit, lacus vel aliquet bibendum, ante libero lacinia purus, quis dictum metus purus id ante. Nam rhoncus neque ut magna gravida, non faucibus tortor porttitor. Mauris ultrices ligula sit amet libero porttitor auctor. Donec gravida elementum posuere. Aliquam scelerisque quam sit amet lorem hendrerit porta. Duis bibendum dapibus orci. Vivamus in libero hendrerit, auctor velit quis, imperdiet odio. Duis hendrerit velit eget pretium feugiat. Quisque metus eros, dictum eget faucibus vel, tincidunt ac est. Sed dapibus pretium sollicitudin.</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus rutrum adipiscing. Donec sollicitudin ultricies dignissim. Aenean blandit, lacus vel aliquet bibendum, ante libero lacinia purus, quis dictum metus purus id ante. Nam rhoncus neque ut magna gravida, non faucibus tortor porttitor. Mauris ultrices ligula sit amet libero porttitor auctor. Donec gravida elementum posuere. Aliquam scelerisque quam sit amet lorem hendrerit porta. Duis bibendum dapibus orci. Vivamus in libero hendrerit, auctor velit quis, imperdiet odio. Duis hendrerit velit eget pretium feugiat. Quisque metus eros, dictum eget faucibus vel, tincidunt ac est. Sed dapibus pretium sollicitudin.</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>DATE</th>
										<th>ACTION</th>
										<th>NOTES</th>
										<th>OTHER</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>14/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>13/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>11/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>10/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>05/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
	$this->load->view('includes/footer.php');
?>		

<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/highcharts-more.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>
<script type="text/javascript">
$(function () {
    $('#container11').highcharts({
	    chart: {
	        polar: true
	    },
		colors: [
		   '#4572A7', 
		   '#AA4643', 
		   '#89A54E', 
		   '#80699B', 
		   '#3D96AE', 
		   '#DB843D', 
		   '#92A8CD', 
		   '#A47D7C', 
		   '#B5CA92'
		],
	    title: {
	        text: '15th Measurement'
	    },
	    pane: {
	        startAngle: 0,
	        endAngle: 360
	    },
	    xAxis: {
	        tickInterval: 90,
	        min: 0,
	        max: 360,
	        labels: {
	        	formatter: function () {
	        		return this.value + '°';
	        	}
	        }
	    },
	    yAxis: {
			tickInterval: 50,
	        min: 0,
			max: 200,
	    },
	    plotOptions: {
	        series: {
	            pointStart: 0,
	            pointInterval: 90
	        },
	    },
	    series: [{
	        type: 'area',
	        name: '15',
	        data: [55, 22, 45, 55]
	    }]
	});
});
</script>
<script type="text/javascript">
$(function () {
    $('#container12').highcharts({
	    chart: {
	        polar: true
	    },
		colors: [ 
		   '#AA4643', 
		],
	    title: {
	        text: '16th Measurement'
	    },
	    pane: {
	        startAngle: 0,
	        endAngle: 360
	    },
	    xAxis: {
	        tickInterval: 90,
	        min: 0,
	        max: 360,
	        labels: {
	        	formatter: function () {
	        		return this.value + '°';
	        	}
	        }
	    },
	    yAxis: {
	        tickInterval: 50,
	        min: 0,
			max: 200,
	    },
	    plotOptions: {
	        series: {
	            pointStart: 0,
	            pointInterval: 90
	        },
	    },
	    series: [{
	        type: 'area',
	        name: '16',
	        data: [89, 44, 67, 89]
	    }]
	});
});
</script>
<script type="text/javascript">
$(function () {
	$('#container2').highcharts({
		chart: {
			type: 'column'
		},
		colors: [
		   '#4572A7', 
		   '#AA4643', 
		   '#89A54E', 
		   '#80699B', 
		   '#3D96AE', 
		   '#DB843D', 
		   '#92A8CD', 
		   '#A47D7C', 
		   '#B5CA92'
		],
		title: {
			text: 'Last 16 Measurement'
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16'],
			tickmarkPlacement: 'on',
			title: {
				text: 'Measurement Point'
			}
		},
		yAxis: {
			title: {
				text: 'Value'
			}
		},
		tooltip: {
			pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
			shared: true
		},
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
                        enabled: false,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
				}
			}
		},
		series: [{
			name: '0°',
			data: [5, 10, 8, 3, 56, 23, 7, 9, 1, 23, 65, 78, 90, 132, 55, 89]
		}, {
			name: '90°',
			data: [3, 6, 9, 30, 12, 54, 8, 9, 34, 98, 21, 60, 25, 109, 22, 44]
		}, {
			name: '180°',
			data: [8, 9, 34, 67, 95, 44, 10, 1, 1, 2, 4, 8, 78, 24, 45, 67]
		}, {
			name: '270°',
			data: [5, 10, 8, 3, 56, 23, 7, 9, 1, 23, 65, 78, 90, 132, 55, 89]
		}]
	});
});
</script>	
	