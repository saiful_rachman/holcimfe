<?php
	$this->load->view('includes/header.php');
?>
<style>
    a.tt{
        position: relative;
        z-index: 24;
        text-decoration: none;
    }
    a.tt span{
        display: none;
    }
    a.tt:hover{
        z-index: 25;
    }
    a.tt:hover span.tooltipx{
        display: block;
        position: absolute;
        //top: 10px; left:0;
        //padding: 0 20px 50px 0;
        //width: 200px;
        margin-left: 54px;
        margin-top: -52px;
        color: #993300;
        text-align: center;
    }
</style>
<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
						 <?php  if ($severity_level == '0'){
                            echo "<div class='green'></div>";
                            }elseif ($severity_level == '1'){
                            echo "<div class='yellow'></div>";
                            }elseif ($severity_level == '2'){
                            echo "<div class='red'></div>";
                            }
                        ?>
					</div>
					<div class="span2 status-haccode">
						<h4><?php echo $hac_code;?></h4>
					</div>
					<div class="span4">
						<div id="pic-container">
                       
                            <?php foreach($top as $top_row) :?>
                                <div style="float: left;padding-left: 3px;margin-top: -10px;">	    
                                    <a href="#" class="tt"><img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="50px" height="50px" style="border: 1px solid;"/>
                                    <span class="topx"></span>
                                    <span class="tooltipx">
                                        <img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="150px" height="150px" style="border: 1px solid;"/>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;NIP : <?php echo $top_row->nip;?></div>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;Nama : <?php echo $top_row->nama;?></div>
                                    </span>
                                    <span class="bottomx"></span></a>
                                </div>	
                            <?php endforeach;?>
                    
						</div>
					</div>
					<div class="span2 inspection">
						<span class="icon-kiln icon-inspection-top"></span><p>&nbsp;Kiln Thickness</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_running_inspection">Running Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_stop_inspection">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_vibration/index/publish">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_lubricant/index/publish">Lubricant Logbook</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_oil/index/publish">Oil Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_ultrasonic/index/publish">Ultrasonic Test</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_penetrant/index/publish">Penetrant Test</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_thickness/general/publish">Thicknes Measurement</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_thermo/index/publish">Thermography</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_mca/index/publish">MCA</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_mcsa/index/publish">MCSA</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_inspection/index/publish">Inspection Report</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_wear_inspection">Wear Measurement</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_others/index/publish">Other Report</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-right back-btn">
							<a href="<?=base_url();?>report/list_thickness/kiln/"><button class="btn"><i class="icon-chevron-left"></i>Back</button></a>
						</div>
					</div>
				</div>
			</div>
                        <div class="row-fluid">
                            <div class="span12" style="padding-top: 10px;">
                                <div class="pull-left">
                                <?php if($prev_last->id == $id ) {?>

                                <?php }else{?>
                                                <a href="<?php echo base_url()?>report/main_report_list/report_thickness_kiln_list/<?php echo $prev->id; ?>" class="btn"><i class="icon-chevron-left"></i>Prev</a>
                                        <?php }?>
                                </div>
				<div class="pull-right">
				<?php if($next_last->id == $id ) {?>
					
					<?php }else{?>
						<a href="<?php echo base_url()?>report/main_report_list/report_thickness_kiln_list/<?php echo $next->id; ?>" class="btn">Next<i class="icon-chevron-right"></i></a>
					<?php }?>
				</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer2"></div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="sparepart kiln">
						<img src="<?=base_url()?>application/views/assets/report/img/kiln.png" width="900">
					</div>
				</div>
			</div>
			<div class="row-fluid">
                            <input type="hidden" id="get_id" value="<?=$kiln_id;?>"/>
                            <input type="hidden" id="min_id" value="<?=$min_id;?>"/>
                            Select Point: <select name="point" id="point" style="width: 50px;" >
                            <?php
                            for($i=1;$i<=80;$i++){
                            ?>
                               <option value='<?=$i;?>'><?=$i;?></option>
                            <?php } ?>
                            </select>
				<div class="span12">
					<div class="span6">
						<div id="container11"></div>
					</div>
					<div class="span6">
						<div id="container12"></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer2"></div>
			</div>
			<div class="row-fluid">
				<div class="span12">
				<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
				</div>
			</div>
                    <div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Data Table</h3>
						<div class="well" style="overflow-x: scroll;">
							<table class="table table-striped">
								<thead>
                                                                            <?php
                                                                                $pt1=explode(",", $kiln_main['point_1']);
                                                                                $pt2=explode(",", $kiln_main['point_2']);
                                                                                $pt3=explode(",", $kiln_main['point_3']);
                                                                                $pt4=explode(",", $kiln_main['point_4']);
                                                                                $pt5=explode(",", $kiln_main['point_5']);
                                                                                $pt6=explode(",", $kiln_main['point_6']);
                                                                                $pt7=explode(",", $kiln_main['point_7']);
                                                                                $pt8=explode(",", $kiln_main['point_8']);
                                                                                $pt9=explode(",", $kiln_main['point_9']);
                                                                                $pt10=explode(",", $kiln_main['point_10']);
                                                                                $pt11=explode(",", $kiln_main['point_11']);
                                                                                $pt12=explode(",", $kiln_main['point_12']);
                                                                                $pt13=explode(",", $kiln_main['point_13']);
                                                                                $pt14=explode(",", $kiln_main['point_14']);
                                                                                $pt15=explode(",", $kiln_main['point_15']);
                                                                                $pt16=explode(",", $kiln_main['point_16']);
                                                                                $pt17=explode(",", $kiln_main['point_17']);
                                                                                $pt18=explode(",", $kiln_main['point_18']);
                                                                                $pt19=explode(",", $kiln_main['point_19']);
                                                                                $pt20=explode(",", $kiln_main['point_20']);
                                                                                
                                                                                $pt21=explode(",", $kiln_main['point_21']);
                                                                                $pt22=explode(",", $kiln_main['point_22']);
                                                                                $pt23=explode(",", $kiln_main['point_23']);
                                                                                $pt24=explode(",", $kiln_main['point_24']);
                                                                                $pt25=explode(",", $kiln_main['point_25']);
                                                                                $pt26=explode(",", $kiln_main['point_26']);
                                                                                $pt27=explode(",", $kiln_main['point_27']);
                                                                                $pt28=explode(",", $kiln_main['point_28']);
                                                                                $pt29=explode(",", $kiln_main['point_29']);
                                                                                $pt30=explode(",", $kiln_main['point_30']);
                                                                                
                                                                                $pt31=explode(",", $kiln_main['point_31']);
                                                                                $pt32=explode(",", $kiln_main['point_32']);
                                                                                $pt33=explode(",", $kiln_main['point_33']);
                                                                                $pt34=explode(",", $kiln_main['point_34']);
                                                                                $pt35=explode(",", $kiln_main['point_35']);
                                                                                $pt36=explode(",", $kiln_main['point_36']);
                                                                                $pt37=explode(",", $kiln_main['point_37']);
                                                                                $pt38=explode(",", $kiln_main['point_38']);
                                                                                $pt39=explode(",", $kiln_main['point_39']);
                                                                                $pt40=explode(",", $kiln_main['point_40']);
                                                                                
                                                                                $pt41=explode(",", $kiln_main['point_41']);
                                                                                $pt42=explode(",", $kiln_main['point_42']);
                                                                                $pt43=explode(",", $kiln_main['point_43']);
                                                                                $pt44=explode(",", $kiln_main['point_44']);
                                                                                $pt45=explode(",", $kiln_main['point_45']);
                                                                                $pt46=explode(",", $kiln_main['point_46']);
                                                                                $pt47=explode(",", $kiln_main['point_47']);
                                                                                $pt48=explode(",", $kiln_main['point_48']);
                                                                                $pt49=explode(",", $kiln_main['point_49']);
                                                                                $pt50=explode(",", $kiln_main['point_50']);
                                                                                
                                                                                $pt51=explode(",", $kiln_main['point_51']);
                                                                                $pt52=explode(",", $kiln_main['point_52']);
                                                                                $pt53=explode(",", $kiln_main['point_53']);
                                                                                $pt54=explode(",", $kiln_main['point_54']);
                                                                                $pt55=explode(",", $kiln_main['point_55']);
                                                                                $pt56=explode(",", $kiln_main['point_56']);
                                                                                $pt57=explode(",", $kiln_main['point_57']);
                                                                                $pt58=explode(",", $kiln_main['point_58']);
                                                                                $pt59=explode(",", $kiln_main['point_59']);
                                                                                $pt60=explode(",", $kiln_main['point_60']);
                                                                                
                                                                                $pt61=explode(",", $kiln_main['point_61']);
                                                                                $pt62=explode(",", $kiln_main['point_62']);
                                                                                $pt63=explode(",", $kiln_main['point_63']);
                                                                                $pt64=explode(",", $kiln_main['point_64']);
                                                                                $pt65=explode(",", $kiln_main['point_65']);
                                                                                $pt66=explode(",", $kiln_main['point_66']);
                                                                                $pt67=explode(",", $kiln_main['point_67']);
                                                                                $pt68=explode(",", $kiln_main['point_68']);
                                                                                $pt69=explode(",", $kiln_main['point_69']);
                                                                                $pt70=explode(",", $kiln_main['point_70']);
                                                                                
                                                                                $pt71=explode(",", $kiln_main['point_71']);
                                                                                $pt72=explode(",", $kiln_main['point_72']);
                                                                                $pt73=explode(",", $kiln_main['point_73']);
                                                                                $pt74=explode(",", $kiln_main['point_74']);
                                                                                $pt75=explode(",", $kiln_main['point_75']);
                                                                                $pt76=explode(",", $kiln_main['point_76']);
                                                                                $pt77=explode(",", $kiln_main['point_77']);
                                                                                $pt78=explode(",", $kiln_main['point_78']);
                                                                                $pt79=explode(",", $kiln_main['point_79']);
                                                                                $pt80=explode(",", $kiln_main['point_80']);
                                                                                
                                                                            ?>
									<tr>
										<th>ANGLE</th>
										<?php
                                                                                for($i=1;$i<=80;$i++){
                                                                                    echo"<th style='text-align: center;'>$i</th>";
                                                                                }
                                                                                ?>
									</tr>
								</thead>
								<tbody>
									<tr>
                                                                            <td>0<sup>o</sup></td>
                                                                            <td style="text-align:center;"><?php echo $pt1[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt2[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt3[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt4[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt7[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt8[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt9[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt10[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt11[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt12[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt13[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt14[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt15[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt16[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt17[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt18[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt19[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt20[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt21[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt22[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt23[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt24[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt25[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt26[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt27[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt28[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt29[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt30[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt31[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt32[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt33[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt34[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt35[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt36[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt37[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt38[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt39[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt40[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt41[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt42[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt43[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt44[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt45[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt46[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt47[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt48[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt49[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt50[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt51[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt52[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt53[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt54[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt55[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt56[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt57[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt58[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt59[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt60[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt61[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt62[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt63[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt64[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt65[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt66[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt67[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt68[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt69[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt70[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt71[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt72[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt73[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt74[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt75[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt76[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt77[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt78[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt79[0];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt80[0];?></td>
									</tr>
									<tr>
                                                                            <td>90<sup>o</sup></td>
                                                                            <td style="text-align:center;"><?php echo $pt1[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt2[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt3[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt4[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt7[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt8[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt9[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt10[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt11[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt12[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt13[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt14[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt15[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt16[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt17[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt18[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt19[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt20[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt21[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt22[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt23[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt24[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt25[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt26[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt27[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt28[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt29[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt30[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt31[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt32[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt33[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt34[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt35[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt36[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt37[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt38[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt39[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt40[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt41[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt42[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt43[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt44[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt45[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt46[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt47[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt48[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt49[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt50[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt51[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt52[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt53[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt54[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt55[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt56[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt57[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt58[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt59[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt60[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt61[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt62[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt63[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt64[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt65[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt66[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt67[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt68[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt69[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt70[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt71[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt72[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt73[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt74[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt75[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt76[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt77[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt78[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt79[1];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt80[1];?></td>
									</tr>
									<tr>
                                                                            <td>180<sup>o</sup></td>
                                                                            <td style="text-align:center;"><?php echo $pt1[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt2[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt3[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt4[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt7[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt8[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt9[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt10[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt11[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt12[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt13[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt14[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt15[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt16[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt17[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt18[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt19[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt20[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt21[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt22[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt23[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt24[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt25[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt26[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt27[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt28[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt29[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt30[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt31[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt32[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt33[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt34[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt35[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt36[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt37[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt38[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt39[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt40[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt41[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt42[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt43[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt44[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt45[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt46[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt47[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt48[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt49[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt50[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt51[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt52[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt53[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt54[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt55[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt56[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt57[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt58[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt59[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt60[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt61[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt62[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt63[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt64[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt65[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt66[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt67[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt68[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt69[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt70[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt71[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt72[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt73[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt74[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt75[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt76[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt77[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt78[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt79[2];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt80[2];?></td>
									</tr>
									<tr>
                                                                            <td>270<sup>o</sup></td>
                                                                            <td style="text-align:center;"><?php echo $pt1[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt2[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt3[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt4[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt5[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt7[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt8[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt9[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt10[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt11[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt12[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt13[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt14[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt15[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt16[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt17[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt18[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt19[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt20[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt21[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt22[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt23[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt24[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt25[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt26[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt27[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt28[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt29[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt30[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt31[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt32[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt33[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt34[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt35[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt36[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt37[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt38[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt39[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt40[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt41[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt42[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt43[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt44[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt45[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt46[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt47[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt48[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt49[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt50[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt51[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt52[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt53[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt54[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt55[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt56[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt57[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt58[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt59[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt60[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt61[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt62[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt63[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt64[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt65[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt66[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt67[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt68[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt69[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt70[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt71[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt72[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt73[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt74[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt75[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt76[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt77[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt78[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt79[3];?></td>
                                                                            <td style="text-align:center;"><?php echo $pt80[3];?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Remarks</h3>
						<div><?php echo $kiln_main['remarks'];?> <input type="hidden" id="aa"></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div><?php echo $kiln_main['recomendation'];?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>DATE</th>
										<th>ACTION</th>
										<th>NOTES</th>
										<th>OTHER</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($kiln_detail->result() as $kiln_detail_row) :?>
									<tr>
										<td><?php echo $kiln_detail_row->datetime; ?></td>
										<td><span><?php echo $kiln_detail_row->upload_file; ?></span></td>
										<td><?php echo $kiln_detail_row->test_object; ?></td>
										<td><a href="<?=base_url();?>report/main_report_list/report_thickness_kiln_list/<?=$kiln_detail_row->id;?>">View</a></td>
									</tr>
                                    <?php endforeach;?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
        </div>
</div>
 
<?php 
	$this->load->view('includes/footer.php');
?>	
<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/highcharts-more.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#point").change(function(){
        var nilai=$("#point").val();
        var id=$("#get_id").val();
        var min_id=$("#min_id").val();
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>report/get_thickness/get_nilai",
          data:"id="+id+"&point=point_"+nilai,
            success: function(response) {
            console.log(response);
            $("#aa").val(response);
            var xx = eval(response);
            //alert(xx);
            $('#container12').highcharts({
	    chart: {
	        polar: true
	    },
		colors: [
		   '#4572A7', 
		   '#AA4643', 
		   '#89A54E', 
		   '#80699B', 
		   '#3D96AE', 
		   '#DB843D', 
		   '#92A8CD', 
		   '#A47D7C', 
		   '#B5CA92'
		],
	    title: {
	        text: '"'+nilai+'" th Measurement Actually'
	    },
	    pane: {
	        startAngle: 0,
	        endAngle: 360
	    },
	    xAxis: {
	        tickInterval: 90,
	        min: 0,
	        max: 360,
	        labels: {
	        	formatter: function () {
	        		return this.value + '°';
	        	}
	        }
	    },
	    yAxis: {
			tickInterval: 50,
	        min: 0,
			max: 200,
	    },
	    plotOptions: {
	        series: {
	            pointStart: 0,
	            pointInterval: 90
	        },
	    },
	    series: [{
	        type: 'area',
	        name: '15',
	        data: xx
	    }]
	});
          },
          error: function(){
            alert("error");
          }
    });
    if(min_id==="0"){
        $('#container11').highcharts({
	    chart: {
	        polar: true
	    },
		colors: [ 
		   '#AA4643', 
		],
	    title: {
	        text: '"'+nilai+'" th Measurement Before'
	    },
	    pane: {
	        startAngle: 0,
	        endAngle: 360
	    },
	    xAxis: {
	        tickInterval: 90,
	        min: 0,
	        max: 360,
	        labels: {
	        	formatter: function () {
	        		return this.value + '°';
	        	}
	        }
	    },
	    yAxis: {
	        tickInterval: 50,
	        min: 0,
			max: 200,
	    },
	    plotOptions: {
	        series: {
	            pointStart: 0,
	            pointInterval: 90
	        },
	    },
	    series: [{
	        type: 'area',
	        name: '16',
	        data: [0,0,0,0]
	    }]
	});
    }else{
    $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>report/get_thickness/get_nilai",
          data:"id="+min_id+"&point=point_"+nilai,
            success: function(response) {
            console.log(response);
            $("#aa").val(response);
            var xx = eval(response);
            //alert(xx);
            $('#container11').highcharts({
	    chart: {
	        polar: true
	    },
		colors: [
		   '#AA4643', 
		],
	    title: {
	        text: '"'+nilai+'" th Measurement Before'
	    },
	    pane: {
	        startAngle: 0,
	        endAngle: 360
	    },
	    xAxis: {
	        tickInterval: 90,
	        min: 0,
	        max: 360,
	        labels: {
	        	formatter: function () {
	        		return this.value + '°';
	        	}
	        }
	    },
	    yAxis: {
			tickInterval: 50,
	        min: 0,
			max: 200,
	    },
	    plotOptions: {
	        series: {
	            pointStart: 0,
	            pointInterval: 90
	        },
	    },
	    series: [{
	        type: 'area',
	        name: '15',
	        data: xx
	    }]
	});
          },
          error: function(){
            alert("error");
          }
    });
    }
});
});
</script>
<script type="text/javascript">
$(function () {
    var data = [
        1.8446, 1.8445, 1.8444, 1.8451,    1.8418, 1.8264,    1.8258, 1.8232,    1.8233, 1.8258,
        1.8283, 1.8278, 1.8256, 1.8292,    1.8239, 1.8239,    1.8245, 1.8265,    1.8261, 1.8269,
        1.8273, 1.8244, 1.8244, 1.8172,    1.8139, 1.8146,    1.8164, 1.82,    1.8269, 1.8269,
        1.8269, 1.8258, 1.8247, 1.8286,    1.8289, 1.8316,    1.832, 1.8333,    1.8352, 1.8357,
        1.8355, 1.8354, 1.8403, 1.8403,    1.8406, 1.8403,    1.8396, 1.8418,    1.8409, 1.8384,
        1.8386, 1.8372, 1.839, 1.84, 1.8389, 1.84, 1.8423, 1.8423, 1.8435, 1.8422,
        1.838, 1.8373, 1.8316, 1.8303,    1.8303, 1.8302,    1.8369, 1.84, 1.8385, 1.84,
        1.8401, 1.8402, 1.8381, 1.8351,    1.8314, 1.8273,    1.8213, 1.8207,    1.8207, 1.8215,
        1.8242, 1.8273, 1.8301, 1.8346,    1.8312, 1.8312,    1.8312, 1.8306,    1.8327, 1.8282,
        1.824, 1.8255, 1.8256, 1.8273, 1.8209, 1.8151, 1.8149, 1.8213, 1.8273, 1.8273,
        1.8261, 1.8252, 1.824, 1.8262, 1.8258, 1.8261, 1.826, 1.8199, 1.8153, 1.8097,
        1.8101, 1.8119, 1.8107, 1.8105,    1.8084, 1.8069,    1.8047, 1.8023,    1.7965, 1.7919,
        1.7921, 1.7922, 1.7934, 1.7918,    1.7915, 1.787, 1.7861, 1.7861, 1.7853, 1.7867,
        1.7827, 1.7834, 1.7766, 1.7751, 1.7739, 1.7767, 1.7802, 1.7788, 1.7828, 1.7816,
        1.7829, 1.783, 1.7829, 1.7781, 1.7811, 1.7831, 1.7826, 1.7855, 1.7855, 1.7845,
        1.7798, 1.7777, 1.7822, 1.7785, 1.7744, 1.7743, 1.7726, 1.7766, 1.7806, 1.785,
        1.7907, 1.7912, 1.7913, 1.7931, 1.7952, 1.7951, 1.7928, 1.791, 1.7913, 1.7912,
        1.7941, 1.7953, 1.7921, 1.7919, 1.7968, 1.7999, 1.7999, 1.7974, 1.7942, 1.796,
        1.7969, 1.7862, 1.7821, 1.7821, 1.7821, 1.7811, 1.7833, 1.7849, 1.7819, 1.7809,
        1.7809, 1.7827, 1.7848, 1.785, 1.7873, 1.7894, 1.7907, 1.7909, 1.7947, 1.7987,
        1.799, 1.7927, 1.79, 1.7878, 1.7878, 1.7907, 1.7922, 1.7937, 1.786, 1.787,
        1.7838, 1.7838, 1.7837, 1.7836, 1.7806, 1.7825, 1.7798, 1.777, 1.777, 1.7772,
        1.7793, 1.7788, 1.7785, 1.7832, 1.7865, 1.7865, 1.7853, 1.7847, 1.7809, 1.778,
        1.7799, 1.78, 1.7801, 1.7765, 1.7785, 1.7811, 1.782, 1.7835, 1.7845, 1.7844,
        1.782, 1.7811, 1.7795, 1.7794, 1.7806, 1.7794, 1.7794, 1.7778, 1.7793, 1.7808,
        1.7824, 1.787, 1.7894, 1.7893, 1.7882, 1.7871, 1.7882, 1.7871, 1.7878, 1.79,
        1.7901, 1.7898, 1.7879, 1.7886, 1.7858, 1.7814, 1.7825, 1.7826, 1.7826, 1.786,
        1.7878, 1.7868, 1.7883, 1.7893, 1.7892, 1.7876, 1.785, 1.787, 1.7873, 1.7901,
        1.7936, 1.7939, 1.7938, 1.7956, 1.7975, 1.7978, 1.7972, 1.7995, 1.7995, 1.7994,
        1.7976, 1.7977, 1.796, 1.7922, 1.7928, 1.7929, 1.7948, 1.797, 1.7953, 1.7907,
        1.7872, 1.7852, 1.7852, 1.786, 1.7862, 1.7836, 1.7837, 1.784, 1.7867, 1.7867,
        1.7869, 1.7837, 1.7827, 1.7825, 1.7779, 1.7791, 1.779, 1.7787, 1.78, 1.7807,
        1.7803, 1.7817, 1.7799, 1.7799, 1.7795, 1.7801, 1.7765, 1.7725, 1.7683, 1.7641,
        1.7639, 1.7616, 1.7608, 1.759, 1.7582, 1.7539, 1.75, 1.75, 1.7507, 1.7505,
        1.7516, 1.7522, 1.7531, 1.7577, 1.7577, 1.7582, 1.755, 1.7542, 1.7576, 1.7616,
        1.7648, 1.7648, 1.7641, 1.7614, 1.757, 1.7587, 1.7588, 1.762, 1.762, 1.7617,
        1.7618, 1.7615, 1.7612, 1.7596, 1.758, 1.758, 1.758, 1.7547, 1.7549, 1.7613,
        1.7655, 1.7693, 1.7694, 1.7688, 1.7678, 1.7708, 1.7727, 1.7749, 1.7741, 1.7741,
        1.7732, 1.7727, 1.7737, 1.7724, 1.7712, 1.772, 1.7721, 1.7717, 1.7704, 1.769,
        1.7711, 1.774, 1.7745, 1.7745, 1.774, 1.7716, 1.7713, 1.7678, 1.7688, 1.7718,
        1.7718, 1.7728, 1.7729, 1.7698, 1.7685, 1.7681, 1.769, 1.769, 1.7698, 1.7699,
        1.7651, 1.7613, 1.7616, 1.7614, 1.7614, 1.7607, 1.7602, 1.7611, 1.7622, 1.7615,
        1.7598, 1.7598, 1.7592, 1.7573, 1.7566, 1.7567, 1.7591, 1.7582, 1.7585, 1.7613,
        1.7631, 1.7615, 1.76, 1.7613, 1.7627, 1.7627, 1.7608, 1.7583, 1.7575, 1.7562,
        1.752, 1.7512, 1.7512, 1.7517, 1.752, 1.7511, 1.748, 1.7509, 1.7531, 1.7531,
        1.7527, 1.7498, 1.7493, 1.7504, 1.75, 1.7491, 1.7491, 1.7485, 1.7484, 1.7492,
        1.7471, 1.7459, 1.7477, 1.7477, 1.7483, 1.7458, 1.7448, 1.743, 1.7399, 1.7395,
        1.7395, 1.7378, 1.7382, 1.7362, 1.7355, 1.7348, 1.7361, 1.7361, 1.7365, 1.7362,
        1.7331, 1.7339, 1.7344, 1.7327, 1.7327, 1.7336, 1.7333, 1.7359, 1.7359, 1.7372,
        1.736, 1.736, 1.735, 1.7365, 1.7384, 1.7395, 1.7413, 1.7397, 1.7396, 1.7385,
        1.7378, 1.7366, 1.74, 1.7411, 1.7406, 1.7405, 1.7414, 1.7431, 1.7431, 1.7438,
        1.7443, 1.7443, 1.7443, 1.7434, 1.7429, 1.7442, 1.744, 1.7439, 1.7437, 1.7437,
        1.7429, 1.7403, 1.7399, 1.7418, 1.7468, 1.748, 1.748, 1.749, 1.7494, 1.7522,
        1.7515, 1.7502, 1.7472, 1.7472, 1.7462, 1.7455, 1.7449, 1.7467, 1.7458, 1.7427,
        1.7427, 1.743, 1.7429, 1.744, 1.743, 1.7422, 1.7388, 1.7388, 1.7369, 1.7345,
        1.7345, 1.7345, 1.7352, 1.7341, 1.7341, 1.734, 1.7324, 1.7272, 1.7264, 1.7255,
        1.7258, 1.7258, 1.7256, 1.7257, 1.7247, 1.7243, 1.7244, 1.7235, 1.7235, 1.7235,
        1.7235, 1.7262, 1.7288, 1.7301, 1.7337, 1.7337, 1.7324, 1.7297, 1.7317, 1.7315,
        1.7288, 1.7263, 1.7263, 1.7242, 1.7253, 1.7264, 1.727, 1.7312, 1.7305, 1.7305,
        1.7318, 1.7358, 1.7409, 1.7454, 1.7437, 1.7424, 1.7424, 1.7415, 1.7419, 1.7414,
        1.7377, 1.7355, 1.7315, 1.7315, 1.732, 1.7332, 1.7346, 1.7328, 1.7323, 1.734,
        1.734, 1.7336, 1.7351, 1.7346, 1.7321, 1.7294, 1.7266, 1.7266, 1.7254, 1.7242,
        1.7213, 1.7197, 1.7209, 1.721, 1.721, 1.721, 1.7209, 1.7159, 1.7133, 1.7105,
        1.7099, 1.7099, 1.7093, 1.7093, 1.7076, 1.707, 1.7049, 1.7012, 1.7011, 1.7019,
        1.7046, 1.7063, 1.7089, 1.7077, 1.7077, 1.7077, 1.7091, 1.7118, 1.7079, 1.7053,
        1.705, 1.7055, 1.7055, 1.7045, 1.7051, 1.7051, 1.7017, 1.7, 1.6995, 1.6994,
        1.7014, 1.7036, 1.7021, 1.7002, 1.6967, 1.695, 1.695, 1.6939, 1.694, 1.6922,
        1.6919, 1.6914, 1.6894, 1.6891, 1.6904, 1.689, 1.6834, 1.6823, 1.6807, 1.6815,
        1.6815, 1.6847, 1.6859, 1.6822, 1.6827, 1.6837, 1.6823, 1.6822, 1.6822, 1.6792,
        1.6746, 1.6735, 1.6731, 1.6742, 1.6744, 1.6739, 1.6731, 1.6761, 1.6761, 1.6785,
        1.6818, 1.6836, 1.6823, 1.6805, 1.6793, 1.6849, 1.6833, 1.6825, 1.6825, 1.6816,
        1.6799, 1.6813, 1.6809, 1.6868, 1.6933, 1.6933, 1.6945, 1.6944, 1.6946, 1.6964,
        1.6965, 1.6956, 1.6956, 1.695, 1.6948, 1.6928, 1.6887, 1.6824, 1.6794, 1.6794,
        1.6803, 1.6855, 1.6824, 1.6791, 1.6783, 1.6785, 1.6785, 1.6797, 1.68, 1.6803,
        1.6805, 1.676, 1.677, 1.677, 1.6736, 1.6726, 1.6764, 1.6821, 1.6831, 1.6842,
        1.6842, 1.6887, 1.6903, 1.6848, 1.6824, 1.6788, 1.6814, 1.6814, 1.6797, 1.6769,
        1.6765, 1.6733, 1.6729, 1.6758, 1.6758, 1.675, 1.678, 1.6833, 1.6856, 1.6903,
        1.6896, 1.6896, 1.6882, 1.6879, 1.6862, 1.6852, 1.6823, 1.6813, 1.6813, 1.6822,
        1.6802, 1.6802, 1.6784, 1.6748, 1.6747, 1.6747, 1.6748, 1.6733, 1.665, 1.6611,
        1.6583, 1.659, 1.659, 1.6581, 1.6578, 1.6574, 1.6532, 1.6502, 1.6514, 1.6514,
        1.6507, 1.651, 1.6489, 1.6424, 1.6406, 1.6382, 1.6382, 1.6341, 1.6344, 1.6378,
        1.6439, 1.6478, 1.6481, 1.6481, 1.6494, 1.6438, 1.6377, 1.6329, 1.6336, 1.6333,
        1.6333, 1.633, 1.6371, 1.6403, 1.6396, 1.6364, 1.6356, 1.6356, 1.6368, 1.6357,
        1.6354, 1.632, 1.6332, 1.6328, 1.6331, 1.6342, 1.6321, 1.6302, 1.6278, 1.6308,
        1.6324, 1.6324, 1.6307, 1.6277, 1.6269, 1.6335, 1.6392, 1.64, 1.6401, 1.6396,
        1.6407, 1.6423, 1.6429, 1.6472, 1.6485, 1.6486, 1.6467, 1.6444, 1.6467, 1.6509,
        1.6478, 1.6461, 1.6461, 1.6468, 1.6449, 1.647, 1.6461, 1.6452, 1.6422, 1.6422,
        1.6425, 1.6414, 1.6366, 1.6346, 1.635, 1.6346, 1.6346, 1.6343, 1.6346, 1.6379,
        1.6416, 1.6442, 1.6431, 1.6431, 1.6435, 1.644, 1.6473, 1.6469, 1.6386, 1.6356,
        1.634, 1.6346, 1.643, 1.6452, 1.6467, 1.6506, 1.6504, 1.6503, 1.6481, 1.6451,
        1.645, 1.6441, 1.6414, 1.6409, 1.6409, 1.6428, 1.6431, 1.6418, 1.6371, 1.6349,
        1.6333, 1.6334, 1.6338, 1.6342, 1.632, 1.6318, 1.637, 1.6368, 1.6368, 1.6383,
        1.6371, 1.6371, 1.6355, 1.632, 1.6277, 1.6276, 1.6291, 1.6274, 1.6293, 1.6311,
        1.631, 1.6312, 1.6312, 1.6304, 1.6294, 1.6348, 1.6378, 1.6368, 1.6368, 1.6368,
        1.636, 1.637, 1.6418, 1.6411, 1.6435, 1.6427, 1.6427, 1.6419, 1.6446, 1.6468,
        1.6487, 1.6594, 1.6666, 1.6666, 1.6678, 1.6712, 1.6705, 2.6718, 1.6784, 1.6811,
        1.6811, 1.6794, 1.6804, 1.6781, 1.6756, 1.6735, 1.6763, 1.6762, 1.6777, 1.6815,
        1.6802, 1.678, 1.6796, 1.6817, 1.6817, 1.6832, 1.6877, 1.6912, 1.6914, 1.7009,
        1.7012, 1.701, 1.7005, 2.7076, 1.7087, 1.717, 1.7105, 1.7031, 1.7029, 1.7006,
        1.7035, 1.7045, 1.6956, 1.6988, 1.6915, 1.6914, 1.6859, 1.6778, 1.6815, 1.6815,
        1.6843, 1.6846, 1.6846, 1.6923, 1.6997, 1.7098, 1.7188, 1.7232, 1.7262, 1.7266,
        1.7359, 1.7368, 1.7337, 1.7317, 1.7387, 1.7467, 1.7461, 1.7366, 1.7319, 1.7361,
        1.7437, 1.7432, 1.7461, 1.7461, 1.7454, 1.7549, 1.7742, 1.7801, 1.7903, 1.7876,
        1.7928, 1.7991, 1.8007, 1.7823, 1.7661, 1.785, 1.7863, 1.7862, 1.7821, 1.7858,
        1.7731, 1.7779, 1.7844, 1.7866, 1.7864, 1.7788, 1.7875, 1.7971, 1.8004, 1.7857,
        1.7932, 1.7938, 1.7927, 1.7918, 1.7919, 1.7989, 1.7988, 1.7949, 1.7948, 1.7882,
        1.7745, 1.771, 1.775, 1.7791, 1.7882, 1.7882, 1.7899, 1.7905, 1.7889, 1.7879,
        1.7855, 1.7866, 1.7865, 1.7795, 1.7758, 1.7717, 1.761, 1.7497, 1.7471, 1.7473,
        1.7407, 1.7288, 1.7074, 1.6927, 1.7083, 1.7191, 1.719, 1.7153, 1.7156, 1.7158,
        1.714, 1.7119, 1.7129, 1.7129, 1.7049, 1.7095
    ];
    
    var masterChart,
        detailChart;
    
    $(document).ready(function() {
    
    
        // create the master chart
        function createMaster() {
            masterChart = $('#master-container').highcharts({
                chart: {
                    reflow: false,
                    borderWidth: 0,
                    backgroundColor: null,
                    marginLeft: 50,
                    marginRight: 20,
                    zoomType: 'x',
                    events: {
    
                        // listen to the selection event on the master chart to update the
                        // extremes of the detail chart
                        selection: function(event) {
                            var extremesObject = event.xAxis[0],
                                min = extremesObject.min,
                                max = extremesObject.max,
                                detailData = [],
                                xAxis = this.xAxis[0];
    
                            // reverse engineer the last part of the data
                            jQuery.each(this.series[0].data, function(i, point) {
                                if (point.x > min && point.x < max) {
                                    detailData.push({
                                        x: point.x,
                                        y: point.y
                                    });
                                }
                            });
    
                            // move the plot bands to reflect the new detail span
                            xAxis.removePlotBand('mask-before');
                            xAxis.addPlotBand({
                                id: 'mask-before',
                                from: Date.UTC(2006, 0, 1),
                                to: min,
                                color: 'rgba(0, 0, 0, 0.2)'
                            });
    
                            xAxis.removePlotBand('mask-after');
                            xAxis.addPlotBand({
                                id: 'mask-after',
                                from: max,
                                to: Date.UTC(2008, 11, 31),
                                color: 'rgba(0, 0, 0, 0.2)'
                            });
    
    
                            detailChart.series[0].setData(detailData);
    
                            return false;
                        }
                    }
                },
                title: {
                    text: null
                },
                xAxis: {
                    type: 'datetime',
                    showLastTickLabel: true,
                    maxZoom: 14 * 24 * 3600000, // fourteen days
                    plotBands: [{
                        id: 'mask-before',
                        from: Date.UTC(2014, 0, 1),
                        to: Date.UTC(2017, 7, 1),
                        color: 'rgba(0, 0, 0, 0.2)'
                    }],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    },
                    min: 0.6,
                    showFirstLabel: false
                },
                tooltip: {
                    formatter: function() {
                        return false;
                    }
                },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        fillColor: {
                            linearGradient: [0, 0, 0, 70],
                            stops: [
                                [0, '#4572A7'],
                                [1, 'rgba(0,0,0,0)']
                            ]
                        },
                        lineWidth: 1,
                        marker: {
                            enabled: false
                        },
                        shadow: false,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        },
                        enableMouseTracking: false
                    }
                },
    
                series: [{
                    type: 'area',
                    name: 'Parameter Trend',
                    pointInterval: 24 * 3600 * 1000,
                    pointStart: Date.UTC(2014, 0, 01),
                    data: data
                }],
    
                exporting: {
                    enabled: false
                }
    
            }, function(masterChart) {
                createDetail(masterChart)
            })
            .highcharts(); // return chart instance
        }
    
        // create the detail chart
        function createDetail(masterChart) {
    
            // prepare the detail chart
            var detailData = [],
                detailStart = Date.UTC(2017, 7, 1);
    
            jQuery.each(masterChart.series[0].data, function(i, point) {
                if (point.x >= detailStart) {
                    detailData.push(point.y);
                }
            });
    
            // create a detail chart referenced by a global variable
            detailChart = $('#detail-container').highcharts({
                chart: {
                    marginBottom: 120,
                    reflow: false,
                    marginLeft: 50,
                    marginRight: 20,
                    style: {
                        position: 'absolute'
                    }
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Severity Level'
                },
                subtitle: {
                    text: 'Select an area by dragging across the lower chart'
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: null
                    },
                    maxZoom: 0.1,
                     tickInterval: 1,
    				  min: 0,
    				  max: 3
                },
                tooltip: {
                    formatter: function() {
                        var point = this.points[0];
                        return '<b>'+ point.series.name +'</b><br/>'+
                            Highcharts.dateFormat('%A %B %e %Y', this.x) + ':<br/>';
                    },
                    shared: true
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        marker: {
                            enabled: false,
                            states: {
                                hover: {
                                    enabled: true,
                                    radius: 3
                                }
                            }
                        }
                    }
                },
                series: [{
                    name: 'Severity Level Point',
                    pointStart: detailStart,
                    pointInterval: 24 * 3600 * 1000,
                    data: detailData
                }],
    
                exporting: {
                    enabled: false
                }
    
            }).highcharts(); // return chart
        }
    
        // make the container smaller and add a second container for the master chart
        var $container = $('#container')
            .css('position', 'relative');
    
        var $detailContainer = $('<div id="detail-container">')
            .appendTo($container);
    
        var $masterContainer = $('<div id="master-container">')
            .css({ position: 'absolute', top: 300, height: 80, width: '100%' })
            .appendTo($container);
    
        // create master and in its callback, create the detail chart
        createMaster();
    });
    
});

</script>