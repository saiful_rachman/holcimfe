<?php
	$this->load->view('includes/header.php');
?>
<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
						<?php  if ($severity_level == '0'){
                            echo "<div class='green'></div>";
                            }elseif ($severity_level == '1'){
                            echo "<div class='yellow'></div>";
                            }elseif ($severity_level == '2'){
                            echo "<div class='red'></div>";
                            }
                        ?>
					</div>
					<div class="span2 status-haccode">
						<h4><?php echo $hac_code;?></h4>
					</div>
					<div class="span4">
						<div id="pic-container">
							<center>
						<li class="dropdown" style="list-style: none;">
                        <a href="#" class="btn dropdown-toggle" data-toggle="dropdown">View User <b class="caret"></b></a>                      
                        <ul class="dropdown-menu mega-menu">
                            
                            <?php foreach($top as $top_row) :?>
						    <li class="mega-menu-column">
    						    <ul>
    						        <li class="nav-header"><div class="first">
    								<img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?> " class=""/>
    								<div class="name"><?php echo $top_row->nama;?></div>
    								<div class="nip">ID: <?php echo $top_row->nip;?></div>
    							</div></li>
    						    </ul>
                            </li> 
                            <?php endforeach;?>
						
                        </ul><!-- dropdown-menu -->
                    </li>
                    </center>
						</div>
					</div>
					<div class="span2 inspection">
						<span class="icon-ultrasonic icon-inspection-top"></span><p>&nbsp;Ultrasonic</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thermo">Thermography</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_stop">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_inspection">Inspection Report</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_vibration">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_general">General Thickness</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_kiln">Kiln Thickness</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_stack">Stack Thickness</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-right back-btn">
							<button onclick="history.go(-1);" class="btn"><i class="icon-chevron-left"></i>Back</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer2"></div>
			</div>
			<div class="row-fluid">
				<div class="span5 sparepart">
					<img src="<?=base_url()?>media/images/<?php echo $ultrasonic_image;?>" width="100%" style="height: 300px; margin: 0 auto"/>
					
				</div>
				<div class="span7">
					<div id="container" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
                    <input type="button" class="btn" value="Previous" onclick="Previous();" />
                    <input type="button" class="btn" value="Next" onclick="Next();" />
                     <a class="btn" href="./report_ultrasonic">New Data</a>
				</div>					
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Remarks</h3>
						<div><?php echo $ultrasonic_main['remarks'];?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div><?php echo $ultrasonic_main['recomendation'];?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>DATE</th>
										<th>ACTION</th>
										<th>NOTES</th>
										<th>OTHER</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($ultrasonic_detail as $ultrasonic_detail_row) :?>
									<tr>
										<td><?php echo $ultrasonic_detail_row->datetime; ?></td>
										<td><span><?php echo $ultrasonic_detail_row->upload_file; ?></span></td>
										<td><?php echo $ultrasonic_detail_row->couplant; ?></td>
										<td><?php echo $ultrasonic_detail_row->nama; ?></td>
									</tr>
                                    <?php endforeach;?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
$this->load->view('includes/footer.php');
?>		

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>	

<script type="text/javascript">
$(function () {
	$('#container').highcharts({
		title: {
			text: 'Severity Level Trend',
			x: -20 //center
		},
		subtitle: {
			text: '',
			x: -20
		},
		xAxis: {
			categories: [<?php sort($severity_chart); 
            foreach($severity_chart as $severity_chart_row) :?>'<?php echo date("d-m-Y", strtotime($severity_chart_row->datetime)) ?>',<?php endforeach;?>]
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		}
        ,
		tooltip: {
			valueSuffix: ' '
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		series: [{
			name: 'Hac',
                        
			data: [<?php 
            sort($severity_chart);
            foreach($severity_chart as $severity_chart_row) :?>{y:<?php echo $severity_chart_row->severity_level;?>,color:'<?php $rian = $severity_chart_row->severity_level;
                                                        
                                                        if($rian =='0'){
                                                            
                                                            echo "#2e9612";
                                                            
                                                        }elseif($rian =='1'){
                                                            
                                                            echo "#f8fb00";
                                                        }
                                                        elseif($rian =='2'){
                                                            
                                                            echo "#e40000";
                                                        }
                                                        
                                                        
                                                        ?>'},<?php endforeach;?>],
		},
        {
            name: 'Normal',
            data: [],
            marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/green.png)'
            }
        },
        {
            name: 'Warning',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/yellow.png)'
            }
        },
        {
            name: 'Danger',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/red.png)'
            }
        }]
	});
});

function Next(){

	$("#container").html("");
	
	var options = {
		chart: {
			renderTo: 'container',
		},
		credits: {
			enabled: false
		},
		title: {
			text: 'Severity Level Trend',
			x: -20
		},
		xAxis: {
			categories: [{}]
		},
        
        yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		},
		tooltip: {
            formatter: function() {
                var s = '<b>'+ this.x +'</b>';
                
                $.each(this.points, function(i, point) {
                    s += '<br/>'+point.series.name+': '+point.y;
                });
                
                return s;
            },
            shared: true
        },
		series: [{},{
            name: 'Normal',
            data: [],
            marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/green.png)'
            }
        },
        {
            name: 'Warning',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/yellow.png)'
            }
        },
        {
            name: 'Danger',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/red.png)'
            }
        }]
	};
	
	$.ajax({
		url: "json_ultra_next",
		data: 'show=data_json',
		type:'post',
		dataType: "json",
		success: function(data){
			options.xAxis.categories = data.categories;
			options.series[0].name = 'Hac';
			options.series[0].data = data.data_json;
            
			var chart = new Highcharts.Chart(options);			
		}
	});
	
}

function Previous(){

	$("#container").html("");
	
	var options = {
		chart: {
			renderTo: 'container',
		},
		credits: {
			enabled: false
		},
		title: {
			text: 'Severity Level Trend',
			x: -20
		},
		xAxis: {
			categories: [{}]
		},
        yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		},
		tooltip: {
            formatter: function() {
                var s = '<b>'+ this.x +'</b>';
                
                $.each(this.points, function(i, point) {
                    s += '<br/>'+point.series.name+': '+point.y;
                });
                
                return s;
            },
            shared: true
        },
		series: [{},{
            name: 'Normal',
            data: [],
            marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/green.png)'
            }
        },
        {
            name: 'Warning',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/yellow.png)'
            }
        },
        {
            name: 'Danger',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/red.png)'
            }
        }]
	};
	
	$.ajax({
		url: "json_ultra_prev",
		data: 'show=data_json',
		type:'post',
		dataType: "json",
		success: function(data){
			options.xAxis.categories = data.categories;
			options.series[0].name = 'Hac';
			options.series[0].data = data.data_json;
			var chart = new Highcharts.Chart(options);			
		}
	});
	
}


</script>