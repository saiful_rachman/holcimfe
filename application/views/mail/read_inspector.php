<?php
  
	$users = $this->main_model->get_detail('users',array('id' => $this->session->userdata('users_id')));
	
 ?>
    
	<div id="main">
		<div id="content">
			<div class="inner" style="padding-top:2%">
				<div class="row-fluid msg">
					<div class="span3">
						<div class="left-bar">
							<?php include('left_message.php');?>
						</div>
					</div>
					
					<div class="span9">
						<div class="msg-area">
							<div class="spacer4"></div>
							<div class="spacer2"></div>
							<?php
                                                            foreach($read as $readmsg){
                                                        ?>
                                                        <div class="row-fluid">
                                                            <div class="span10">
                                                                    <p>
                                                                        <i class="icon-user"></i> From: <strong><?php echo $readmsg->nama; ?></strong><br />
                                                                        <i class="icon-calendar"></i> Date: <strong><?php echo $readmsg->timestamp;?></strong>
                                                                    </p>
                                                            </div>

                                                        </div>
							<div class="msg-entry" style="background-color:#efefef">
                                                            <?php echo $readmsg->message;?>
                                                        </div>
                                                        <div class="spacer5" style="margin-bottom: 10px;border-bottom: 1px dashed;">
							</div>
                                                        <?php } ?>
                                                        <div class="row-fluid">
                                                            <form method="post" action="<?=base_url();?>inbox/reply">
                                                            <div class="span12">
                                                                <h4>Reply Here:</h4>
                                                                <div>
                                                                    <input type="hidden" name="id2" value="<?=$id;?>">
                                                                    <input type="hidden" name="user2" value="<?=$user;?>">
                                                                    <input type="hidden" name="idx" value="<?=$idx;?>">
                                                                    <textarea name="message" style="max-width: 98%;min-width: 98%;max-height: 80px;min-height: 80px;"></textarea>
                                                                </div>
                                                                <div>
                                                                    <button type="submit" class="btn"><i class="icon-check icon-black"></i> Send</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                            </form>
                                                        </div>
							
						</div>
					</div>					
				</div>
			</div>
		</div>
	</div>
