<form method="post" action="<?=base_url();?>inbox/newmsgs">
<div id="main">
    <div id="content">
        <div class="inner" style="padding-top:2%">
            <div class="row-fluid msg">
                <div class="span3">
                    <div class="left-bar">
                        <ul>
                            <li><a href="<?php echo base_url();?>inbox/newmsger"><i class="icon-pencil"></i> Create Message</a></li>
                            <li><a href="<?php echo base_url();?>inbox/index"><i class="icon-bookmark"></i> Inbox</a></li>
                            <li><a href="<?php echo base_url();?>inbox/outbox"><i class="icon-share"></i> Outbox</a></li>
                        </ul>
                    </div>
                </div>
                <div class="span9">
                    <div class="msg-area">
                        <h4>Create New Message</h4>
                        <div class="spacer4"></div>
                        <div class="spacer2"></div>
                        <div class="control-group">
                            <label class="control-label" for="recipient">Level:</label>
                            <div class="controls">
                                <select name="level" id="level" required class="span6">
                                    <option value="" selected>--select--</option>
                                    <option value="Administrator">Administrator</option>
                                    <option value="Engineer">Engineer</option>
                                    <option value="Inspector">Inspector</option>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="subject">User:</label>
                                <div class="controls">
                                    <select name="user" class="span6" required id="user">
                                    </select>
                                </div>
                        </div>
                        <div style="float: right;position: absolute;display: none;">
                        <img id="image">
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="message">Message:</label>
                            <div class="controls">
                                <textarea class="msg-form-txt-area" rows="6" name="message"></textarea><br />
                                <div class="spacer2"></div>
                                <input class="btn" type="submit" value="Send"/>
                                &nbsp;<a class="btn" href="<?php echo base_url();?>inbox/">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>					
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $("#level").change(function(){
         var id = $(this).val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>inbox/get_user",
             data: "id="+id,
             success: function(data){
                 $("#user").html(data);
             }
         });
      });
      $("#user").change(function(){
        var id = $(this).val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>inbox/get_image",
             data: "id="+id,
             success: function(data){
                 $("#image").attr("src","<?=base_url();?>media/images/"+data);
             }
         });
      });
    });
</script>
	


 
 