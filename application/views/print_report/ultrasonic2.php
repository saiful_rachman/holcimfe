<style>
    table{
        font-size: 11px;
    }
    table .judul-center{
        font-weight: bolder;
        text-align: center;
    }
    table .judul-left{
        font-weight: bolder;
        text-align: left;
    }
    table .gambar{
        padding: 10px;
    }
    @media print {
    body {-webkit-print-color-adjust: exact;}
    .dontprint{ display: none; }
    @page {size: potrait}

}
</style>
<table border="1" width="100%" cellpading="0" cellspacing="0">
    <tr>
        <td rowspan="6" colspan="2" align="center"><img src="<?php echo base_url();?>media/logo.png" width="100px" height="50px" ></td>
    </tr>
    <tr>
        <td rowspan="5" valign="middle" colspan="4"><p class="judul-center">CBM TUBAN PLANT<br />ULTRASONIC EXAMINATION REPORT</p></td>
    </tr>
    <tr>
        <td width="190px">Form Version: </td>
    </tr>
    <tr>
        <td>Release Date: </td>
    </tr>
    <tr>
        <td>Inspection Date: </td>
    </tr>
    <tr>
        <td>Reported By: </td>
    </tr>
    <tr>
        <td colspan="2"><p class="judul-left">Hac: <?php echo $record_hac->hac_code; ?></P></td>
        <td colspan="6">Test Object:</td>
    </tr>
    <tr>
        <td colspan="2"><p class="judul-center">EQUIPMENT</p></td>
        <td colspan="2"><p class="judul-center">PARAMETER</p></td>
        <td><p class="judul-center">X</p></td>
        <td><p class="judul-center">NAME</p></td>
        <td><p class="judul-center">APPROVAL</p></td>
    </tr>
    <tr>
        <td>Model</td>
        <td width="70px"><?php echo $record_detail->model; ?></td>
        <td>Gain</td>
        <td width="70px"><?php echo $record_detail->gain; ?></td>
        <td>Name</td>
        <td><?php echo $record_user->nama; ?></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Couplant</td>
        <td><?php echo $record_detail->couplant; ?></td>
        <td>Vel</td>
        <td><?php echo $record_detail->vel; ?></td>
        <td>Sign</td>
        <td><?php echo $record_user->signature; ?></td></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Probe Type</td>
        <td><?php echo $record_detail->probe_type; ?></td>
        <td>Range</td>
        <td><?php echo $record_detail->range; ?></td>
        <td>Date</td>
        <td><?php echo $record_detail->date; ?></td></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Frequency</td>
        <td><?php echo $record_detail->frequency; ?></td>
        <td>Sa</td>
        <td><?php echo $record_detail->sa; ?></td>
        <td rowspan="3" colspan="3" >&nbsp;</td>
    </tr>
    <tr>
        <td>P-Zero</td>
        <td><?php echo $record_detail->p_zero; ?></td>
        <td>Ra</td>
        <td><?php echo $record_detail->ra; ?></td>
    </tr>
    <tr>
        <td>Thickness</td>
        <td><?php echo $record_detail->thickness; ?></td>
        <td>Da</td>
        <td><?php echo $record_detail->da; ?></td>
    </tr>
    <tr>
        <td colspan="7"><p class="judul-center">PICTURE / SKETCH</p></td>
    </tr>
    <tr>
        <td colspan="7" align="center"><img class="gambar" src="<?php echo base_url();?>media/images/"<?php echo $record_detail->upload_file; ?> width="600px" height="400px"></td>
    </tr>
    <tr>
        <td colspan="7" height="100px" valign="top"><p class="judul-left">Remarks: </p> <?php echo $record_main->remarks; ?></td>
    </tr>
</table>
 <div style="text-align: center;margin-top: 5px;">
    <input id="printpagebutton" type="button" onclick="printpage();" value="Print" >
</div>
<script type="text/javascript">
    function printpage() {
        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");
        //Set the print button visibility to 'hidden' 
        printButton.style.visibility = 'hidden';
        //Print the page content
        window.print()
        //Set the print button to 'visible' again 
        //[Delete this line if you want it to stay hidden after printing]
        printButton.style.visibility = 'visible';
    }
</script>