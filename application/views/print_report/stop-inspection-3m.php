<style type="text/css" media="print">
@media print {
body {-webkit-print-color-adjust: exact;}

.dontprint
{ display: none; }
}

</style>

<button class="dontprint" onclick="PrintFunction()">Print Layout</button>
		  
<script>
function PrintFunction()
{
window.print();
}
</script>
<div class="printable" align="center">
<div style="border:1px solid #000; font-size:14px; padding:5px; border-radius: 5px; width:750px; margin-top:1px;">
	<table width="100%">
	<tr>
	<td width="30%">
	<img src="<?php echo base_url()?>application/views/assets/img/logo_header.png" height="94px" width="285px">
	</td>
	<td width="70%">
	 <table  style="font-size:12px" width="100%" height="100%" cellpadding="0" cellspacing="0" border="1">
			<thead>	
				<tr>
					<td colspan="4" align="center">&nbsp;&nbsp; STOP INSPECTION (3 MONTHLY)</td>
				</tr>
				<tr>
					<td  align="left">&nbsp;&nbsp; HAC NO</td>
					<td  align="left">&nbsp;&nbsp; XXX-ACX/PNX</td>
					</tr>
					<tr>
					<td  align="left">&nbsp;&nbsp; Equip. Description</td>
					<td  align="left">&nbsp;&nbsp; Apron Conveyor</td>
				</tr>
				
				  
			</thead>
			<tbody id="content">
				<tr>
					<td  align="left">&nbsp;&nbsp; Maker/Type</td>
					<td  align="left">&nbsp;&nbsp; AUMUND</td>
					</tr>
					<tr>
					<td  align="left">&nbsp;&nbsp; Duration (h)</td>
					<td  align="left"> &nbsp;</td>
					
				</tr>
			</tbody>
	</table>	
	</td>
	</tr>
	</table>
	</div>
    <div style="border:1px solid #000; font-size:14px; padding:5px; border-radius: 5px; width:750px; margin-top:1px; min-height:150px;">
	<table style="width:950px;" cellpadding="0" border="0" cellspacing="0">
			<tr>
				<td><img src="<?php echo base_url()?>application/views/assets/img/product1.png" height="300px" width="750px"></td>
			</tr>
            </table>			
    </div>
<div style="border:1px solid #000;padding:5px; border-radius: 5px; width:750px; margin-top:1px; min-height:490px;">
	<table  style="font-size:12px" width="100%" cellpadding="0" cellspacing="0" border="1">
			<thead>	
				<tr>
					<td rowspan="12" align="center">NO</td>
					<td rowspan="12" align="center">PART</td>	
					<td rowspan="11" align="center">ITEM CHECK</td>
					<td rowspan="11" align="center">METHOD</td>	
					<td rowspan="11" align="center">STANDARD</td>	
					<td colspan="11" align="center">RESULT OF CHECK & MEASUREMENT</td>
				</tr>
				<tr>
				<td colspan="13" align="center">DATE</td>
				<tr>
				<tr>
					
					<td  align="center"> Monday</td>
					<td  align="center"> Tuesday</td>
					<td  align="center"> Wednesday</td>
					<td  align="center"> Thursday</td>
					<td  align="center"> Friday</td>
					<td  align="center"> Saturday</td>
				</tr>
				
				  
			</thead>
			<tbody id="content">
				<tr>
					<td  align="center"> 1</td>
					<td  align="center"> Name</td>
					<td  align="center"> Name</td>
					<td  align="center"> Name</td>
					<td  align="center"> Name</td>
					<td  align="center"> &#10004;</td>
					<td  align="center"> &#10004;</td>
					<td  align="center"> &#10004;</td>
					<td  align="center"> &#10004;</td>
					<td  align="center"> &#10004;</td>
					<td  align="center"> &#10004;</td>
				</tr>
			</tbody>
	</table>			
</div>
	<div  align="left" style="border:1px solid #000; font-size:14px; padding:5px; border-radius: 5px; width:750px; margin-top:1px; min-height:150px;">
		<p>REMARKS : <br />
			<label> - Base on ISO 10816-3, the velocity vibration on point 2H and 2A is slightly high for new machine. It was fall into zone B (unlimitted long term operation)</label><br/>
			<label> - The timewaveform analysis show amplitude modulation that related to the the load variation due to there is no smooth running on apron chain (jumping)</label>
			</p>
    </div>
 </div>
