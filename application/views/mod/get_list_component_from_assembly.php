<?php 
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<button class="btn btn-success">ADD COMPONENTS</button>
<button class="btn btn-danger pull-right" id="close">CLOSE</button>
<hr/>
<table class="table table-bordered">

<thead>
	<tr>
		<th>Component name</th>
		<th>Component code</th>
		<th>Action</th>
	</tr>
</thead>

<?php foreach($list_component->result() as $lists) :?>



<tbody>
<tr>
	<td><?=$lists->component_name?></td>
	<td><?=$lists->component_code?></td>
	<td><a href="#" class="btn">Edit</a> <a href="#" class="btn">Delete</a></td>
</tr>
</tbody>
<?php endforeach; ?>
</table>

<script type="text/javascript" src="<?=base_url()?>application/views/assets/js/jquery-1.9.0.min.js"></script>
<script type="text/javascript">
$("#list_components #close").click(function(){
	$(".overlay").hide();
});
</script>	