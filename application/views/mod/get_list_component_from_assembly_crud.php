<?php 
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>


<?=$output?>

<script type="text/javascript" src="<?=base_url()?>application/views/assets/js/jquery-1.9.0.min.js"></script>
<script type="text/javascript">
$("#list_components #close").click(function(){
	$(".overlay").hide();
});
</script>	

<?php foreach($js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>