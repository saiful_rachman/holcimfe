<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<script type="text/javascript">
$(document).ready(function(){
$(function() {

  function split( val ) {
            return val.split( /,\s*/ );
    }
            function extractLast( term ) {
             return split( term ).pop();
    }

$("#txtinput")
        // don't navigate away from the field on tab when selecting an item
          .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).data( "autocomplete" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            source: function( request, response ) {
                $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunction",{  //Url of controller
                    term: extractLast( request.term )
                },response );
            },
            search: function() {
                // custom minLength
                var term = extractLast( this.value );
                if ( term.length < 1 ) {
                    return false;
                }
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var terms = split( this.value );
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push( ui.item.value );
                // add placeholder to get the comma-and-space at the end
                terms.push( "" );
                this.value = terms.join( "" );
                return false;
            }
        });   

});
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
$(function() {
            $( ".datepicker" ).datepicker({
                    dateFormat: "yy-mm-dd"
            });
    });           
</script>
<form method="post" id="form" action="<?php echo base_url() ;?>engine/inspection_manager/save_wearing_activity" enctype="multipart/form-data">
<div id="main">
    <div id="content">
            <div class="inner">	
                    <div class="row-fluid">
                            <div class="span12">
                                    <h2>Create Form Wizard</h2>
                                    <h4>Wear Measuring Form <span class="pull-right"></</span></h4>
                                    <div class="well well-small">
                                        <table class="table">
                                            <thead>	<input type="hidden" name="id" value="<?php echo $detail->id; $form=$detail->id; ?>" />
                                                            <tr>
                                                                    <td width="200px">HAC</td>
                                                                    <td>
                                                                        <input type="text" name="hac" id="txtinput" class="span6" required value="<?php echo $detail->hac_code; ?>" readonly="true" />
                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                    <td width="200px">AREA</td>
                                                                    <td><input type="text" name="hac" id="txtinput" class="span6" required value="<?php echo $detail->area_name; ?>" readonly="true" /></td>
                                                            </tr>
                                                    </thead>	
                                                    <tbody>	
                                                        <tr style="display: none;">
                                                                    <td>Measurement Number</td>
                                                                    <td><input type="text" name="meas_no" class="span6" required value="<?php echo $detail->meas_no; ?>" readonly="true" /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Material</td>
                                                                    <td><input type="text" name="material"  class="span6" required value="<?php echo $detail->material; ?>" readonly="true" /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Date</td>
                                                                    <td><input type="text" name="date" class="span6" required value="<?php echo $detail->create_date; ?>" readonly="true" /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Instalation Date</td>
                                                                    <td><input type="text" name="instalation_date" class="span6" required value="<?php echo $detail->instalation_date; ?>" readonly="true" /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Measurement Point</td>
                                                                    <td><input type="text" name="point"  class="span6" required value="<?php echo $detail->point; ?>" readonly="true" /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Image</td>
                                                                    <td><img id="blah" src="<?php echo base_url(); ?>/media/images/<?php echo $detail->gambar; ?>" width="150" height="70" /></td>
                                                            </tr>
                                                    </tbody>
                                            </table>
                                            <table border="1">
                                                <tbody id="listingx">
                                                <tr>
                                                    <td colspan="2">Measurement Point</td><input type="hidden" name="form_id" value="<?php echo $detail->id; ?>">
                                                    <?php
                                                    for($i=1;$i<=20;$i++){
                                                        echo "<td width='30px' align='center'>$i</td>";
                                                    }
                                                    ?>
                                                </tr>
                                                <tr style="text-align: center;">
                                                    <td colspan="2">Distance (mm) from Big Diameter of Roller</td>
                                                    <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td>
                                                    <td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
                                                    <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td>
                                                    <td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
                                                </tr>
                                                <?php 
                                                    for($i=0;$i<$detail->roller;$i++){                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                                                        $x1=$i+1;
                                                        
                                                ?>
                                                <tr style='text-align: center;'>
                                                   
                                                    <td><b>Roller <?php echo $i+1; ?></b><br/><?php echo $i; ?> - Point Measuring</td>
                                                        
                                                </tr>
                                                        <?php } ?>
                                                </tbody>
                                            </table>
                                            <div>&nbsp;</div>
                                        <button type="submit" class="btn"><i class="icon-check icon-black"></i> OK</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                                    </div>
                                    <div class="spacer"></div>
                            </div>
                    </div>
            </div>
    </div>
</div>
</form>
<script type="text/javascript"> 
$('#form').submit(function(){
     alert('Data has been saved !');
    });
</script>
<?php $this->load->view("includes/footer.php"); ?>