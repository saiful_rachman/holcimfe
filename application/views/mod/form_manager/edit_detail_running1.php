<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<form method="post" action="<?php echo $form_link; ?>" id="form">
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Edit Form Wizard</h2>
					<h4>Running Inspection Form <span class="pull-right">STEP 1</</span></h4>
					<div class="well well-small">
                                            <table class="table">
							<thead>	
								<tr>
									<td width="200px">AREA</td>
                                                                        <td><select name="area" required class="span6">
                                                                                <option value="">- Select AREA -</option>
                                                                                        <?php  foreach ($area as $data){
                                                                                                $ld=$area_detail->area;
                                                                                                if($ld == $data->id){
                                                                                                    $cek ="selected";
                                                                                                }else{
                                                                                                    $cek="";
                                                                                                }
                                                                                                echo "<option value='$data->id' $cek>$data->area_name</option>";
                                                                                                }
                                                                                         ?>";
										</select>
									</td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td><input type="text" name="frequency" class="span6" required value="<?php echo $area_detail->frequency; ?>"/><input type="hidden" name="id" value="<?php echo $area_detail->id; ?>"</td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><input type="text" name="mechanical_type"  class="span6" required value="<?php echo $area_detail->type; ?>"/></td>
								</tr>
								<tr>
									<td>Form No.</td>
									<td><input type="text" name="form_no"  class="span6" required value="<?php echo $area_detail->form_number; ?>"/></td>
								</tr>
                                                                <tr>
									<td>Periode</td>
                                                                        <td>
                                                                            <select name="periode" required >
                                                                                <?php
                                                                                if($area_detail->periode == "mon"){
                                                                                    $m="selected";
                                                                                }else{
                                                                                    $m="";
                                                                                }
                                                                                if($area_detail->periode == "tue"){
                                                                                    $t="selected";
                                                                                }else{
                                                                                    $t="";
                                                                                }
                                                                                if($area_detail->periode == "wed"){
                                                                                    $w="selected";
                                                                                }else{
                                                                                    $w="";
                                                                                }
                                                                                if($area_detail->periode == "thu"){
                                                                                    $th="selected";
                                                                                }else{
                                                                                    $th="";
                                                                                }
                                                                                if($area_detail->periode == "fri"){
                                                                                    $f="selected";
                                                                                }else{
                                                                                    $f="";
                                                                                }
                                                                                if($area_detail->periode == "sat"){
                                                                                    $s="selected";
                                                                                }else{
                                                                                    $s="";
                                                                                }
                                                                                echo"
                                                                                <option value='mon' $m>Monday</option>
                                                                                <option value='tue' $t>Tuesday</option>
                                                                                <option value='wed' $w>Wednesday</option>
                                                                                <option value='thu' $th>Thursday</option>
                                                                                <option value='fri' $f>Friday</option>
                                                                                <option value='sat' $s>Saturday</option>
                                                                                ";
                                                                                ?>
                                                                            </select>
                                                                        </td>
								</tr>
                                                                <tr>
									<td>Publish</td>
                                                                        <td>
                                                                            <select name="publish" required >
                                                                                <?php
                                                                                if($area_detail->publish == "Y"){
                                                                                    $yx="selected";
                                                                                }else{
                                                                                    $yx="";
                                                                                }
                                                                                if($area_detail->publish == "N"){
                                                                                    $nx="selected";
                                                                                }else{
                                                                                    $nx="";
                                                                                }
                                                                                echo"
                                                                                <option value='Y' $yx>Yes</option>
                                                                                <option value='N' $nx>No</option>
                                                                                ";
                                                                                ?>
                                                                            </select>
                                                                        </td>
								</tr>
							</tbody>
						</table>
						<button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
    $('#form').submit(function(){
     alert('Data has been Update !');
    });
</script>    
<?php $this->load->view("includes/footer.php"); ?>