<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<script type="text/javascript">
$(document).ready(function(){
$(function() {

        
         
});
});
function coba(id){
   function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }

    $("#txtinput"+id)
            // don't navigate away from the field on tab when selecting an item
              .bind( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunction",{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
            
            $("#txtinput"+id).change(function (){
             var kelas_id = $(this).val();
            $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
               data : "id="+kelas_id,
               success: function(data){
                   $("#matapelajaran_id"+id).html(data);
               }
});
});
            
}
function updateselect(id){
    var valdata = $("#txtinput"+id).val();
    $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
               data : "id="+valdata,
               success: function(data){
                   $("#matapelajaran_id"+id).html(data);
               }
});
}
</script>
<form method="post" id="form" action="<?php echo base_url(); ?>engine/form_manager/simpan_detail_running2">">
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update Form Wizard</h2>
					<h4>Running Detail Inspection Form <span class="pull-right">STEP 2</</span></h4>
					<div class="well well-small">
						<table class="table">
							<thead>	
								<tr>
									<td width="200px">AREA</td>
                                                                        <td><?php echo $data_1stform->area_name; ?><input type="hidden" name="form_id1" value="<?php echo $data_1stform->id; ?>"></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td><?php echo $data_1stform->frequency; ?></td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><?php echo $data_1stform->type; ?></td>
								</tr>
								<tr>
									<td>Form No.</td>
                                                                        <td><?php echo $data_1stform->form_number; ?><input name="status" type="hidden" value="R"/></td>
								</tr>
							</tbody>
						</table>
                                            <h4>Component <span class="pull-right"><a id="add_listing" class="btn btn-info"><i class="icon-plus icon-white"></i></a></span></h4>
						<table class="table table-bordered" id="tablexx" id="tablexx">
							<tbody id="listing">	
								<tr class="success">
                                                                    <td><strong>HAC</strong></td>
                                                                    <td colspan="2"><strong>COMPONENT</strong> <input type="hidden" id="jum" name="jum"></td>
								</tr>
                                                                <?php foreach($data_2ndform as $hec){ ?>
                                                                <tr id="<?php echo "tr_".$hec->id; ?>">
                                                                    <td>
                                                                        <input name="frequencyx[]" type="text" id="txtinput<?php echo $hec->hac; ?>" onkeypress="coba(<?php echo $hec->hac; ?>)" class="span12" value="<?php echo $hec->hac_code; ?>"/>
                                                                        <input type="hidden" name="old_frequency[]" value="<?php echo $hec->hac; ?>" >
                                                                    </td>
                                                                    <td><select required name='areax[]' class='span12' id="matapelajaran_id<?php echo $hec->hac; ?>" onfocus="updateselect(<?php echo $hec->hac; ?>)" >
                                                                            <option value="">-</option>
                                                                            <?php  foreach ($component as $data){
                                                                                 if($hec->component==$data->id){
                                                                                    $cek="selected";
                                                                                }else{
                                                                                    $cek="";
                                                                                }
                                                                            echo "<option value='$data->id' $cek>$data->component_code</option>";
                                                                            }
                                                                            ?>
                                                                        </select><input type="hidden" name="old_area[]" value="<?php echo $hec->component; ?>">
                                                                        <input type="hidden" name="get_idform[]" value="<?php echo $hec->id; ?>">
                                                                    </td>
                                                                    <td width='20px'><input type="hidden" value="update" onclick="update(<?php echo $hec->id; ?>)"> <input type='button' value='X' class="delIngredient" onclick="dela(<?php echo $hec->id; ?>)"></td>
                                                                </tr>
                                                                <?php } ?>
							</tbody>
						</table>
						<button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
$(document).ready(function(){
    var i = 0;
    $('#jum').val(0);
$('#add_listing').click(function() {
        $jum1 = eval($('#jum').val());
        var tot = $jum1 + 1;
        $('#jum').val(tot);
        
        var c = "<?php  foreach ($component as $data){
                        echo "<option value='$data->id'>$data->component_code</option>";
                        }
                  ?>";
         var d = "<?php  foreach ($hac as $data){
                        echo "<option value='$data->id'>$data->hac_code</option>";
                        }
                  ?>";
        var j = i++;
	var x = parseInt($(this).val()) + 1; 
	$(this).val(x);
        var data_list = "<tr><td><input name='frequency[]' type='text' id='txtinput"+j+"' onkeypress='coba("+j+")' class='span12'></td><td><select required name='area[]' class='span12' id='matapelajaran_id"+j+"' ><option value=''>-</option></select</td><td width='20px'><input type='button' value='X' onClick='$(this).parent().parent().remove();'></td></tr>";
	$("#listing").append(data_list);
});
$("#rem_listing").click(function() {
    var rowCount = $('#listing tr').length;
    if(rowCount <= 2){
        alert('Row minimum 1');
    }else{
        $("#listing tr:last-child").remove();
        $jum1 = $('#jum').val();
        var tot = $jum1 - 1;
        $('#jum').val(tot);
    }
    });
});

//$('.delIngredient').click(function(){

  // $(this).parent().parent().remove();
//});

function dela(id){
 var r=confirm("Are you sure remove this component?");
    if (r==true)
      {
      //x="You pressed OK!";
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/form_manager/delete_edit_detail_running2",
          data:"id="+id,
          success: function(response) {

          //if (response == "Success")
          //{
              //window.history.back();
              
           $("#tr_"+id).remove();
            $jum1 = $('#jum').val();
            var tot = $jum1 - 1;
            $('#jum').val(tot);
          //}
         // else
          //{
              //alert("Error");
          //}

       }
    });
      }
    else
      {
      //alert('proses aborted');
      } 
    
              
}


function update(id){
    //alert(id);
    var r=confirm("Are you sure update this component?");
    if (r==true)
      {
      //x="You pressed OK!";
      $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/form_manager/update_edit_detail_running2",
          data:"id="+id,
          success: function(response) {

          //if (response == "Success")
          //{
              //window.history.back();
              alert(response);
          //}
         // else
          //{
              //alert("Error");
          //}

       }
    });
      }
    else
      {
      alert('proses aborted');
      } 
}
    $('#form').submit(function(){
     alert('Data has been Update !');
    }); 
</script>		