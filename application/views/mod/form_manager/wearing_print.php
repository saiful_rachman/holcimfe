<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style>
    table{
        font-size: 9px;
    }
    table tr td{
        border: 1px solid;
    }
.jtable{
-moz-transform: rotate(-90deg);
-o-transform: rotate(-90deg);
-webkit-transform: rotate(-90deg);
transform: rotate(-90deg);
}
</style>
<script type="text/javascript">
    function printpage() {
        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");
        //Set the print button visibility to 'hidden' 
        printButton.style.visibility = 'hidden';
        //Print the page content
        window.print()
        //Set the print button to 'visible' again 
        //[Delete this line if you want it to stay hidden after printing]
        printButton.style.visibility = 'visible';
    }
</script>
</head>

<body>
    <table style="width: 100%;">
  <tr>
      <td width="131" rowspan="4" colspan="2"><img src="<?php echo base_url(); ?>/media/logo.png" height="40px" width="120px"></td>
      <td width="662" rowspan="4" colspan="3"><div align="center">CONDITION BASES MONITORING REPORT<br />
      WEAR MEASUREMENT REPORT<br />
    </div></td>
    <td width="120">Form Version </td>
    <td width="85">:</td>
  </tr>
  <tr>
    <td>Release Date</td>
    <td>:</td>
  </tr>
  <tr>
    <td>Inspection Date</td>
    <td>:</td>
  </tr>
  <tr>
    <td>Reported By</td>
    <td>:</td>
  </tr>
  <tr>
  	<td colspan="8"><div align="center"><?php echo $form->hac_code; ?></div></td>
  </tr>
  <tr>
      <td>-</td>
      <td>meas no: <?php echo $form->meas_no; ?></td>
       <td>Material: <?php echo $form->material; ?></td>
      <td>Date: <?php echo $form->create_date; ?></td>
       <td>Instalation Date: <?php echo $form->instalation_date; ?></td>
       <td colspan="3" width="500px"><div align="center">Depth of Wear (mm) at Measuring Point</div></td>
  </tr>
</table>
    <table style="width: 100%;" style="font-size: 3px;">
        <tr>
            <td colspan="5">Measurement Point</td>
            <td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td>
            <td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td><td>18</td><td>19</td><td>20</td>
        </tr>
        <tr>
            <td colspan="5">Distance (mm) from Big Diameter of Roller</td>
            <td style= "width: 20px;text-align: center;">0</td>
            <td style= "width: 20px;text-align: center;">25</td>
            <td style= "width: 20px;text-align: center;">50</td>
            <td style= "width: 20px;text-align: center;">100</td>
            <td style= "width: 20px;text-align: center;">150</td>
            <td style= "width: 20px;text-align: center;">200</td>
            <td style= "width: 20px;text-align: center;">250</td>
            <td style= "width: 20px;text-align: center;">300</td>
            <td style= "width: 20px;text-align: center;">350</td>
            <td style= "width: 20px;text-align: center;">400</td>
            <td style= "width: 20px;text-align: center;">450</td>
            <td style= "width: 20px;text-align: center;">500</td>
            <td style= "width: 20px;text-align: center;">550</td>
            <td style= "width: 20px;text-align: center;">600</td>
            <td style= "width: 20px;text-align: center;">650</td>
            <td style= "width: 20px;text-align: center;">700</td>
            <td style= "width: 20px;text-align: center;">750</td>
            <td style= "width: 20px;text-align: center;">800</td>
            <td style= "width: 20px;text-align: center;">850</td>
            <td style= "width: 20px;text-align: center;">900</td>
        </tr>
        <?php 
        $table = $form->id;
        for($i=0;$i<$form->roller;$i++){
            $a=$i+1;
        ?>
        <tr>
            <td rowspan="7" width="20px"><p class="jtable">Roller <?php echo $a; ?></p></td>
           <td colspan="2" style="background-color: silver;">0 - Point Measurement</td>
           <?php
            $sql=mysql_query("select * from roller where form_id='$table' and no_roller = '$a'");
            $data=mysql_fetch_assoc($sql);
           ?>
            <td style="background-color: silver;">Date</td>
            <td style="background-color: silver;"><?php echo $data['date'];?></td>
            <td style="background-color: silver;"><?php echo $data['val1'];?></td>
            <td style="background-color: silver;"><?php echo $data['val2'];?></td>
            <td style="background-color: silver;"><?php echo $data['val3'];?></td>
            <td style="background-color: silver;"><?php echo $data['val4'];?></td>
            <td style="background-color: silver;"><?php echo $data['val5'];?></td>
            <td style="background-color: silver;"><?php echo $data['val6'];?></td>
            <td style="background-color: silver;"><?php echo $data['val7'];?></td>
            <td style="background-color: silver;"><?php echo $data['val8'];?></td>
            <td style="background-color: silver;"><?php echo $data['val9'];?></td>
            <td style="background-color: silver;"><?php echo $data['val10'];?></td>
            <td style="background-color: silver;"><?php echo $data['val11'];?></td>
            <td style="background-color: silver;"><?php echo $data['val12'];?></td>
            <td style="background-color: silver;"><?php echo $data['val13'];?></td>
            <td style="background-color: silver;"><?php echo $data['val14'];?></td>
            <td style="background-color: silver;"><?php echo $data['val15'];?></td>
            <td style="background-color: silver;"><?php echo $data['val16'];?></td>
            <td style="background-color: silver;"><?php echo $data['val17'];?></td>
            <td style="background-color: silver;"><?php echo $data['val18'];?></td>
            <td style="background-color: silver;"><?php echo $data['val19'];?></td>
            <td style="background-color: silver;"><?php echo $data['val20'];?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="30">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="5">Measurement Point</td>
            <td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td>
            <td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td><td>18</td><td>19</td><td>20</td>
        </tr>
        <tr>
            <td colspan="5">Distance (mm) from Big Diameter of Table</td>
            <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td><td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
            <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td><td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
        </tr>
        <?php 
        for($i=0;$i<$form->grinding;$i++){
            $a=$i+1;
        ?>
        <tr>
            <td rowspan="7"><p class="jtable">Grinding<?php echo $a; ?></p></td>
            <td colspan="2" style="background-color: silver;">0 - Point Measurement</td>
            <?php
            $sql1=mysql_query("select * from grinding where form_id='$table' and no_grinding = '1' and no_urut='$a'");
            $data1=mysql_fetch_assoc($sql1);
            ?>
            <td style="background-color: silver;">Date</td>
            <td style="background-color: silver;"><?php echo $data1['date']; ?></td>
            <td style="background-color: silver;"><?php echo $data1['val1'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val2'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val3'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val4'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val5'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val6'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val7'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val8'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val9'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val10'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val11'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val12'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val13'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val14'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val15'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val16'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val17'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val18'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val19'];?></td>
            <td style="background-color: silver;"><?php echo $data1['val20'];?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: silver;">0 - Point Measurement</td>
            <?php
            $sql2=mysql_query("select * from grinding where form_id='$table' and no_grinding = '2' and no_urut='$a'");
            $data2=mysql_fetch_assoc($sql2);
            ?>
            <td style="background-color: silver;">Date</td>
            <td style="background-color: silver;"><?php echo $data2['date']; ?></td>
            <td style="background-color: silver;"><?php echo $data2['val1'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val2'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val3'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val4'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val5'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val6'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val7'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val8'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val9'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val10'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val11'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val12'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val13'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val14'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val15'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val16'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val17'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val18'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val19'];?></td>
            <td style="background-color: silver;"><?php echo $data2['val20'];?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: silver;">0 - Point Measurement</td>
            <?php
            $sql3=mysql_query("select * from grinding where form_id='$table' and no_grinding = '3' and no_urut='$a'");
            $data3=mysql_fetch_assoc($sql3);
            ?>
            <td style="background-color: silver;">Date</td>
            <td style="background-color: silver;"><?php echo $data3['date']; ?></td>
            <td style="background-color: silver;"><?php echo $data3['val1'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val2'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val3'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val4'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val5'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val6'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val7'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val8'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val9'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val10'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val11'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val12'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val13'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val14'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val15'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val16'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val17'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val18'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val19'];?></td>
            <td style="background-color: silver;"><?php echo $data3['val20'];?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <?php } ?>
    </table>
    <div style="text-align: center;margin-top: 5px;">
    <input id="printpagebutton" type="button" onclick="printpage();" value="Print" >
</div>
</body>
</html>
