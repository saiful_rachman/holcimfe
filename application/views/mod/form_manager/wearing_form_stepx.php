<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<script type="text/javascript">
$(document).ready(function(){
$(function() {

        
         
});
});
function coba(id){
   function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }

    $("#txtinput"+id)
            // don't navigate away from the field on tab when selecting an item
              .bind( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunction",{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
            
            $("#txtinput"+id).change(function (){
             var kelas_id = $(this).val();
            $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
               data : "id="+kelas_id,
               success: function(data){
                   $("#matapelajaran_id"+id).html(data);
               }
});
});
            
}
</script>
<form method="post" action="<?php echo base_url();?>engine/form_manager/simpan_wearingstep2" id="form">
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Create Form Wizard</h2>
					<h4>Measurement Inspection Form <span class="pull-right">STEP 2</</span></h4>
					<div class="well well-small">
                                            <table border="1">
                                                <tbody id="listingx">
                                                <tr>
                                                    <td style="text-align: center;" colspan="22">WEAR MEASUREMENT REPORT</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="22" align="center">Hac Code: <?php echo $list->hac_code; ?><input type="hidden" name="form_id" value="<?php echo $list->id; ?>"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Measurement Point</td>
                                                    <?php
                                                    for($i=1;$i<=20;$i++){
                                                        echo "<td width='30px' align='center'>$i</td>";
                                                    }
                                                    ?>
                                                </tr>
                                                <tr style="text-align: center;">
                                                    <td colspan="2">Distance (mm) from Big Diameter of Roller</td>
                                                    <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td>
                                                    <td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
                                                    <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td>
                                                    <td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
                                                </tr>
                                                <?php 
                                                    for($i=0;$i<$list->roller;$i++){
                                                ?>
                                                <tr style='text-align: center;'>
                                                    <td><b>Roller <?php echo $i+1; ?></b><br/>0 - Point Measuring</td>
                                                    <td><?php echo date("Y-m-d");?><input type="hidden" name="roller[]" value="<?php echo $i+1;?>"></td>
                                                    <?php
                                                        for($x=1;$x<21;$x++){
                                                            if($x > $list->point){
                                                                $disabledx="disabled";
                                                            }else{
                                                                $disabledx="";
                                                            }
                                                            echo"<td><input type='text' name='rol".$x."[]' style='width:23px;' $disabledx>";
                                                        }
                                                    ?>
                                                    <?php } ?>
                                                <tr><td colspan="30">&nbsp;</tr>
                                                 <tr>
                                                    <td colspan="2">Measurement Point</td>
                                                    <?php
                                                    for($i=1;$i<=20;$i++){
                                                        echo "<td width='30px' align='center'>$i</td>";
                                                    }
                                                    ?>
                                                </tr>
                                                <tr style="text-align: center;">
                                                    <td colspan="2">Distance (mm) from Big Diameter of Roller</td>
                                                    <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td>
                                                    <td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
                                                    <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td>
                                                    <td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
                                                </tr>
                                                <?php 
                                                    for($i=0;$i<$list->grinding;$i++){
                                                ?>
                                                <tr><td colspan="30" style="background-color: #44688C;color: white;"><b>Grinding <?php echo $i+1; ?></b></tr>
                                                <tr style='text-align: center;'>
                                                    <td>0 - Point Measuring</td>
                                                    <td><?php echo date("Y-m-d");?><input type="hidden" name="grinding1" value="1"><input type="hidden" name="no_urut[]" value="<?php echo $i+1; ?>"></td>
                                                    <?php
                                                        for($y=1;$y<21;$y++){
                                                            if($y > $list->point){
                                                                $disabledy="disabled";
                                                            }else{
                                                                $disabledy="";
                                                            }
                                                            echo"<td><input type='text' name='".$y."a[]' style='width:23px;' $disabledy></td>";
                                                        }
                                                    ?>
                                                </tr>
                                                 <tr style='text-align: center;'>
                                                    <td>0 - Point Measuring</td>
                                                    <td><?php echo date("Y-m-d");?><input type="hidden" name="grinding2" value="2"></td>
                                                    <?php
                                                        for($y2=1;$y2<21;$y2++){
                                                            if($y2 > $list->point){
                                                                $disabledy2="disabled";
                                                            }else{
                                                                $disabledy2="";
                                                            }
                                                            echo"<td><input type='text' name='".$y2."b[]' style='width:23px;' $disabledy2></td>";
                                                        }
                                                    ?>
                                                </tr>
                                                <tr style='text-align: center;'>
                                                    <td>0 - Point Measuring</td>
                                                    <td><?php echo date("Y-m-d");?><input type="hidden" name="grinding3" value="3"></td>
                                                    <?php
                                                        for($y3=1;$y3<21;$y3++){
                                                            if($y3 > $list->point){
                                                                $disabledy3="disabled";
                                                            }else{
                                                                $disabledy3="";
                                                            }
                                                            echo"<td><input type='text' name='".$y3."c[]' style='width:23px;' $disabledy3></td>";
                                                        }
                                                    ?>
                                                </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            <div>&nbsp;</div>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" id="back"><i class="icon-backward icon-black"></i> Back</a>
                                        </div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>
<script type="text/javascript">
$(document).ready(function(){
    //var i = 0;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = dd+'/'+mm+'/'+yyyy;
    var rowCount = $('#listing tr').length;
$('#add_roller1').click(function() {
        var j = rowCount++;   
	//var x = parseInt($(this).val()) + 1; 
	//$(this).val(x);
        var data_list = 
        "<tr style='text-align: center;'>\n\
            <td>"+j+" - Point Measuring</td>\n\
            <td>"+today+"</td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
            <td><input type='text' name='' style='width:23px;'></td>\n\
        </tr>"
	$("#listingx").append(data_list);
        
});     
$("#rem_listing").click(function() {
    var rowCount = $('#listing tr').length;
    if(rowCount <= 2){
        alert('Row minimum 1');
    }else{
        $("#listing tr:last-child").remove();
    }
    });
});
$('#back').click(function(){
    $.ajax({
          type: "GET",
          url: "<?php echo base_url(); ?>engine/form_manager/delete_back_form_wear",
          success: function(response) {

          if (response == "Success")
          {
              window.history.back();
          }
          else
          {
              alert("Error");
          }

       }
    });
});
$('#form').submit(function(){
     alert('Data has been Update !');
    }); 
</script>		