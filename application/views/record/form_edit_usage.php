<?php $this->load->view('includes/header.php') ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<style type="text/css">
    .txtarea{
        max-height: 100px;
        min-height: 100px;
        max-width: 360px;
        min-width: 360px;
    }
</style>
<script type="text/javascript">
function coba(){
function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }
        var subarea = $("#subarea").val();
        $("#txtinput")
            // don't navigate away from the field on tab when selecting an item
              .focusout( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunction/"+subarea+"/",{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
}
</script>
<form method="post" action="<?php echo site_url();?>record/usage/edit_post" enctype="multipart/form-data" id="formx">
<div id="main">
<div id="content">
    <div class="inner">	
        <div class="row-fluid">
            <div class="span12">
                <h2>Update Form Wizard</h2>
                    <h4>Usage Oil Form <span class="pull-right"></span></h4>
                        <div class="well well-small">
                            <table class="table">
                                <tr>
                                    <td width="200px">Plant Name</td><input type="hidden" name="id" value="<?=$list->id;?>"/>
                                    <td>
                                        <select name="area" class="span6" required id="plant">
                                        <option value="">-Select Plant-</option>
                                        <?php
                                            $arx=$list->area;
                                            $sql=mysql_query("select a.*,b.id as plant,c.id as area_id
                                                             from master_mainarea a
                                                             inner join master_plant b on a.id_plant=b.id
                                                             inner join area c on a.id=c.area
                                                             where c.id='$arx'");
                                            $data=  mysql_fetch_assoc($sql);
                                            foreach ($list_plant as $plant){
                                                if($plant->id==$data['plant']){
                                                    $cek_plant="selected";
                                                }else{
                                                    $cek_plant="";
                                                }
                                        ?>
                                            <option value="<?=$plant->id;?>" <?=$cek_plant;?>><?=$plant->plant_name;?></option>
                                        <?php } ?>
                                        </select>
                                    </td><input type="hidden" id="plant_id" value="<?=$data['plant'];?>"/><input type="hidden" id="main_area_id" value="<?=$data['id'];?>"/><input type="hidden" id="sub_area_id" value="<?=$data['area_id'];?>"/>
                                </tr>	
                                <tr>
                                    <td width="200px">Area Name</td>
                                    <td>
                                        <select name="area" class="span6" required id="area">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">Sub Area Name</td>
                                    <td>
                                        <select name="subarea" class="span6" required id="subarea">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">Work Order</td><input type="hidden" id="wo_hidden" value="<?=$list->wo_id;?>"/>
                                    <td>
                                        <select name="wo" class="span6" required id="wo">
                                            <option value="">-Select WO-</option>
                                        <?php
                                            foreach ($list_wo as $list_wox){
                                        ?>
                                            <option value="<?=$list_wox->id;?>"><?=$list_wox->work_order;?></option>
                                        <?php } ?>
                                        </select>
                                    </td>
                                </tr>	
                                <tr>
                                    <td width="200px">Batch No.</td><input type="hidden" id="batch_hidden" value="<?=$list->batch_id;?>"/>
                                    <td>
                                        <select name="batch_no" class="span6" required id="batch_no">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Lubricant Name</td>
                                    <td><input type="text" name="lubricant_name" class="span6" value="<?=$list->lubricant_name;?>"></td>
                                </tr>
                                <tr>
                                    <td>Lubricant Type</td>
                                    <td><input type="text" name="lubricant_type" id="lubricant_type" class="span6" readonly></td>
                                </tr>
                                <tr>
                                    <td>Quantity</td>
                                    <td><input type="text" name="quantity" onkeypress="return validate(event);" class="span3" value="<?=$list->quantity;?>" required> <input type="text" name="unit" id="unit" class="span1" readonly/></td>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td><input type="text" name="date" class=" datepicker span3" value="<?=substr($list->date,0,10);?>" required></td>
                                </tr>
                                <tr>
                                        <td>Description</td>
                                        <td><textarea name="description" id="description" placeholder="Description"><?=$list->description;?></textarea></td>
                                </tr>
                                <tr>
                                        <td>Remarks</td>
                                        <td><textarea name="remarks" placeholder="Remarks"><?=$list->remarks;?></textarea></td>
                                </tr>
                                <tr>
                                        <td>Recomendation</td>
                                        <td><textarea name="recomendation"  placeholder="Recomendation"><?=$list->recomendation;?></textarea></td>
                                </tr>
                                <tr>
                                        <td>&nbsp;</td>
                                        <td><input type="hidden" name="user" value="<?php echo $this->session->userdata('users_id');?>"></td>
                                </tr>
                            </table>
                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                        </div>
            </div>
        </div>
    </div>
</div>
</div>
</form>
 
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
</script>		
<script type="text/javascript">
$(document).ready(function(){
        var plant_id=$("#plant_id").val();
        var main_area_id=$("#main_area_id").val();
        var sub_area_id=$("#sub_area_id").val();
        var wo=$("#wo_hidden").val();
        var batch=$("#batch_hidden").val();
        //post data main area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area_edit",
             data: "plant_id="+plant_id+"&main_area_id="+main_area_id,
             success: function(data){
                 $("#area").html(data);
             }
         });
         
        //post data sub area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea_edit",
             data: "main_area_id="+main_area_id+"&sub_area_id="+sub_area_id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
       //post data wo
         $.ajax({
             type:'post',
             url:"<?=base_url();?>record/usage/get_wo_edit",
             data: "area="+sub_area_id+"&wo="+wo,
             success: function(data){
                 $("#wo").html(data);
             }
         });
         //post data batch
         $.ajax({
             type:'post',
             url:"<?=base_url();?>record/usage/get_batch_edit",
             data: "wo="+wo+"&batch="+batch,
             success: function(data){
                 $("#batch_no").html(data);
             }
         });
         //post data batch detail
         $.ajax({
            type:"POST",
            url:"<?=base_url();?>record/usage/get_data_batch",
            data:"id="+batch,
            success: function(dt){
                hasil=dt.split("|");
                //console.log(hasil);
                $("#lubricant_type").val(hasil[0].split("|"));
                $("#unit").val(hasil[2].split("|"));
            },
            error: function(dt){
                alert("gagal");
            }
        });
$("#plant").change(function(){
     var id = $("#plant").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_area",
         data: "id="+id,
         success: function(data){
             $("#area").html(data);
         }
     });
  });

  $("#area").change(function(){
     var id = $("#area").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_subarea",
         data: "id="+id,
         success: function(data){
             $("#subarea").html(data);
         }
     });
   });
   $("#subarea").change(function(){
     var id = $(this).val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>record/usage/get_wo",
         data: "id="+id,
         success: function(data){
             $("#wo").html(data);
         }
     });
   });
$("#wo").change(function(){
     var id = $("#wo").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>record/usage/get_batch",
         data: "id="+id,
         success: function(data){
             $("#batch_no").html(data);
         }
     });
  });
$("#batch_no").change(function (){
    var id=$(this).val();
     $.ajax({
            type:"POST",
            url:"<?=base_url();?>record/usage/get_data_batch",
            data:"id="+id,
            success: function(dt){
                hasil=dt.split("|");
                //console.log(hasil);
                $("#lubricant_type").val(hasil[0].split("|"));
                $("#unit").val(hasil[2].split("|"));
            },
            error: function(dt){
                alert("gagal");
            }
        });
});
$( ".datepicker" ).datepicker({
                    dateFormat: "yy-mm-dd"
            });
  });
function validate(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
    </script>