<?php $this->load->view('includes/header.php') ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<style type="text/css">
    .txtarea{
        max-height: 100px;
        min-height: 100px;
        max-width: 360px;
        min-width: 360px;
    }
</style>
<script type="text/javascript">
function coba(){
function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }
        var subarea = $("#areaname").val();
        var plant = $("#plant").val();
        $("#txtinput")
            // don't navigate away from the field on tab when selecting an item
              .focusout( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunctionx/"+subarea+"/"+plant,{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
}
</script>
<form method="post" action="<?php echo site_url();?>record/add_mcsa/edit_post" enctype="multipart/form-data" id="formx">
<div id="main">
<div id="content">
    <div class="inner">	
        <div class="row-fluid">
            <div class="span12">
                <h2>Update Form Wizard</h2>
                    <h4>MCSA Form <span class="pull-right"></span></h4>
                        <div class="well well-small">
                            <table class="table">
                                <tr>
                                    <td width="200px">Plant Name</td><input type="hidden" name="id" value="<?=$list->record_id;?>"/>
                                    <td>
                                    <select name="area" class="span6" required id="plant">
                                        <option value="">-Select Plant-</option>
                                        <?php
                                            $arx=$list->area;
                                            $sql=mysql_query("select a.*,b.id as plant,c.id as area_id
                                                             from master_mainarea a
                                                             inner join master_plant b on a.id_plant=b.id
                                                             inner join area c on a.id=c.area
                                                             where c.id='$arx'");
                                            $data=  mysql_fetch_assoc($sql);
                                            foreach ($list_plant as $plant){
                                                if($plant->id==$data['plant']){
                                                    $cek_plant="selected";
                                                }else{
                                                    $cek_plant="";
                                                }
                                        ?>
                                            <option value="<?=$plant->id;?>" <?=$cek_plant;?>><?=$plant->plant_name;?></option>
                                        <?php } ?>
                                        </select>
                                    </td><input type="hidden" id="plant_id" value="<?=$data['plant'];?>"/><input type="hidden" id="main_area_id" value="<?=$data['id'];?>"/><input type="hidden" id="sub_area_id" value="<?=$data['area_id'];?>"/>
                                </tr>	
                                <tr>
                                    <td width="200px">Area Name</td>
                                    <td>
                                        <select name="area" class="span6" required id="area"><input type="hidden" id="areaname">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">Sub Area Name</td>
                                    <td>
                                        <select name="subarea" class="span6" required id="subarea">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Hac</td>
                                    <td><input required autocomplete="off" type="text" name="hac" id="txtinput" onkeypress="coba()"  placeholder="HAC" class="span6" value="<?=$list->hac_code;?>"/><input type="hidden" name="hac_id" value="<?=$list->hac;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Description</td>
                                    <td><textarea name="description" class="txtarea" id="description" placeholder="Description"><?=$list->description;?></textarea></td>
                                </tr>
                                <tr>
                                    <td>Upload File(pdf)</td>
                                    <td>
                                        <input type="file" class="btn" name="upload_file" id="upload_file"/>
                                        <input type="hidden" class="btn" name="upload_file_hidden" id="upload_file_hidden" value="<?=$list->upload_file;?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td><textarea name="remarks" cols="60" rows="5" placeholder="Remarks" class="txtarea"><?=$list->remarks;?></textarea></td>
                                </tr>
                                <tr>
                                    <td>Recomendation</td>
                                    <td><textarea name="recomendation"  cols="60" rows="5" placeholder="Recomendation" class="txtarea"><?=$list->recomendation;?></textarea></td>
                                </tr>
                                <tr>
                                    <td>Voltage Level</td>
                                    <td>
                                        <select name="voltage_level" class="span6">
                                            <?php
                                            if($list->voltage_level=="0"){
                                                $vl_a="selected";
                                            }else{
                                                $vl_a="";
                                            }
                                            if($list->voltage_level=="1"){
                                                $vl_b="selected";
                                            }else{
                                                $vl_b="";
                                            }
                                            if($list->voltage_level=="2"){
                                                $vl_c="selected";
                                            }else{
                                                $vl_c="";
                                            }
                                            if($list->voltage_level=="3"){
                                                $vl_d="selected";
                                            }else{
                                                $vl_d="";
                                            }
                                            if($list->voltage_level=="4"){
                                                $vl_e="selected";
                                            }else{
                                                $vl_e="";
                                            }
                                            if($list->voltage_level=="5"){
                                                $vl_f="selected";
                                            }else{
                                                $vl_f="";
                                            }
                                            ?>
                                            <option value="0" <?=$vl_a;?>>0</option>
                                            <option value="1" <?=$vl_b;?>>1</option>
                                            <option value="2" <?=$vl_c;?>>2</option>
                                            <option value="3" <?=$vl_d;?>>3</option>
                                            <option value="4" <?=$vl_e;?>>4</option>
                                            <option value="5" <?=$vl_f;?>>5</option>
                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Voltage Unbalance</td>
                                    <td>
                                        <select name="voltage_unbalance" class="span6">
                                            <?php
                                            if($list->voltage_unbalance=="0"){
                                                $vc_a="selected";
                                            }else{
                                                $vc_a="";
                                            }
                                            if($list->voltage_unbalance=="1"){
                                                $vc_b="selected";
                                            }else{
                                                $vc_b="";
                                            }
                                            if($list->voltage_unbalance=="2"){
                                                $vc_c="selected";
                                            }else{
                                                $vc_c="";
                                            }
                                            if($list->voltage_unbalance=="3"){
                                                $vc_d="selected";
                                            }else{
                                                $vc_d="";
                                            }
                                            if($list->voltage_unbalance=="4"){
                                                $vc_e="selected";
                                            }else{
                                                $vc_e="";
                                            }
                                            if($list->voltage_unbalance=="5"){
                                                $vc_f="selected";
                                            }else{
                                                $vc_f="";
                                            }
                                            ?>
                                            <option value="0" <?=$vc_a;?>>0</option>
                                            <option value="1" <?=$vc_b;?>>1</option>
                                            <option value="2" <?=$vc_c;?>>2</option>
                                            <option value="3" <?=$vc_d;?>>3</option>
                                            <option value="4" <?=$vc_e;?>>4</option>
                                            <option value="5" <?=$vc_f;?>>5</option>
                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Current Level</td>
                                    <td>
                                        <select name="current_level" class="span6">
                                            <?php
                                            if($list->current_level=="0"){
                                                $cl_a="selected";
                                            }else{
                                                $cl_a="";
                                            }
                                            if($list->current_level=="1"){
                                                $cl_b="selected";
                                            }else{
                                                $cl_b="";
                                            }
                                            if($list->current_level=="2"){
                                                $cl_c="selected";
                                            }else{
                                                $cl_c="";
                                            }
                                            if($list->current_level=="3"){
                                                $cl_d="selected";
                                            }else{
                                                $cl_d="";
                                            }
                                            if($list->current_level=="4"){
                                                $cl_e="selected";
                                            }else{
                                                $cl_e="";
                                            }
                                            if($list->current_level=="5"){
                                                $cl_f="selected";
                                            }else{
                                                $cl_f="";
                                            }
                                            ?>
                                            <option value="0" <?=$cl_a;?>>0</option>
                                            <option value="1" <?=$cl_b;?>>1</option>
                                            <option value="2" <?=$cl_c;?>>2</option>
                                            <option value="3" <?=$cl_d;?>>3</option>
                                            <option value="4" <?=$cl_e;?>>4</option>
                                            <option value="5" <?=$cl_f;?>>5</option>
                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Current Unbalance</td>
                                    <td>
                                        <select name="current_unbalance" class="span6">
                                            <?php
                                            if($list->current_unbalance=="0"){
                                                $cu_a="selected";
                                            }else{
                                                $cu_a="";
                                            }
                                            if($list->current_unbalance=="1"){
                                                $cu_b="selected";
                                            }else{
                                                $cu_b="";
                                            }
                                            if($list->current_unbalance=="2"){
                                                $cu_c="selected";
                                            }else{
                                                $cu_c="";
                                            }
                                            if($list->current_unbalance=="3"){
                                                $cu_d="selected";
                                            }else{
                                                $cu_d="";
                                            }
                                            if($list->current_unbalance=="4"){
                                                $cu_e="selected";
                                            }else{
                                                $cu_e="";
                                            }
                                            if($list->current_unbalance=="5"){
                                                $cu_f="selected";
                                            }else{
                                                $cu_f="";
                                            }
                                            ?>
                                            <option value="0" <?=$cu_a;?>>0</option>
                                            <option value="1" <?=$cu_b;?>>1</option>
                                            <option value="2" <?=$cu_c;?>>2</option>
                                            <option value="3" <?=$cu_d;?>>3</option>
                                            <option value="4" <?=$cu_e;?>>4</option>
                                            <option value="5" <?=$cu_f;?>>5</option>
                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td>THD</td>
                                    <td>
                                        <select name="thd" class="span6">
                                            <?php
                                            if($list->current_unbalance=="0"){
                                                $th_a="selected";
                                            }else{
                                                $th_a="";
                                            }
                                            if($list->current_unbalance=="1"){
                                                $th_b="selected";
                                            }else{
                                                $th_b="";
                                            }
                                            if($list->current_unbalance=="2"){
                                                $th_c="selected";
                                            }else{
                                                $th_c="";
                                            }
                                            if($list->current_unbalance=="3"){
                                                $th_d="selected";
                                            }else{
                                                $th_d="";
                                            }
                                            if($list->current_unbalance=="4"){
                                                $th_e="selected";
                                            }else{
                                                $th_e="";
                                            }
                                            if($list->current_unbalance=="5"){
                                                $th_f="selected";
                                            }else{
                                                $th_f="";
                                            }
                                            ?>
                                            <option value="0" <?=$th_a;?>>0</option>
                                            <option value="1" <?=$th_b;?>>1</option>
                                            <option value="2" <?=$th_c;?>>2</option>
                                            <option value="3" <?=$th_d;?>>3</option>
                                            <option value="4" <?=$th_e;?>>4</option>
                                            <option value="5" <?=$th_f;?>>5</option>
                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                        <td>Recomendation</td>
                                        <td><textarea name="recomendation" class="txtarea" placeholder="Recomendation"><?=$list->recomendation;?></textarea></td>
                                </tr>
                                <tr>
                                    <td>Severity_level</td>
                                    <td>
                                        <select name="severity_level" class="span6">
                                            <?php
                                                if($list->severity_level=="0"){
                                                    $normal="selected";
                                                }else{
                                                    $normal="";
                                                }
                                                if($list->severity_level=="1"){
                                                    $warning="selected";
                                                }else{
                                                    $warning="";
                                                }
                                                if($list->severity_level=="2"){
                                                    $danger="selected";
                                                }else{
                                                    $danger="";
                                                }
                                                ?>
                                            <option value="0">Normal</option>
                                            <option value="1">Warning</option>
                                            <option value="2">Danger</option>
                                        </select>

                                    </td>
                                </tr>
                                <input type="hidden" name="user" value="<?php echo $this->session->userdata('users_id');?>">
			</table>
                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                        </div>
            </div>
        </div>
    </div>
</div>
</div>
</form>
 
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
</script>	
<script type="text/javascript">
$(document).ready(function(){
    var plant_id=$("#plant_id").val();
        var main_area_id=$("#main_area_id").val();
        var sub_area_id=$("#sub_area_id").val();
        
        //post data main area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area_edit",
             data: "plant_id="+plant_id+"&main_area_id="+main_area_id,
             success: function(data){
                 $("#area").html(data);
             }
         });
         
        //post data sub area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea_edit",
             data: "main_area_id="+main_area_id+"&sub_area_id="+sub_area_id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
         
         //post data mainareaname
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_areaname",
             data: "id="+sub_area_id,
             success: function(data){
                 $("#areaname").val(data);
             }
         });
         
         //post data hac code
         $.ajax({
            type:"POST",
            url:"<?=base_url();?>engine/crud_hac/get_data",
            data:"id="+main_area_id+"&sub="+sub_area_id,
            success: function(dt){
                hasil=dt.split("|");
                console.log(hasil);
                //$("#plant").val(hasil[3].split("|"));
                $("#n_area").text("TQ."+hasil[7].split("|"));
                $("#n_plant").html(hasil[5].split("|"));
                $("#n_area_h").val("TQ."+hasil[7].split("|"));
                $("#n_plant_h").val(hasil[5].split("|"));
            },
            error: function(dt){
                alert("gagal");
            }
        });
$("#plant").change(function(){
     var id = $("#plant").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_area",
         data: "id="+id,
         success: function(data){
             $("#area").html(data);
         }
     });
  });

  $("#area").change(function(){
     var id = $("#area").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_subarea",
         data: "id="+id,
         success: function(data){
             $("#subarea").html(data);
         }
     });
   });
   $("#subarea").change(function(){
    var id = $(this).val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_areaname",
         data: "id="+id,
         success: function(data){
             console.log(data);
             $("#areaname").val(data);
         }
     });
           $("#txtinput").val('');
           //$("#listingx").remove();
      });
});
function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah2')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah3')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
    </script>