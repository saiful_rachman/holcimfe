<?php $this->load->view('includes/header.php') ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<style type="text/css">
    .txtarea{
        max-height: 100px;
        min-height: 100px;
        max-width: 360px;
        min-width: 360px;
    }
</style>
<script type="text/javascript">
function coba(){
function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }
        var subarea = $("#areaname").val();
        var plant = $("#plant").val();
        $("#txtinput")
            // don't navigate away from the field on tab when selecting an item
              .focusout( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunctionx/"+subarea+"/"+plant,{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
}
</script>
<form method="post" action="<?php echo site_url();?>record/add_ultrasonic/edit_post" enctype="multipart/form-data" id="formx">
<div id="main">
<div id="content">
    <div class="inner">	
        <div class="row-fluid">
            <div class="span12">
                <h2>Update Form Wizard</h2>
                    <h4>Ultrasonic Form <span class="pull-right"></span></h4>
                        <div class="well well-small">
                            <table class="table">
                                <tr>
                                    <td width="200px">Plant Name</td><input type="hidden" name="id" value="<?=$list->record_id;?>"/>
                                    <td>
                                    <select name="area" class="span6" required id="plant">
                                        <option value="">-Select Plant-</option>
                                        <?php
                                            $arx=$list->area;
                                            $sql=mysql_query("select a.*,b.id as plant,c.id as area_id
                                                             from master_mainarea a
                                                             inner join master_plant b on a.id_plant=b.id
                                                             inner join area c on a.id=c.area
                                                             where c.id='$arx'");
                                            $data=  mysql_fetch_assoc($sql);
                                            foreach ($list_plant as $plant){
                                                if($plant->id==$data['plant']){
                                                    $cek_plant="selected";
                                                }else{
                                                    $cek_plant="";
                                                }
                                        ?>
                                            <option value="<?=$plant->id;?>" <?=$cek_plant;?>><?=$plant->plant_name;?></option>
                                        <?php } ?>
                                        </select>
                                    </td><input type="hidden" id="plant_id" value="<?=$data['plant'];?>"/><input type="hidden" id="main_area_id" value="<?=$data['id'];?>"/><input type="hidden" id="sub_area_id" value="<?=$data['area_id'];?>"/>
                                </tr>	
                                <tr>
                                    <td width="200px">Area Name</td>
                                    <td>
                                        <select name="area" class="span6" required id="area"><input type="hidden" id="areaname">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">Sub Area Name</td>
                                    <td>
                                        <select name="subarea" class="span6" required id="subarea">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Hac</td>
                                    <td><input type="text" name="hac" id="txtinput" onkeypress="coba()"  placeholder="HAC" class="span6" value="<?=$list->hac_code;?>" required autocomplete="off"/><input type="hidden" name="hac_id" value="<?=$list->hac;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Test Object</td>
                                    <td><input type="text" class="span6" placeholder="Test Object" name="test_object" value="<?=$list->test_object;?>" /></td>
                                </tr>
                                <tr>
                                    <td>Model</td>
                                    <td><input type="text" class="span6" placeholder="Model" name="model" value="<?=$list->model;?>" /></td>
                                </tr>
                                <tr>
                                    <td>Couplant</td>
                                    <td>
                                        <select name="couplant" class="span6" required>
                                            <?php
                                                if($list->couplant=="Grease"){
                                                    $a="selected";
                                                }else{
                                                    $a="";
                                                }
                                                if($list->couplant=="Gliserin"){
                                                    $b="selected";
                                                }else{
                                                    $b="";
                                                }
                                                if($list->couplant=="Other"){
                                                    $c="selected";
                                                }else{
                                                    $c="";
                                                }
                                            ?>
                                            <option value="">-select cuplant-</option>
                                            <option value="Grease" <?=$a;?>>Grease</option>
                                            <option value="Gliserin" <?=$ba;?>>Gliserin</option>
                                            <option value="Other" <?=$c;?>>Other</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Probe type</td>
                                    <td>
                                        <select name="probe_type" class="span6" required>
                                            <?php
                                            if($list->probe_type=="Single"){
                                                $d="selected";
                                            }else{
                                                $d="";
                                            }
                                            if($list->probe_type=="Double"){
                                                $e="selected";
                                            }else{
                                                $e="";
                                            }
                                            ?>
                                            <option value="">-select cuplant-</option>
                                            <option value="Single" <?=$d;?>>Single</option>
                                            <option value="Double" <?=$e;?>>Double</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Frequency</td>
                                    <td>
                                        <input type="text" class="span6" name="frequency" value="<?=$list->frequency;?>"/> Mhz
                                    </td>
                                </tr>
                                <tr>
                                    <td>P_Zero</td>
                                    <td><input type="text" class="span6" placeholder="P_Zero" name="p_zero" value="<?=$list->p_zero;?>"/> Us</td>
                                </tr>
                                <tr>
                                    <td>Thickness</td>
                                    <td><input type="text" class="span6" placeholder="Thickness" name="thickness" value="<?=$list->thickness;?>"/> mm</td>
                                </tr>
                                <tr>
                                    <td>Gain</td>
                                    <td><input type="text" class="span6" placeholder="Gain"  name="gain" value="<?=$list->gain;?>"/> Db</td>
                                </tr>
                                <tr>
                                    <td>Vel</td>
                                    <td><input type="text" class="span6" placeholder="Vel"name="vel" value="<?=$list->vel;?>"/> m/s</td>
                                </tr>
                                <tr>
                                    <td>Range</td>
                                    <td><input type="text" class="span6" placeholder="Range" name="range" value="<?=$list->range;?>"/> mm</td>
                                </tr>
                                <tr>
                                    <td>Sa</td>
                                    <td><input type="text" class="span6" placeholder="Sa" name="sa" value="<?=$list->sa;?>"/> mm</td>
                                </tr>
                                <tr>
                                    <td>Ra</td>
                                    <td><input type="text" class="span6" placeholder="Ra" name="ra" value="<?=$list->ra;?>"/> mm</td>
                                </tr>
                                <tr>
                                    <td>Da</td>
                                    <td><input type="text" class="span6" placeholder="Da" name="da" value="<?=$list->da;?>"/> mm</td>
                                </tr>
                                <tr>
                                    <td>Image</td>
                                    <td>
                                        <input class="span6" onchange="readURL1(this);" type="file" class="btn" name="upload_file"/>
                                        <img id="blah1" src="#" /><input type="hidden" name="upload_image_hidden" value="<?=$list->upload_file;?>"/>     
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td><textarea name="remarks" class="txtarea" placeholder="Remarks"><?=$list->remarks;?></textarea></td>
                                </tr>
                                <tr>
                                    <td>Recomendation</td>
                                    <td><textarea name="recomendation" class="txtarea" placeholder="Recomendation"><?=$list->recomendation;?></textarea></td>
                                </tr>
                                <tr>
                                    <td>Severity_level</td>
                                    <td>
                                        <select name="severity_level" class="span6">
                                            <?php
                                            if($list->severity_level=="0"){
                                                $n="selected";
                                            }else{
                                                $n="";
                                            }
                                            if($list->severity_level=="1"){
                                                $w="selected";
                                            }else{
                                                $w="";
                                            }
                                            if($list->severity_level=="2"){
                                                $dg="selected";
                                            }else{
                                                $dg="";
                                            }
                                             ?>
                                            <option value="0" <?=$n;?>>Normal</option>
                                            <option value="1" <?=$w;?>>Warning</option>
                                            <option value="2" <?=$dg;?>>Danger</option>
                                        </select>

                                    </td>
                                </tr>
                                <input type="hidden" name="user" value="<?php echo $this->session->userdata('users_id');?>">
			</table>
		<button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                        </div>
            </div>
        </div>
    </div>
</div>
</div>
</form>
<?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
</script>	
<script type="text/javascript">
$(document).ready(function(){
var plant_id=$("#plant_id").val();
        var main_area_id=$("#main_area_id").val();
        var sub_area_id=$("#sub_area_id").val();
        
        //post data main area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area_edit",
             data: "plant_id="+plant_id+"&main_area_id="+main_area_id,
             success: function(data){
                 $("#area").html(data);
             }
         });
         
        //post data sub area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea_edit",
             data: "main_area_id="+main_area_id+"&sub_area_id="+sub_area_id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
         
          //post data mainareaname
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_areaname",
             data: "id="+sub_area_id,
             success: function(data){
                 $("#areaname").val(data);
             }
         });
         
         //post data hac code
         $.ajax({
            type:"POST",
            url:"<?=base_url();?>engine/crud_hac/get_data",
            data:"id="+main_area_id+"&sub="+sub_area_id,
            success: function(dt){
                hasil=dt.split("|");
                console.log(hasil);
                //$("#plant").val(hasil[3].split("|"));
                $("#n_area").text("TQ."+hasil[7].split("|"));
                $("#n_plant").html(hasil[5].split("|"));
                $("#n_area_h").val("TQ."+hasil[7].split("|"));
                $("#n_plant_h").val(hasil[5].split("|"));
            },
            error: function(dt){
                alert("gagal");
            }
        });
$("#plant").change(function(){
     var id = $("#plant").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_area",
         data: "id="+id,
         success: function(data){
             $("#area").html(data);
         }
     });
  });

  $("#area").change(function(){
     var id = $("#area").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_subarea",
         data: "id="+id,
         success: function(data){
             $("#subarea").html(data);
         }
     });
   });
   $("#subarea").change(function(){
    var id = $(this).val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_areaname",
         data: "id="+id,
         success: function(data){
             console.log(data);
             $("#areaname").val(data);
         }
     });
           $("#txtinput").val('');
           //$("#listingx").remove();
      });
});
function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
    </script>