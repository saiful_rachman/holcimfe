<?php include "header.php"; ?>

<script type="text/javascript" src="<?php echo base_url('/assets/highcharts/js/highcharts.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/highcharts/js/modules/exporting.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/highcharts/js/themes/grid.js'); ?>"></script>

<script type="text/javascript">
    
    function ChangeChartType(chart, series, newType) {
    newType = newType.toLowerCase();
    for (var i = 0; i < series.length; i++) {
        var srs = series[0];
        try {
            srs.chart.addSeries({
                type: newType,
                stack: srs.stack,
                yaxis: srs.yaxis,
                name: srs.name,
                color: srs.color,
                data: srs.options.data
            },
            false);
            series[0].remove();
        } catch (e) {
        }
    }
}

var chart1;

    $(document).ready(function() {

    // First chart initialization
    chart1 = new Highcharts.Chart({
        chart: {
            renderTo: 'chart',
            type: 'line',
        },
        title: {
            text: 'Grafik',
            x: -20
        },
         <?php 
        foreach($type as $trend_name => $value_name): 
        
            $type_trend = '[';
            for($i=0;$i<count($date[$trend_name]);$i++)
            {
                $type_trend .= '"'.$value_name[$i].'",';
            }
            $type_trend .= ']';
        endforeach;
        ?>
        
        subtitle: {
            text: 'Oil Pollution',
            x: -20
        },
        
         <?php 
        foreach($date as $trend_name => $value_name): 
        
            $data_date = '[';
            for($i=0;$i<count($date[$trend_name]);$i++)
            {
                $data_date .= '"'.$value_name[$i].'",';
            }
            $data_date .= ']';
        endforeach;
        ?>
        
        
        xAxis: {
           categories: <?=$data_date?>,
           
        },
        yAxis: {
            title: {
                text: 'Value'
            }
        },
        
        series: [
        
        
        <?php 
        foreach($chart as $trend_name => $value_name): 
        
            $data_value = '[';
            for($i=0;$i<count($chart[$trend_name]);$i++)
            {
                $data_value .= $value_name[$i].',';
            }
            $data_value .= ']';
        
        ?>
        {
            color: '',
            name: '<?php echo $trend_name ?>',
            data: <?=$data_value?>
        }
        <?php 
        echo ",";
        endforeach; ?>
        
        ]
    });
    
     $('.switcher').click(function () {  
        var newType = $(this).attr('id');
        ChangeChartType(chart1, chart1.series, newType);
    });
}); 
</script>
    <div id="content">
    
		<div class="inner">
		<div class="row-fluid">
			<div class="span12" style="padding-top:12%"> 
			<div class="actions">
				<button class="switcher" id="">Column</button>
				<button class="switcher" id="area">Area</button>
				<button class="switcher" id="line">Line</button>
				<button class="switcher" id="spline">Spline</button>
				<button class="switcher" id="areaspline">Areaspline</button>
				 <button class="switcher" id="areaspline">Areaspline</button>
				<button class="switcher" id="pie">Pie</button>
			</div>
			<div id="chart"></div>
		</div>
		</div>
	</div>
</div>
 <?php include "footer.php"; ?>


