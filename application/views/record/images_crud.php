<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
    color: blue;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
	text-decoration: underline;
}
</style>
</head>
<?php 
$this->load->view('includes/header_crud.php');
?>
<body>
	<div id="main">
		<div id="content">
			<div class="inner">	
				<div class="row-fluid">
					<div class="span12">
						<div>
								<?php echo $output; ?>
								
							</div>
							<div class="pull-left back-btn">
									<button onclick="history.go(-1);" class="btn">
									<i class="icon-chevron-left"></i>Submit</button>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
<?php 
$this->load->view('includes/footer.php');
?>
</html>
