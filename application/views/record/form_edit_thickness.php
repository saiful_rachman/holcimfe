<?php $this->load->view('includes/header.php'); error_reporting(0); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<style type="text/css">
    .txtarea{
        max-height: 100px;
        min-height: 100px;
        max-width: 360px;
        min-width: 360px;
    }
</style>
<script type="text/javascript">
function coba(){
function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }
        var subarea = $("#areaname").val();
        var plant = $("#plant").val();
        $("#txtinput")
            // don't navigate away from the field on tab when selecting an item
              .focusout( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunctionx/"+subarea+"/"+plant,{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
}
</script>
<form method="post" action="<?php echo site_url();?>record/add_thickness/edit_post" enctype="multipart/form-data" id="formx">
<div id="main">
<div id="content">
    <div class="inner">	
        <div class="row-fluid">
            <div class="span12">
                <h2>Create Form Wizard</h2>
                    <h4>Vibration Form <span class="pull-right"></span></h4>
                        <div class="well well-small">
                            <table class="table">
                                <tr>
                                    <td width="200px">Plant Name</td><input type="hidden" name="id" id="idc" value="<?=$list->record_id;?>"/>
                                    <td>
                                    <select name="area" class="span6" required id="plant">
                                        <option value="">-Select Plant-</option>
                                        <?php
                                            $arx=$list->area;
                                            $sql=mysql_query("select a.*,b.id as plant,c.id as area_id
                                                             from master_mainarea a
                                                             inner join master_plant b on a.id_plant=b.id
                                                             inner join area c on a.id=c.area
                                                             where c.id='$arx'");
                                            $data=  mysql_fetch_assoc($sql);
                                            foreach ($list_plant as $plant){
                                                if($plant->id==$data['plant']){
                                                    $cek_plant="selected";
                                                }else{
                                                    $cek_plant="";
                                                }
                                        ?>
                                            <option value="<?=$plant->id;?>" <?=$cek_plant;?>><?=$plant->plant_name;?></option>
                                        <?php } ?>
                                        </select>
                                    </td><input type="hidden" id="plant_id" value="<?=$data['plant'];?>"/><input type="hidden" id="main_area_id" value="<?=$data['id'];?>"/><input type="hidden" id="sub_area_id" value="<?=$data['area_id'];?>"/>
                                </tr>	
                                <tr>
                                    <td width="200px">Area Name</td>
                                    <td>
                                        <select name="area" class="span6" required id="area"><input type="hidden" id="areaname">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">Sub Area Name</td>
                                    <td>
                                        <select name="subarea" class="span6" required id="subarea">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                        <td>Hac</td>
                                        <td><input required autocomplete="off" type="text" name="hac" id="txtinput" onkeypress="coba()"  placeholder="HAC" class="span6" value="<?=$list->hac_code;?>"/><input type="hidden" name="hac_id" value="<?=$list->hac;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Thickness Type</td>
                                    <td>
                                        <select name="thickness_type" id="type" class="span6" required>
                                            <?php
                                                if($list->thick_type=="general"){
                                                    $gre="selected";
                                                }else{
                                                    $gre="";
                                                }
                                                if($list->thick_type=="stack"){
                                                    $gli="selected";
                                                }else{
                                                    $gli="";
                                                }
                                                if($list->thick_type=="kiln"){
                                                    $ot="selected";
                                                }else{
                                                    $ot="";
                                                }
                                            ?>
                                            <option value="" selected>-select type-</option>
                                            <option value="general" <?=$gre;?>>General</option>
                                            <option value="stack" <?=$gli;?>>Stack</option>
                                            <option value="kiln" <?=$ot;?>>Kiln</option>
                                        </select>
                                    </td>
                                </tr>
                                <tbody id="add"></tbody>
                                <?php
                                if($list->thick_type=="general" || $list->thick_type=="stack"){
                                    $point_1=  explode(",", $point1);
                                    $point_1a=$point_1[0];
                                    $point_1b=$point_1[1];
                                    $point_1c=$point_1[2];
                                    $point_1d=$point_1[3];
                                    echo "<tr class='rem'>
                                            <td>Point 1</td>
                                            <td>
                                                <input type='text' name='point1[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_1a'>
                                                <input type='text' name='point1[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_1b'>
                                                <input type='text' name='point1[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_1c'> 
                                                <input type='text' name='point1[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_1d'>
                                            </td>
                                          </tr>";
                                    $point_2=  explode(",", $point2);
                                    $point_2a=$point_2[0];
                                    $point_2b=$point_2[1];
                                    $point_2c=$point_2[2];
                                    $point_2d=$point_2[3];
                                    echo "<tr class='rem'>
                                            <td>Point 2</td>
                                            <td>
                                                <input type='text' name='point2[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_2a'>
                                                <input type='text' name='point2[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_2b'>
                                                <input type='text' name='point2[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_2c'> 
                                                <input type='text' name='point2[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_2d'>
                                            </td>
                                          </tr>";
                                    $point_3=  explode(",", $point3);
                                    $point_3a=$point_3[0];
                                    $point_3b=$point_3[1];
                                    $point_3c=$point_3[2];
                                    $point_3d=$point_3[3];
                                    echo "<tr class='rem'>
                                            <td>Point 3</td>
                                            <td>
                                                <input type='text' name='point3[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_3a'>
                                                <input type='text' name='point3[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_3b'>
                                                <input type='text' name='point3[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_3c'> 
                                                <input type='text' name='point3[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_3d'>
                                            </td>
                                          </tr>";
                                    $point_4=  explode(",", $point4);
                                    $point_4a=$point_4[0];
                                    $point_4b=$point_4[1];
                                    $point_4c=$point_4[2];
                                    $point_4d=$point_4[3];
                                    echo "<tr class='rem'>
                                            <td>Point 4</td>
                                            <td>
                                                <input type='text' name='point4[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_4a'>
                                                <input type='text' name='point4[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_4b'>
                                                <input type='text' name='point4[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_4c'> 
                                                <input type='text' name='point4[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_4d'>
                                            </td>
                                          </tr>";
                                    $point_5=  explode(",", $point5);
                                    $point_5a=$point_5[0];
                                    $point_5b=$point_5[1];
                                    $point_5c=$point_5[2];
                                    $point_5d=$point_5[3];
                                    echo "<tr class='rem'>
                                            <td>Point 5</td>
                                            <td>
                                                <input type='text' name='point5[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_5a'>
                                                <input type='text' name='point5[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_5b'>
                                                <input type='text' name='point5[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_5c'> 
                                                <input type='text' name='point5[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_5d'>
                                            </td>
                                          </tr>";
                                    $point_6=  explode(",", $point6);
                                    $point_6a=$point_6[0];
                                    $point_6b=$point_6[1];
                                    $point_6c=$point_6[2];
                                    $point_6d=$point_6[3];
                                    echo "<tr class='rem'>
                                            <td>Point 6</td>
                                            <td>
                                                <input type='text' name='point6[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_6a'>
                                                <input type='text' name='point6[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_6b'>
                                                <input type='text' name='point6[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_6c'> 
                                                <input type='text' name='point6[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_6d'>
                                            </td>
                                          </tr>";
                                    $point_7=  explode(",", $point7);
                                    $point_7a=$point_7[0];
                                    $point_7b=$point_7[1];
                                    $point_7c=$point_7[2];
                                    $point_7d=$point_7[3];
                                    echo "<tr class='rem'>
                                            <td>Point 7</td>
                                            <td>
                                                <input type='text' name='point7[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_7a'>
                                                <input type='text' name='point7[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_7b'>
                                                <input type='text' name='point7[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_7c'> 
                                                <input type='text' name='point7[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_7d'>
                                            </td>
                                          </tr>";
                                    $point_8=  explode(",", $point8);
                                    $point_8a=$point_8[0];
                                    $point_8b=$point_8[1];
                                    $point_8c=$point_8[2];
                                    $point_8d=$point_8[3];
                                    echo "<tr class='rem'>
                                            <td>Point 8</td>
                                            <td>
                                                <input type='text' name='point8[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_8a'>
                                                <input type='text' name='point8[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_8b'>
                                                <input type='text' name='point8[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_8c'> 
                                                <input type='text' name='point8[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_8d'>
                                            </td>
                                          </tr>";
                                    $point_9=  explode(",", $point9);
                                    $point_9a=$point_9[0];
                                    $point_9b=$point_9[1];
                                    $point_9c=$point_9[2];
                                    $point_9d=$point_9[3];
                                    echo "<tr class='rem'>
                                            <td>Point 9</td>
                                            <td>
                                                <input type='text' name='point9[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_9a'>
                                                <input type='text' name='point9[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_9b'>
                                                <input type='text' name='point9[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_9c'> 
                                                <input type='text' name='point9[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_9d'>
                                            </td>
                                          </tr>";
                                    $point_10=  explode(",", $point10);
                                    $point_10a=$point_10[0];
                                    $point_10b=$point_10[1];
                                    $point_10c=$point_10[2];
                                    $point_10d=$point_10[3];
                                    echo "<tr class='rem'>
                                            <td>Point 10</td>
                                            <td>
                                                <input type='text' name='point10[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_10a'>
                                                <input type='text' name='point10[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_10b'>
                                                <input type='text' name='point10[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_10c'> 
                                                <input type='text' name='point10[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_10d'>
                                            </td>
                                          </tr>";
                                    $point_11=  explode(",", $point11);
                                    $point_11a=$point_11[0];
                                    $point_11b=$point_11[1];
                                    $point_11c=$point_11[2];
                                    $point_11d=$point_11[3];
                                    echo "<tr class='rem'>
                                            <td>Point 11</td>
                                            <td>
                                                <input type='text' name='point11[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_11a'>
                                                <input type='text' name='point11[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_11b'>
                                                <input type='text' name='point11[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_11c'> 
                                                <input type='text' name='point11[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_11d'>
                                            </td>
                                          </tr>";
                                    $point_12=  explode(",", $point12);
                                    $point_12a=$point_12[0];
                                    $point_12b=$point_12[1];
                                    $point_12c=$point_12[2];
                                    $point_12d=$point_12[3];
                                    echo "<tr class='rem'>
                                            <td>Point 12</td>
                                            <td>
                                                <input type='text' name='point12[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_12a'>
                                                <input type='text' name='point12[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_12b'>
                                                <input type='text' name='point12[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_12c'> 
                                                <input type='text' name='point12[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_12d'>
                                            </td>
                                          </tr>";
                                    $point_13=  explode(",", $point13);
                                    $point_13a=$point_13[0];
                                    $point_13b=$point_13[1];
                                    $point_13c=$point_13[2];
                                    $point_13d=$point_13[3];
                                    echo "<tr class='rem'>
                                            <td>Point 13</td>
                                            <td>
                                                <input type='text' name='point13[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_13a'>
                                                <input type='text' name='point13[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_13b'>
                                                <input type='text' name='point13[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_13c'> 
                                                <input type='text' name='point13[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_13d'>
                                            </td>
                                          </tr>";
                                    $point_14=  explode(",", $point14);
                                    $point_14a=$point_14[0];
                                    $point_14b=$point_14[1];
                                    $point_14c=$point_14[2];
                                    $point_14d=$point_14[3];
                                    echo "<tr class='rem'>
                                            <td>Point 4</td>
                                            <td>
                                                <input type='text' name='point14[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_14a'>
                                                <input type='text' name='point14[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_14b'>
                                                <input type='text' name='point14[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_14c'> 
                                                <input type='text' name='point14[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_14d'>
                                            </td>
                                          </tr>";
                                    $point_15=  explode(",", $point15);
                                    $point_15a=$point_15[0];
                                    $point_15b=$point_15[1];
                                    $point_15c=$point_15[2];
                                    $point_15d=$point_15[3];
                                    echo "<tr class='rem'>
                                            <td>Point 15</td>
                                            <td>
                                                <input type='text' name='point15[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_15a'>
                                                <input type='text' name='point15[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_15b'>
                                                <input type='text' name='point15[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_15c'> 
                                                <input type='text' name='point15[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_15d'>
                                            </td>
                                          </tr>";
                                    $point_16=  explode(",", $point16);
                                    $point_16a=$point_16[0];
                                    $point_16b=$point_16[1];
                                    $point_16c=$point_16[2];
                                    $point_16d=$point_16[3];
                                    echo "<tr class='rem'>
                                            <td>Point 16</td>
                                            <td>
                                                <input type='text' name='point16[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_16a'>
                                                <input type='text' name='point16[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_16b'>
                                                <input type='text' name='point16[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_16c'> 
                                                <input type='text' name='point16[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_16d'>
                                            </td>
                                          </tr>";
                                }else if($list->thick_type=="kiln"){
                                    $point_1=  explode(",", $point1);
                                    $point_1a=$point_1[0];
                                    $point_1b=$point_1[1];
                                    $point_1c=$point_1[2];
                                    $point_1d=$point_1[3];
                                    echo "<tr class='rem'>
                                            <td>Point 1</td>
                                            <td>
                                                <input type='text' name='point1[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_1a'>
                                                <input type='text' name='point1[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_1b'>
                                                <input type='text' name='point1[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_1c'> 
                                                <input type='text' name='point1[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_1d'>
                                            </td>
                                          </tr>";
                                    $point_2=  explode(",", $point2);
                                    $point_2a=$point_2[0];
                                    $point_2b=$point_2[1];
                                    $point_2c=$point_2[2];
                                    $point_2d=$point_2[3];
                                    echo "<tr class='rem'>
                                            <td>Point 2</td>
                                            <td>
                                                <input type='text' name='point2[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_2a'>
                                                <input type='text' name='point2[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_2b'>
                                                <input type='text' name='point2[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_2c'> 
                                                <input type='text' name='point2[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_2d'>
                                            </td>
                                          </tr>";
                                    $point_3=  explode(",", $point3);
                                    $point_3a=$point_3[0];
                                    $point_3b=$point_3[1];
                                    $point_3c=$point_3[2];
                                    $point_3d=$point_3[3];
                                    echo "<tr class='rem'>
                                            <td>Point 3</td>
                                            <td>
                                                <input type='text' name='point3[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_3a'>
                                                <input type='text' name='point3[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_3b'>
                                                <input type='text' name='point3[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_3c'> 
                                                <input type='text' name='point3[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_3d'>
                                            </td>
                                          </tr>";
                                    $point_4=  explode(",", $point4);
                                    $point_4a=$point_4[0];
                                    $point_4b=$point_4[1];
                                    $point_4c=$point_4[2];
                                    $point_4d=$point_4[3];
                                    echo "<tr class='rem'>
                                            <td>Point 4</td>
                                            <td>
                                                <input type='text' name='point4[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_4a'>
                                                <input type='text' name='point4[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_4b'>
                                                <input type='text' name='point4[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_4c'> 
                                                <input type='text' name='point4[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_4d'>
                                            </td>
                                          </tr>";
                                    $point_5=  explode(",", $point5);
                                    $point_5a=$point_5[0];
                                    $point_5b=$point_5[1];
                                    $point_5c=$point_5[2];
                                    $point_5d=$point_5[3];
                                    echo "<tr class='rem'>
                                            <td>Point 5</td>
                                            <td>
                                                <input type='text' name='point5[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_5a'>
                                                <input type='text' name='point5[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_5b'>
                                                <input type='text' name='point5[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_5c'> 
                                                <input type='text' name='point5[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_5d'>
                                            </td>
                                          </tr>";
                                    $point_6=  explode(",", $point6);
                                    $point_6a=$point_6[0];
                                    $point_6b=$point_6[1];
                                    $point_6c=$point_6[2];
                                    $point_6d=$point_6[3];
                                    echo "<tr class='rem'>
                                            <td>Point 6</td>
                                            <td>
                                                <input type='text' name='point6[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_6a'>
                                                <input type='text' name='point6[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_6b'>
                                                <input type='text' name='point6[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_6c'> 
                                                <input type='text' name='point6[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_6d'>
                                            </td>
                                          </tr>";
                                    $point_7=  explode(",", $point7);
                                    $point_7a=$point_7[0];
                                    $point_7b=$point_7[1];
                                    $point_7c=$point_7[2];
                                    $point_7d=$point_7[3];
                                    echo "<tr class='rem'>
                                            <td>Point 7</td>
                                            <td>
                                                <input type='text' name='point7[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_7a'>
                                                <input type='text' name='point7[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_7b'>
                                                <input type='text' name='point7[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_7c'> 
                                                <input type='text' name='point7[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_7d'>
                                            </td>
                                          </tr>";
                                    $point_8=  explode(",", $point8);
                                    $point_8a=$point_8[0];
                                    $point_8b=$point_8[1];
                                    $point_8c=$point_8[2];
                                    $point_8d=$point_8[3];
                                    echo "<tr class='rem'>
                                            <td>Point 8</td>
                                            <td>
                                                <input type='text' name='point8[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_8a'>
                                                <input type='text' name='point8[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_8b'>
                                                <input type='text' name='point8[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_8c'> 
                                                <input type='text' name='point8[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_8d'>
                                            </td>
                                          </tr>";
                                    $point_9=  explode(",", $point9);
                                    $point_9a=$point_9[0];
                                    $point_9b=$point_9[1];
                                    $point_9c=$point_9[2];
                                    $point_9d=$point_9[3];
                                    echo "<tr class='rem'>
                                            <td>Point 9</td>
                                            <td>
                                                <input type='text' name='point9[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_9a'>
                                                <input type='text' name='point9[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_9b'>
                                                <input type='text' name='point9[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_9c'> 
                                                <input type='text' name='point9[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_9d'>
                                            </td>
                                          </tr>";
                                    $point_10=  explode(",", $point10);
                                    $point_10a=$point_10[0];
                                    $point_10b=$point_10[1];
                                    $point_10c=$point_10[2];
                                    $point_10d=$point_10[3];
                                    echo "<tr class='rem'>
                                            <td>Point 10</td>
                                            <td>
                                                <input type='text' name='point10[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_10a'>
                                                <input type='text' name='point10[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_10b'>
                                                <input type='text' name='point10[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_10c'> 
                                                <input type='text' name='point10[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_10d'>
                                            </td>
                                          </tr>";
                                    $point_11=  explode(",", $point11);
                                    $point_11a=$point_11[0];
                                    $point_11b=$point_11[1];
                                    $point_11c=$point_11[2];
                                    $point_11d=$point_11[3];
                                    echo "<tr class='rem'>
                                            <td>Point 11</td>
                                            <td>
                                                <input type='text' name='point11[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_11a'>
                                                <input type='text' name='point11[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_11b'>
                                                <input type='text' name='point11[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_11c'> 
                                                <input type='text' name='point11[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_11d'>
                                            </td>
                                          </tr>";
                                    $point_12=  explode(",", $point12);
                                    $point_12a=$point_12[0];
                                    $point_12b=$point_12[1];
                                    $point_12c=$point_12[2];
                                    $point_12d=$point_12[3];
                                    echo "<tr class='rem'>
                                            <td>Point 12</td>
                                            <td>
                                                <input type='text' name='point12[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_12a'>
                                                <input type='text' name='point12[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_12b'>
                                                <input type='text' name='point12[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_12c'> 
                                                <input type='text' name='point12[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_12d'>
                                            </td>
                                          </tr>";
                                    $point_13=  explode(",", $point13);
                                    $point_13a=$point_13[0];
                                    $point_13b=$point_13[1];
                                    $point_13c=$point_13[2];
                                    $point_13d=$point_13[3];
                                    echo "<tr class='rem'>
                                            <td>Point 13</td>
                                            <td>
                                                <input type='text' name='point13[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_13a'>
                                                <input type='text' name='point13[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_13b'>
                                                <input type='text' name='point13[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_13c'> 
                                                <input type='text' name='point13[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_13d'>
                                            </td>
                                          </tr>";
                                    $point_14=  explode(",", $point14);
                                    $point_14a=$point_14[0];
                                    $point_14b=$point_14[1];
                                    $point_14c=$point_14[2];
                                    $point_14d=$point_14[3];
                                    echo "<tr class='rem'>
                                            <td>Point 4</td>
                                            <td>
                                                <input type='text' name='point14[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_14a'>
                                                <input type='text' name='point14[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_14b'>
                                                <input type='text' name='point14[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_14c'> 
                                                <input type='text' name='point14[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_14d'>
                                            </td>
                                          </tr>";
                                    $point_15=  explode(",", $point15);
                                    $point_15a=$point_15[0];
                                    $point_15b=$point_15[1];
                                    $point_15c=$point_15[2];
                                    $point_15d=$point_15[3];
                                    echo "<tr class='rem'>
                                            <td>Point 15</td>
                                            <td>
                                                <input type='text' name='point15[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_15a'>
                                                <input type='text' name='point15[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_15b'>
                                                <input type='text' name='point15[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_15c'> 
                                                <input type='text' name='point15[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_15d'>
                                            </td>
                                          </tr>";
                                    $point_16=  explode(",", $point16);
                                    $point_16a=$point_16[0];
                                    $point_16b=$point_16[1];
                                    $point_16c=$point_16[2];
                                    $point_16d=$point_16[3];
                                    echo "<tr class='rem'>
                                            <td>Point 16</td>
                                            <td>
                                                <input type='text' name='point16[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_16a'>
                                                <input type='text' name='point16[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_16b'>
                                                <input type='text' name='point16[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_16c'> 
                                                <input type='text' name='point16[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_16d'>
                                            </td>
                                          </tr>";
                                    $point_17=  explode(",", $point17);
                                    $point_17a=$point_17[0];
                                    $point_17b=$point_17[1];
                                    $point_17c=$point_17[2];
                                    $point_17d=$point_17[3];
                                    echo "<tr class='rem'>
                                            <td>Point 17</td>
                                            <td>
                                                <input type='text' name='point17[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_17a'>
                                                <input type='text' name='point17[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_17b'>
                                                <input type='text' name='point17[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_17c'> 
                                                <input type='text' name='point17[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_17d'>
                                            </td>
                                          </tr>";
                                    $point_18=  explode(",", $point18);
                                    $point_18a=$point_18[0];
                                    $point_18b=$point_18[1];
                                    $point_18c=$point_18[2];
                                    $point_18d=$point_18[3];
                                    echo "<tr class='rem'>
                                            <td>Point 18</td>
                                            <td>
                                                <input type='text' name='point18[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_18a'>
                                                <input type='text' name='point18[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_18b'>
                                                <input type='text' name='point18[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_18c'> 
                                                <input type='text' name='point18[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_18d'>
                                            </td>
                                          </tr>";
                                    $point_19=  explode(",", $point19);
                                    $point_19a=$point_19[0];
                                    $point_19b=$point_19[1];
                                    $point_19c=$point_19[2];
                                    $point_19d=$point_19[3];
                                    echo "<tr class='rem'>
                                            <td>Point 19</td>
                                            <td>
                                                <input type='text' name='point19[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_19a'>
                                                <input type='text' name='point19[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_19b'>
                                                <input type='text' name='point19[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_19c'> 
                                                <input type='text' name='point19[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_19d'>
                                            </td>
                                          </tr>";
                                    $point_20=  explode(",", $point20);
                                    $point_20a=$point_20[0];
                                    $point_20b=$point_20[1];
                                    $point_20c=$point_20[2];
                                    $point_20d=$point_20[3];
                                    echo "<tr class='rem'>
                                            <td>Point 20</td>
                                            <td>
                                                <input type='text' name='point20[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_20a'>
                                                <input type='text' name='point20[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_20b'>
                                                <input type='text' name='point20[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_20c'> 
                                                <input type='text' name='point20[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_20d'>
                                            </td>
                                          </tr>";
                                    $point_21=  explode(",", $point21);
                                    $point_21a=$point_21[0];
                                    $point_21b=$point_21[1];
                                    $point_21c=$point_21[2];
                                    $point_21d=$point_21[3];
                                    echo "<tr class='rem'>
                                            <td>Point 21</td>
                                            <td>
                                                <input type='text' name='point21[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_21a'>
                                                <input type='text' name='point21[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_21b'>
                                                <input type='text' name='point21[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_21c'> 
                                                <input type='text' name='point21[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_21d'>
                                            </td>
                                          </tr>";
                                    $point_22=  explode(",", $point22);
                                    $point_22a=$point_22[0];
                                    $point_22b=$point_22[1];
                                    $point_22c=$point_22[2];
                                    $point_22d=$point_22[3];
                                    echo "<tr class='rem'>
                                            <td>Point 22</td>
                                            <td>
                                                <input type='text' name='point22[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_22a'>
                                                <input type='text' name='point22[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_22b'>
                                                <input type='text' name='point22[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_22c'> 
                                                <input type='text' name='point22[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_22d'>
                                            </td>
                                          </tr>";
                                    $point_23=  explode(",", $point23);
                                    $point_23a=$point_23[0];
                                    $point_23b=$point_23[1];
                                    $point_23c=$point_23[2];
                                    $point_23d=$point_23[3];
                                    echo "<tr class='rem'>
                                            <td>Point 23</td>
                                            <td>
                                                <input type='text' name='point23[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_23a'>
                                                <input type='text' name='point23[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_23b'>
                                                <input type='text' name='point23[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_23c'> 
                                                <input type='text' name='point23[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_23d'>
                                            </td>
                                          </tr>";
                                    $point_24=  explode(",", $point24);
                                    $point_24a=$point_24[0];
                                    $point_24b=$point_24[1];
                                    $point_24c=$point_24[2];
                                    $point_24d=$point_24[3];
                                    echo "<tr class='rem'>
                                            <td>Point 24</td>
                                            <td>
                                                <input type='text' name='point24[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_24a'>
                                                <input type='text' name='point24[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_24b'>
                                                <input type='text' name='point24[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_24c'> 
                                                <input type='text' name='point24[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_24d'>
                                            </td>
                                          </tr>";
                                    $point_25=  explode(",", $point25);
                                    $point_25a=$point_25[0];
                                    $point_25b=$point_25[1];
                                    $point_25c=$point_25[2];
                                    $point_25d=$point_25[3];
                                    echo "<tr class='rem'>
                                            <td>Point 25</td>
                                            <td>
                                                <input type='text' name='point25[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_25a'>
                                                <input type='text' name='point25[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_25b'>
                                                <input type='text' name='point25[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_25c'> 
                                                <input type='text' name='point25[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_25d'>
                                            </td>
                                          </tr>";
                                    $point_26=  explode(",", $point26);
                                    $point_26a=$point_26[0];
                                    $point_26b=$point_26[1];
                                    $point_26c=$point_26[2];
                                    $point_26d=$point_26[3];
                                    echo "<tr class='rem'>
                                            <td>Point 26</td>
                                            <td>
                                                <input type='text' name='point26[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_26a'>
                                                <input type='text' name='point26[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_26b'>
                                                <input type='text' name='point26[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_26c'> 
                                                <input type='text' name='point26[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_26d'>
                                            </td>
                                          </tr>";
                                    $point_27=  explode(",", $point27);
                                    $point_27a=$point_27[0];
                                    $point_27b=$point_27[1];
                                    $point_27c=$point_27[2];
                                    $point_27d=$point_27[3];
                                    echo "<tr class='rem'>
                                            <td>Point 27</td>
                                            <td>
                                                <input type='text' name='point27[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_27a'>
                                                <input type='text' name='point27[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_27b'>
                                                <input type='text' name='point27[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_27c'> 
                                                <input type='text' name='point27[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_27d'>
                                            </td>
                                          </tr>";
                                    $point_28=  explode(",", $point28);
                                    $point_28a=$point_28[0];
                                    $point_28b=$point_28[1];
                                    $point_28c=$point_28[2];
                                    $point_28d=$point_28[3];
                                    echo "<tr class='rem'>
                                            <td>Point 28</td>
                                            <td>
                                                <input type='text' name='point28[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_28a'>
                                                <input type='text' name='point28[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_28b'>
                                                <input type='text' name='point28[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_28c'> 
                                                <input type='text' name='point28[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_28d'>
                                            </td>
                                          </tr>";
                                    $point_29=  explode(",", $point29);
                                    $point_29a=$point_29[0];
                                    $point_29b=$point_29[1];
                                    $point_29c=$point_29[2];
                                    $point_29d=$point_29[3];
                                    echo "<tr class='rem'>
                                            <td>Point 29</td>
                                            <td>
                                                <input type='text' name='point29[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_29a'>
                                                <input type='text' name='point29[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_29b'>
                                                <input type='text' name='point29[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_29c'> 
                                                <input type='text' name='point29[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_29d'>
                                            </td>
                                          </tr>";
                                    $point_30=  explode(",", $point30);
                                    $point_30a=$point_30[0];
                                    $point_30b=$point_30[1];
                                    $point_30c=$point_30[2];
                                    $point_30d=$point_30[3];
                                    echo "<tr class='rem'>
                                            <td>Point 30</td>
                                            <td>
                                                <input type='text' name='point30[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_30a'>
                                                <input type='text' name='point30[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_30b'>
                                                <input type='text' name='point30[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_30c'> 
                                                <input type='text' name='point30[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_30d'>
                                            </td>
                                          </tr>";
                                    $point_31=  explode(",", $point31);
                                    $point_31a=$point_31[0];
                                    $point_31b=$point_31[1];
                                    $point_31c=$point_31[2];
                                    $point_31d=$point_31[3];
                                    echo "<tr class='rem'>
                                            <td>Point 31</td>
                                            <td>
                                                <input type='text' name='point31[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_31a'>
                                                <input type='text' name='point31[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_31b'>
                                                <input type='text' name='point31[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_31c'> 
                                                <input type='text' name='point31[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_31d'>
                                            </td>
                                          </tr>";
                                    $point_32=  explode(",", $point32);
                                    $point_32a=$point_32[0];
                                    $point_32b=$point_32[1];
                                    $point_32c=$point_32[2];
                                    $point_32d=$point_32[3];
                                    echo "<tr class='rem'>
                                            <td>Point 32</td>
                                            <td>
                                                <input type='text' name='point32[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_32a'>
                                                <input type='text' name='point32[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_32b'>
                                                <input type='text' name='point32[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_32c'> 
                                                <input type='text' name='point32[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_32d'>
                                            </td>
                                          </tr>";
                                    $point_33=  explode(",", $point33);
                                    $point_33a=$point_33[0];
                                    $point_33b=$point_33[1];
                                    $point_33c=$point_33[2];
                                    $point_33d=$point_33[3];
                                    echo "<tr class='rem'>
                                            <td>Point 33</td>
                                            <td>
                                                <input type='text' name='point33[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_33a'>
                                                <input type='text' name='point33[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_33b'>
                                                <input type='text' name='point33[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_33c'> 
                                                <input type='text' name='point33[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_33d'>
                                            </td>
                                          </tr>";
                                    $point_34=  explode(",", $point34);
                                    $point_34a=$point_34[0];
                                    $point_34b=$point_34[1];
                                    $point_34c=$point_34[2];
                                    $point_34d=$point_34[3];
                                    echo "<tr class='rem'>
                                            <td>Point 34</td>
                                            <td>
                                                <input type='text' name='point34[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_34a'>
                                                <input type='text' name='point34[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_34b'>
                                                <input type='text' name='point34[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_34c'> 
                                                <input type='text' name='point34[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_34d'>
                                            </td>
                                          </tr>";
                                    $point_35=  explode(",", $point35);
                                    $point_35a=$point_35[0];
                                    $point_35b=$point_35[1];
                                    $point_35c=$point_35[2];
                                    $point_35d=$point_35[3];
                                    echo "<tr class='rem'>
                                            <td>Point 35</td>
                                            <td>
                                                <input type='text' name='point35[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_35a'>
                                                <input type='text' name='point35[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_35b'>
                                                <input type='text' name='point35[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_35c'> 
                                                <input type='text' name='point35[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_35d'>
                                            </td>
                                          </tr>";
                                    $point_36=  explode(",", $point36);
                                    $point_36a=$point_36[0];
                                    $point_36b=$point_36[1];
                                    $point_36c=$point_36[2];
                                    $point_36d=$point_36[3];
                                    echo "<tr class='rem'>
                                            <td>Point 36</td>
                                            <td>
                                                <input type='text' name='point36[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_36a'>
                                                <input type='text' name='point36[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_36b'>
                                                <input type='text' name='point36[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_36c'> 
                                                <input type='text' name='point36[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_36d'>
                                            </td>
                                          </tr>";
                                    $point_37=  explode(",", $point37);
                                    $point_37a=$point_37[0];
                                    $point_37b=$point_37[1];
                                    $point_37c=$point_37[2];
                                    $point_37d=$point_37[3];
                                    echo "<tr class='rem'>
                                            <td>Point 37</td>
                                            <td>
                                                <input type='text' name='point37[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_37a'>
                                                <input type='text' name='point37[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_37b'>
                                                <input type='text' name='point37[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_37c'> 
                                                <input type='text' name='point37[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_37d'>
                                            </td>
                                          </tr>";
                                    $point_38=  explode(",", $point38);
                                    $point_38a=$point_38[0];
                                    $point_38b=$point_38[1];
                                    $point_38c=$point_38[2];
                                    $point_38d=$point_38[3];
                                    echo "<tr class='rem'>
                                            <td>Point 38</td>
                                            <td>
                                                <input type='text' name='point38[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_38a'>
                                                <input type='text' name='point38[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_38b'>
                                                <input type='text' name='point38[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_38c'> 
                                                <input type='text' name='point38[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_38d'>
                                            </td>
                                          </tr>";
                                    $point_39=  explode(",", $point39);
                                    $point_39a=$point_39[0];
                                    $point_39b=$point_39[1];
                                    $point_39c=$point_39[2];
                                    $point_39d=$point_39[3];
                                    echo "<tr class='rem'>
                                            <td>Point 39</td>
                                            <td>
                                                <input type='text' name='point39[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_39a'>
                                                <input type='text' name='point39[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_39b'>
                                                <input type='text' name='point39[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_39c'> 
                                                <input type='text' name='point39[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_39d'>
                                            </td>
                                          </tr>";
                                    $point_40=  explode(",", $point40);
                                    $point_40a=$point_40[0];
                                    $point_40b=$point_40[1];
                                    $point_40c=$point_40[2];
                                    $point_40d=$point_40[3];
                                    echo "<tr class='rem'>
                                            <td>Point 20</td>
                                            <td>
                                                <input type='text' name='point40[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_40a'>
                                                <input type='text' name='point40[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_40b'>
                                                <input type='text' name='point40[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_40c'> 
                                                <input type='text' name='point40[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_40d'>
                                            </td>
                                          </tr>";
                                    $point_41=  explode(",", $point41);
                                    $point_41a=$point_41[0];
                                    $point_41b=$point_41[1];
                                    $point_41c=$point_41[2];
                                    $point_41d=$point_41[3];
                                    echo "<tr class='rem'>
                                            <td>Point 41</td>
                                            <td>
                                                <input type='text' name='point41[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_41a'>
                                                <input type='text' name='point41[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_41b'>
                                                <input type='text' name='point41[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_41c'> 
                                                <input type='text' name='point41[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_41d'>
                                            </td>
                                          </tr>";
                                    $point_42=  explode(",", $point42);
                                    $point_42a=$point_42[0];
                                    $point_42b=$point_42[1];
                                    $point_42c=$point_42[2];
                                    $point_42d=$point_42[3];
                                    echo "<tr class='rem'>
                                            <td>Point 42</td>
                                            <td>
                                                <input type='text' name='point42[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_42a'>
                                                <input type='text' name='point42[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_42b'>
                                                <input type='text' name='point42[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_42c'> 
                                                <input type='text' name='point42[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_42d'>
                                            </td>
                                          </tr>";
                                    $point_43=  explode(",", $point43);
                                    $point_43a=$point_43[0];
                                    $point_43b=$point_43[1];
                                    $point_43c=$point_43[2];
                                    $point_43d=$point_43[3];
                                    echo "<tr class='rem'>
                                            <td>Point 43</td>
                                            <td>
                                                <input type='text' name='point43[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_43a'>
                                                <input type='text' name='point43[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_43b'>
                                                <input type='text' name='point43[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_43c'> 
                                                <input type='text' name='point43[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_43d'>
                                            </td>
                                          </tr>";
                                    $point_44=  explode(",", $point44);
                                    $point_44a=$point_44[0];
                                    $point_44b=$point_44[1];
                                    $point_44c=$point_44[2];
                                    $point_44d=$point_44[3];
                                    echo "<tr class='rem'>
                                            <td>Point 44</td>
                                            <td>
                                                <input type='text' name='point44[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_44a'>
                                                <input type='text' name='point44[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_44b'>
                                                <input type='text' name='point44[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_44c'> 
                                                <input type='text' name='point44[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_44d'>
                                            </td>
                                          </tr>";
                                    $point_45=  explode(",", $point45);
                                    $point_45a=$point_45[0];
                                    $point_45b=$point_45[1];
                                    $point_45c=$point_45[2];
                                    $point_45d=$point_45[3];
                                    echo "<tr class='rem'>
                                            <td>Point 45</td>
                                            <td>
                                                <input type='text' name='point45[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_45a'>
                                                <input type='text' name='point45[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_45b'>
                                                <input type='text' name='point45[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_45c'> 
                                                <input type='text' name='point45[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_45d'>
                                            </td>
                                          </tr>";
                                    $point_46=  explode(",", $point46);
                                    $point_46a=$point_46[0];
                                    $point_46b=$point_46[1];
                                    $point_46c=$point_46[2];
                                    $point_46d=$point_46[3];
                                    echo "<tr class='rem'>
                                            <td>Point 46</td>
                                            <td>
                                                <input type='text' name='point46[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_46a'>
                                                <input type='text' name='point46[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_46b'>
                                                <input type='text' name='point46[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_46c'> 
                                                <input type='text' name='point46[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_46d'>
                                            </td>
                                          </tr>";
                                    $point_47=  explode(",", $point47);
                                    $point_47a=$point_47[0];
                                    $point_47b=$point_47[1];
                                    $point_47c=$point_47[2];
                                    $point_47d=$point_47[3];
                                    echo "<tr class='rem'>
                                            <td>Point 47</td>
                                            <td>
                                                <input type='text' name='point47[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_47a'>
                                                <input type='text' name='point47[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_47b'>
                                                <input type='text' name='point47[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_47c'> 
                                                <input type='text' name='point47[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_47d'>
                                            </td>
                                          </tr>";
                                    $point_48=  explode(",", $point48);
                                    $point_48a=$point_48[0];
                                    $point_48b=$point_48[1];
                                    $point_48c=$point_48[2];
                                    $point_48d=$point_48[3];
                                    echo "<tr class='rem'>
                                            <td>Point 48</td>
                                            <td>
                                                <input type='text' name='point48[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_48a'>
                                                <input type='text' name='point48[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_48b'>
                                                <input type='text' name='point48[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_48c'> 
                                                <input type='text' name='point48[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_48d'>
                                            </td>
                                          </tr>";
                                    $point_49=  explode(",", $point49);
                                    $point_49a=$point_49[0];
                                    $point_49b=$point_49[1];
                                    $point_49c=$point_49[2];
                                    $point_49d=$point_49[3];
                                    echo "<tr class='rem'>
                                            <td>Point 49</td>
                                            <td>
                                                <input type='text' name='point49[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_49a'>
                                                <input type='text' name='point49[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_49b'>
                                                <input type='text' name='point49[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_49c'> 
                                                <input type='text' name='point49[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_49d'>
                                            </td>
                                          </tr>";
                                    $point_50=  explode(",", $point50);
                                    $point_50a=$point_50[0];
                                    $point_50b=$point_50[1];
                                    $point_50c=$point_50[2];
                                    $point_50d=$point_50[3];
                                    echo "<tr class='rem'>
                                            <td>Point 50</td>
                                            <td>
                                                <input type='text' name='point50[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_50a'>
                                                <input type='text' name='point50[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_50b'>
                                                <input type='text' name='point50[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_50c'> 
                                                <input type='text' name='point50[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_50d'>
                                            </td>
                                          </tr>";
                                    $point_51=  explode(",", $point51);
                                    $point_51a=$point_51[0];
                                    $point_51b=$point_51[1];
                                    $point_51c=$point_51[2];
                                    $point_51d=$point_51[3];
                                    echo "<tr class='rem'>
                                            <td>Point 51</td>
                                            <td>
                                                <input type='text' name='point51[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_51a'>
                                                <input type='text' name='point51[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_51b'>
                                                <input type='text' name='point51[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_51c'> 
                                                <input type='text' name='point51[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_51d'>
                                            </td>
                                          </tr>";
                                    $point_52=  explode(",", $point52);
                                    $point_52a=$point_52[0];
                                    $point_52b=$point_52[1];
                                    $point_52c=$point_52[2];
                                    $point_52d=$point_52[3];
                                    echo "<tr class='rem'>
                                            <td>Point 52</td>
                                            <td>
                                                <input type='text' name='point52[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_52a'>
                                                <input type='text' name='point52[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_52b'>
                                                <input type='text' name='point52[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_52c'> 
                                                <input type='text' name='point52[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_52d'>
                                            </td>
                                          </tr>";
                                    $point_53=  explode(",", $point53);
                                    $point_53a=$point_53[0];
                                    $point_53b=$point_53[1];
                                    $point_53c=$point_53[2];
                                    $point_53d=$point_53[3];
                                    echo "<tr class='rem'>
                                            <td>Point 53</td>
                                            <td>
                                                <input type='text' name='point53[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_53a'>
                                                <input type='text' name='point53[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_53b'>
                                                <input type='text' name='point53[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_53c'> 
                                                <input type='text' name='point53[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_53d'>
                                            </td>
                                          </tr>";
                                    $point_54=  explode(",", $point54);
                                    $point_54a=$point_54[0];
                                    $point_54b=$point_54[1];
                                    $point_54c=$point_54[2];
                                    $point_54d=$point_54[3];
                                    echo "<tr class='rem'>
                                            <td>Point 54</td>
                                            <td>
                                                <input type='text' name='point54[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_54a'>
                                                <input type='text' name='point54[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_54b'>
                                                <input type='text' name='point54[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_54c'> 
                                                <input type='text' name='point54[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_54d'>
                                            </td>
                                          </tr>";
                                    $point_55=  explode(",", $point55);
                                    $point_55a=$point_55[0];
                                    $point_55b=$point_55[1];
                                    $point_55c=$point_55[2];
                                    $point_55d=$point_55[3];
                                    echo "<tr class='rem'>
                                            <td>Point 55</td>
                                            <td>
                                                <input type='text' name='point55[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_55a'>
                                                <input type='text' name='point55[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_55b'>
                                                <input type='text' name='point55[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_55c'> 
                                                <input type='text' name='point55[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_55d'>
                                            </td>
                                          </tr>";
                                    $point_56=  explode(",", $point56);
                                    $point_56a=$point_56[0];
                                    $point_56b=$point_56[1];
                                    $point_56c=$point_56[2];
                                    $point_56d=$point_56[3];
                                    echo "<tr class='rem'>
                                            <td>Point 46</td>
                                            <td>
                                                <input type='text' name='point56[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_56a'>
                                                <input type='text' name='point56[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_56b'>
                                                <input type='text' name='point56[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_56c'> 
                                                <input type='text' name='point56[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_56d'>
                                            </td>
                                          </tr>";
                                    $point_57=  explode(",", $point57);
                                    $point_57a=$point_57[0];
                                    $point_57b=$point_57[1];
                                    $point_57c=$point_57[2];
                                    $point_57d=$point_57[3];
                                    echo "<tr class='rem'>
                                            <td>Point 57</td>
                                            <td>
                                                <input type='text' name='point57[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_57a'>
                                                <input type='text' name='point57[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_57b'>
                                                <input type='text' name='point57[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_57c'> 
                                                <input type='text' name='point57[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_57d'>
                                            </td>
                                          </tr>";
                                    $point_58=  explode(",", $point58);
                                    $point_58a=$point_58[0];
                                    $point_58b=$point_58[1];
                                    $point_58c=$point_58[2];
                                    $point_58d=$point_58[3];
                                    echo "<tr class='rem'>
                                            <td>Point 58</td>
                                            <td>
                                                <input type='text' name='point58[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_58a'>
                                                <input type='text' name='point58[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_58b'>
                                                <input type='text' name='point58[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_58c'> 
                                                <input type='text' name='point58[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_58d'>
                                            </td>
                                          </tr>";
                                    $point_59=  explode(",", $point59);
                                    $point_59a=$point_59[0];
                                    $point_59b=$point_59[1];
                                    $point_59c=$point_59[2];
                                    $point_59d=$point_59[3];
                                    echo "<tr class='rem'>
                                            <td>Point 59</td>
                                            <td>
                                                <input type='text' name='point59[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_59a'>
                                                <input type='text' name='point59[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_59b'>
                                                <input type='text' name='point59[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_59c'> 
                                                <input type='text' name='point59[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_59d'>
                                            </td>
                                          </tr>";
                                    $point_60=  explode(",", $point60);
                                    $point_60a=$point_60[0];
                                    $point_60b=$point_60[1];
                                    $point_60c=$point_60[2];
                                    $point_60d=$point_60[3];
                                    echo "<tr class='rem'>
                                            <td>Point 50</td>
                                            <td>
                                                <input type='text' name='point60[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_60a'>
                                                <input type='text' name='point60[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_60b'>
                                                <input type='text' name='point60[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_60c'> 
                                                <input type='text' name='point60[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_60d'>
                                            </td>
                                          </tr>";
                                    $point_61=  explode(",", $point61);
                                    $point_61a=$point_61[0];
                                    $point_61b=$point_61[1];
                                    $point_61c=$point_61[2];
                                    $point_61d=$point_61[3];
                                    echo "<tr class='rem'>
                                            <td>Point 61</td>
                                            <td>
                                                <input type='text' name='point61[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_61a'>
                                                <input type='text' name='point61[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_61b'>
                                                <input type='text' name='point61[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_61c'> 
                                                <input type='text' name='point61[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_61d'>
                                            </td>
                                          </tr>";
                                    $point_62=  explode(",", $point62);
                                    $point_62a=$point_62[0];
                                    $point_62b=$point_62[1];
                                    $point_62c=$point_62[2];
                                    $point_62d=$point_62[3];
                                    echo "<tr class='rem'>
                                            <td>Point 62</td>
                                            <td>
                                                <input type='text' name='point62[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_62a'>
                                                <input type='text' name='point62[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_62b'>
                                                <input type='text' name='point62[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_62c'> 
                                                <input type='text' name='point62[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_62d'>
                                            </td>
                                          </tr>";
                                    $point_63=  explode(",", $point63);
                                    $point_63a=$point_63[0];
                                    $point_63b=$point_63[1];
                                    $point_63c=$point_63[2];
                                    $point_63d=$point_63[3];
                                    echo "<tr class='rem'>
                                            <td>Point 63</td>
                                            <td>
                                                <input type='text' name='point63[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_63a'>
                                                <input type='text' name='point63[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_63b'>
                                                <input type='text' name='point63[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_63c'> 
                                                <input type='text' name='point63[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_63d'>
                                            </td>
                                          </tr>";
                                    $point_64=  explode(",", $point64);
                                    $point_64a=$point_64[0];
                                    $point_64b=$point_64[1];
                                    $point_64c=$point_64[2];
                                    $point_64d=$point_64[3];
                                    echo "<tr class='rem'>
                                            <td>Point 64</td>
                                            <td>
                                                <input type='text' name='point64[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_64a'>
                                                <input type='text' name='point64[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_64b'>
                                                <input type='text' name='point64[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_64c'> 
                                                <input type='text' name='point64[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_64d'>
                                            </td>
                                          </tr>";
                                    $point_65=  explode(",", $point65);
                                    $point_65a=$point_65[0];
                                    $point_65b=$point_65[1];
                                    $point_65c=$point_65[2];
                                    $point_65d=$point_65[3];
                                    echo "<tr class='rem'>
                                            <td>Point 65</td>
                                            <td>
                                                <input type='text' name='point65[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_65a'>
                                                <input type='text' name='point65[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_65b'>
                                                <input type='text' name='point65[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_65c'> 
                                                <input type='text' name='point65[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_65d'>
                                            </td>
                                          </tr>";
                                    $point_66=  explode(",", $point66);
                                    $point_66a=$point_66[0];
                                    $point_66b=$point_66[1];
                                    $point_66c=$point_66[2];
                                    $point_66d=$point_66[3];
                                    echo "<tr class='rem'>
                                            <td>Point 66</td>
                                            <td>
                                                <input type='text' name='point66[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_66a'>
                                                <input type='text' name='point66[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_66b'>
                                                <input type='text' name='point66[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_66c'> 
                                                <input type='text' name='point66[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_66d'>
                                            </td>
                                          </tr>";
                                    $point_67=  explode(",", $point67);
                                    $point_67a=$point_67[0];
                                    $point_67b=$point_67[1];
                                    $point_67c=$point_67[2];
                                    $point_67d=$point_67[3];
                                    echo "<tr class='rem'>
                                            <td>Point 67</td>
                                            <td>
                                                <input type='text' name='point67[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_67a'>
                                                <input type='text' name='point67[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_67b'>
                                                <input type='text' name='point67[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_67c'> 
                                                <input type='text' name='point67[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_67d'>
                                            </td>
                                          </tr>";
                                    $point_68=  explode(",", $point68);
                                    $point_68a=$point_68[0];
                                    $point_68b=$point_68[1];
                                    $point_68c=$point_68[2];
                                    $point_68d=$point_68[3];
                                    echo "<tr class='rem'>
                                            <td>Point 68</td>
                                            <td>
                                                <input type='text' name='point68[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_68a'>
                                                <input type='text' name='point68[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_68b'>
                                                <input type='text' name='point68[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_68c'> 
                                                <input type='text' name='point68[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_68d'>
                                            </td>
                                          </tr>";
                                    $point_69=  explode(",", $point69);
                                    $point_69a=$point_69[0];
                                    $point_69b=$point_69[1];
                                    $point_69c=$point_69[2];
                                    $point_69d=$point_69[3];
                                    echo "<tr class='rem'>
                                            <td>Point 69</td>
                                            <td>
                                                <input type='text' name='point69[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_69a'>
                                                <input type='text' name='point69[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_69b'>
                                                <input type='text' name='point69[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_69c'> 
                                                <input type='text' name='point69[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_69d'>
                                            </td>
                                          </tr>";
                                    $point_70=  explode(",", $point70);
                                    $point_70a=$point_70[0];
                                    $point_70b=$point_70[1];
                                    $point_70c=$point_70[2];
                                    $point_70d=$point_70[3];
                                    echo "<tr class='rem'>
                                            <td>Point 70</td>
                                            <td>
                                                <input type='text' name='point70[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_70a'>
                                                <input type='text' name='point70[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_70b'>
                                                <input type='text' name='point70[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_70c'> 
                                                <input type='text' name='point70[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_70d'>
                                            </td>
                                          </tr>";
                                    $point_71=  explode(",", $point71);
                                    $point_71a=$point_71[0];
                                    $point_71b=$point_71[1];
                                    $point_71c=$point_71[2];
                                    $point_71d=$point_71[3];
                                    echo "<tr class='rem'>
                                            <td>Point 71</td>
                                            <td>
                                                <input type='text' name='point71[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_71a'>
                                                <input type='text' name='point71[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_71b'>
                                                <input type='text' name='point71[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_71c'> 
                                                <input type='text' name='point71[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_71d'>
                                            </td>
                                          </tr>";
                                    $point_72=  explode(",", $point72);
                                    $point_72a=$point_72[0];
                                    $point_72b=$point_72[1];
                                    $point_72c=$point_72[2];
                                    $point_72d=$point_72[3];
                                    echo "<tr class='rem'>
                                            <td>Point 72</td>
                                            <td>
                                                <input type='text' name='point72[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_72a'>
                                                <input type='text' name='point72[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_72b'>
                                                <input type='text' name='point72[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_72c'> 
                                                <input type='text' name='point72[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_72d'>
                                            </td>
                                          </tr>";
                                    $point_73=  explode(",", $point73);
                                    $point_73a=$point_73[0];
                                    $point_73b=$point_73[1];
                                    $point_73c=$point_73[2];
                                    $point_73d=$point_73[3];
                                    echo "<tr class='rem'>
                                            <td>Point 73</td>
                                            <td>
                                                <input type='text' name='point73[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_73a'>
                                                <input type='text' name='point73[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_73b'>
                                                <input type='text' name='point73[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_73c'> 
                                                <input type='text' name='point73[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_73d'>
                                            </td>
                                          </tr>";
                                    $point_74=  explode(",", $point74);
                                    $point_74a=$point_74[0];
                                    $point_74b=$point_74[1];
                                    $point_74c=$point_74[2];
                                    $point_74d=$point_74[3];
                                    echo "<tr class='rem'>
                                            <td>Point 74</td>
                                            <td>
                                                <input type='text' name='point74[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_74a'>
                                                <input type='text' name='point74[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_74b'>
                                                <input type='text' name='point74[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_74c'> 
                                                <input type='text' name='point74[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_74d'>
                                            </td>
                                          </tr>";
                                    $point_75=  explode(",", $point75);
                                    $point_75a=$point_75[0];
                                    $point_75b=$point_75[1];
                                    $point_75c=$point_75[2];
                                    $point_75d=$point_75[3];
                                    echo "<tr class='rem'>
                                            <td>Point 75</td>
                                            <td>
                                                <input type='text' name='point75[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_75a'>
                                                <input type='text' name='point75[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_75b'>
                                                <input type='text' name='point75[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_75c'> 
                                                <input type='text' name='point75[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_75d'>
                                            </td>
                                          </tr>";
                                    $point_76=  explode(",", $point76);
                                    $point_76a=$point_76[0];
                                    $point_76b=$point_76[1];
                                    $point_76c=$point_76[2];
                                    $point_76d=$point_76[3];
                                    echo "<tr class='rem'>
                                            <td>Point 76</td>
                                            <td>
                                                <input type='text' name='point76[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_76a'>
                                                <input type='text' name='point76[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_76b'>
                                                <input type='text' name='point76[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_76c'> 
                                                <input type='text' name='point76[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_76d'>
                                            </td>
                                          </tr>";
                                    $point_77=  explode(",", $point77);
                                    $point_77a=$point_77[0];
                                    $point_77b=$point_77[1];
                                    $point_77c=$point_77[2];
                                    $point_77d=$point_77[3];
                                    echo "<tr class='rem'>
                                            <td>Point 77</td>
                                            <td>
                                                <input type='text' name='point77[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_77a'>
                                                <input type='text' name='point77[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_77b'>
                                                <input type='text' name='point77[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_77c'> 
                                                <input type='text' name='point77[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_77d'>
                                            </td>
                                          </tr>";
                                    $point_78=  explode(",", $point78);
                                    $point_78a=$point_78[0];
                                    $point_78b=$point_78[1];
                                    $point_78c=$point_78[2];
                                    $point_78d=$point_78[3];
                                    echo "<tr class='rem'>
                                            <td>Point 78</td>
                                            <td>
                                                <input type='text' name='point78[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_78a'>
                                                <input type='text' name='point78[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_78b'>
                                                <input type='text' name='point78[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_78c'> 
                                                <input type='text' name='point78[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_78d'>
                                            </td>
                                          </tr>";
                                    $point_79=  explode(",", $point79);
                                    $point_79a=$point_79[0];
                                    $point_79b=$point_79[1];
                                    $point_79c=$point_79[2];
                                    $point_79d=$point_79[3];
                                    echo "<tr class='rem'>
                                            <td>Point 79</td>
                                            <td>
                                                <input type='text' name='point79[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_79a'>
                                                <input type='text' name='point79[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_79b'>
                                                <input type='text' name='point79[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_79c'> 
                                                <input type='text' name='point79[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_79d'>
                                            </td>
                                          </tr>";
                                    $point_80=  explode(",", $point80);
                                    $point_80a=$point_80[0];
                                    $point_80b=$point_80[1];
                                    $point_80c=$point_80[2];
                                    $point_80d=$point_80[3];
                                    echo "<tr class='rem'>
                                            <td>Point 80</td>
                                            <td>
                                                <input type='text' name='point80[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_80a'>
                                                <input type='text' name='point80[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_80b'>
                                                <input type='text' name='point80[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_80c'> 
                                                <input type='text' name='point80[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_80d'>
                                            </td>
                                          </tr>";
                                }
                                ?>
                                <tr>
                                    <td>Test Object</td>
                                    <td>
                                        <input type="text" name="test_object" class="span6" value="<?=$list->test_object;?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Model</td>
                                    <td>
                                        <input type="text" name="model" class="span6" value="<?=$list->model;?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Couplant</td>
                                    <td>
                                        <select name="couplant" class="span6" required>
                                            <?php
                                                if($list->couplant=="Grease"){
                                                    $gr="selected";
                                                }else{
                                                    $gr="";
                                                }
                                                if($list->couplant=="Gliserin"){
                                                    $gl="selected";
                                                }else{
                                                    $gl="";
                                                }
                                                if($list->couplant=="Other"){
                                                    $oot="selected";
                                                }else{
                                                    $oot="";
                                                }
                                            ?>
                                            <option value="">-select couplant-</option>
                                            <option value="Grease" <?=$gr;?>>Grease</option>
                                            <option value="Gliserin" <?=$gl;?>>Gliserin</option>
                                            <option value="Other" <?=$oot;?>>Other</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Frequency</td>
                                    <td>
                                        <input type="text" name="frequency" class="span6" value="<?=$list->frequency;?>"> Mhz
                                    </td>
                                </tr>
                                <tr>
                                    <td>Thickness</td>
                                    <td>
                                        <input type="text" name="thickness" class="span6" value="<?=$list->thickness;?>"> mm
                                    </td>
                                </tr>
                                <tr>
                                    <td>Probe Type</td>
                                    <td>
                                        <select name="probe_type" class="span6" required>
                                            <?php
                                                if($list->probe_type=="Double"){
                                                    $db="selected";
                                                }else{
                                                    $db="";
                                                }
                                                if($list->probe_type=="Single"){
                                                    $sl="selected";
                                                }else{
                                                    $sl="";
                                                }
                                            ?>
                                            <option value="">-select probe-</option>
                                            <option value="Double" <?=$db;?>>Double</option>
                                            <option value="Single" <?=$sl;?>>SIngle</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Equipment</td>
                                    <td>
                                        <input type="text" name="equipment" class="span6" value="<?=$list->equipment;?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Upload Image</td>
                                    <td>
                                        <input type="file" class="btn" name="upload_file" onchange="readURL1(this);"/>
                                        <img src="#" id="blah1"><input type="hidden" name="upload_file_hidden" value="<?=$list->upload_file;?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td>
                                        <textarea name="remarks" cols="60" rows="5" placeholder="Remarks" class="txtarea"><?=$list->remarks;?></textarea>
                                    </td>
                                </tr>
				<tr>
                                    <td>Recomendation</td>
                                    <td><textarea name="recomendation"  cols="60" rows="5" placeholder="Recomendation" class="txtarea"><?=$list->recomendation;?></textarea></td>
                                </tr>
                                <tr>
                                        <td>Severity_level</td>
                                        <td>
                                            <select class="span6" name="severity_level">
                                               <?php
                                                if($list->severity_level=="0"){
                                                    $normal="selected";
                                                }else{
                                                    $normal="";
                                                }
                                                if($list->severity_level=="1"){
                                                    $warning="selected";
                                                }else{
                                                    $warning="";
                                                }
                                                if($list->severity_level=="2"){
                                                    $danger="selected";
                                                }else{
                                                    $danger="";
                                                }
                                                ?>
                                                <option value="0" <?=$normal;?>>Normal</option>
                                                <option value="1" <?=$warning;?>>Warning</option>
                                                <option value="2" <?=$danger;?>>Danger</option>
                                            </select>

                                        </td>
                                </tr>
                                <input type="hidden" name="user" value="<?php echo $this->session->userdata('users_id');?>">
			</table>
		<button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                        </div>
            </div>
        </div>
    </div>
</div>
</div>
</form>
<?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
</script>		
<script type="text/javascript">
$(document).ready(function(){
//for(c=1;c<=80;c++){
//    x=1;
//    var idc=$("#idc").val();
//    $.ajax({
//         type:'post',
//         url:"<?=base_url();?>record/add_thickness/get_point",
//         data: "id="+idc+"&point="+c,
//         success: function(dt){
//                hasilx=dt.split(",");
//                console.log(hasilx);
//                $("#a"+x).val(hasilx[0]);
//                $("#b"+x).val(hasilx[1]);
//                $("#c"+x).val(hasilx[2]);
//                $("#d"+x).val(hasilx[3]);
//                x++;
//         }
//     });   
//}    
//if($("#type").val()=="kiln"){
//        $(".rem").remove();
//        var att_php = "<?php for ($i=1;$i<=80;$i++){
                             echo "<tr class='rem'><td>Point $i</td><td><input type='text' name='point".$i."[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' id='a".$i."'> <input type='text' name='point".$i."[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' id='b".$i."'> <input type='text' name='point".$i."[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' id='c".$i."'> <input type='text' name='point".$i."[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' id='d".$i."'></td></tr>";
                             }
                        ?>//";
//         $("#add").append(att_php);
//      }
//      if($("#type").val()=="stack"){
//        $(".rem").remove();
//        var att_php = "<?php for ($i=1;$i<=16;$i++){
                             echo "<tr class='rem'><td>Point $i</td><td><input type='text' name='point".$i."[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' id='a".$i."'> <input type='text' name='point".$i."[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)'  id='b".$i."'> <input type='text' name='point".$i."[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' id='c".$i."'> <input type='text' name='point".$i."[]' class='span2'  placeholder='270&#176;' onkeypress='return validate(event)' id='d".$i."'></td></tr>";
                             }
                        ?>//";
//         $("#add").append(att_php);
//      }
//      if($("#type").val()=="general"){
//        $(".rem").remove();
//        var att_php = "<?php for ($i=1;$i<=16;$i++){
                             echo "<tr class='rem'><td>Point $i</td><td><input type='text' name='point".$i."[]' class='span2'  placeholder='0&#176;' onkeypress='return validate(event)' id='a".$i."'> <input type='text' name='point".$i."[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' id='b".$i."'> <input type='text' name='point".$i."[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' id='c".$i."'> <input type='text' name='point".$i."[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' id='d".$i."'></td></tr>";
                             }
                        ?>//";
//         $("#add").append(att_php);
//      }
//      
var plant_id=$("#plant_id").val();
        var main_area_id=$("#main_area_id").val();
        var sub_area_id=$("#sub_area_id").val();
        
        //post data main area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area_edit",
             data: "plant_id="+plant_id+"&main_area_id="+main_area_id,
             success: function(data){
                 $("#area").html(data);
             }
         });
         
        //post data sub area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea_edit",
             data: "main_area_id="+main_area_id+"&sub_area_id="+sub_area_id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
         
         //post data mainareaname
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_areaname",
             data: "id="+sub_area_id,
             success: function(data){
                 $("#areaname").val(data);
             }
         });
         
         //post data hac code
         $.ajax({
            type:"POST",
            url:"<?=base_url();?>engine/crud_hac/get_data",
            data:"id="+main_area_id+"&sub="+sub_area_id,
            success: function(dt){
                hasil=dt.split("|");
                console.log(hasil);
                //$("#plant").val(hasil[3].split("|"));
                $("#n_area").text("TQ."+hasil[7].split("|"));
                $("#n_plant").html(hasil[5].split("|"));
                $("#n_area_h").val("TQ."+hasil[7].split("|"));
                $("#n_plant_h").val(hasil[5].split("|"));
            },
            error: function(dt){
                alert("gagal");
            }
        });
$("#plant").change(function(){
     var id = $("#plant").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_area",
         data: "id="+id,
         success: function(data){
             $("#area").html(data);
         }
     });
  });

  $("#area").change(function(){
     var id = $("#area").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_subarea",
         data: "id="+id,
         success: function(data){
             $("#subarea").html(data);
         }
     });
   });
   $("#subarea").change(function(){
    var id = $(this).val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_areaname",
         data: "id="+id,
         success: function(data){
             console.log(data);
             $("#areaname").val(data);
         }
     });
           $("#txtinput").val('');
           //$("#listingx").remove();
      });
   $("#type").change(function(){
      if($(this).val()=="kiln"){
        $(".rem").remove();
        var att_php = "<?php for ($i=1;$i<=80;$i++){
                             echo "<tr class='rem'><td>Point $i</td><td><input type='text' name='point".$i."[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)'> <input type='text' name='point".$i."[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)'> <input type='text' name='point".$i."[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)'> <input type='text' name='point".$i."[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)'></td></tr>";
                             }
                        ?>";
         $("#add").append(att_php);
      }
      if($(this).val()=="stack"){
        $(".rem").remove();
        var att_php = "<?php 
                                    $point_1=  explode(",", $point1);
                                    $point_1a=$point_1[0];
                                    $point_1b=$point_1[1];
                                    $point_1c=$point_1[2];
                                    $point_1d=$point_1[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 1</td>\
                                            <td>\
                                                <input type='text' name='point1[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_1a'>\
                                                <input type='text' name='point1[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_1b'>\
                                                <input type='text' name='point1[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_1c'>\
                                                <input type='text' name='point1[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_1d'>\
                                            </td>\
                                          </tr>";
                                    $point_2=  explode(",", $point2);
                                    $point_2a=$point_2[0];
                                    $point_2b=$point_2[1];
                                    $point_2c=$point_2[2];
                                    $point_2d=$point_2[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 2</td>\
                                            <td>\
                                                <input type='text' name='point2[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_2a'>\
                                                <input type='text' name='point2[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_2b'>\
                                                <input type='text' name='point2[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_2c'>\
                                                <input type='text' name='point2[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_2d'>\
                                            </td>\
                                          </tr>";
                                    $point_3=  explode(",", $point3);
                                    $point_3a=$point_3[0];
                                    $point_3b=$point_3[1];
                                    $point_3c=$point_3[2];
                                    $point_3d=$point_3[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 3</td>\
                                            <td>\
                                                <input type='text' name='point3[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_3a'>\
                                                <input type='text' name='point3[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_3b'>\
                                                <input type='text' name='point3[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_3c'>\
                                                <input type='text' name='point3[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_3d'>\
                                            </td>\
                                          </tr>";
                                    $point_4=  explode(",", $point4);
                                    $point_4a=$point_4[0];
                                    $point_4b=$point_4[1];
                                    $point_4c=$point_4[2];
                                    $point_4d=$point_4[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 4</td>\
                                            <td>\
                                                <input type='text' name='point4[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_4a'>\
                                                <input type='text' name='point4[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_4b'>\
                                                <input type='text' name='point4[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_4c'>\
                                                <input type='text' name='point4[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_4d'>\
                                            </td>\
                                          </tr>";
                                    $point_5=  explode(",", $point5);
                                    $point_5a=$point_5[0];
                                    $point_5b=$point_5[1];
                                    $point_5c=$point_5[2];
                                    $point_5d=$point_5[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 5</td>\
                                            <td>\
                                                <input type='text' name='point5[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_5a'>\
                                                <input type='text' name='point5[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_5b'>\
                                                <input type='text' name='point5[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_5c'>\
                                                <input type='text' name='point5[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_5d'>\
                                            </td>\
                                          </tr>";
                                    $point_6=  explode(",", $point6);
                                    $point_6a=$point_6[0];
                                    $point_6b=$point_6[1];
                                    $point_6c=$point_6[2];
                                    $point_6d=$point_6[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 6</td>\
                                            <td>\
                                                <input type='text' name='point6[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_6a'>\
                                                <input type='text' name='point6[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_6b'>\
                                                <input type='text' name='point6[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_6c'>\
                                                <input type='text' name='point6[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_6d'>\
                                            </td>\
                                          </tr>";
                                    $point_7=  explode(",", $point7);
                                    $point_7a=$point_7[0];
                                    $point_7b=$point_7[1];
                                    $point_7c=$point_7[2];
                                    $point_7d=$point_7[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 7</td>\
                                            <td>\
                                                <input type='text' name='point6[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_7a'>\
                                                <input type='text' name='point7[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_7b'>\
                                                <input type='text' name='point7[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_7c'>\
                                                <input type='text' name='point7[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_7d'>\
                                            </td>\
                                          </tr>";
                                    $point_8=  explode(",", $point8);
                                    $point_8a=$point_8[0];
                                    $point_8b=$point_8[1];
                                    $point_8c=$point_8[2];
                                    $point_8d=$point_8[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 8</td>\
                                            <td>\
                                                <input type='text' name='point8[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_8a'>\
                                                <input type='text' name='point8[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_8b'>\
                                                <input type='text' name='point8[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_8c'>\
                                                <input type='text' name='point8[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_8d'>\
                                            </td>\
                                          </tr>";
                                    $point_9=  explode(",", $point9);
                                    $point_9a=$point_9[0];
                                    $point_9b=$point_9[1];
                                    $point_9c=$point_9[2];
                                    $point_9d=$point_9[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 9</td>\
                                            <td>\
                                                <input type='text' name='point9[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_9a'>\
                                                <input type='text' name='point9[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_9b'>\
                                                <input type='text' name='point9[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_9c'>\
                                                <input type='text' name='point9[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_9d'>\
                                            </td>\
                                          </tr>";
                                    $point_10=  explode(",", $point10);
                                    $point_10a=$point_10[0];
                                    $point_10b=$point_10[1];
                                    $point_10c=$point_10[2];
                                    $point_10d=$point_10[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 10</td>\
                                            <td>\
                                                <input type='text' name='point10[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_10a'>\
                                                <input type='text' name='point10[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_10b'>\
                                                <input type='text' name='point10[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_10c'>\
                                                <input type='text' name='point10[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_10d'>\
                                            </td>\
                                          </tr>";
                                    $point_11=  explode(",", $point11);
                                    $point_11a=$point_11[0];
                                    $point_11b=$point_11[1];
                                    $point_11c=$point_11[2];
                                    $point_11d=$point_11[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 11</td>\
                                            <td>\
                                                <input type='text' name='point11[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_11a'>\
                                                <input type='text' name='point11[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_11b'>\
                                                <input type='text' name='point11[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_11c'>\
                                                <input type='text' name='point11[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_11d'>\
                                            </td>\
                                          </tr>";
                                    $point_12=  explode(",", $point12);
                                    $point_12a=$point_12[0];
                                    $point_12b=$point_12[1];
                                    $point_12c=$point_12[2];
                                    $point_12d=$point_12[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 22</td>\
                                            <td>\
                                                <input type='text' name='point12[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_12a'>\
                                                <input type='text' name='point12[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_12b'>\
                                                <input type='text' name='point12[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_12c'>\
                                                <input type='text' name='point12[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_12d'>\
                                            </td>\
                                          </tr>";
                                    $point_13=  explode(",", $point13);
                                    $point_13a=$point_13[0];
                                    $point_13b=$point_13[1];
                                    $point_13c=$point_13[2];
                                    $point_13d=$point_13[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 13</td>\
                                            <td>\
                                                <input type='text' name='point13[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_13a'>\
                                                <input type='text' name='point13[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_13b'>\
                                                <input type='text' name='point13[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_13c'>\
                                                <input type='text' name='point13[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_13d'>\
                                            </td>\
                                          </tr>";
                                    $point_14=  explode(",", $point14);
                                    $point_14a=$point_14[0];
                                    $point_14b=$point_14[1];
                                    $point_14c=$point_14[2];
                                    $point_14d=$point_14[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 14</td>\
                                            <td>\
                                                <input type='text' name='point14[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_14a'>\
                                                <input type='text' name='point14[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_14b'>\
                                                <input type='text' name='point14[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_14c'>\
                                                <input type='text' name='point14[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_14d'>\
                                            </td>\
                                          </tr>";
                                    $point_15=  explode(",", $point15);
                                    $point_15a=$point_15[0];
                                    $point_15b=$point_15[1];
                                    $point_15c=$point_15[2];
                                    $point_15d=$point_15[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 15</td>\
                                            <td>\
                                                <input type='text' name='point15[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_15a'>\
                                                <input type='text' name='point15[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_15b'>\
                                                <input type='text' name='point15[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_15c'>\
                                                <input type='text' name='point15[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_15d'>\
                                            </td>\
                                          </tr>";
                                    $point_16=  explode(",", $point16);
                                    $point_16a=$point_16[0];
                                    $point_16b=$point_16[1];
                                    $point_16c=$point_16[2];
                                    $point_16d=$point_16[3];
                                    echo "<tr class='rem'>\
                                            <td>Point 16</td>\
                                            <td>\
                                                <input type='text' name='point16[]' class='span2' placeholder='0&#176;' onkeypress='return validate(event)' value='$point_16a'>\
                                                <input type='text' name='point16[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)' value='$point_16b'>\
                                                <input type='text' name='point16[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)' value='$point_16c'>\
                                                <input type='text' name='point16[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)' value='$point_16d'>\
                                            </td>\
                                          </tr>";
                        ?>";
         $("#add").append(att_php);
      }
      if($(this).val()=="general"){
        $(".rem").remove();
        var att_php = "<?php for ($i=1;$i<=16;$i++){
                             echo "<tr class='rem'><td>Point $i</td><td><input type='text' name='point".$i."[]' class='span2'  placeholder='0&#176;' onkeypress='return validate(event)'> <input type='text' name='point".$i."[]' class='span2' placeholder='90&#176;' onkeypress='return validate(event)'> <input type='text' name='point".$i."[]' class='span2' placeholder='180&#176;' onkeypress='return validate(event)'> <input type='text' name='point".$i."[]' class='span2' placeholder='270&#176;' onkeypress='return validate(event)'></td></tr>";
                             }
                        ?>";
         $("#add").append(att_php);
      }
   });
});
function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah2')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah3')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
    </script>