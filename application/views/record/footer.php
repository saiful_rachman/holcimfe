<div id="footer">
    	<div class="row-fluid">
        <div class="span6">
			<div class="row">
			<div class="span9 title" style="padding-left:28%;padding-top:5px">Copyright &copy; 2014 CBM Team Holcim Tuban</div>
			</div>
		</div>
            <div class="span6 severity">
            	<div class="row">
                	<div class="span3 title">Severity Level Stats</div>
                    <div class="span9 status">
                    	<div class="row-fluid">
                        	<div class="span6"><span style="padding-right:10px">TUBAN 1</span> <span class="bar-red"></span> 12 <span class="bar-yellow"></span> 5  <span class="bar-green"></span> 80 </div>
                          <div class="span6"><span style="padding-right:10px">TUBAN 2</span>  <span class="bar-red"></span> 12 <span class="bar-yellow"> </span> 5 <span class="bar-green"></span> 80 </div>
                        </div>
                    
                    </div>
                </div>
            
            </div>
        </div>
    
    </div>


<div class="overlay">
	<div id="list_components">
	
	</div>
</div>

<!-- Javascript here -->
<!-- Javascript here -->
<script type="text/javascript" src="<?=base_url()?>application/views/assets/js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>application/views/assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(function(){
	$( ".menu-top .login" ).hover(
		function() {
			$(".form_login").fadeIn("fast");
	}, function() {
		$(".form_login").fadeOut("fast");
	}
	);
	
	
});
	
</script>
</body>
</html>