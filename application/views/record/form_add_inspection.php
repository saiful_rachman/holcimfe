<?php $this->load->view('includes/header.php') ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<style type="text/css">
    .txtarea{
        max-height: 100px;
        min-height: 100px;
        max-width: 360px;
        min-width: 360px;
    }
</style>
<script type="text/javascript">
function coba(){
function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }
        var subarea = $("#areaname").val();
        var plant = $("#plant").val();
        $("#txtinput")
            // don't navigate away from the field on tab when selecting an item
              .focusout( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunctionx/"+subarea+"/"+plant,{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
}
</script>
<script type="text/javascript">
var i = 0;       

function tambah(){
  i++;
  var addImages = "<input class='span6' name='userfile[]' class='btn' id='userfile' type='file' multiple onchange='readURLx(this,"+i+");' required/><img id='blah"+i+"' src='#' /> ";
  var addTitle = '<input type="text" name="titlefie[]" placeholder="Image Title" required>';
  $("#oilTrend tbody").append("<tr class='"+i+"'><td>"+addImages+"</td><td valign='middle'>"+addTitle+"</td></tr>")
};

function kurang() {
  if(i>0){
    $("#oilTrend tbody tr").remove("."+i);
    i--;
  } else {
    i = 1;
  }
};
function readURLx(input,id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#blah"+id)
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
<form method="post" action="<?php echo site_url();?>record/add_inspection/add_post" enctype="multipart/form-data" id="formx">
<div id="main">
<div id="content">
    <div class="inner">	
        <div class="row-fluid">
            <div class="span12">
                <h2>Create Form Wizard</h2>
                    <h4>Inspection Record Form <span class="pull-right"></span></h4>
                        <div class="well well-small">
                            <table class="table">
                                <tr>
                                    <td width="200px">Plant Name</td>
                                    <td colspan="3">
                                        <select name="area" class="span6" required id="plant">
                                            <option value="">-Select Plant-</option>
                                        <?php
                                            foreach ($list_plant as $plant){
                                        ?>
                                            <option value="<?=$plant->id;?>"><?=$plant->plant_name;?></option>
                                        <?php } ?>
                                        </select>
                                    </td>
                                </tr>	
                                <tr>
                                    <td width="200px">Area Name</td>
                                    <td colspan="3">
                                        <select name="area" class="span6" required id="area"><input type="hidden" id="areaname">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200px">Sub Area Name</td>
                                    <td colspan="3">
                                        <select name="subarea" class="span6" required id="subarea">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Hac</td>
                                    <td colspan="3"><input type="text" name="hac" id="txtinput" onkeypress="coba()"  placeholder="HAC" class="span6" required autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Manufacture</td>
                                     <td colspan="3"><input type="text" name="manufacture"  placeholder="Manufacture" class="span6" required autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Type/Design</td>
                                     <td colspan="3"><input type="text" name="type"  placeholder="Type/Design" class="span6" required autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Year of Build</td>
                                     <td colspan="3"><input type="text" name="year_build"  placeholder="Year of Build" class="span6" required autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Last Check</td>
                                     <td colspan="3"><input type="text" name="last_check"  placeholder="Last Check" class="span6" required autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Last Repaired</td>
                                     <td colspan="3"><input type="text" name="last_repaired"  placeholder="Last Repaired" class="span6" required autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Power</td>
                                     <td colspan="3"><input type="text" name="power"  placeholder="Power" class="span6" required autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Speed Input</td>
                                     <td colspan="3"><input type="text" name="speed_input"  placeholder="Speed Input" class="span6" required autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Speed Output</td>
                                     <td colspan="3"><input type="text" name="speed_output"  placeholder="Speed Output" class="span6" required autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Add Information 1</td>
                                    <td><input type="text" name="title1"  placeholder="Title" class="span9" required autocomplete="off"/></td>
                                    <td><input type="text" name="value1"  placeholder="Value" class="span9" required autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Add Information 2</td>
                                    <td><input type="text" name="title2"  placeholder="Title" class="span9" required autocomplete="off"/></td>
                                    <td><input type="text" name="value2"  placeholder="Value" class="span9" required autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Description</td>
                                    <td colspan="3"><textarea name="description" cols="60" rows="5" placeholder="Report COntent"></textarea></td>
                                </tr>
                                <tr>
                                    <td>Upload File(pdf)</td>
                                    <td colspan="3"><input type="file" class="btn" name="upload_file" id="upload_file" /></td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td colspan="3"><textarea name="remarks" cols="60" rows="5" placeholder="Textarea"></textarea></td>
                                </tr>
                                <tr>
                                    <td>Recomendation</td>
                                    <td colspan="3"><textarea name="recomendation" cols="60" rows="5" placeholder="Recomendation"></textarea></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="3">
                                        <div class="btn-group">
                                            <a id="tambah" class="btn btn-info" onclick="tambah();"><i class="icon-plus icon-white"></i>Add</a>
                                            <a id="kurang" class="btn btn-info" onclick="kurang();"><i class="icon-remove icon-white"></i>Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Image</td>
                                    <td colspan="3">
                                        <table id="oilTrend" class="table table-bordered">
                                            <tbody id="listing">	
                                                    <tr class="">
                                                        <td>
                                                            <input class="span6" name="userfile[]" class="btn" id="userfile" type="file" multiple onchange="readURL1(this);" />
                                                            <img id="blah100" src="#" />
                                                        </td>
                                                        <td valign="middle"><input type="text" name="titlefie[]" placeholder="Image Title" ></td>
                                                    </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Severity Level</td>
                                    <td colspan="3"><select name="severity_level">
                                            <option value="0">Normal</option>
                                            <option value="1">Warning</option>
                                            <option value="2">Danger</option>
                                        </select>
                                    </td>
                                </tr>
                                   <input type="hidden" name="user" value="<?php echo $this->session->userdata('users_id');?>">
                                </tr>
			</table>
                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                        </div>
            </div>
        </div>
    </div>
</div>
</div>
</form>
<?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
</script>
<script type="text/javascript">
$(document).ready(function(){
$("#plant").change(function(){
     var id = $("#plant").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_area",
         data: "id="+id,
         success: function(data){
             $("#area").html(data);
         }
     });
  });

  $("#area").change(function(){
     var id = $("#area").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_subarea",
         data: "id="+id,
         success: function(data){
             $("#subarea").html(data);
         }
     });
   });
   $("#subarea").change(function(){
     var id = $(this).val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_areaname",
         data: "id="+id,
         success: function(data){
             console.log(data);
             $("#areaname").val(data);
         }
     });
           $("#txtinput").val('');
           //$("#listingx").remove();
      });
});
function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah100')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
    </script>