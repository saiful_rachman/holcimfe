<?php $this->load->view('includes/header.php') ?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner" style="padding-top:2%">
			<div class="btn-group">
				<a href="<?=base_url()?>record/add_list_withdraw/add/<?=$idx;?>" class="btn btn-success">ADD</a>
             	<a href="<?=base_url()?>record/withdraw" class="btn btn-warning">BACK</a>
			</div>
			<div class="well">
				<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No</th>
						<th>Batch No.</th>
						<th>Lubricant Type</th>
						<th>Qty</th>
                                                <th>Unit</th>
						<th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                $offset = $this->uri->segment(5);
                                $id= 1; 
                                if(count($data)==""){
                                    echo"<tr><td colspan='6' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                foreach($data as $row) :
                                   
                                ?>
                                        
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->batch_no; ?></td>
						<td><?php echo $row->lubricant; ?></td>
						<td><?php echo $row->qty; ?></td>
                                                <td><?php echo $row->unit; ?></td>
                                                <td style="text-align: center;">
                                                    <a href="<?php echo base_url(); ?>record/add_list_withdraw/edit/<?=$idx;?>/<?php echo $row->id; ?>" class="btn btn-warning">EDIT</a>
                                                    <a href="<?php echo base_url(); ?>record/add_list_withdraw/delete/<?=$idx;?>/<?php echo $row->id; ?>" onclick="return confirm('Are you sure to delete?')" class="btn btn-warning">DELETE</a>
                                                </td>
					</tr>
					<?php endforeach; ?>
                                         <tr> 
                                            <td></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/add_list_withdraw/index/<?php echo $idx; ?>" id="fbatch_no"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('batch_no');}else{return false;};" name="val" /><input type="hidden" name="field" value="batch_no"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/add_list_withdraw/index/<?php echo $idx; ?>" id="flubricant">
                                                    <select style="width: 100px;" onchange="cobax('lubricant');" name="val" />
                                                    <option value="" selected> </option>
                                                    <option value="Oil">Oil</option>
                                                    <option value="Grease">Grease</option>
                                                    </select>
                                                    <input type="hidden" name="field" value="lubricant">
                                                </form>
                                            </td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/add_list_withdraw/index/<?php echo $idx; ?>" id="fqty"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('qty');}else{return false;};" name="val" /><input type="hidden" name="field" value="qty"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>record/add_list_withdraw/index/<?php echo $idx; ?>" id="funit">
                                                    <select style="width: 100px;" onchange="cobax('unit');" name="val" />
                                                    <option value="" selected> </option>
                                                    <option value="Liter">Liter</option>
                                                    <option value="Kg">Kg</option>
                                                    </select>
                                                    <input type="hidden" name="field" value="unit">
                                                </form></td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
    <script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>