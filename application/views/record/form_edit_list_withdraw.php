<?php $this->load->view('includes/header.php') ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
</style>
<form method="post" action="<?php echo site_url();?>record/add_list_withdraw/edit_proses" enctype="multipart/form-data" id="formx">
<div id="main">
<div id="content">
    <div class="inner">	
        <div class="row-fluid">
            <div class="span12">
                <h2>Update Form Wizard</h2>
                    <h4>Withdraw Oil List Form <span class="pull-right"></span></h4>
                        <div class="well well-small">
                            <table class="table">
                                <tr>
                                    <td>Batch No.</td>
                                    <td><input type="text" name="batch_no" class="span6" value="<?=$list->batch_no;?>" required/><input type="hidden" name="id" value="<?=$list->id;?>"/><input type="hidden" name="idx" value="<?=$idx;?>"/></td>
                                </tr>
                                <tr>
                                    <td>Lubricant Type</td>
                                    <td>
                                        <select name="lubricant" required id="lubx1">
                                            <?php
                                                if($list->lubricant == "Oil"){
                                                    $oil="selected";
                                                }else{
                                                    $oil="";
                                                }
                                                if($list->lubricant == "Grease"){
                                                    $grease="selected";
                                                }else{
                                                    $grease="";
                                                }
                                            ?>
                                            <option value=""></option>
                                            <option value="Oil" <?=$oil;?>>Oil</option>
                                            <option value="Grease" <?=$grease;?>>Grease</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Unit</td>
                                    <td><input type="text" name="unit" id="lubxx" readonly></td>
                                </tr>
                                <tr>
                                    <td>Qty</td>
                                    <td><input type="text" name="qty" required onkeypress="return validate(event);" value="<?=$list->qty;?>"></td>
                                </tr>
                            </table>
                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                        </div>
            </div>
        </div>
    </div>
</div>
</div>
</form>
<?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript">
$(document).ready(function(){
    if($("#lubx1").val()==="Oil"){
        $("#lubxx").val('Liter'); 
    }else{
        $("#lubxx").val('Kg');
    }
    
    $("#lubx1").change(function(){
    var isi = $(this).val();
    if(isi === "Oil"){
        $("#lubxx").val('Liter');
    }else{
        $("#lubxx").val('Kg');
    }
});
});
function validate(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>