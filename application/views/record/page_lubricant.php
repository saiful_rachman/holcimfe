<?php
$this->load->view('includes/header.php');
error_reporting(0);
?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner" style="padding-top:2%">
            <form method="post" action="<?=base_url();?>record/lubricant/get_list">
            <table>
                <tr>
                    <td width="100px" valign="top">Plant Name</td>
                    <td>
                        <select name="plant" class="span3" required id="plant">
                            <option value="">-Select Plant-</option>
                        <?php
                            foreach ($list_plant as $plant){
                                if($plant->id==$plantx){
                                    $cplant="selected";
                                }else{
                                    $cplant="";
                                }
                        ?>
                            <option value="<?=$plant->id;?>" <?=$cplant;?>><?=$plant->plant_name;?></option>
                        <?php } ?>
                        </select>
                    </td>
                    <td valign='top' style="padding-left: 50px;">
                        From
                    </td>
                    <td>
                        <input type="text" class="datepicker span2" name="from" value="<?=$from;?>">
                    </td>
                    <td valign='top' style="padding-left: 10px;">
                        To
                    </td>
                    <td>
                        <input type="text" class="datepicker span2" name="to" value="<?=$to;?>">
                    </td>
                </tr>	
                <tr>
                    <td valign='top'>Area Name</td>
                    <td>
                        <select name="area" class="span3" required id="area"><input type="hidden" id="area_hidden" value="<?=$area;?>">
                        </select>
                    </td>
                    <td colspan="2" valign='top' style="padding-left: 50px;">
                        <input type="submit" name="submit" value="Search" style="width: 100px;background-color: blue;color: white;"> <a href="<?=base_url();?>record/lubricant/print_data/<?=$subarea;?>/<?=$from;?>/<?=$to;?>/<?=$plantx;?>/<?=$area;?>" target="_blank"><input type="button" name="priny" value="Print" style="width: 100px;background-color: red;color: white;"></a>
                    </td>
                </tr>
                <tr>
                    <td valign='top'>Sub Area Name</td>
                    <td>
                        <select name="subarea" class="span3" required id="subarea"><input type="hidden" id="subarea_hidden" value="<?=$subarea;?>">
                        </select>
                    </td>
                </tr>
            </table>
            </form>
            <div>
                <div style="font-size: 15px;font-weight: bolder;background-color: green;color: white;">Lubricant Usage Information</div>
                <table style="margin-top: 8px;margin-bottom: 8px;margin-left: 40px;font-weight: bolder;">
                    <tr>
                        <td>Grease Installed</td>
                        <td width="580px">:  <?=$plant_kg == "" ? 0 : $plant_kg;?> Kg</td>
                        <td>Oil Installed</td>
                        <td>: <?=$plant_liter == "" ? 0 : $plant_liter; ?> Liter</td>
                    </tr>
                    <tr>
                        <td>Grease Usage</td>
                        <td>: <?=$usage_grease->jum == "" ? 0 : $usage_grease->jum;?> Kg</td>
                        <td>Oil Usage</td>
                        <td>: <?=$usage_oil->jum == "" ? 0 : $usage_oil->jum;?> Liter</td>
                    </tr>
                </table>
            </div>
                        <div style="font-size: 15px;font-weight: bolder;background-color: red;color: white;">Grease List</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
                                                <th width='200'>Grease Type</th>
						<th width='400' style="text-align: center;">Unit</th>
						<th style="text-align: center;">Quantity Used</th>
<!--                                                <th style="text-align: center;">Detail</th>-->
					</tr>
				</thead>
				<tbody>
				<?php 
                                if(count($list_grease)==""){
                                    echo"<tr><td colspan='5' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                $no=1; foreach($list_grease as $row) :?>
					<tr>
						<td><?php echo $no; ?></td>
						<td><?php echo $row->lubricant_name; ?></td>
						<td style="text-align: center;"><?php echo $row->unit; ?></td>
						<td style="text-align: center;"><?php echo $row->jum; ?></td>
<!--						<td style="width: 70px;text-align: center;">
                                                    <a href="<?=base_url();?>print_data/lubricant/<?php echo $row->id; ?>" class="btn btn-warning">View</a> 
                                                </td>-->
						
					</tr>
					<?php $jum=$jum+$row->jum; $no++; endforeach; ?>
                                        <tr>
                                            <td colspan="3" style="text-align: center;">Total</td>
                                            <td style="text-align: center;"><?=$jum;?></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div id="container"></div>
                        <div style="font-size: 15px;font-weight: bolder;background-color: red;color: white;margin-top: 20px;">Oil List</div>
                        <div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
                                                <th>Oil Type</th>
						<th style="text-align: center;">Unit</th>
						<th style="text-align: center;">Quantity Used</th>
<!--                                                <th style="text-align: center;">Detail</th>-->
					</tr>
				</thead>
				<tbody>
				<?php 
                                if(count($list_oil)==""){
                                    echo"<tr><td colspan='5' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                $no=1; foreach($list_oil as $rowx) :?>
					<tr>
						<td><?php echo $no; ?></td>
						<td><?php echo $rowx->lubricant_name; ?></td>
						<td style="text-align: center;"><?php echo $rowx->unit; ?></td>
						<td style="text-align: center;"><?php echo $rowx->jum; ?></td>
<!--						<td style="width: 70px;text-align: center;">
                                                    <a href="<?=base_url();?>print_data/lubricant/<?php echo $rowx->id; ?>" class="btn btn-warning">View</a> 
                                                </td>-->
						
					</tr>
					<?php $jumx=$jumx+$rowx->jum; $no++; endforeach; ?>
                                        <tr>
                                            <td colspan="3" style="text-align: center;">Total</td>
                                            <td style="text-align: center;"><?=$jumx;?></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div id="container2"></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>	
<script type="text/javascript">
$(document).ready(function(){
        var plant_id=$("#plant").val();
        var main_area_id=$("#area_hidden").val();
        var sub_area_id=$("#subarea_hidden").val();
    //post data main area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area_edit",
             data: "plant_id="+plant_id+"&main_area_id="+main_area_id,
             success: function(data){
                 $("#area").html(data);
             }
         });
         
        //post data sub area
        $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea_edit",
             data: "main_area_id="+main_area_id+"&sub_area_id="+sub_area_id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
$("#plant").change(function(){
     var id = $("#plant").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_area",
         data: "id="+id,
         success: function(data){
             $("#area").html(data);
         }
     });
  });

  $("#area").change(function(){
     var id = $("#area").val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_subarea",
         data: "id="+id,
         success: function(data){
             $("#subarea").html(data);
         }
     });
   });
   $("#subarea").change(function(){
           $("#txtinput").val('');
           //$("#listingx").remove();
      });
  $( ".datepicker" ).datepicker({
                    dateFormat: "yy-mm-dd"
            });
});
</script>
<script type="text/javascript">
    $(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Lubricant Usage Chart'
        },
        subtitle: {
            text: 'Grease Type'
        },
        xAxis: {
            categories: [""
            ]
        },
        yAxis: {
            min: 0,
            max: 140,
            title: {
                text: 'Quantity Usage (Kg)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} Kg</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            <?php
            foreach($list_grease as $gr){
                echo "name: '$gr->lubricant_name', data: [$gr->jum]},{";
            }
            ?>

        }]
    });
    
    $('#container2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Lubricant Usage Chart'
        },
        subtitle: {
            text: 'Oil Type'
        },
        xAxis: {
            categories: [""
            ]
        },
        yAxis: {
            min: 0,
            max: 140,
            title: {
                text: 'Quantity Usage (L)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} Liter</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            <?php
            foreach($list_oil as $gr){
                echo "name: '$gr->lubricant_name', data: [$gr->jum]},{";
            }
            ?>

        }]
    });
});
</script>
