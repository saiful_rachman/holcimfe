<?php $this->load->view('includes/header_crud.php') ?>


    <div id="content">
	   <div class="inner" style="padding-top:2%">
			<div class="btn-group">
				<a href="<?=base_url()?>record/withdraw/index/unpublish" class="btn btn-warning">UNPUBLISH</a>
				<a href="<?=base_url()?>record/withdraw/index/publish" class="btn btn-success">PUBLISH</a>
   	            <a href="<?=base_url()?>record/main" class="btn">BACK</a>
			</div>
			<?=$output ?>
		</div>
    </div>
    

<?php $this->load->view('includes/footer.php') ?>
<?php foreach($js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>