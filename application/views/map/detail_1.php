<?php 
// Check usera 
if($this->session->userdata('access_login') == TRUE) 
{
	$users = $this->main_model->get_detail('users',array('id' => $this->session->userdata('users_id')));
}
?>	


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Holcim CBM Tuban</title>
<link href="<?php echo base_url()?>application/views/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="<?php echo base_url()?>application/views/assets/css/style.css" rel="stylesheet" />

</head>

<body>
<div id="sheet">
	<div id="header">
    	<div class="row-fluid inner top">
        	<div class="span9">
            	<div class="row-fluid">
                	<div class="span2">
                    	 <a href="#"><img src="<?php echo base_url()?>application/views/assets/img/logo.png" /></a>
                    </div>
                	<div class="span10  menu-top">
                    	 <ul>
								<li><a href="#" class="active">Home</a></li>
								<li><a href="#">Report</a></li>
								<li><a href="#">Record</a></li>
							  <li><a href="<?php echo base_url()?>main/view_about">About Us</a></li> 
							  
							<?php if($this->session->userdata('access_login') != FALSE) { ?>
								<li><a href="<?=base_url();?>main/edit_profile/">My Account</a></li>
								<li><a href="#">Message</a></li>
							<?php } else { ?>
								<li><a href="#">Contact Us</a></li>
							 <?php } ?> 
                          
							
							<?php if($this->session->userdata('access_login') != TRUE) { ?>
								<li  class="login"><a href="#">Login</a>
									<div class="form_login">
									LOGIN<br/><br/>
									<form action="<?=base_url()?>main/login" method="post" >
									<input type="text" name="nip" placeholder="Username"/><br/>
									<input type="password" name="password" placeholder="*****"/>
									<button type="submit" class="btn btn-block active">Login</button>
									Forgot password? <a href="#">Get one here</a>
									</form>
									</div>
								</li>
							<?php } else { ?>
								<li><a href="<?=base_url() ?>main/logout">Log out</a></li>
							 <?php } ?> 
                            
                          
                            
                        </ul>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="span3 tagline"> We are world class maintenance</div>
            <div class="clearfix"></div>
        </div>
        
        <div class="menu-middle">   
        	<div class="inner">
            
            <div class="row-fluid">
            	<div class="span9">
            
               <div class="row-fluid">
               	<div class="span2 logo-bottom">CBM TUBAN</div>
                  <div class="span10 status-user"> <?php if($this->session->userdata('access_login') != TRUE) { ?>
                       WELCOME TO HOLCIM, PLEASE YOUR LOGIN
							<?php } else { ?>
						YOU"RE LOGGED AS
							 <?php } ?>  
						<strong>
							<?=$users['nama'] ?> 
						</strong> 
							<?php ?>  
					</div>
				<div class="clearfix"></div>
               </div>
               
               </div>
               <div class="span3">
               </div>
               
             </div>  
                    
            </div>
        </div>
        
		
		<?php if($this->session->userdata('users_level') == 'Administrator') { ?>
        <div class="menu-bottom">
             <div class="inner">
             <ul>
                	<li><a href="#" class="active">HAC</a></li>
                    <li><a href="#">Form Management</a></li>
                    <li><a href="#">User Management</a></li>
                    <li><a href="#">Area Management</a></li>
                    <li><a href="#">Content Management</a></li>
                    <li><a href="#">Activity Report </a></li>
                <div class="clearfix"></div>
                </ul>
            </div>    
        </div>
		<?php } ?>
        
        <div class="row-fluid">
        </div>
        
    </div>
	
	<div class="span4">&nbsp;</div>
			<div class="span8" style="padding-top:3%">
			detail 1 <a class="btn" href="<?php echo base_url();?>map/detail_2">Klik</a>
		</div>
 	<div id="footer">
    	<div class="row-fluid">
        	<div class="span6"></div>
            <div class="span6 severity">
            	<div class="row">
                	<div class="span3 title">Severity Level Stats</div>
                    <div class="span9 status">
                    	<div class="row-fluid">
                        	<div class="span6">TUBAN 1 <span class="bar-red"></span> 12 <span class="bar-yellow"></span> 5  <span class="bar-green"></span> 80 </div>
                          <div class="span6">TUBAN 2 <span class="bar-red"></span> 12 <span class="bar-yellow"> </span> 5 <span class="bar-green"></span> 80 </div>
                        </div>
                    
                    </div>
                </div>
            
            </div>
        </div>
    
    </div>

</div>

<div class="overlay">
	<div id="list_components">
	
	</div>
</div>

<!-- Javascript here -->
<!-- Javascript here -->
<script type="text/javascript" src="<?=base_url()?>application/views/assets/js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>application/views/assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(function(){
	$( ".menu-top .login" ).hover(
		function() {
			$(".form_login").fadeIn("fast");
	}, function() {
		$(".form_login").fadeOut("fast");
	}
	);
});
	
</script>
</body>
</html>
