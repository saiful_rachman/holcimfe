<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<div id="main">
	<div id="content">
		<div class="inner">	
<div class="row-fluid">
    <div class="span12">
            <h2>Update Frequency</h2>
            <div class="well well-small">
                <form method="post" action="<?=base_url();?>engine/crud_frequency/edit_proses" />
                    <table class="table">	
                        <tr>
                                <td width="200px">Frequency</td>
                                <td><input type="text" value="<?php echo $list->frequency; ?>" required name="frequency"/><input type="hidden" name="id" value="<?php echo $list->id; ?>"></td>
                        </tr>	
                    </table>
                <button type="submit" class="btn"><i class="icon-check icon-black"></i> Edit</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
            </form>
            </div>   
    </div>
</div>
</div>         
    </div>
</div>
<?php $this->load->view("includes/footer.php"); ?>