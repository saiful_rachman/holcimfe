<center>
    <div style='width: 210mm;height:297mm;border: 1px solid;'>
        <table style="width: 100%;padding: 0px;font-size: 15px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td rowspan="5" style="width: 180px;text-align: center;">
                    <img src="<?php echo base_url()?>application/views/assets/img/logo_header.png" height="90px" width="150px">
                </td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center;font-size: 18px;">CONDITION BASED MONITORING REPORT</td>
                <td style="width: 110px;">&nbsp;Form Version</td>
                <td style="width: 110px;">&nbsp;: 2.0</td>
            <tr>
                <td>&nbsp;Release Date</td>
                <td>&nbsp;: 01/01/2014</td>
            </tr>

            <tr>
                <td rowspan="2" style="text-align: center;font-size: 21px;">VIBRATION REPORT</td>
                <td>&nbsp;Reported Date</td>
                <td>&nbsp;: <?=date("d/m/Y",  strtotime($list->datetime));?></td>
            </tr>
            <tr>
                <td>&nbsp;Reporterd By</td>
                <td>&nbsp;: <?=$list->nama;?></td>
            </tr>
            <tr>
                <td colspan="4" style="text-transform: capitalize;font-size: 9px;">
                    <?=$list->description;?>
                </td>
            </tr>
        </table>
        <table style="width: 100%;padding: 0px;font-size: 15px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td align="center"><img src="<?=base_url();?>application/views/assets/img/product1.png" width="370px" height="300px" style="padding: 1%;"></td>
                <td align="center"><img src="<?=base_url();?>application/views/assets/img/product2.png" width="370px" height="300px" style="padding: 1%;"></td>
            </tr>
        </table>
        <div style="height: 5px;"></div>
        <table style="width: 100%;padding: 0px;font-size: 10px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td rowspan="2" align="center">PARAMETER</td>
<!--            <td rowspan="2" align="center">STD</td>-->
                <td colspan="20" align="center">MEASUREMENT POINTS</td>
            </tr>
            <tr>
                <td align="center">1H</td>
                <td align="center">2H</td>
                <td align="center">2V</td>
                <td align="center">2A</td>
                <td align="center">3H</td>
                <td align="center">3V</td>
                <td align="center">3A</td>
                <td align="center">4H</td>
                <td align="center">5H</td>
                <td align="center">6H</td>
                <td align="center">6A</td>
                <td align="center">7H</td>
                <td align="center">7A</td>
                <td align="center">8H</td>
                <td align="center">9H</td>
            </tr>
            <?php 
                $no=0;
                foreach($meas as $measx){
                $no++;
            ?>
            <tr>
                <td align="center">Parameter <?=$no;?></td>
                <td align="center"><?=$measx->x1h?></td>
                <td align="center"><?=$measx->x2h?></td>
                <td align="center"><?=$measx->x2v?></td>
                <td align="center"><?=$measx->x2a?></td>
                <td align="center"><?=$measx->x3h?></td>
                <td align="center"><?=$measx->x3v?></td>
                <td align="center"><?=$measx->x3a?></td>
                <td align="center"><?=$measx->x4h?></td>
                <td align="center"><?=$measx->x5h?></td>
                <td align="center"><?=$measx->x6h?></td>
                <td align="center"><?=$measx->x6a?></td>
                <td align="center"><?=$measx->x7h?></td>
                <td align="center"><?=$measx->x7a?></td>
                <td align="center"><?=$measx->x8h?></td>
                <td align="center"><?=$measx->x9h?></td>
            </tr>
            <?php } ?>
        </table>
        <div style="height: 5px;"></div>
        <table style="width: 100%;padding: 0px;font-size: 10px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td align="center"><img src="<?=base_url();?>media/images/<?=$list->image_guide;?>" width="770px" height="300px" style="padding: 1%;"></td>
            </tr>
        </table>
        <div style="width: 100%;padding: 5px;text-align: left;font-size: 10px;">
            <p>Remarks:</p>
            <?=$list->remarks;?>
        </div>
    </div>
	<form action="<?php echo base_url();?>print_data/update_vibration/<?php echo $list->record_id;?>" method="post">
            <div class="btn-group" style="padding-top: 10px;">
		<?php if($list->status != "publish"){
			echo "<button type='submit' name='status' value='publish' class='dontprint'>Publish</button>
			<button type='submit' name='status' value='reject' class='dontprint' onclick='return confirm(Are You Sure To Reject?);' >Reject</button><input type='hidden' name='idc' value='$list->record_id'>";
		}else{
			echo "<input class='dontprint' type='button' style='text-align:center' value='Print' onclick='window.print()'>";
		}
		?>
                <a href="<?=base_url();?>record/vibration/index/unpublish"><input type="button" class='dontprint' style="display: <?=$display;?>;" value='Back'></a>
	</div>
	</form>
    </div>
</center>
<style>
@media print
{        
    .dontprint{
        display: none;
    }   
}
</style>