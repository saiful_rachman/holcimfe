<style type="text/css" media="print">
@media print {
body {-webkit-print-color-adjust: exact;}

.dontprint
{ display: none; }
}

</style>

<button class="dontprint" onclick="PrintFunction()">Print Layout</button>
		  
<script>
function PrintFunction()
{
window.print();
}
</script>
<div class="printable" align="center">
<img src="<?php echo base_url()?>application/views/assets/img/header.png" width="960" height="100" style="margin-top:1px; padding-bottom:5px;" />
<div style="border:1px solid #000;padding:5px; border-radius: 5px; width:950px; margin-top:1px; min-height:1050px;">
	<table  style="font-size:12px" width="100%" cellpadding="0" cellspacing="0" border="1">	
			<thead style="background-color:#99ccff">
				<tr>
					<td align="center">HAC Code</td>
					<td align="center">Inspection Activity</td>	
					<td align="center">#</td>
					<td align="center">PICTURE</td>	
					<td align="center">Target Value</td>
					<td align="center">Actual Value</td>
					<td align="center">OK / Not OK</td>	
					<td align="center">Comments</td>	
				</tr>
				 
			</thead>
			<tbody id="content">
				<tr>
					<td  align="center"> 1</td>
					<td  align="center"> Name</td>
					<td  align="center"> &nbsp;</td>
					<td  align="center" align="center" valign="center"><img src="<?php echo base_url()?>application/views/assets/img/logo.png" style="height:100px; width:100px; padding-top:5px; padding-bottom:5px"/></td>
					<td  align="center"> &#10004;</td>
					<td  align="center"> &#10004;</td>
					<td  align="center"> &#10004;</td>
					<td  align="center"> Test Comment</td>
				</tr>
			</tbody>
	</table>			
</div>
	<div  align="left" style="border:1px solid #000; font-size:14px; padding:5px; border-radius: 5px; width:950px; margin-top:1px; min-height:150px;">
		<p>REMARKS : <br />
			<label> - Base on ISO 10816-3, the velocity vibration on point 2H and 2A is slightly high for new machine. It was fall into zone B (unlimitted long term operation)</label><br/>
			<label> - The timewaveform analysis show amplitude modulation that related to the the load variation due to there is no smooth running on apron chain (jumping)</label>
			</p>
    </div>
</div>
