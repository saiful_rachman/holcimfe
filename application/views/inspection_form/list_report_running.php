<?php $this->load->view('includes/header.php') ?>
<style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner">
            <div style="background-color: #5bb75b;"><b><h4>REPORT RUNNING INSPECTION</b></h4></div>
			<div class="btn-group">
				<!--<a href="<?=base_url()?>engine/form_manager" class="btn btn-success">Add</a>-->
             	
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
						<th>Name</th>
						<th>Mechanical Type</th>
						<th>Periode</th>
                                                <th>Form No.</th>
                                                <th>User</th>
                                                <th>Date</th>
                                                <th style="text-align: center">No. Publish</th>
                                                <th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php
                                if(count($data)==""){
                                    echo"<tr><td colspan='7' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                $offset = $this->uri->segment(4);
                                $id= 1; foreach($data as $row){?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->form_name; ?></td>
						<td><?php echo $row->mechanichal_type; ?></td>
                                                <td><?php echo $row->periode; ?></td>
                                                <td><?php echo $row->form_number; ?></td>
                                                <td><?php echo $row->nama; ?></td>
                                                <td><?php echo substr($row->datetime,0,10); ?></td>
                                                <td style="text-align: center;"><?php echo $row->publish_order; ?></td>
						<td style="width: 150px;text-align: center;">
                                                    <a href="<?= base_url(); ?>report/main_report_list/report_runningx_list/<?php echo $row->id; ?>" class="btn btn-warning">View</a>
                                                    <a href="<?php echo base_url();?>engine/inspection_manager/print_form_running/<?php echo $row->id; ?>" target="_blank" class="btn btn-warning">Print</a>
                                                </td>
						
					</tr>
                                <?php } ?>
                                        <tr>
                                            <td></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_report_running_inspection" id="farea_name"><input type="text" style="width: 150px;" onkeyup="javascript:if(event.keyCode == 13){coba('area_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="b.area_name"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_report_running_inspection" id="fmechanichal_type"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('mechanichal_type');}else{return false;};" name="val" /><input type="hidden" name="field" value="a.mechanichal_type"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_report_running_inspection" id="fperiode">
                                                    <select style="width: 70px;" onchange="cobax('periode');" name="val" />
                                                    <option value="" selected> </option>
                                                    <option value="Monday">Monday</option>
                                                    <option value="Tuesday">Tuesday</option>
                                                    <option value="Wednesday">Wednesday</option>
                                                    <option value="Thursday">Thursday</option>
                                                    <option value="Friday">Friday</option>
                                                    </select>
                                                    <input type="hidden" name="field" value="d.periode">
                                                </form>
                                            </td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_report_running_inspection" id="fform_number"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('form_number');}else{return false;};" name="val" /><input type="hidden" name="field" value="a.form_number"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_report_running_inspection" id="fnama"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('nama');}else{return false;};" name="val" /><input type="hidden" name="field" value="e.nama"></form></td>
                                            <td style="text-align:center;"><form method="post" action="<?php echo base_url(); ?>engine/inspection_manager/list_report_running_inspection" id="fpublish_order"><input type="text" style="width: 50px;" onkeyup="javascript:if(event.keyCode == 13){coba('publish_order');}else{return false;};" name="val" /><input type="hidden" name="field" value="a.publish_order"></form></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>