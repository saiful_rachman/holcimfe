<?php $this->load->view("includes/header.php"); ?>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Report Running Form</h2>
					<div class="well well-small">
						<table class="table">
							<thead>	
								<tr>
									<td width="200px">AREA</td>
                                                                        <td><?php echo $form1->area_name; ?><input type="hidden" name="form_id1" value="<?php echo $form1->id; ?>"></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td>
                                                                            <?php 
                                                                                $freq =  $form1->frequency; 
                                                                                $fr =  mysql_fetch_assoc(mysql_query("select * from master_frequency where id='$freq'"));
                                                                                echo $fr['frequency'];
                                                                            ?>
                                                                        </td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><?php echo $form1->mechanichal_type; ?></td>
								</tr>
								<tr>
									<td>Form No.</td>
                                                                        <td><?php echo $form1->form_number; ?><input name="status" type="hidden" value="R"/></td>
								</tr>
                                                                <tr>
									<td>Remarks</td>
                                                                        <td><?php echo $form1->remarks; ?></td>
								</tr>
                                                                <tr>
									<td>Recomendation</td>
                                                                        <td><?php echo $form1->recomendation; ?></td>
								</tr>
							</tbody>
						</table>
                                            <table class="table table-bordered" id="">
                                                <?php
                                                $form_id=$form1->id;
                                                foreach ($form2 as $dt_from2){ 
                                                $id=$dt_from2->id; 
                                                $hac=$dt_from2->hac; 
                                                $component=$dt_from2->component; 
                                                $id_form=$dt_from2->form_id;
                                                ?>
                                                <tr class="success">
                                                    <td style="font-weight: bolder;"><?php echo $dt_from2->hac_code; ?></td>
                                                    <td style="font-weight: bolder;" colspan="5"><?php echo $dt_from2->assembly_name; ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center;font-weight: bolder;">Inspection Activity</td>
                                                    <td style="text-align: center;font-weight: bolder;">Target Value</td>
                                                    <td style="text-align: center;font-weight: bolder;">Actual Value</td>
                                                    <td style="text-align: center;font-weight: bolder;">Severity Level</td>
                                                    <td style="text-align: center;font-weight: bolder;">Status</td>
                                                    <td style="text-align: center;font-weight: bolder;">Comment</td>
                                                </tr>
                                                <?php 
                                                $sql=mysql_query("select * from record_running_activity where record_id='$id' and form_id='$id_form'");
                                                while($data=  mysql_fetch_array($sql)){
                                                    if($data['severity_level']=="0"){
                                                        $sev_lev = "Normal";
                                                    }elseif($data['severity_level']=="1"){
                                                        $sev_lev = "Warning";
                                                    }else{
                                                        $sev_lev = "Warning";
                                                    }
                                                ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo $data['inspection_activity']; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['target_value']; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['actual_value']; ?></td>
                                                    <td style="text-align: center;"><?php echo $sev_lev; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['status']; ?></td>
                                                    <td style="text-align: center;"><?php echo $data['comment']; ?></td>
                                                </tr>
                                                <?php }} ?>
                                            </table>
                                            <div style="text-align: center;">
                                                <a href="<?php echo base_url(); ?>engine/inspection_manager/print_form_running/<?php echo $form1->id; ?>"><input type="button" value="print"></a>
                                                <input type="button" value="Back" onclick="window.history.back();">
                                            </div>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("includes/footer.php"); ?>
<script type="text/javascript">
$(document).ready(function (){
    $("#remarks").hide();
    $("#remarks_judul").hide();
    $("#subreject").hide();
    $("#cancel").hide();
    $("#reject").click(function(){
         var remarks = $("#remarks").val();
        var id = $("#id_form").val();
        var tanya = confirm("Are you sure?");
        if(tanya){
          $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/inspection_manager/update_view_running2",
          data:"id="+id+"&remarks="+remarks,
          success: function(response) {
              alert("Data Has Been Update");
              window.location.replace("<?php echo base_url(); ?>engine/inspection_manager/list_vrunning_inspection");
          },
          error: function(){
            alert("error");
          }
    });
        }else{
        }
        //window.location.replace("http://stackoverflow.com");
    });
    $("#cancel").click(function(){
        $("#remarks").hide();
        $("#remarks_judul").hide();
        $("#subreject").hide();
        $("#cancel").hide();
        
        $("#publish").show();
        $("#reject").show();
        $("#back").show();
        //window.location.replace("http://stackoverflow.com");
    });
    $("#publish").click(function(){
        var remarks = $("#remarks").val();
        var id = $("#id_form").val();
        var tanya = confirm("Are you sure?");
        if(tanya){
          $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/inspection_manager/update_view_running",
          data:"id="+id,
          success: function(response) {
              alert("Data Has Been Update");
              window.location.replace("<?php echo base_url(); ?>engine/inspection_manager/list_vrunning_inspection");
          },
          error: function(){
            alert("error");
          }
    });
        }else{
        }
    });
    $("#subreject").click(function(){
        var remarks = $("#remarks").val();
        var id = $("#id_form").val();
        var tanya = confirm("Are you sure?");
        if(tanya){
          $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>engine/inspection_manager/update_view_running2",
          data:"id="+id+"&remarks="+remarks,
          success: function(response) {
              alert("Data Has Been Update");
              window.location.replace("<?php echo base_url(); ?>engine/inspection_manager/list_vrunning_inspection");
          },
          error: function(){
            alert("error");
          }
    });
        }else{
        }
    });
    $('#form').submit(function(){
         alert('Data has been saved !');
        });

});
</script>