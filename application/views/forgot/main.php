<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>

<style type="text/css">
    .headerx{
        width: 50%;
        height: auto;
        border-radius: 5px 5px 0px 0px;
        border: 1px solid;
        margin: 0 auto;
        background-color: #000;
        z-index: 9999;
    }
    
     .headerx2{
        width: 50%;
        height: auto;
        border-radius: 5px 5px 0px 0px;
        border: 1px solid;
        margin: 0 auto;
        background-color: blue;
        z-index: 9999;
    }
    
    .header_isi{
        padding: 3px;
        color: #ffffff;
        font-size: 15px;
        font-weight: 900;
    }
    .headerx_body{
        width: 50%;
        height: auto;
        border-radius: 0px 0px 5px 5px;
        border: 1px solid;
        margin: 0 auto;
        background-color: #E3E3C7;
        margin-top: -2px;
        padding-bottom: 10px
    }
    .lb{
        padding-top: 5px;
        padding-left: 10px;
        font-weight: 900;
        font-size: 12px;
    }
    .sp{
        padding-left: 10px;
    }
    .inpt{
        width: 90%;
        font-size: 10px;
        border-radius: 5px;
    }
    .pesan{
        text-align: center;
        color: red;
        font-weight: 900;
    }
</style>
<div id="main">
    <div id="content">
        <div class="inner">	
            <div class="row-fluid">
                <div class="span12">
                    <div class="headerx">
                        <b class="header_isi">Forgot Password Form</b>
                    </div>
                    <div class="headerx_body">
                        <label class="lb">NIP</label>
                        <span class="sp"><input class="inpt" type="text" id="nip"></span>
                        <label class="lb">Email</label>
                        <span class="sp"><input class="inpt" type="email" id="email"></span>
                        <span class="sp"><input class="inpt" type="button" value="Submit" id="submit"></span>
                    </div>
                    <div class="headerx_body pesan"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="main2">
    <div id="content">
        <div class="inner">	
            <div class="row-fluid">
                <div class="span12">
                    <div class="headerx2">
                        <b class="header_isi">Update Password Form</b>
                    </div>
                    <div class="headerx_body">
                        <label class="lb">NIP</label>
                        <span class="sp"><input class="inpt" type="text" id="nipx" readonly></span><input class="inpt" type="hidden" id="idx" readonly></span>
                        <label class="lb">Email</label>
                        <span class="sp"><input class="inpt" type="email" id="emailx" readonly></span>
                        <label class="lb">New Password</label>
                        <span class="sp"><input class="inpt" type="password" id="new_pass"></span>
                        <label class="lb">Confirm New Password</label>
                        <span class="sp"><input class="inpt" type="password" id="conf_pass"></span>
                        <span class="sp"><input class="inpt" type="button" value="Submit" id="submitx"></span>
                    </div>
                    <div class="headerx_body pesan"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
    $(document).ready(function (){
        $(".pesan").hide();
        $("#main2").hide();
        $("#nip").keypress(function(){
            $(".pesan").hide();
        });
        $("#email").keypress(function(){
            $(".pesan").hide();
        });
        $("#submit").click(function(){
        if($("#nip").val()=="" || $("#email").val()==""){
            //alert('Please Fill All Field !!!');
            $(".pesan").show();
            $(".pesan").html('Please Fill All Field !!!');
        }else{
            var nip=$("#nip").val();
            var email=$("#email").val();
            $.ajax({
              type: "POST",
              url: "<?php echo base_url(); ?>forgot/cek_data",
              data:"nip="+nip+"&email="+email,
              success: function(response) {
                      if(response=="error"){
                          $(".pesan").show();
                          $(".pesan").html('Data Not Valid !!!');
                      }else{
                          hasil=response.split("|");
                          console.log(hasil);
//                          $("#idx").val(hasil[1].split("|"));
//                          $("#nipx").val(hasil[3].split("|"));
//                          $("#emailx").val(hasil[6].split("|"));
//                          $("#main").hide();
//                          $("#main2").show();
                            var idd=hasil[0].split("|");
                            var nipd=hasil[1].split("|");
                            var passd=hasil[3].split("|");
                            var emaild=hasil[2].split("|");
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>forgot/email",
                                data:"id="+idd+"&nip="+nipd+"&pass="+passd+"&email="+emaild,
                                success: function(response) {
                                    if(response=="success"){
                                        alert('Password Update, Please Cek Your Email');
                                        window.location.replace('<?=base_url();?>');
                                    }else{
                                        alert('Error');
                                    }
                                },
                                error: function(response){
                                    alert('salah');
                                }
                              });
                      }
                
              },
              error: function(response){
                  alert('salah');
              }
            });
            }
        });
        
        /////////////////////////////////////////form 2//////////////////////////////
        
        $("#new_pass").keypress(function(){
            $(".pesan").hide();
        });
        $("#conf_pass").keypress(function(){
            $(".pesan").hide();
        });
        
         $("#submitx").click(function(){
            if($("#new_pass").val()=="" || $("#conf_pass").val()==""){
                //alert('Please Fill All Field !!!');
                $(".pesan").show();
                $(".pesan").html('Please Fill All Field !!!');
            }else if($("#new_pass").val() !== $("#conf_pass").val()){
                $(".pesan").show();
                $(".pesan").html('New Password and Confirm Password Not Valid!!!');
            }else{
            var new_pass=$("#new_pass").val();
            var idx=$("#idx").val();
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>forgot/update_pass",
                  data:"new_pass="+new_pass+"&id="+idx,
                  success: function(response) {
                      if(response=="success"){
                          alert('Password Update, Please Login');
                          window.location.replace('<?=base_url();?>');
                      }else{
                          alert('Error');
                      }
                  },
                  error: function(response){
                      alert('salah');
                  }
                });
                }
            });
     });
</script>