<?php $this->load->view('includes/header.php') ?>

    <div id="content">
	<div class="inner" style="padding-top:2%">
		<h2>User Management</h2>
			<div class="btn-group">
                            <a href="<?=base_url()?>engine/crud_users/add" class="btn btn-success">Add</a>
             	
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No.</th>
                                                <th>NIP</th>
						<th>Name</th>
						<th>Phone</th>
                                                <th>Level</th>
						<th>Photo</th>
                                                <th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php $id=1; foreach($list as $row) :?>
					<tr>
						<td><?php echo $id++ ?></td>
						<td><?php echo $row->nip; ?></td>
						<td><?php echo $row->nama; ?></td>
						<td><?php echo $row->phone; ?></td>
                                                <td><?php echo $row->level; ?></td>
                                                <td><a href="<?php echo base_url(); ?>/media/images/<?php echo $row->photo; ?>" target="_blank"><img src="<?php echo base_url(); ?>/media/images/<?php echo $row->photo; ?>" width="50" height="50" /></a></td>
						<td style="width: 150px;">
                                                    <a href="<?=base_url();?>engine/crud_users/edit/<?php echo $row->id; ?>" class="btn btn-warning">Update</a> 
                                                    <button onclick="deletex('delete/<?php echo $row->id; ?>');" class="btn btn-warning">Delete</button>
                                                </td>
						
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			</div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript">
  function deletex(id){
   var tanya = confirm("Are you sure?");
        if(tanya){
            var url = "<?php echo base_url(); ?>engine/crud_users/"+id;
            window.location.replace(url);
        }else{
        }
}
</script>