<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="<?=base_url();?>engine/crud_users/add_proses" enctype="multipart/form-data"/>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Add Users Form</h2>
					<div class="well well-small">
                                            <table class="table">
							<thead>	
								<tr>
									<td width="200px">NIP</td>
                                                                        <td><input type="text" required name="nip" class="span6"/></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Password</td>
                                                                        <td><input type="password" class="span6" name="password" required /></td>
								</tr>
								<tr>
									<td>Email</td>
                                                                        <td><input type="email" name="email" class="span6" required /></td>
								</tr>
                                                                 <tr>
									<td>Title</td>
                                                                        <td>
                                                                            <select class="span6" name="title">
                                                                                <option value="mr">Mr.</option>
                                                                                <option value="mrs">Mrs.</option>
                                                                                <option value="sir">Sir.</option>
                                                                            </select>
                                                                        </td>
								</tr>
                                                                <tr>
                                                                    <td>Name</td>
                                                                    <td><input type="text" class="span6" required name="nama"/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Phone</td>
                                                                    <td><input type="text" class="span6" required name="phone"/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Jabatan</td>
                                                                    <td>
                                                                        <select name="jabatan" required>
                                                                            <option value="">-Select Jabatan-</option>
                                                                            <?php
                                                                                foreach($list_jabatan as $jabatan){
                                                                            ?>
                                                                            <option value="<?=$jabatan->id;?>"><?=$jabatan->jabatan;?></option>
                                                                            <?php
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Department</td>
                                                                    <td><input type="text" class="span6" required name="department"/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Area</td>
                                                                    <td>
                                                                        <select class="span6" name="area[]" required>
                                                                            <option value="">-select area-</option>
                                                                            <?php
                                                                                foreach($list_area as $area){?>
                                                                                    <option value='<?=$area->id;?>'><?=$area->description;?> (Plant <?=$area->id_plant;?>)</option>
                                                                            <?php } ?>
                                                                        </select> <input type="button" id="tb_add" value="Add Area" style='margin-top: -11px;'>
                                                                        <div id="add_area"></div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Level</td>
                                                                    <td>
                                                                        <select class="span6" name="level">
                                                                            <option value="Viewer">Viewer</option>
                                                                            <option value="Inspector">Inspector</option>
                                                                            <option value="Engineer">Engineer</option>
                                                                            <option value="Administrator">Administrator</option>
                                                                        </select>
                                                                    </td>
								</tr>
                                                                <tr>
                                                                    <td>Skill</td>
                                                                    <td><textarea name="skill" style="max-height: 120px;min-height: 120px;min-width: 500px;max-width: 500px;"></textarea></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Image</td>
                                                                    <td><input type='file' onchange="readURL1(this);" name="image" />
                                                                        <img id="blah1" src="#" width="150" height="70" />       
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Signature</td>
                                                                    <td><input type='file' onchange="readURL2(this);" name="signature" />
                                                                        <img id="blah2" src="#" width="150" height="70" />                  
                                                                    </td>
                                                                </tr>
							</tbody>
						</table>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var i = 1;
        $("#tb_add").click(function(){
            var att_php = "<?php  $sql=  mysql_query("select * from master_mainarea");
                                  while($data=  mysql_fetch_array($sql)){
                        echo "<option value='$data[id]'>$data[description] (Plant $data[id_plant])</option>";
                         }
                      ?>";
        var j = i++;
        var append = "  <div id='ap"+j+"'>\n\
                            <div style='margin-top: 10px;'>\n\
                                <select required name='area[]' class='span6' id='area_"+j+"'><option value=''>-select area-</option>"+att_php+"</select>\n\
                                <input type='button' value='X' onclick='aprem("+j+");' style='margin-top: -11px;' />\n\
                            </div>\n\
                        </div>";
        $("#add_area").append(append);
        });
    });
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function aprem(id){
       $("#ap"+id).remove();
    }
</script>