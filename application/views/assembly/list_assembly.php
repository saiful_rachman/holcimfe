<?php $this->load->view('includes/header.php') ?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner" style="padding-top:0.4%">
            <div style="margin-bottom: 0.7%;">
                <div style="width: 175px;height: 25px;background: red;border-radius: 5px;color: white;font-size: 25px;font-weight: bolder;text-align: center;padding-top: 5px;float: left;"><b><?=$hac->hac_code;?></b></div>
                <a href="<?=base_url()?>engine/crud_hac"><div style="width: 100px;height: 25px;background: #0066FF;border-radius: 5px;margin-left: 180px;padding-top: 5px;color: white;font-size: 17px;font-weight: bolder;text-align: center;"><i class="icon-list icon-white"></i><b>HAC List</b></div></a>
            </div>
                <div class="btn-group">
                    <a href="<?=base_url()?>engine/crud_assembly/add/<?=$id_hac;?>" class="btn btn-success">ADD</a>
                    <a href="<?=base_url()?>engine/crud_hac" class="btn btn-warning">BACK</a>
                </div>
			<div class="well">
				<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No</th>
						<th>Assembly Code</th>
                                                <th>Assembly Name</th>
                                                <th>Image</th>
						<th style="width: 250px;text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                $offset = $this->uri->segment(5);
                                $id= 1; 
                                if(count($data)==""){
                                    echo"<tr><td colspan='6' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                foreach($data as $row) :?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->assembly_code; ?></td>
						<td><?php echo $row->assembly_name;?></td>
                                                <td><img src="<?=base_url();?>media/images/<?php echo $row->image;?>" height="50" width="100"></td>
                                                <td style="text-align: center;">
                                                    <!--<a href="./add_vibration/edit/<?php echo $row->id; ?>" class="btn btn-warning">VIEW</a>-->
                                                    <a href="<?=base_url();?>engine/crud_component/index/<?php echo $row->id; ?>" class="btn btn-warning">Component</a> 
                                                    <a href="<?=base_url();?>engine/crud_assembly/edit/<?=$id_hac;?>/<?php echo $row->id; ?>" class="btn btn-warning">EDIT</a> 
                                                    <a href="<?=base_url();?>engine/crud_assembly/delete/<?=$id_hac;?>/<?php echo $row->id; ?>" class="btn btn-warning" onclick="return confirm('Are you sure to delete?')">DELETE</a> 
                                                </td>
						
					</tr>
					<?php endforeach; ?>
                                         <tr>
                                            <td></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/crud_assembly/index/<?=$id_hac;?>" id="fassembly_code"><input type="text" style="width: 200px;" onkeyup="javascript:if(event.keyCode == 13){coba('assembly_code');}else{return false;};" name="val" /><input type="hidden" name="field" value="assembly_code"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/crud_assembly/index/<?=$id_hac;?>" id="fassembly_name">
                                                    <input type="text" style="width: 200px;" onkeyup="javascript:if(event.keyCode == 13){coba('assembly_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="assembly_name">
                                                </form>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
    <script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>