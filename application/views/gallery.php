<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
    color: blue;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
	text-decoration: underline;
}
</style>
</head>
<body>
	<?php include ("includes/header.php"); ?>
<div id="content">
	<div class="inner">
		<div class="span12">
		<br />

		<?php echo $output; ?>
		<br />
		<a class="btn" href="<?php echo base_url()?>main/view_about">Back</a>
    </div>   
</div>   
</div>
</body>
</html>
