<?php include "includes/header.php"; ?>

<div id="content">
<div class="inner">
	<div class="row-fluid">
		<div class="form-horizontal">
			<div class="span12" style="padding-top:2%">
				<h3><center><?php echo $users['nama'] ?></center></h3>
				<?php echo form_open_multipart('main/update_profile/'); ?>
					<div class="span6">
						<div class="control-group">
							<label class="control-label">Title</label>
							<div class="controls">	
								<input type="hidden" name="id" value="<?=$default['id'];?>"/>
								<input type="text" name="title"  value="<?=$default['title'];?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Name</label>
							<div class="controls">	
								<input type="text" name="nama"  value="<?=$default['nama'];?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">NIP</label>
							<div class="controls">	
                                <input type="text" name="nip"  value="<?=$default['nip'];?>"/>
                            </div>
						</div>
						<div class="control-group">
							<label class="control-label">Password</label>
							<div class="controls">	
								<input type="password" name="password" value="<?php echo $this->encrypt->decode($default['password']);?>" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Email</label>
							<div class="controls">	
								<input type="email" name="email" value="<?=$default['email'];?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Phone</label>
							<div class="controls">	
								<input type="text" name="phone" value="<?=$default['phone'];?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Position</label>
							<div class="controls">	
								<input type="text" name="jabatanx" value="<?=$default['jabatanx'];?>" readonly="readonly"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Department</label>
							<div class="controls">	
								<input type="text" name="department" value="<?=$default['department'];?>" readonly="readonly"/>
							</div>
						</div>
                        <div class="control-group">
							<label class="control-label">Area</label>
							<div class="controls">	
								<input type="text" name="area" value="<?=$default['areax'];?>" readonly="readonly"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Skills</label>
							<div class="controls">	
								<textarea name="skills" colspan="40"><?=$default['skills'];?></textarea>
							</div>
						</div>
						<br/>
						<br/>
						<br/>
					</div>
					<div class="span6">	
						<div class="control-group">
							<label class="control-label">Photo</label>
								<div class="controls">	
									<input type="file" class="btn" name="photo" onchange="readURL1(this);"/> 
									<input type="hidden" name="upload_photo" value="<?=$default['photo'];?>"/>
                                    <img id="blah1" src="<?php echo base_url();?>media/images/<?=$default['photo'];?>" style="height:200px;width: 300px"/>
								</div>
						</div>
						<div class="control-group">
							<label class="control-label">Signature</label>
							<div class="controls">	
								<input type="file" class="btn" name="signature" id="signature" onchange="readURL2(this);"/>
								<input type="hidden" name="upload_signature" value="<?=$default['signature'];?>"/>
                                <img id="blah2" src="<?php echo base_url();?>media/images/<?=$default['signature'];?>" style="height:200px;width: 300px"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label"></label>
							<div class="controls">	
								<button type="submit" class="btn btn-info">Update</button>
								<a class="btn btn-warning" href="<?php echo base_url();?>main/index">Back</a>
							</div>
						</div>
                         <br /> <br />
					</div>
                   
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(300)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(300)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<?php include "includes/footer.php"; ?>