<?php $this->load->view('includes/header.php') ?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner" style="padding-top:2%">
		<h2>Main Area Management</h2>
			<div class="btn-group">
				<a href="<?=base_url()?>engine/crud_mainarea/add" class="btn btn-success">ADD</a>
             	<a href="<?=base_url()?>engine/crud_plant" class="btn btn-warning">BACK</a>
			</div>
			<div class="well">
				<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>No</th>
                                                <th style="width: 60px;">Plant Name</th>
                                                <th style="width: 60px;">Main Area Code</th>
                                                <th>Description</th>
                                                <th style="text-align: center;width: 200px;">Image</th>
						<th style="width: 150px;text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                $offset = $this->uri->segment(4);
                                $id= 1; 
                                if(count($data)==""){
                                    echo"<tr><td colspan='6' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                foreach($data as $row) :?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->plant_name; ?></td>
                                                <td><?php echo $row->main_area_name; ?></td>
                                                <td><?php echo $row->description; ?></td>
                                                <td style="text-align: center;"><img src="<?=base_url();?>media/images/<?php echo $row->image_main_area; ?>" width="50px" height="30px"/></td>
                                                <td style="text-align: center;">
                                                    <a href="<?=base_url();?>engine/crud_mainarea/edit/<?php echo $row->id; ?>" class="btn btn-warning">EDIT</a> 
                                                    <a href="<?=base_url();?>engine/crud_mainarea/delete/<?php echo $row->id; ?>" class="btn btn-warning" onclick="return confirm('Are you sure to delete?')">DELETE</a> 
                                                </td>
						
					</tr>
					<?php endforeach; ?>
                                         <tr>
                                            <td></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/crud_mainarea/index" id="fplant_name"><input type="text" style="width: 140px;" onkeyup="javascript:if(event.keyCode == 13){coba('plant_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="plant_name"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/crud_mainarea/index" id="fmain_area_name"><input type="text" style="width: 140px;" onkeyup="javascript:if(event.keyCode == 13){coba('main_area_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="main_area_name"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/crud_mainarea/index" id="fdescription"><input type="text" style="width: 140px;" onkeyup="javascript:if(event.keyCode == 13){coba('description');}else{return false;};" name="val" /><input type="hidden" name="field" value="description"></form></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
    <script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>