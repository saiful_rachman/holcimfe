<?php $this->load->view("includes/header.php"); ?>
<form method="post" id="form" action="<?php echo base_url(); ?>engine/form_manager/simpan_detail_running3">
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update Form Wizard</h2>
					<h4>Running Detail Inspection Form <span class="pull-right">STEP 2</</span></h4>
					<div class="well well-small">
                                            <table class="table" style="text-transform: capitalize;">
							<thead>	
								<tr>
									<td width="200px">Area</td><input type="hidden" name="idx" value="<?php echo $this->uri->segment(4); ?>" />
                                                                        <td><?php echo $data_1stform->area_name; ?><input type="hidden" name="form_id1" value="<?php echo $data_1stform->id; ?>"></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td>
                                                                            <?php 
                                                                            $freq = $data_1stform->frequency; 
                                                                            $dt=mysql_fetch_assoc(mysql_query("select * from master_frequency where id='$freq'"));
                                                                            echo $dt['frequency'];
                                                                            ?>
                                                                        </td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><?php echo $data_1stform->mechanichal_type; ?></td>
								</tr>
							</tbody>
						</table>
                                            <h4>Component List</h4>
                                            <table class="table table-bordered">
							<tbody>	
								<tr class="success">
                                                                    <td><strong>HAC</strong></td><input type="hidden" name="id" value="<?php echo $data_2ndform->id; ?>">
									<td width=378px"><strong>COMPONENT<strong></td>
                                                                                    <td width=90px">
                                                                                        <span class="pull-right">
                                                                                            <a class="btn btn-info" onclick="add(<?php echo $data_2ndform->id; ?>)"><i class="icon-plus icon-white"></i></a>&nbsp;
                                                                                            <a class="btn btn-info" onclick="rem(<?php echo $data_2ndform->id; ?>)"><i class="icon-minus icon-white"></i></a>
                                                                                        </span>
                                                                                    </td>
								</tr>
                                                                <tr>
                                                                    <td style="width: 446px;"><strong><?php echo $data_2ndform->hac_code; ?></strong></td>
                                                                    <td><strong><?php echo $data_2ndform->assembly_name; ?></strong></td>
                                                                    
                                                                                    <input type='hidden' name='hc' value='<?php echo $data_2ndform->hac; ?>'>
                                                                                    <input type='hidden' name='com' value='<?php echo $data_2ndform->component; ?>'>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <table  class="table table-bordered" align="center">
                                                                            <tbody id="<?php echo "listing".$data_2ndform->id; ?>">
                                                                            <tr>
                                                                                <th style="text-align: center;">Inspection Type</th>
                                                                                <th style="text-align: center;">Value</th>
                                                                                <th style="text-align: center;">Vibra</th>
                                                                            </tr>
                                                                            <?php foreach ($value as $valdata){ ?>
                                                                            <tr>
                                                                                <td style="text-align: center;"><input type="text" name='inspection[]' class='span12' value="<?php echo $valdata->inspection_activity; ?>" required></td>
                                                                                <td style="text-align: center;"><input type="text" name='value[]' class='span12' value="<?php echo $valdata->target_value; ?>" required>
                                                                                    <input type='hidden' name='idr[]' value='<?php echo $valdata->id; ?>'><input type='hidden' name='status'  value='R'>
                                                                                </td>
                                                                                <td style="text-align: center;">
                                                                                    <select name="vibra[]" class="span12">
                                                                                        <?php 
                                                                                            if($valdata->vibration_check=="0"){
                                                                                                $nol="selected";
                                                                                            }else{
                                                                                                $nol="";
                                                                                            }
                                                                                            if($valdata->vibration_check=="1"){
                                                                                                $satu="selected";
                                                                                            }else{
                                                                                                $satu="";
                                                                                            }
                                                                                            if($valdata->vibration_check=="2"){
                                                                                                $dua="selected";
                                                                                            }else{
                                                                                                $dua="";
                                                                                            }
                                                                                            if($valdata->vibration_check=="3"){
                                                                                                $tiga="selected";
                                                                                            }else{
                                                                                                $tiga="";
                                                                                            }
                                                                                            
                                                                                        ?>
                                                                                        <option value="0" <?php echo $nol;?>> - </option>
                                                                                        <option value="1" <?php echo $satu;?>> Vibration </option>
                                                                                        <option value="2" <?php echo $dua;?>> Temperature </option>
                                                                                        <option value="3" <?php echo $tiga;?>> Pressure </option>
                                                                                    </select>
                                                                                </td>
                                                                                <td style="width: 25px;"><input type='button' value='X' onClick="$(this).parent().parent().remove();"></td>
                                                                            </tr>
                                                                            <?php } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
							</tbody>
						</table>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
    var i = 0;
function add(id){
    //alert(id);
    //$("#listing"+id).hide();
        var data_list = "<tr><td><input type='text' name='inspection[]' class='span12' required></td><td><input type='text' name='value[]' class='span12' required/><input type='hidden' name='idr[]' value='"+id+"'></td><td><select name='vibra[]' class='span12'><option value='0'> - </option><option value='1'> Vibration </option><option value='2'> Temperature </option><option value='3'> Pressure </option></select></td><td><input type='button' value='X' onClick='$(this).parent().parent().remove();'></td></tr>";
	$("#listing"+id).append(data_list);
}
function rem(idx){
    var rowCount = $("#listing"+idx+" tr").length;
    if(rowCount <= 2){
        alert('Cannot remove again');
    }else{
        $("#listing"+idx+" tr:last-child").remove();
    }
}
function del(idx){
     var rowCount = $("#listing"+idx+" tr").length;
    if(rowCount <= 2){
        alert('Cannot remove again');
    }else{
    $(this).parent().parent().remove();
    }
}
 $('#form').submit(function(){
     alert('Data has been Update !');
    }); 
</script>