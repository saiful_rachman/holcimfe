<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<script type="text/javascript">
$(document).ready(function(){
$(function() {

        
         
});
});
function coba(id){
   function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }
    var subarea = $("#areaname").val();
        var plant = $("#plant").val();
    $("#txtinput"+id)
            // don't navigate away from the field on tab when selecting an item
              .bind( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunctionx/"+subarea+"/"+plant,{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
            
            $("#txtinput"+id).focusout(function (){
             var kelas_id = $(this).val();
            $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
               data : "id="+kelas_id,
               success: function(data){
                   $(".matapelajaran_id"+id).html(data);
               }
});
               $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_haxx",
               data : "id="+kelas_id,
               success: function(data){
                   $("#hx"+id).val(data);
               }
            });
});
            
}
</script>
<form method="post" action="form_manager/simpan_step1">
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Create Form Wizard</h2>
					<h4>Running Inspection Form <span class="pull-right">STEP 1</</span></h4>
					<div class="well well-small">
                                            <table class="table">
							<thead>	
                                                                <tr>
                                                                    <td width="200px">Plant Name</td>
                                                                    <td>
                                                                        <select name="area" class="span6" required id="plant">
                                                                            <option value="">-Select Plant-</option>
                                                                        <?php
                                                                            foreach ($list_plant as $plant){
                                                                        ?>
                                                                            <option value="<?=$plant->id;?>"><?=$plant->plant_name;?></option>
                                                                        <?php } ?>
                                                                        </select>
                                                                    </td>
								</tr>	
                                                                <tr>
                                                                    <td>Form Name</td>
                                                                    <td><input type="text" name="form_name"  class="span6" required/></td>
								</tr>
<!--                                                                <tr>
                                                                    <td width="200px">Area Name</td>
                                                                    <td>
                                                                        <select name="area" class="span6" required id="area"><input type="hidden" id="areaname">
                                                                        </select>
                                                                    </td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Sub Area Name</td>
                                                                    <td>
                                                                        <select name="subarea" class="span6" required id="subarea">
                                                                        </select>
                                                                    </td>
								</tr>-->
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td><select name="frequency" required class="span6">
                                                                                <option value="">- Select Frequency -</option>
                                                                                        <?php  foreach ($frequency as $data){
                                                                                                echo "<option value='$data->id' style='text-transform: capitalize;'>$data->frequency</option>";
                                                                                                }
                                                                                         ?>";
										</select>
									</td>
								</tr>
								<tr>
									<td>Type</td>
									<td><input type="text" name="mechanical_type"  class="span6" required/></td>
								</tr>
                                                                 <tr>
									<td>Periode</td>
                                                                        <td><select name="periode" required class="span6">
                                                                                <option value="">- Select Periode -</option>
                                                                                        <?php  foreach ($periode as $data){
                                                                                                echo "<option value='$data->id' style='text-transform: capitalize;'>$data->periode</option>";
                                                                                                }
                                                                                         ?>";
										</select>
									</td>
								</tr>
                                                                <!--<tr>
									<td>Publish</td>
                                                                        <td>
                                                                            <select name="publish" required >
                                                                                <option value="">-select publish-</option>
                                                                                <option value="pub">Yes</option>
                                                                                <option value="N">No</option>
                                                                            </select>
                                                                        </td>
								</tr>-->
							</tbody>
						</table>
                                            <h4><span class="pull-right"><a id="add_listing" class="btn btn-warning" style="margin-bottom: 10px;">Add Hac</a></span></h4>
                                                    <table class="table table-bordered" id="tablexx" id="tablexx">
							<tbody id="listing"> 
                                                            <tr>
                                                                <td>
                                                                    <strong>HAC</strong> <input name="frequencyx[]" type="text" id="txtinput1" onkeypress="coba(1)" class="span12 hacInput" style="text-transform: uppercase;" required>
                                                                    Equipment Name <input class="equipmentName" name="equipment_name[]" type="text" required>
                                                                    <span class="pull-right"><a onclick="addlist(1)"class="btn btn-info assembly-1" id="addassy">Add Assembly</a></span>
                                                                    <table style="width: 100%;">
                                                                        <tbody id="list1">
                                                                            
                                                                         </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
							</tbody>
						</table>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
$(document).ready(function(){
    //$("#add_listing").attr('disabled', true);
    //$("#addassy").attr('disabled', true);
    var i = 2;
$('#add_listing').click(function() {
        var j = i++;   
	var x = parseInt($(this).val()) + 1; 
	$(this).val(x);
        //var data_list = "<tr><td><input name='frequencyx[]' type='text' id='txtinput"+j+"' onkeypress='coba("+j+")' class='span12'></td><td width='20px'><input type='button' value='X' onClick='$(this).parent().parent().remove();'></td></tr>";
	var data_list="<tr id='subhac"+j+"' class='listingxxx'>\n\
                            <td>\n\
                                <input type='button' value='X' onClick='remsubhac("+j+");' style='margin-bottom: 5px;'> &nbsp;<strong>HAC</strong> <input name='frequencyx[]' type='text' id='txtinput"+j+"' onkeypress='coba("+j+")' class='span12 hacdel hacInput' style='text-transform: uppercase;' required>  Equipment Name <input class='equipmentName' name='equipment_name[]' type='text' required>\n\
                                <span class='pull-right'><a id='add_listing' onclick='addlist("+j+");' class='btn btn-info assembly-"+j+"'>Add Assembly</a></span>\n\
                                    <table style='width: 100%;'>\n\
                                     <tbody id='list"+j+"'>\n\
									 </tbody>\n\
                                    </table>\n\
                            </td>\n\
                        </tr>";
        $("#listing").append(data_list);
        hacLoad();
        
});     
$("#rem_listing").click(function() {
    var rowCount = $('#listing tr').length;
    if(rowCount <= 1){
        alert('Row minimum 1');
    }else{
        $("#listing tr:last-child").remove();
    }
    });
    
    $("#plant").change(function(){
         var id = $("#plant").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area",
             data: "id="+id,
             success: function(data){
                 $("#area").html(data);
                 $(".listingxxx").remove();
                 $(".hacdel").val('');
             }
         });
      });
      
      $("#area").change(function(){
         var id = $("#area").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea",
             data: "id="+id,
             success: function(data){
                 $("#subarea").html(data);
                 $(".listingxxx").remove();
                 $(".hacdel").val('');
             }
         });
      });
      
      $("#subarea").change(function(){
     var id = $(this).val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_areaname",
         data: "id="+id,
         success: function(data){
             console.log(data);
             $("#areaname").val(data);
             $(".listingxxx").remove();
             $(".hacdel").val('');
         }
     });
      });
});
function addlist(id){
    var z = '.assembly-'+id;
    var zz = $(z).parent().parent();
    var zzz = zz.find('input.equipmentName').val();
    if(zzz != ''){
    var kelas_id = $("#txtinput"+id).val();
            $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
               data : "id="+kelas_id,
               success: function(data){
                   $(".matapelajaran_id"+id).html(data);
               }
            });
            $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_haxx",
               data : "id="+kelas_id,
               success: function(data){
                   $(".hx"+id).val(data);
                   var c = $(".hx"+id).parent().parent().parent().parent().parent();
                   c = c.find('.equipmentName').val();
                   $(".en"+id).val(c);
               }
            });
    var listd = "<tr class='listingxxx'>\n\
                            <td><select required name='areax[]' style='width: 100%' class='matapelajaran_id"+id+"' ><option value=''>-</option></select></td>\n\
                            <td width='20px'><input type='button' value='X' onClick='$(this).parent().parent().remove();'><input type='hidden' name='hx[]' class='hx"+id+"'><input type='hidden' name='en[]' class='en"+id+"'></td>\n\
                          </tr>";
    
   $("#list"+id).append(listd);
   }
   else{
       alert('Please input the Equipment Name');
       //zzz.focus();
   }
}
function remsubhac(id){
    $("#subhac"+id).remove();
}
</script>





<script>
    function hacLoad(){
    /*$('.hacInput').focusout(function(){
        var n = $('.hacInput').toArray();
        var nn = $('.hacInput').toArray().length;
        var c = $(this).val();
        var xg = 0;
        for(i=0; i<nn; i++){
            var xu = n[i].value;
            xu = xu.toLowerCase();
            c = c.toLowerCase();
            if(c == xu){
                xg += 1;
            }
        }
        if(xg > 1){
            $(this).val('');
            alert('Double HAC is not allowed');
        }
        else{
            sss = '';
        }
    });*/
    }
</script>