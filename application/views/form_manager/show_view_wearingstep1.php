<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<script type="text/javascript">
$(document).ready(function(){
$(function() {

  function split( val ) {
            return val.split( /,\s*/ );
    }
            function extractLast( term ) {
             return split( term ).pop();
    }

$("#txtinput")
        // don't navigate away from the field on tab when selecting an item
          .bind( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).data( "autocomplete" ).menu.active ) {
                event.preventDefault();
            }
        })
        .autocomplete({
            source: function( request, response ) {
                $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunction",{  //Url of controller
                    term: extractLast( request.term )
                },response );
            },
            search: function() {
                // custom minLength
                var term = extractLast( this.value );
                if ( term.length < 1 ) {
                    return false;
                }
            },
            focus: function() {
                // prevent value inserted on focus
                return false;
            },
            select: function( event, ui ) {
                var terms = split( this.value );
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push( ui.item.value );
                // add placeholder to get the comma-and-space at the end
                terms.push( "" );
                this.value = terms.join( "" );
                return false;
            }
        });   

});
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(150)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
$(function() {
            $( ".datepicker" ).datepicker({
                    dateFormat: "yy-mm-dd"
            });
    });           
</script>
<form method="post" id="form" action="<?php echo base_url() ;?>engine/form_manager/simpan_edit_wearingstop1" enctype="multipart/form-data">
<div id="main">
    <div id="content">
            <div class="inner">	
                    <div class="row-fluid">
                            <div class="span12">
                                    <h2>Create Form Wizard</h2>
                                    <h4>Wear Measuring Form <span class="pull-right"></</span></h4>
                                    <div class="well well-small">
                                        <table class="table">
                                            <thead>	<input type="hidden" name="id" value="<?php echo $detail->id; $form=$detail->id; ?>" />
                                                            <tr>
                                                                    <td width="200px">HAC</td>
                                                                    <td>
                                                                        <input type="text" name="hac" id="txtinput" required value="<?php echo $detail->hac_code; ?>" readonly />
                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                    <td width="200px">AREA</td>
                                                                    <td><select name="area" required class="span6" disabled>
                                                                            <option value="">- Select AREA -</option>
                                                                                    <?php
                                                                                    foreach ($area as $data){  
                                                                                    if($detail->area == $data->id){
                                                                                        $cek="selected";
                                                                                    }else{
                                                                                        $cek="";
                                                                                    }
                                                                                        echo "<option value='$data->id' $cek>$data->area_name</option>";
                                                                                    }
                                                                                     ?>";
                                                                            </select>
                                                                    </td>
                                                            </tr>
                                                    </thead>	
                                                    <tbody>	
                                                        <tr style="display: none;">
                                                                    <td>Measurement Number</td>
                                                                    <td><input type="text" name="meas_no" class="span6" required value="<?php echo $detail->meas_no; ?>" readonly/></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Material</td>
                                                                    <td><input type="text" name="material"  class="span6" required value="<?php echo $detail->material; ?>" readonly/></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Date</td>
                                                                    <td><input type="text" name="date" class="datepicker"  class="span6" required value="<?php echo $detail->create_date; ?>" readonly /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Instalation Date</td>
                                                                    <td><input type="text" name="instalation_date" class="datepicker"  class="span6" required value="<?php echo $detail->instalation_date; ?>" readonly /></td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Measurement Point</td>
                                                                    <td>
                                                                        <select name="value" required disabled>
                                                                        <?php for($i=1;$i<=20;$i++){
                                                                            if($detail->point == $i){
                                                                                        $cek="selected";
                                                                                    }else{
                                                                                        $cek="";
                                                                                    }
                                                                            echo"<option value='$i' $cek> $i </option>"; 
                                                                        }
                                                                        ?>
                                                                        </select>
                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                    <td>Image</td>
                                                                    <td><input type='file' onchange="readURL(this);" name="image" disabled/>
                                                                    <img id="blah" src="<?php echo base_url(); ?>/media/images/<?php echo $detail->gambar; ?>" width="150" height="70" /></td>
                                                            </tr>
                                                    </tbody>
                                            </table>
                                            <table border="1">
                                                <tbody id="listingx">
                                                <tr>
                                                    <td colspan="2">Measurement Point</td><input type="hidden" name="form_id" value="<?php echo $detail->id; ?>">
                                                    <?php
                                                    for($i=1;$i<=20;$i++){
                                                        echo "<td width='30px' align='center'>$i</td>";
                                                    }
                                                    ?>
                                                </tr>
                                                <tr style="text-align: center;">
                                                    <td colspan="2">Distance (mm) from Big Diameter of Roller</td>
                                                    <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td>
                                                    <td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
                                                    <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td>
                                                    <td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
                                                </tr>
                                                <?php 
                                                    for($i=0;$i<$detail->roller;$i++){
                                                        $x1=$i+1;
                                                ?>
                                                <tr style='text-align: center;'>
                                                    <td><b>Roller <?php echo $i+1; ?></b><br/>0 - Point Measuring</td>
                                                    <td><?php echo date("Y-m-d");?><input type="hidden" name="roller[]" value="<?php echo $i+1;?>"></td>
                                                    <?php
                                                        $sqlx=  mysql_query("select * from roller where form_id='$form' and no_roller='$x1'");
                                                        $nox=1;
                                                            while($datax= mysql_fetch_array($sqlx)){
                                                            echo"<td><input type='text' name='rol1[]' style='width:23px;' value='$datax[val1]' readonly>";
                                                            echo"<td><input type='text' name='rol2[]' style='width:23px;' value='$datax[val2]' readonly>";
                                                            echo"<td><input type='text' name='rol3[]' style='width:23px;' value='$datax[val3]' readonly>";
                                                            echo"<td><input type='text' name='rol4[]' style='width:23px;' value='$datax[val4]' readonly>";
                                                            echo"<td><input type='text' name='rol5[]' style='width:23px;' value='$datax[val5]' readonly>";
                                                            echo"<td><input type='text' name='rol6[]' style='width:23px;' value='$datax[val6]' readonly>";
                                                            echo"<td><input type='text' name='rol7[]' style='width:23px;' value='$datax[val7]' readonly>";
                                                            echo"<td><input type='text' name='rol8[]' style='width:23px;' value='$datax[val8]' readonly>";
                                                            echo"<td><input type='text' name='rol9[]' style='width:23px;' value='$datax[val9]' readonly>";
                                                            echo"<td><input type='text' name='rol10[]' style='width:23px;' value='$datax[val10]' readonly>";
                                                            echo"<td><input type='text' name='rol11[]' style='width:23px;' value='$datax[val11]' readonly>";
                                                            echo"<td><input type='text' name='rol12[]' style='width:23px;' value='$datax[val12]' readonly>";
                                                            echo"<td><input type='text' name='rol13[]' style='width:23px;' value='$datax[val13]' readonly>";
                                                            echo"<td><input type='text' name='rol14[]' style='width:23px;' value='$datax[val14]' readonly>";
                                                            echo"<td><input type='text' name='rol15[]' style='width:23px;' value='$datax[val15]' readonly>";
                                                            echo"<td><input type='text' name='rol16[]' style='width:23px;' value='$datax[val16]' readonly>";
                                                            echo"<td><input type='text' name='rol17[]' style='width:23px;' value='$datax[val17]' readonly>";
                                                            echo"<td><input type='text' name='rol18[]' style='width:23px;' value='$datax[val18]' readonly>";
                                                            echo"<td><input type='text' name='rol19[]' style='width:23px;' value='$datax[val19]' readonly>";
                                                            echo"<td><input type='text' name='rol20[]' style='width:23px;' value='$datax[val20]' readonly>";
                                                            $nox++;
                                                        }
                                                    ?>
                                                    <?php } ?>
                                                <tr><td colspan="30">&nbsp;</tr>
                                                 <tr>
                                                    <td colspan="2">Measurement Point</td>
                                                    <?php
                                                    for($i=1;$i<=20;$i++){
                                                        echo "<td width='30px' align='center'>$i</td>";
                                                    }
                                                    ?>
                                                </tr>
                                                <tr style="text-align: center;">
                                                    <td colspan="2">Distance (mm) from Big Diameter of Roller</td>
                                                    <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td>
                                                    <td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
                                                    <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td>
                                                    <td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
                                                </tr>
                                                <?php 
                                                    for($i=0;$i<$detail->grinding;$i++){
                                                        $x2=$i+1;
                                                ?>
                                                <tr><td colspan="30" style="background-color: #44688C;color: white;"><b>Grinding <?php echo $i+1; ?></b></tr>
                                                <tr style='text-align: center;'>
                                                    <td>0 - Point Measuring</td>
                                                    <td><?php echo date("Y-m-d");?><input type="hidden" name="grinding1" value="1"><input type="hidden" name="no_urut[]" value="<?php echo $i+1; ?>"></td>
                                                    <?php
                                                        $sqlx=  mysql_query("select * from grinding where form_id='$form' and no_grinding='1' and no_urut='$x2'");
                                                        $nox=1;
                                                            while($datax= mysql_fetch_array($sqlx)){
                                                            echo"<td><input type='text' name='1a[]' style='width:23px;' value='$datax[val1]' readonly>";
                                                            echo"<td><input type='text' name='2a[]' style='width:23px;' value='$datax[val2]' readonly>";
                                                            echo"<td><input type='text' name='3a[]' style='width:23px;' value='$datax[val3]' readonly>";
                                                            echo"<td><input type='text' name='4a[]' style='width:23px;' value='$datax[val4]' readonly>";
                                                            echo"<td><input type='text' name='5a[]' style='width:23px;' value='$datax[val5]' readonly>";
                                                            echo"<td><input type='text' name='6a[]' style='width:23px;' value='$datax[val6]' readonly>";
                                                            echo"<td><input type='text' name='7a[]' style='width:23px;' value='$datax[val7]' readonly>";
                                                            echo"<td><input type='text' name='8a[]' style='width:23px;' value='$datax[val8]' readonly>";
                                                            echo"<td><input type='text' name='9a[]' style='width:23px;' value='$datax[val9]' readonly>";
                                                            echo"<td><input type='text' name='10a[]' style='width:23px;' value='$datax[val10]' readonly>";
                                                            echo"<td><input type='text' name='11a[]' style='width:23px;' value='$datax[val11]' readonly>";
                                                            echo"<td><input type='text' name='12a[]' style='width:23px;' value='$datax[val12]' readonly>";
                                                            echo"<td><input type='text' name='13a[]' style='width:23px;' value='$datax[val13]' readonly>";
                                                            echo"<td><input type='text' name='14a[]' style='width:23px;' value='$datax[val14]' readonly>";
                                                            echo"<td><input type='text' name='15a[]' style='width:23px;' value='$datax[val15]' readonly>";
                                                            echo"<td><input type='text' name='16a[]' style='width:23px;' value='$datax[val16]' readonly>";
                                                            echo"<td><input type='text' name='17a[]' style='width:23px;' value='$datax[val17]' readonly>";
                                                            echo"<td><input type='text' name='18a[]' style='width:23px;' value='$datax[val18]' readonly>";
                                                            echo"<td><input type='text' name='19a[]' style='width:23px;' value='$datax[val19]' readonly>";
                                                            echo"<td><input type='text' name='20a[]' style='width:23px;' value='$datax[val20]' readonly>";
                                                            $nox++;
                                                        }
                                                    ?>
                                                </tr>
                                                 <tr style='text-align: center;'>
                                                    <td>0 - Point Measuring</td>
                                                    <td><?php echo date("Y-m-d");?><input type="hidden" name="grinding2" value="2"></td>
                                                    <?php
                                                        $sqlx=  mysql_query("select * from grinding where form_id='$form' and no_grinding='2' and no_urut='$x2'");
                                                        $nox=1;
                                                            while($datax= mysql_fetch_array($sqlx)){
                                                            echo"<td><input type='text' name='1b[]' style='width:23px;' value='$datax[val1]' readonly>";
                                                            echo"<td><input type='text' name='2b[]' style='width:23px;' value='$datax[val2]' readonly>";
                                                            echo"<td><input type='text' name='3b[]' style='width:23px;' value='$datax[val3]' readonly>";
                                                            echo"<td><input type='text' name='4b[]' style='width:23px;' value='$datax[val4]' readonly>";
                                                            echo"<td><input type='text' name='5b[]' style='width:23px;' value='$datax[val5]' readonly>";
                                                            echo"<td><input type='text' name='6b[]' style='width:23px;' value='$datax[val6]' readonly>";
                                                            echo"<td><input type='text' name='7b[]' style='width:23px;' value='$datax[val7]' readonly>";
                                                            echo"<td><input type='text' name='8b[]' style='width:23px;' value='$datax[val8]' readonly>";
                                                            echo"<td><input type='text' name='9b[]' style='width:23px;' value='$datax[val9]' readonly>";
                                                            echo"<td><input type='text' name='10b[]' style='width:23px;' value='$datax[val10]' readonly>";
                                                            echo"<td><input type='text' name='11b[]' style='width:23px;' value='$datax[val11]' readonly>";
                                                            echo"<td><input type='text' name='12b[]' style='width:23px;' value='$datax[val12]' readonly>";
                                                            echo"<td><input type='text' name='13b[]' style='width:23px;' value='$datax[val13]' readonly>";
                                                            echo"<td><input type='text' name='14b[]' style='width:23px;' value='$datax[val14]' readonly>";
                                                            echo"<td><input type='text' name='15b[]' style='width:23px;' value='$datax[val15]' readonly>";
                                                            echo"<td><input type='text' name='16b[]' style='width:23px;' value='$datax[val16]' readonly>";
                                                            echo"<td><input type='text' name='17b[]' style='width:23px;' value='$datax[val17]' readonly>";
                                                            echo"<td><input type='text' name='18b[]' style='width:23px;' value='$datax[val18]' readonly>";
                                                            echo"<td><input type='text' name='19b[]' style='width:23px;' value='$datax[val19]' readonly>";
                                                            echo"<td><input type='text' name='20b[]' style='width:23px;' value='$datax[val20]' readonly>";
                                                            $nox++;
                                                        }
                                                    ?>
                                                </tr>
                                                <tr style='text-align: center;'>
                                                    <td>0 - Point Measuring</td>
                                                    <td><?php echo date("Y-m-d");?><input type="hidden" name="grinding3" value="3"></td>
                                                    <?php
                                                        $sqlx=  mysql_query("select * from grinding where form_id='$form' and no_grinding='3' and no_urut='$x2'");
                                                        $nox=1;
                                                            while($datax= mysql_fetch_array($sqlx)){
                                                            echo"<td><input type='text' name='1c[]' style='width:23px;' value='$datax[val1]' readonly>";
                                                            echo"<td><input type='text' name='2c[]' style='width:23px;' value='$datax[val2]' readonly>";
                                                            echo"<td><input type='text' name='3c[]' style='width:23px;' value='$datax[val3]' readonly>";
                                                            echo"<td><input type='text' name='4c[]' style='width:23px;' value='$datax[val4]' readonly>";
                                                            echo"<td><input type='text' name='5c[]' style='width:23px;' value='$datax[val5]' readonly>";
                                                            echo"<td><input type='text' name='6c[]' style='width:23px;' value='$datax[val6]' readonly>";
                                                            echo"<td><input type='text' name='7c[]' style='width:23px;' value='$datax[val7]' readonly>";
                                                            echo"<td><input type='text' name='8c[]' style='width:23px;' value='$datax[val8]' readonly>";
                                                            echo"<td><input type='text' name='9c[]' style='width:23px;' value='$datax[val9]' readonly>";
                                                            echo"<td><input type='text' name='10c[]' style='width:23px;' value='$datax[val10]' readonly>";
                                                            echo"<td><input type='text' name='11c[]' style='width:23px;' value='$datax[val11]' readonly>";
                                                            echo"<td><input type='text' name='12c[]' style='width:23px;' value='$datax[val12]' readonly>";
                                                            echo"<td><input type='text' name='13c[]' style='width:23px;' value='$datax[val13]' readonly>";
                                                            echo"<td><input type='text' name='14c[]' style='width:23px;' value='$datax[val14]' readonly>";
                                                            echo"<td><input type='text' name='15c[]' style='width:23px;' value='$datax[val15]' readonly>";
                                                            echo"<td><input type='text' name='16c[]' style='width:23px;' value='$datax[val16]' readonly>";
                                                            echo"<td><input type='text' name='17c[]' style='width:23px;' value='$datax[val17]' readonly>";
                                                            echo"<td><input type='text' name='18c[]' style='width:23px;' value='$datax[val18]' readonly>";
                                                            echo"<td><input type='text' name='19c[]' style='width:23px;' value='$datax[val19]' readonly>";
                                                            echo"<td><input type='text' name='20c[]' style='width:23px;' value='$datax[val20]' readonly>";
                                                            $nox++;
                                                        }
                                                    ?>
                                                </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            <div>&nbsp;</div>
                                        <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Back</a>
                                    </div>
                                    <div class="spacer"></div>
                            </div>
                    </div>
            </div>
    </div>
</div>
</form>
<script type="text/javascript"> 
$('#form').submit(function(){
     alert('Data has been saved !');
    });
</script>
<?php $this->load->view("includes/footer.php"); ?>