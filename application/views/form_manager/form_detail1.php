<?php $this->load->view('includes/header.php') ?>
    <style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner" style="padding-top:2%">
		<h2>Form Running Management</h2>
			<div class="btn-group">
				<a href="<?=base_url()?>engine/form_manager" class="btn btn-success">Add</a>
             	
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
						<th>NO</th>
						<th>Form Name</th>
						<th>Frequency</th>
						<th>Mechanical Type</th>
						<th>Periode</th>
                                                <th style="text-align: center">No Publish</th>
                                                <th style="width: 300;text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                if(count($data)==""){
                                    echo"<tr><td colspan='7' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                $offset = $this->uri->segment(4);
                                $id= 1; foreach($data as $row) :?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->form_name; ?></td>
						<td><?php echo $row->frequency; ?></td>
						<td><?php echo $row->mechanichal_type; ?></td>
                                                <td><?php echo $row->periode; ?></td>
                                                <td style="text-align: center;"><?php echo $row->publish_order; ?></td>
						<td style="width: 305px;">
                                                    <button class="btn btn-warning" onclick="publish('publish_form_running/<?php echo $row->id; ?>');">Publish</button>
                                                    <a href="<?php echo base_url();?>engine/form_manager/edit_detailrunning1/<?php echo $row->id; ?>" class="btn btn-warning">Edit</a>
                                                    <a href="<?php echo base_url();?>engine/form_manager/form_detailrunning2/<?php echo $row->id; ?>" class="btn btn-warning">Sub</a>
                                                    <button onclick="deletex('delete_running/<?php echo $row->id; ?>');" class="btn btn-warning">Delete</button>
                                                    <a href="<?php echo base_url();?>report_excel/form_running/<?php echo $row->id; ?>" class="btn btn-warning">Print</a>
                                                </td>
						
					</tr>
					<?php endforeach; ?>
                                        <tr>
                                            <td></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/form_manager/form_detailrunning1" id="farea_name"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('area_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="b.area_name"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/form_manager/form_detailrunning1" id="ffrequency">
                                                    <select style="width: 70px;" onchange="cobax('frequency');" name="val" />
                                                    <option value="" selected> </option>
                                                    <option value="Weekly">Weekly</option>
                                                    <option value="Monthly">Monthly</option>
                                                    <option value="Yearly">Yearly</option>
                                                    </select>
                                                    <input type="hidden" name="field" value="c.frequency">
                                                </form>
                                            </td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/form_manager/form_detailrunning1" id="fmechanical_type"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('mechanical_type');}else{return false;};" name="val" /><input type="hidden" name="field" value="a.mechanichal_type"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/form_manager/form_detailrunning1" id="fperiode">
                                                    <select style="width: 70px;" onchange="cobax('periode');" name="val" />
                                                    <option value="" selected> </option>
                                                    <option value="Monday">Mon</option>
                                                    <option value="Tuesday">Tue</option>
                                                    <option value="Wednesday">Wed</option>
                                                    <option value="Thursday">Thu</option>
                                                    <option value="Friday">Fri</option>
                                                    </select>
                                                    <input type="hidden" name="field" value="d.periode">
                                                </form>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
			</div>
                        <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript">
$(document).ready(function () {
   
});

function publish(id){
   var tanya = confirm("Are you sure?");
        if(tanya){
            var url = "<?php echo base_url(); ?>engine/form_manager/"+id;
            window.location.replace(url);
        }else{
        }
}

function deletex(id){
   var tanya = confirm("Are you sure?");
        if(tanya){
            var url = "<?php echo base_url(); ?>engine/form_manager/"+id;
            window.location.replace(url);
        }else{
        }
}

</script>
<script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>