<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<script type="text/javascript">
//$(document).ready(function(){
function coba(){
function split( val ) {
                return val.split( /,\s*/ );
        }
                function extractLast( term ) {
                 return split( term ).pop();
        }
        var subarea = $("#areaname").val();
        var plant = $("#plant").val();
        $("#txtinput")
            // don't navigate away from the field on tab when selecting an item
              .focusout( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                        $( this ).data( "autocomplete" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "<?php echo base_url() ?>engine/form_manager/getFunctionx/"+subarea+"/"+plant,{  //Url of controller
                        term: extractLast( request.term )
                    },response );
                },
                search: function() {
                    // custom minLength
                    var term = extractLast( this.value );
                    if ( term.length < 1 ) {
                        return false;
                    }
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( "" );
                    return false;
                }
            });
         

}
</script>
<form method="post" action="<?php echo site_url();?>engine/form_manager/simpan_stop_step1" enctype="multipart/form-data" id="formx">
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Create Form Wizard</h2>
					<h4>Stop Inspection Form <span class="pull-right">STEP 1</</span></h4>
					<div class="well well-small">
                                            <table class="table">
							<thead>	
								<tr>
                                                                    <td width="200px">Plant Name</td>
                                                                    <td>
                                                                        <select name="area" class="span6" required id="plant">
                                                                            <option value="">-Select Plant-</option>
                                                                        <?php
                                                                            foreach ($list_plant as $plant){
                                                                        ?>
                                                                            <option value="<?=$plant->id;?>"><?=$plant->plant_name;?></option>
                                                                        <?php } ?>
                                                                        </select>
                                                                    </td>
								</tr>	
                                                                <tr>
                                                                    <td width="200px">Area Name</td>
                                                                    <td>
                                                                        <select name="area" class="span6" required id="area"><input type="hidden" id="areaname">
                                                                        </select>
                                                                    </td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Sub Area Name</td>
                                                                    <td>
                                                                        <select name="subarea" class="span6" required id="subarea">
                                                                        </select>
                                                                    </td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td>
                                                                            <select name="frequency" required class="span6">
                                                                                <option value="">- Select Frequency -</option>
                                                                                        <?php  foreach ($frequency as $data){
                                                                                                echo "<option value='$data->id' style='text-transform: capitalize;'>$data->frequency</option>";
                                                                                                }
                                                                                         ?>";
									   </select>
                                                                        </td>
								</tr>
								<tr>
									<td>Type</td>
									<td><input type="text" name="mechanical_type"  class="span6" required/></td>
								</tr>
                                                                <tr>
									<td>Form Type</td>
									<td>
                                                                            <select name="form_type" required>
                                                                                <option value=""> - </option>
                                                                                <option value="1M">1 M</option>
                                                                                <option value="3M">3 M</option>
                                                                                <option value="6M">6 M</option>
                                                                                <option value="1Y">1 Y</option>
                                                                            </select>
                                                                        </td>
								</tr>
                                                                <tr>
                                                                    <td>Image 1</td>
                                                                    <td><input type='file' onchange="readURL1(this);" name="image1" />
                                                                        <img id="blah1" src="#" />                      
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td>Image 2</td>
                                                                    <td><input type='file' onchange="readURL2(this);" name="image2" />
                                                                        <img id="blah2" src="#" />                      
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td>Image 3</td>
                                                                    <td><input type='file' onchange="readURL3(this);" name="image3" />
                                                                        <img id="blah3" src="#" />                      
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td>Image 4</td>
                                                                    <td><input type='file' onchange="readURL4(this);" name="image4" />
                                                                        <img id="blah4" src="#" />                      
                                                                    </td>
                                                                </tr>
                                                                <tr>
									<td>Periode</td>
                                                                        <td>
                                                                            <select name="periode" required >
                                                                                <option value="">-select periode-</option>
                                                                                <option value="jan">January</option>
                                                                                <option value="feb">February</option>
                                                                                <option value="mar">March</option>
                                                                                <option value="apr">April</option>
                                                                                <option value="may">May</option>
                                                                                <option value="jun">June</option>
                                                                                <option value="jul">July</option>
                                                                                <option value="aug">Augustus</option>
                                                                                <option value="sep">September</option>
                                                                                <option value="oct">October</option>
                                                                                <option value="nov">November</option>
                                                                                <option value="dec">December</option>
                                                                            </select>
                                                                        </td>
								</tr>
							</tbody>
						</table>
                                            <h4>Component List <span class="pull-right"><div style="display: none;"><a id="add_listing" class="btn btn-info"><i class="icon-plus icon-white"></i>Add</a>&nbsp;<a id="rem_listing" class="btn btn-info"><i class="icon-plus icon-white"></i>rem</a></span></div></h4>
						<table class="table table-bordered" id="tablexx" id="tablexx">
							<tbody id="listingx">	
								<tr class="success">
                                                                    <td colspan="5"><strong>HAC</strong></td>
                                                                    
								</tr>
                                                                <tr>
                                                                    <td width="500px" colspan="5">
                                                                        <input type="text" name="hac" id="txtinput" onkeypress="coba()" required class='span12'/>
                                                                    </td>
                                                                </tr>
                                                                <tr class="success">
                                                                    <td align="center">
                                                                        <strong>COMPONENT</strong>
                                                                    </td>
                                                                    <td align="center">
                                                                        <strong>ITEM CHECK</strong>
                                                                    </td>
                                                                    <td align="center">
                                                                        <strong>METHOD</strong>
                                                                    </td>
                                                                    <td align="center">
                                                                        <strong>STANDARD</strong>
                                                                    </td>
                                                                    <td align="center">
                                                                        <span class="pull-right"><a id="add_listingx" class="btn btn-info"><i class="icon-plus icon-white"></i></a></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <select required name='component[]' class="matapelajaran_id">
                                                                            <option value=""> please select hac before </option>
                                                                        </select>
                                                                    <td><input type="text" name="item_check[]" required></td>
                                                                    <td><input type="text" name="method[]" required></td>
                                                                    <td><input type="text" name="standard[]" required></td>
                                                                    </td>
                                                                </tr>
							</tbody>
						</table>
						<button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
$(document).ready(function(){
    var i = 0;
$('#add_listingx').click(function() {
        var j = i++;
        var data_list = "<tr>\n\
                            <td><select required name='component[]' class='matapelajaran_id' ><option value=''>-</option></select></td><td><input type='text' name='item_check[]' required></td><td><input type='text' name='method[]' required></td><td><input type='text' name='standard[]' required></td>\n\
                            <td width='20px'><input type='button' value='X' onClick='$(this).parent().parent().remove();'></td>\n\
                          </tr>";
        var kelas_id = $("#txtinput").val();
        $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
               data : "id="+kelas_id,
               success: function(data){
                   $(".matapelajaran_id").html(data);
               }
        });
	$("#listingx").append(data_list);
});
$("#rem_listing").click(function() {
    var rowCount = $('#listing tr').length;
    if(rowCount <= 2){
        alert('Row minimum 1');
    }else{
        $("#listing tr:last-child").remove();
    }
    });
    
    $("#plant").change(function(){
         var id = $("#plant").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area",
             data: "id="+id,
             success: function(data){
                 $("#area").html(data);
             }
         });
      });
      
      $("#area").change(function(){
         var id = $("#area").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea",
             data: "id="+id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
      });
      $("#subarea").change(function(){
     var id = $(this).val();
     $.ajax({
         type:'post',
         url:"<?=base_url();?>engine/crud_hac/get_areaname",
         data: "id="+id,
         success: function(data){
             console.log(data);
             $("#areaname").val(data);
         }
     });
           $("#txtinput").val('');
           //$("#listingx").remove();
      });
      
});
function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah3')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
 function readURL4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah4')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
$("#txtinput").focusout(function (){
   var kelas_id = $(this).val();
            $.ajax({
               type : "POST",
               url: "<?php echo base_url(); ?>engine/form_manager/get_chain",
               data : "id="+kelas_id,
               success: function(data){
                   $(".matapelajaran_id").html(data);
               }
});
});
$('#formx').submit(function(){
     alert('Data has been saved !');
    });
</script>		