<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="simpan_step2">
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Create Form Wizard</h2>
					<h4>Stop Detail Inspection Form <span class="pull-right">STEP 2</</span></h4>
					<div class="well well-small">
						<table class="table">
							<thead>	
								<tr>
									<td width="200px">AREA</td>
                                                                        <td><?php echo $data_1stform->area_name; ?><input type="hidden" name="form_id1" value="<?php echo $data_1stform->id; ?>"></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td><?php echo $data_1stform->frequency; ?></td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><?php echo $data_1stform->type; ?></td>
								</tr>
								<tr>
									<td>Form No.</td>
                                                                        <td><?php echo $data_1stform->form_number; ?><input name="status" type="hidden" value="S"/></td>
								</tr>
							</tbody>
						</table>
                                            <h4>Component List</h4>
                                            <?php foreach($data_2ndform as $data){ ?>
                                            <table class="table table-bordered">
							<tbody>	
								<tr class="success">
                                                                    <td><strong>HAC</strong></td><input type="hidden" name="id" value="<?php echo $data->id; ?>"><input type="hidden" name="hac[]" value="<?php echo $data->hac; ?>">
									<td width=378px"><strong>COMPONENT<strong></td>
                                                                                    <td width=90px">
                                                                                        <span class="pull-right">
                                                                                            <a class="btn btn-info" onclick="add(<?php echo $data->id; ?>)"><i class="icon-plus icon-white"></i></a>&nbsp;
                                                                                            <a class="btn btn-info" onclick="rem(<?php echo $data->id; ?>)"><i class="icon-minus icon-white"></i></a>
                                                                                        </span>
                                                                                    </td>
								</tr>
                                                                <tr>
                                                                    <td><strong><?php echo $data->hac_code; ?></strong></td>
                                                                    <td><strong><?php echo $data->component_code; ?></strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <table  class="table table-bordered" align="center">
                                                                            <tbody id="<?php echo "listing".$data->id; ?>">
                                                                            <tr>
                                                                                <th style="text-align: center;">Inspection Type</th>
                                                                                <th style="text-align: center;">Value</th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align: center;"><input type="text" name='inspection[]' class='span12'></td>
                                                                                <td style="text-align: center;"><input type="text" name='value[]' class='span12'><input type='hidden' name='idr[]' value='<?php echo $data->id; ?>'></td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
							</tbody>
						</table>
                                            <?php } ?>
						<button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" id="back"><i class="icon-backward icon-black"></i> Back</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
    var i = 0;
function add(id){
    //alert(id);
    //$("#listing"+id).hide();
        var data_list = "<tr><td><input type='text' name='inspection[]' class='span12'></td><td><input type='text' name='value[]' class='span12' /><input type='hidden' name='idr[]' value='"+id+"'></td></tr>";
	$("#listing"+id).append(data_list);
}
function rem(idx){
    var rowCount = $("#listing"+idx+" tr").length;
    if(rowCount <= 1){
        alert('Cannot remove again');
    }else{
        $("#listing"+idx+" tr:last-child").remove();
    }
}
$('#back').click(function(){
    $.ajax({
          type: "GET",
          url: "<?php echo base_url(); ?>engine/form_manager/delete_back_form_stop",
          success: function(response) {

          if (response == "Success")
          {
              window.history.back();
          }
          else
          {
              alert("Error");
          }

       }
    });
});
 $('#form').submit(function(){
     alert('Data has been saved !');
    });
</script>