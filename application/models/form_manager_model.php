<?php
error_reporting(0);
class Form_manager_model extends CI_model{
	function __construct()
	{
		parent::__construct();	
	}
    function log_activity($data){
        $this->db->insert("users_activity", $data);
    }
    function add_main($form_type, $hac, $upload_image){
        $query = $this->db->query("INSERT INTO `form_stop` VALUES (NULL, '$form_type', '$hac', '$upload_image')");
        
    }
	
    function add_detail($form_id, $part, $item_check, $methode, $standard, $checkbox){
        $query = $this->db->query("INSERT INTO `rel_component_to_form_stop` VALUES (NULL, '$form_id', '$part', '$item_check', '$methode', '$standard', '$checkbox')");
    }
	
	function get_by_id() {
        
        $this->db->query("select * from hac, hac_assembly hs where hac.hac_id=hs.assembly_hac_id");
        return $this->db->get();
    }
    
	
	function get_comp() {
        
        $this->db->query("select * from hac, hac_assembly hs where hac.hac_id=hs.assembly_hac_id");
        return $this->db->get();
    }
    
    function insert_trend($data) {
        $this->db->insert_batch("record_oil_analysis_trend", $data);
    }
    
    
    function get_max_trend() {
        
        $query = $this->db->query("SELECT MAX(id) as id FROM record_oil_analysis_trend");
        return $query;
    }
    
     function get_id_trend($id) {
        $this->db->select("*");
        $this->db->from("record_oil_analysis");
        $this->db->where("record_id", $id);
        return $this->db->get();
    } 
    
    function form_managerstep1($table,$data){
        $this->db->insert($table, $data);
    }
    
    function form_managerstep2($data){
        $this->db->insert("rel_activity_inspection", $data);
    }
    function formtop($data){
        $this->db->insert("form_running", $data);
        return true;
    }
    function formtopstop($data){
         $this->db->insert("form_stop", $data);
         return true;
    }
        function get_component(){
        $this->db->select('*');
        $this->db->from('hac_component');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_assembly(){
        $this->db->select('*');
        $this->db->from('hac_assembly');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_hac_assembly(){
        $this->db->select('*');
        $this->db->from('hac_assembly');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_area(){
        $this->db->select('*');
        $this->db->from('area');
        $this->db->order_by('area_name','ASC');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_area_detail($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id',$where);
        $query = $this->db->get();
        return $query->row();
    }
    
    function get_max_id($table){
        $this->db->select_max('id');
        $this->db->from($table);
        $query = $this->db->get();
        return $query->row('id');
    }
    
    function get_hac(){
        $this->db->select('*');
        $this->db->from('hac');
        $this->db->order_by('hac_code','ASC');
        $query = $this->db->get();
        return $query->result();
    }
    
     function getData($id,$term)
    {
        $sql = $this->db->query('select * from hac where hac_code like "'. mysql_real_escape_string($term) .'%" and area_id="'. mysql_real_escape_string($id) .'" order by hac_code asc limit 0,10');

    return $sql ->result();
        }
        
    function getDatax($id,$term)
    {
        $sql = $this->db->query('select * from hac where hac_code like "'. mysql_real_escape_string($term) .'%" and area_id="'. mysql_real_escape_string($id) .'" order by hac_code asc limit 0,10');

    return $sql ->result();
        }
        
     function get_topform($where,$table){
         $this->db->select('a.*,b.hac_code,equipment,c.assembly_name');
         $this->db->from($table.' a');
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('hac_assembly c','a.component=c.id');
         $this->db->where('form_id',$where);
         $this->db->group_by('a.id');
         $query = $this->db->get();
         return $query;
     }
     
     function get_topformstop($where,$table){
         $this->db->select('a.*,b.hac_code,equipment,c.assembly_name');
         $this->db->from($table.' a');
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('hac_assembly c','a.component=c.id');
         $this->db->where('form_id',$where);
         $query = $this->db->get();
         return $query;
     }
     
     function get_topformrun($where,$table){
         $this->db->select('a.*,b.hac_code,equipment,description,c.assembly_name');
         $this->db->from($table.' a');
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('hac_assembly c','a.component=c.id');
         $this->db->where('form_id',$where);
         $this->db->order_by('a.id','asc');
         //$this->db->group_by('a.hac');
         $query = $this->db->get();
         return $query;
     }
     
     function get_topformrun_print($where,$table){
         $this->db->select('a.*,b.hac_code,equipment,description,c.assembly_name');
         $this->db->from($table.' a');
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('hac_assembly c','a.component=c.id');
         $this->db->where('form_id',$where);
         $this->db->order_by('a.id','asc');
         $this->db->group_by('a.hac');
         $query = $this->db->get();
         return $query;
     }
     
     function get_topformxx($where,$table){
         $query = $this->db->query("SELECT DISTINCT rel_component_to_form_running.hac, hac.hac_code,hac.equipment 
                           from rel_component_to_form_running
                           LEFT JOIN hac on rel_component_to_form_running.hac = hac.hac_id
                           where rel_component_to_form_running.form_id = '$where'");
         return $query;
     }
     
     function get_topformxxx($where,$table){
         $this->db->select('a.*,c.component_code');
         $this->db->from('rel_component_to_form_running a');
         $this->db->join('hac_component c','a.component=c.id');
         $this->db->where('form_id',$where);
         $query = $this->db->get();
         return $query;
     }
     
     function get_topformedit($where,$table){
         $this->db->select('a.*,b.hac_code,c.assembly_name');
         $this->db->from($table.' a');
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('hac_assembly c','a.component=c.id');
         $this->db->where('a.id',$where);
         $query = $this->db->get();
         return $query;
     }
     function get_data_value($id){
         $this->db->select('*');
         $this->db->from('rel_activity_inspection');
         $this->db->where('rel_component_id',$id);
         $query = $this->db->get();
         return $query->result();
     }
         function get_topformx($where,$table){
         $this->db->select('a.*,b.hac_code,c.component_code');
         $this->db->from('rel_component_to_form_stop a');
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('hac_component c','a.component=c.id');
         $this->db->where('form_id',$where);
         $query = $this->db->get();
         return $query;
     }
     
      function get_topform1($where,$table){
         $this->db->select('a.*,b.area_name,c.hac_code,equipment,c.description as deskripsi');
         $this->db->from($table.' a');
         $this->db->join('area b','a.area=b.id','inner');
         $this->db->join('hac c','a.hac=c.hac_id','inner');
         $this->db->where('a.id',$where);
         $query = $this->db->get();
         return $query;
     }
     function get_topform1x($where,$table){
         $this->db->select('a.*,b.area_name,b.id as idx,c.frequency as frequency_name');
         $this->db->from($table.' a');
         $this->db->join('area b','a.area=b.id','left');
         $this->db->join('master_frequency c','a.frequency=c.id','left');
         $this->db->where('a.id',$where);
         $query = $this->db->get();
         return $query;
     }
     
     function get_topform1xx($where,$table){
         $this->db->select('a.*,b.area_name,c.frequency,d.periode');
         $this->db->from($table.' a');
         $this->db->join('area b','a.area=b.id','left');
         $this->db->join('master_frequency c','a.frequency=c.id','left');
         $this->db->join('master_periode d','a.periode=d.id','left');
         $this->db->where('a.id',$where);
         $query = $this->db->get();
         return $query;
     }
     
     function get_idform($id){
         $this->db->select('form_id');
         $this->db->from('rel_component_to_form_running');
         $this->db->where('id',$id);
         $query = $this->db->get();
         return $query->row('form_id');
     }
         function get_topform2($where,$table){
         $this->db->select('a.*,b.area_name');
         $this->db->from('form_stop a');
         $this->db->join('area b','a.area=b.id','left');
         $this->db->where('a.id',$where);
         $query = $this->db->get();
         return $query;
     }
     
     function formstop_managerstep1($dt2){
         $this->db->insert("rel_component_to_form_stop", $dt2);
     }
     
     function get_idhac($hac){
         $this->db->select('*');
         $this->db->from('hac');
         $this->db->where('hac_code',$hac);
         $query = $this->db->get();
         return $query->row('id');
     }
     
     function get_idhac2($hac){
         $this->db->select('*');
         $this->db->from('hac');
         $this->db->where('hac_code',$hac);
         $query = $this->db->get();
         return $query->result('id');
     }
     
     function get_idassembly($id){
         $query = $this->db->query("select * from hac_component where assembly_id in (select id from hac_assembly where assembly_hac_id = '$id')");
         return $query->result();
     }
     
     function get_idassembly2($id){
         $query = $this->db->query("select * from hac_assembly where assembly_hac_id = '$id'");
         return $query->result();
     }
     
     function simpan_detail_running1($table,$where,$data){
         $this->db->where('id',$where);
         $this->db->update($table,$data);
         return true;
     }
     
     function delete_rel($table,$id){
         $this->db->where('form_id',$id);
         $this->db->delete($table);
         return true;
     }
     
     function insert_temp($id){
        $del = $this->db->query("insert into rel_component_to_form_running (form_id,hac,hac_to_form_running_id,component) SELECT form_id,hac,hac_to_form_running_id,component from rel_component_to_form_running_temp where form_id ='$id'");
        //if($del){
        //$this->db->query("Delete from rel_component_to_form_running_temp where form_id='$id'");
        //}
     }
     
     function delete_rel_inspection($table,$id){
         $this->db->where('rel_component_id',$id);
         $this->db->delete($table);
         return true;
     }
     function delete_rel_inspection_stop($table,$id){
         $this->db->where('id',$id);
         $this->db->delete($table);
         return true;
     }
     function delete_rel_inspection2($table,$id){
         $this->db->where('form_id',$id);
         $this->db->delete($table);
         return true;
     }
     
     function insert_temp_inspection($id){
         $del = $this->db->query("insert into rel_activity_inspection (form_id,inspection_activity,target_value,rel_component_id,form_status,hac,component,vibration_check) SELECT form_id,inspection_activity,target_value,rel_component_id,form_status,hac,component,vibration_check from rel_activity_inspection_temp where rel_component_id ='$id'");
     }
     
     function delete_back_form_run($table,$where,$id){
         $this->db->where($where,$id);
         $this->db->delete($table);
         return true;
     }
     
     function update_detail_running2($data,$dt_form){
         $this->db->where('id',$dt_form);
         $this->db->update('rel_component_to_form_running',$data);
     }
     
     function delete_0(){
         $this->db->query("delete FROM `rel_component_to_form_running` where hac='0' and component='0'");
         $this->db->query("delete from rel_component_to_form_running where hac is NULL and component is Null");
     }
     
     function delete_edit_detail_running2($id){
         $this->db->where('id',$id);
         $this->db->delete('rel_component_to_form_running');
         
     }
     
     function simpan_detail_stop2($data,$id){
         $this->db->where('form_id',$id);
         $this->db->update('rel_component_to_form_stop',$data);
     }
     
     function get_formtype(){
         $this->db->select('*');
         $this->db->from('form_type');
         $query = $this->db->get();
         return $query;
     }
     function insert($table,$data){
         $this->db->insert($table,$data);
         return true;
     }
     
     function update($table,$data,$key_where){
         $this->db->where('id',$key_where);
         $this->db->update($table,$data);
         return true;
     }
     
     function delete($table,$where,$keywhere){
         $this->db->where($where,$keywhere);
         $this->db->delete($table);
         return true;
     }
     
     function select_all($table){
         $query = $this->db->get($table);
         return $query->result();
     }
     
     function select_all_where($table,$where,$keywhere){
         $this->db->select('*');
         $this->db->from($table);
         $this->db->where($where,$keywhere);
         $query = $this->db->get();
         return $query;
     }
     
     function get_detail_data($table,$where,$key_where){
         $this->db->select('*');
         $this->db->from($table);
         $this->db->where($key_where,$where);
         $query = $this->db->get();
         return $query;
     }
     
     function get_detail_relation1($table,$where){
         $this->db->select('a.*,b.hac_code');
         $this->db->from($table." a");
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->where("a.id",$where);
         $query = $this->db->get();
         return $query;
     }
     function update_inspection($data,$id_form,$com,$hac){
         $this->db->where('rel_component_id',$id_form);
         $this->db->where('hac',$hac);
         $this->db->where('component',$com);
         $this->db->update('rel_activity_inspection',$data);
     }
     function select_wearing($id){
         $this->db->select('a.*,b.hac_code,equipment,c.area_name');
         $this->db->from('form_measuring a');
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('area c','a.area=c.id','left');
         $this->db->where('a.id',$id);
         $query = $this->db->get();
         return $query;
     }
     
     function get_max_id_copy($table,$where,$id){
         $this->db->select_max('id');
         $this->db->from($table);
         //$this->db->where($where,$id);
         $query = $this->db->get();
         return $query->row('id');
     }
     function insert_copy_table_form_running($id,$maxid,$form_no,$jum){
        $query=$this->db->query("insert into form_running_copy SET
        form_number = '$form_no',
        inspection_type = 'RUN',
        form_running_id = '$id',
        area = (select area from form_running where id='$id'),
        form_name = (select form_name from form_running where id='$id'),
        periode = (select periode from form_running where id='$id'),
        frequency = (select frequency from form_running where id='$id'),
        mechanichal_type = (select mechanichal_type from form_running where id='$id'),     
        publish_order='$jum',
        id = '$maxid'");
      }
      
      function insert_copy_table_rel_component_to_form_running($maxid,$id){
        $this->db->select('*');
        $this->db->from('rel_component_to_form_running');
        $this->db->where('form_id',$id);
        $query = $this->db->get();
        $queryx = $query->result();
        foreach ($queryx as $key){
            
            $max_id_copy2 =  mysql_fetch_array(mysql_query("select max(id) as maxid from rel_component_to_form_running_copy where form_id='$maxid'"));
            if($max_id_copy2['maxid'] == "" || $max_id_copy2['maxid'] == 0){
                 $maxid2x = "1";
            }else{
                 $maxid2x = $max_id_copy2['maxid'] + 1;
            }
        $query=$this->db->query("insert into rel_component_to_form_running_copy SET
        id = '$maxid2x',
        form_id = '$maxid',
        hac = (select hac from rel_component_to_form_running where form_id='$id' and id='$key->id'),
        hac_to_form_running_id = (select hac_to_form_running_id from rel_component_to_form_running where form_id='$id' and id='$key->id'),
        component = (select component from rel_component_to_form_running where form_id='$id' and id='$key->id'),
        hac_record = (select hac_record from rel_component_to_form_running where form_id='$id' and id='$key->id'),
        rel_component_to_form_running_id = (select id from rel_component_to_form_running where form_id='$id' and id='$key->id')
        ");
        }
      }
      
      function insert_copy_table_rel_inspection_copy($maxid,$id){
          $this->db->select('*');
          $this->db->from('rel_activity_inspection');
          $this->db->where('form_id',$id);
          $query = $this->db->get();
          $queryx = $query->result();
          
          foreach($queryx as $key){
            $query=$this->db->query("insert into rel_activity_inspection_copy SET
            form_id = '$maxid',
            inspection_activity = (select inspection_activity from rel_activity_inspection where id='$key->id'),
            target_value = (select target_value from rel_activity_inspection where form_id='$id' and id='$key->id'),
            rel_component_id = (select rel_component_id from rel_activity_inspection where form_id='$id' and id='$key->id'),
            form_status = (select form_status from rel_activity_inspection where form_id='$id' and id='$key->id'),
            vibration_check = (select vibration_check from rel_activity_inspection where form_id='$id' and id='$key->id')
            ");  
          }
      }
      
      function insert_copy_table_form_stop($id,$maxid,$form,$jum){
        $query=$this->db->query("insert into form_stop_copy SET
        form_type = (select form_type from form_stop where id='$id'),
        area = (select area from form_stop where id='$id'),
        frequency = (select frequency from form_stop where id='$id'),
        type = (select type from form_stop where id='$id'),
        form_number = '$form',
        image1 = (select image1 from form_stop where id='$id'),
        image2 = (select image2 from form_stop where id='$id'),
        image3 = (select image3 from form_stop where id='$id'),
        image4 = (select image4 from form_stop where id='$id'),
        hac = (select hac from form_stop where id='$id'),
        periode = (select periode from form_stop where id='$id'),
        publish = (select publish from form_stop where id='$id'),
        inspection_type = (select inspection_type from form_stop where id='$id'),
        form_stop_id = '$id',
        publish_order='$jum',
        id = '$maxid'");
      }
      
      function insert_copy_table_rel_component_to_form_stop($maxid,$id){
        $this->db->select('*');
        $this->db->from('rel_component_to_form_stop');
        $this->db->where('form_id',$id);
        $query = $this->db->get();
        $queryx = $query->result();
        foreach ($queryx as $key){
            
            $max_id_copy2 =  mysql_fetch_array(mysql_query("select max(id) as maxid from rel_component_to_form_stop_copy where form_id='$maxid'"));
            if($max_id_copy2['maxid'] == "" || $max_id_copy2['maxid'] == 0){
                 $maxid2x = "1";
            }else{
                 $maxid2x = $max_id_copy2['maxid'] + 1;
            }
        $query=$this->db->query("insert into rel_component_to_form_stop_copy SET
        id = '$maxid2x',
        form_id = '$maxid',
        record_id = (select record_id from rel_component_to_form_stop where form_id='$id' and id='$key->id'),
        hac = (select hac from rel_component_to_form_stop where form_id='$id' and id='$key->id'),
        component = (select component from rel_component_to_form_stop where form_id='$id' and id='$key->id'),
        item_check = (select item_check from rel_component_to_form_stop where form_id='$id' and id='$key->id'),
        method = (select method from rel_component_to_form_stop where form_id='$id' and id='$key->id'),
        standard = (select standard from rel_component_to_form_stop where form_id='$id' and id='$key->id'),
        value = (select value from rel_component_to_form_stop where form_id='$id' and id='$key->id'),
        rel_component_to_form_stop_id = (select id from rel_component_to_form_stop where form_id='$id' and id='$key->id')
        ");
        }
      }
      
      function form_number($form){
          $this->db->select('*');
          $this->db->from('auto_form_number');
          $this->db->where('form_code',$form);
          $query = $this->db->get();
          return $query->row();
      }
             
      function insert_copy_table_form_wear($id,$maxid,$form_no,$max_meas_no,$jum){
        $date=date("Y-m-d");
        $query=$this->db->query("insert into form_measuring_copy SET
        form_number = '$form_no',
        inspection_type = 'WEAR',
        form_measuring_id = '$id',
        area = (select area from form_measuring where id='$id'),
        hac = (select hac from form_measuring where id='$id'),
        meas_no = $max_meas_no,
        material = (select material from form_measuring where id='$id'),     
        instalation_date = (select instalation_date from form_measuring where id='$id'), 
        create_date = (select create_date from form_measuring where id='$id'), 
        point = (select point from form_measuring where id='$id'), 
        gambar = (select gambar from form_measuring where id='$id'), 
        grinding = (select grinding from form_measuring where id='$id'), 
        roller = (select roller from form_measuring where id='$id'), 
        sys_create_date='$date',
        publish_order='$jum',
        id = '$maxid'");
      }
      
      function insert_copy_roller($maxid,$id,$max_meas_no){
        $this->db->select('*');
        $this->db->from('roller');
        $this->db->where('form_id',$id);
        $query = $this->db->get();
        $queryx = $query->result();
        foreach ($queryx as $key){
            
            $max_id_copy2 =  mysql_fetch_array(mysql_query("select max(id) as maxid from roller_copy where form_id='$maxid'"));
            if($max_id_copy2['maxid'] == "" || $max_id_copy2['maxid'] == 0){
                 $maxid2x = "1";
            }else{
                 $maxid2x = $max_id_copy2['maxid'] + 1;
            }
        $query=$this->db->query("insert into roller_copy SET
        form_id = '$maxid',
        no_roller = (select no_roller from roller where form_id='$id' and id='$key->id'),
        date = (select date from roller where form_id='$id' and id='$key->id'),
        meas_no = '$max_meas_no',
        val1 = (select val1 from roller where form_id='$id' and id='$key->id'),
        val2 = (select val2 from roller where form_id='$id' and id='$key->id'),
        val3 = (select val3 from roller where form_id='$id' and id='$key->id'),
        val4 = (select val4 from roller where form_id='$id' and id='$key->id'),
        val5 = (select val5 from roller where form_id='$id' and id='$key->id'),
        val6 = (select val6 from roller where form_id='$id' and id='$key->id'),
        val7 = (select val7 from roller where form_id='$id' and id='$key->id'),
        val8 = (select val8 from roller where form_id='$id' and id='$key->id'),
        val9 = (select val9 from roller where form_id='$id' and id='$key->id'),
        val10 = (select val10 from roller where form_id='$id' and id='$key->id'),
        val11 = (select val11 from roller where form_id='$id' and id='$key->id'),
        val12 = (select val12 from roller where form_id='$id' and id='$key->id'),
        val13 = (select val13 from roller where form_id='$id' and id='$key->id'),
        val14 = (select val14 from roller where form_id='$id' and id='$key->id'),
        val15 = (select val15 from roller where form_id='$id' and id='$key->id'),
        val16 = (select val16 from roller where form_id='$id' and id='$key->id'),
        val17 = (select val17 from roller where form_id='$id' and id='$key->id'),
        val18 = (select val18 from roller where form_id='$id' and id='$key->id'),
        val19 = (select val19 from roller where form_id='$id' and id='$key->id'),
        val20 = (select val20 from roller where form_id='$id' and id='$key->id'),
        roller_id = (select id from roller where form_id='$id' and id='$key->id')
        ");
        }
      }
      function insert_copy_grinding($maxid,$id,$max_meas_no){
        $this->db->select('*');
        $this->db->from('grinding');
        $this->db->where('form_id',$id);    
        $query = $this->db->get();
        $queryx = $query->result();
        foreach ($queryx as $key){
            
            $max_id_copy2 =  mysql_fetch_array(mysql_query("select max(id) as maxid from grinding_copy where form_id='$maxid'"));
            if($max_id_copy2['maxid'] == "" || $max_id_copy2['maxid'] == 0){
                 $maxid2x = "1";
            }else{
                 $maxid2x = $max_id_copy2['maxid'] + 1;
            }
        $query=$this->db->query("insert into grinding_copy SET
        form_id = '$maxid',
        no_grinding = (select no_grinding from grinding where form_id='$id' and id='$key->id'),
        date = (select date from grinding where form_id='$id' and id='$key->id'),
        meas_no = '$max_meas_no',
        val1 = (select val1 from grinding where form_id='$id' and id='$key->id'),
        val2 = (select val2 from grinding where form_id='$id' and id='$key->id'),
        val3 = (select val3 from grinding where form_id='$id' and id='$key->id'),
        val4 = (select val4 from grinding where form_id='$id' and id='$key->id'),
        val5 = (select val5 from grinding where form_id='$id' and id='$key->id'),
        val6 = (select val6 from grinding where form_id='$id' and id='$key->id'),
        val7 = (select val7 from grinding where form_id='$id' and id='$key->id'),
        val8 = (select val8 from grinding where form_id='$id' and id='$key->id'),
        val9 = (select val9 from grinding where form_id='$id' and id='$key->id'),
        val10 = (select val10 from grinding where form_id='$id' and id='$key->id'),
        val11 = (select val11 from grinding where form_id='$id' and id='$key->id'),
        val12 = (select val12 from grinding where form_id='$id' and id='$key->id'),
        val13 = (select val13 from grinding where form_id='$id' and id='$key->id'),
        val14 = (select val14 from grinding where form_id='$id' and id='$key->id'),
        val15 = (select val15 from grinding where form_id='$id' and id='$key->id'),
        val16 = (select val16 from grinding where form_id='$id' and id='$key->id'),
        val17 = (select val17 from grinding where form_id='$id' and id='$key->id'),
        val18 = (select val18 from grinding where form_id='$id' and id='$key->id'),
        val19 = (select val19 from grinding where form_id='$id' and id='$key->id'),
        val20 = (select val20 from grinding where form_id='$id' and id='$key->id'),
        grinding_id = (select id from grinding where form_id='$id' and id='$key->id')
        ");
        }
      }
      
      function delete_roller($table,$form_id,$meas_no){
          $this->db->where("form_id",$form_id);
          $this->db->where("meas_no",$meas_no);
          $this->db->delete($table);
      }
      
      function get_list_running1(){
          $query = $this->db->query('select a.*,b.area_name,c.frequency,d.periode from form_running a
                           left join area b on a.area=b.id
                           left join master_frequency c on a.frequency=c.id
                           left join master_periode d on a.periode=d.id order by id desc
                           ');
          return $query;
      }
      
     function get_list_running2($id){
         $query = $this->db->query("select a.*,b.hac_code,c.assembly_code,assembly_name
                                   from rel_component_to_form_running a
                                   left join hac b on a.hac=b.id
                                   left join hac_assembly c on a.component=c.id
                                   where a.form_id='$id' order by a.id asc
                                   ");
         return $query;
     }
     
     function get_detailrunning3($id){
         $query = $this->db->query("select * from rel_activity_inspection
                                   where rel_component_id='$id'
                                   ");
         return $query;
     }
}
?>