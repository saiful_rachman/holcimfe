<?php

/* Model Main */

class Withdraw_model extends CI_Model {

	function __construct()
	{
		parent::__construct();	
	}

	
	
	
	function get_by_id($id) {
        $this->db->select("*");
        $this->db->from('record_withdraw_oil');
        $this->db->where("work_order", $id);
        return $this->db->get();
    }
	
	function get_detail($id) {
        $this->db->select("*");
        $this->db->from('record_withdraw_oil_list');
        $this->db->like("work_order", $id);
        return $this->db->get();
    }
	
	function find_last(){
		$this->db->select("work_order");
		$this->db->order_by("date", "desc");
		$result = $this->db->get("record_withdraw_oil");
		if($result->num_rows()>0){
		$result = $result->row_array();
		return $result['work_order'];
	}else{
		return "";
	}
	
	}
	
	function get_edit_detail($id){
        $this->db->select("*");
        $this->db->from('record_withdraw_oil_list');
        $this->db->where("id", $id);
        return $this->db->get();
        
    }
	
	function get_next_search($cur_val){
	$column = "work_order";
	$query = "select min($column) value from record_withdraw_oil where $column>'$cur_val'";
	return $this->db->query($query);
	}

function get_prev_search($cur_val){
	$column = "work_order";
	$query = "select max($column) value from record_withdraw_oil where $column<'$cur_val'";
	return $this->db->query($query);
}

function generateAutoid(){
		$query=$this->db->query("select id jumlah from record_withdraw_oil");
		$max=$query->num_rows()+1;
		$menus= ('WO-0'.$max);
		return $menus;
	}
    
	
}