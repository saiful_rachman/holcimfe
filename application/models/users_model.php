<?php

class Users_model extends CI_model{
	function __construct()
	{
		parent::__construct();	
	}
        
        function count_page($table,$key_where,$where,$key_where2,$where2,$table_join,$join1,$join2,$join2_list,$table_join2,$join1_2,$join2_2,$join2_list2,$like_where,$like){
            $this->db->select($table.".*,".$table_join.".".$join2_list.",".$table_join2.".".$join2_list2);
            $this->db->from($table);
            $this->db->join($table_join,$table.".".$join1."=".$table_join.".$join2",'inner');
            $this->db->join($table_join2,$table.".".$join1_2."=".$table_join2.".$join2_2",'inner');
            $this->db->where($where,$key_where);
            $this->db->where($where2,$key_where2);
            $this->db->like($like_where,$like);
            return $this->db->get();
        }
        
        function select_all($table){
            return $this->db->get($table);
        }
        
        function select_all_where($table,$key_where,$where){
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($where,$key_where);
            return $this->db->get();
        }
        
        function select_all_where2($table,$key_where,$where,$key_where2,$where2){
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($where,$key_where);
            $this->db->where($where2,$key_where2);
            return $this->db->get();
        }
        
        function select_all_where2_join_1($table,$key_where,$where,$key_where2,$where2,$table_join,$join1,$join2,$join2_list){
            $this->db->select($table.".*,".$table_join.".".$join2_list);
            $this->db->from($table);
            $this->db->join($table_join,$table.".".$join1."=".$table_join.".$join2",'inner');
            $this->db->where($where,$key_where);
            $this->db->where($where2,$key_where2);
            return $this->db->get();
        }
        
        function select_all_where2_join_2($table,$key_where,$where,$key_where2,$where2,$table_join,$join1,$join2,$join2_list,$table_join2,$join1_2,$join2_2,$join2_list2,$like_where,$like,$a,$b){
            $this->db->select($table.".*,".$table_join.".".$join2_list.",".$table_join2.".".$join2_list2);
            $this->db->from($table);
            $this->db->join($table_join,$table.".".$join1."=".$table_join.".$join2",'inner');
            $this->db->join($table_join2,$table.".".$join1_2."=".$table_join2.".$join2_2",'inner');
            $this->db->where($where,$key_where);
            $this->db->where($where2,$key_where2);
            $this->db->like($like_where,$like);
            $this->db->order_by($table.".id","desc");
            $this->db->limit($a,$b);
            return $this->db->get();
        }
        
        function select_all_where2_join_2_order_by($table,$key_where,$where,$key_where2,$where2,$table_join,$join1,$join2,$join2_list,$table_join2,$join1_2,$join2_2,$join2_list2,$like_where,$like,$a,$b,$field,$order){
            $this->db->select($table.".*,".$table_join.".".$join2_list.",".$table_join2.".".$join2_list2);
            $this->db->from($table);
            $this->db->join($table_join,$table.".".$join1."=".$table_join.".$join2",'inner');
            $this->db->join($table_join2,$table.".".$join1_2."=".$table_join2.".$join2_2",'inner');
            $this->db->where($where,$key_where);
            $this->db->where($where2,$key_where2);
            $this->db->like($like_where,$like);
            $this->db->order_by($field,$order);
            $this->db->limit($a,$b);
            return $this->db->get();
        }
        function select_all_where_group($table,$key_where,$where,$group_by){
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($where,$key_where);
            $this->db->group_by($group_by);
            return $this->db->get();
        }
        
        function update($table,$key_where,$where,$data){
            $this->db->where($where,$key_where);
            $this->db->update($table,$data);
            return true;
        }
        
        function update_2($table,$key_where1,$key_where2,$key_where3,$where1,$where2,$where3,$data){
            $this->db->where($where1,$key_where1);
            $this->db->where($where2,$key_where2);
            $this->db->where($where3,$key_where3);
            $this->db->update($table,$data);
            return true;
        }
        
        function delete($table,$key_where,$where){
            $this->db->where($where,$key_where);
            $this->db->delete($table);
            return true;
        }
        
        function insert($table,$data){
            $this->db->insert($table,$data);
            return true;
        }
        
        function get_max($id,$field,$table,$where){
           $query = $this->db->query("select max($field) as max from $table where $where = '$id'");
           
           return $query->row('max');
        }
        
        function get_max_table($field,$table){
           $query = $this->db->query("select max($field) as max from $table");
           
           return $query->row('max');
        }
                
        function get_detail_relation1($table,$where){
         $this->db->select('a.*,b.hac_code,c.area_name');
         $this->db->from($table." a");
         $this->db->join('hac b','a.hac=b.hac_id','left');
         $this->db->join('area c','a.area=c.id','left');
         $this->db->where("a.id",$where);
         $query = $this->db->get();
         return $query;
     }
     
     function select_all_where2_join_2_thick($table,$key_where,$where,$key_where2,$where2,$table_join,$join1,$join2,$join2_list,$table_join2,$join1_2,$join2_2,$join2_list2){
            $this->db->select($table.".*,".$table_join.".".$join2_list.",".$table_join2.".".$join2_list2);
            $this->db->from($table);
            $this->db->join($table_join,$table.".".$join1."=".$table_join.".$join2",'inner');
            $this->db->join($table_join2,$table.".".$join1_2."=".$table_join2.".$join2_2",'inner');
            //$this->db->where($where,$key_where);
            $this->db->where($where2,$key_where2);
            $this->db->like('inspection_type','THICK_GENERAL');
            $this->db->or_like('inspection_type','THICK_KILN');
            $this->db->or_like('inspection_type','THICK_STACK');
            return $this->db->get();
        }
        
        function select_all_where2_join_2_thick_count($table,$key_where,$where,$key_where2,$where2,$table_join,$join1,$join2,$join2_list,$table_join2,$join1_2,$join2_2,$join2_list2,$a,$b){
            $this->db->select($table.".*,".$table_join.".".$join2_list.",".$table_join2.".".$join2_list2);
            $this->db->from($table);
            $this->db->join($table_join,$table.".".$join1."=".$table_join.".$join2",'inner');
            $this->db->join($table_join2,$table.".".$join1_2."=".$table_join2.".$join2_2",'inner');
            //$this->db->where($where,$key_where);
            $this->db->where($where2,$key_where2);
            $this->db->like('inspection_type','THICK_GENERAL');
            $this->db->or_like('inspection_type','THICK_KILN');
            $this->db->or_like('inspection_type','THICK_STACK');
            $this->db->limit($a,$b);
            return $this->db->get();
        }
     
}