<?php

class Report_model extends CI_model{
	function __construct()
	{
		parent::__construct();	
	}
	
    function add_main($form_type, $hac, $upload_image){
        $query = $this->db->query("INSERT INTO `form_stop` VALUES (NULL, '$form_type', '$hac', '$upload_image')");
        
    }
	
    function add_detail($form_id, $part, $item_check, $methode, $standard, $checkbox){
        $query = $this->db->query("INSERT INTO `rel_component_to_form_stop` VALUES (NULL, '$form_id', '$part', '$item_check', '$methode', '$standard', '$checkbox')");
    }
	
	function get_by_id() {
        
        $this->db->query("select * from hac, hac_assembly hs where hac.hac_id=hs.assembly_hac_id");
        return $this->db->get();
    }
    
	
	function get_comp() {
        
        $this->db->query("select * from hac, hac_assembly hs where hac.hac_id=hs.assembly_hac_id");
        return $this->db->get();
    }
    
    function insert_trend($data) {
        $this->db->insert_batch("record_oil_analysis_trend", $data);
    }
    
    
    function get_max_trend() {
        
        $query = $this->db->query("SELECT MAX(id) as id FROM record_oil_analysis_trend");
        return $query;
    }
    
     function get_id_trend($id) {
        $this->db->select("*");
        $this->db->from("record_oil_analysis");
        $this->db->where("record_id", $id);
        return $this->db->get();
    } 
    
    function get_oil_trend($id){
        $this->db->select("*");
        $this->db->from('record_oil_analysis_trend AS re1, record_oil_analysis_trend_parameter AS re2');
          $this->db->where('re1.trend_name = re2.id_param');
        $this->db->where("record_oil_analysis_id", $id);
        return $this->db->get();
        
    }
    
     function get_oil_edit_trend($id){
        $this->db->select("*");
        $this->db->from('record_oil_analysis_trend AS re1, record_oil_analysis_trend_parameter AS re2');
          $this->db->where('re1.trend_name = re2.id_param');
        $this->db->like("id", $id);
        return $this->db->get();
        
    }
    
  
        

}
?>