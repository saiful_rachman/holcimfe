<?php

class Master_model extends CI_Model{
    function select_all($table){
        $this->db->select('*');
        $this->db->from($table);
        return $this->db->get();
    }
    
    function select_where($table,$id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id',$id);
        return $this->db->get();
    }
    
    function edit_proses($table,$id,$data){
        $this->db->where('id',$id);
        $this->db->update($table,$data);
        return true;
    }
    
    function delete($table,$id){
        $this->db->where('id',$id);
        $this->db->delete($table);
        return true;
    }
    
    function add_proses($table,$data){
        $this->db->insert($table,$data);
        return true;
    }
}
