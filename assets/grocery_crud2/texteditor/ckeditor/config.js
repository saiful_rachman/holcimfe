﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	var url = 'http://localhost/watchbro/assets/grocery_crud/texteditor/';
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.filebrowserBrowseUrl = url + 'ckeditor/kcfinder/browse.php?type=files';
   config.filebrowserImageBrowseUrl = url +'ckeditor/kcfinder/browse.php?type=images';
   config.filebrowserFlashBrowseUrl = url +'ckeditor/kcfinder/browse.php?type=flash';
   config.filebrowserUploadUrl = url +'ckeditor/kcfinder/upload.php?type=files';
   config.filebrowserImageUploadUrl = url + 'ckeditor/kcfinder/upload.php?type=images';
   config.filebrowserFlashUploadUrl = url + 'ckeditor/kcfinder/upload.php?type=flash';
};
